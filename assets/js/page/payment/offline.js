$(document).ready(function(){
	$('.order_table tr').click(function(event) {
		if (event.target.type !== 'checkbox') {
			$(':checkbox', this).trigger('click');
		}
	});
	$("input[type='checkbox']").change(function (e) {
	    if ($(this).is(":checked")) { //If the checkbox is checked
	        $(this).closest('tr').addClass("success");
	    } else {
	        $(this).closest('tr').removeClass("success");
	    }
	});
	$('.chkBuyer').on('click, change', updateAmount);
	$('input[name=paymentChannel]').on('click, change', showChannel);
	updateAmount();
	showChannel();
});
function updateAmount(){
	var totalAmount = 0;
	var creditcardFee = 0.035;
	$('.chkBuyer:checked').each(function(){
		totalAmount += $(this).data('totalamount');
	});
	$('.totalAmount').html(currencyFormat(totalAmount));
	var creditcardFeeAmount = totalAmount*creditcardFee;
	var creditcardAmount = totalAmount + creditcardFeeAmount;
	$('.creditcardFeeAmount').html(currencyFormat(creditcardFeeAmount));
	$('.creditcardAmount').html(currencyFormat(creditcardAmount));
}
function showChannel(){
		var channel = $('input[name=paymentChannel]:checked').val();
		$('.transferForm').hide();
		$('#'+channel+'Form').show();
}
function currencyFormat(amount){
	return amount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
}