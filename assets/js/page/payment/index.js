$(document).ready(function(){
  $('#txtPaidDate').datepicker({
    format: 'dd-mm-yyyy',
    todayHighlight:true
  });
  $('#form-payment').validate();
  calcPrice();
});
function calcPrice(){
	var totalAmount = $('#totalPrice').data('totalamount');
  var surchargeAmount = $('#totalPrice').data('surchargeamount');
  var vatAmount = $('#totalPrice').data('vatamount');
  var paidAmount = $('#totalPrice').data('paidamount');
  if($('#paymentChannel').val() == 'creditcard'){
    $('.subTotalAmount').html(currencyFormat(totalAmount));
  	$('.creditcardFeeAmount').html(currencyFormat(surchargeAmount));
    $('.creditcardVatAmount').html(currencyFormat(vatAmount));
  }
  $('#txtAmount').val(currencyFormat(paidAmount));
}
function currencyFormat(amount){
	return amount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
}
