$(document).ready(function(){
  checkDisplayCompany();
  $('input[name=rdoCompany]').on('change', checkDisplayCompany);
});

function checkDisplayCompany(){
  var isCompany = $('input[name=rdoCompany]:checked').val()
  var label_data = '';
  if(isCompany == 1){
    $('#company-payment-form-group').slideDown();
    var label_data ='company-label';
  }else{
    $('#company-payment-form-group').slideUp();
    var label_data ='individual-label';
  }
  var label_name = $('#labelCompanyAddress').data(label_data);
  $('#labelCompanyAddress').html(label_name);
}
