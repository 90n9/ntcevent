var $formLogin = $('#login-form');
var $formLost = $('#lost-form');
var $formRegister = $('#register-form');
var $divForms = $('#div-forms');
$(document).ready(function(){
  $('#form-sel-ticket').on('submit', function(){
    if($('#login_top').length == 1){
      $formLogin.show();
      $formLost.hide();
      $formRegister.hide();
      $divForms.css("height","auto");
      $('#login-modal').modal('show')
      return false;
    }
  });
});
