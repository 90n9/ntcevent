$(document).ready(function(){
	checkShowPayment();
	onCheckSame();
	$('input[name=paymentChannel]').on('propertychange change keyup paste input', showChannel);
	showChannel();
	$('#chkCompany').on('change, click', showCompany);
	showCompany();
});
function checkShowPayment(){
	if($('#subTotalPrice').data('amount') == 0){
		$('.zero-payment').hide();
    if($('#txtVatAddress').val() == ''){
      $('#txtVatAddress').val('-');
    }
	}
}
function onCheckSame(){
	$('.buyerInp').on('change', printSame);
	$('.chkSame').on('click', printSame);
}
function printSame(){
	$('.chkSame').each(function(index){
		if($(this).is(':checked')){
			$('.divFormRegister').eq(index).hide();
			$('.txtRegisterFirstname').eq(index).val($('#txtFirstname').val());
			$('.txtRegisterLastname').eq(index).val($('#txtLastname').val());
			$('.txtRegisterEmail').eq(index).val($('#txtEmail').val());
			$('.txtRegisterMobile').eq(index).val($('#txtMobile').val());
			$('.txtRegisterCompany').eq(index).val($('#txtCompany').val());
		}else{
			$('.divFormRegister').eq(index).show();
			$('.txtRegisterFirstname').eq(index).val('');
			$('.txtRegisterLastname').eq(index).val('');
			$('.txtRegisterEmail').eq(index).val('');
			$('.txtRegisterMobile').eq(index).val('');
			$('.txtRegisterCompany').eq(index).val('');
		}
	});
}
function showChannel(){
	var channel = $('input[name=paymentChannel]:checked').val();
	$('.transferForm').hide();
	$('#'+channel+'Form').show();
	calcPrice();
}
function showCompany(){
  var header_data = '';
	if($('#chkCompany').is(':checked')){
		$('#companyForm').show();
    header_data = 'company-html';
	}else{
		$('#companyForm').hide();
    header_data = 'individual-html';
	}
  var html_header = $('#header-vat-form').data(header_data);
  $('#header-vat-form').html(html_header);
}
function calcPrice(){
	console.log('call cal price');
	var subTotalPrice = $('#subTotalPrice').data('amount');
	var surchargePrice = 0;
	if($('input[name=paymentChannel]:checked').val() == 'creditcard'){
		var kbankFee = parseFloat($('#kbank_fee').val());
		surchargePrice = subTotalPrice * kbankFee / 100;
		$('#surchargeRow').show();
	}else{
		$('#surchargeRow').hide();
	}
	var vatPrice = (subTotalPrice + surchargePrice) * 0.07;
	var totalPaid = subTotalPrice + surchargePrice + vatPrice;
  $('#vatPrice').html(currencyFormat(vatPrice));
	$('#paidPrice').html(currencyFormat(totalPaid));
	$('#txtAmount').val(totalPaid);
	$('.subTotalAmount').html(currencyFormat(subTotalPrice));
	$('.creditcardFeeAmount').html(currencyFormat(surchargePrice));
	$('.creditcardVatAmount').html(currencyFormat(vatPrice));
	$('.creditcardAmount').html(currencyFormat(totalPaid));
}
function currencyFormat(amount){
	return amount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
}
