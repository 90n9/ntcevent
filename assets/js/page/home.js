$(document).ready(function(){
	eventClickBookmark();
  if (sessionStorage.getItem('whenToShowDialog') == null) {
    sessionStorage.setItem('whenToShowDialog', 'yes');
    showPopup();
  }
  $('body').show();
  initCheckHashSubscribe();
  setTimeout(function(){initCheckHashSubscribe();}, 100);
  setTimeout(function(){initCheckHashSubscribe();}, 200);
  setTimeout(function(){initCheckHashSubscribe();}, 300);
});

function initCheckHashSubscribe(){
  if(window.location.hash == '#subscribeus' || window.location.hash == '#subscribe'){
    $('html body').scrollTop($("#subscribeus").offset().top);
    $('#email_newsletter').focus();
  }
}
function showPopup(){
  var dt = new Date();
  var base_url = $('#base_url').val();
  var uniqueStr = "?v="+dt.getUTCFullYear()+dt.getMonth()+dt.getDate()+dt.getHours()+dt.getMinutes()+dt.getSeconds();
	$.getJSON(base_url+"index.php/home/popup/"+uniqueStr, function(json) {
	    console.log(json);
	    if(json.popupStatus == 'show'){
	    	var popupStr = '<div style="max-width:800px;"><img src="'+json.uploadFile+'" alt="popup" style="max-width:100%;"></div>';
	    	if(json.redirectUrl != ''){
	    		popupStr = '<a href="'+json.redirectUrl+'" target="blank">'+popupStr+'</a>';
	    	}
	    	var loadImage = new Image();
        	loadImage.src = json.uploadFile;
        	loadImage.onload=function(){
            $.fancybox.open(
              popupStr,
              {
                'padding' : 10,
              }
            );
	        }
	    }
	});
}
