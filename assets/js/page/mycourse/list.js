$(document).ready(function($) {
    $(".clickable-row td").not('.unclickable').click(function() {
        window.document.location = $(this).parent().data("href");
    });
});
