(new PNotify({
    title: 'Confirmation Needed',
    text: 'Are you sure?',
    icon: 'glyphicon glyphicon-question-sign',
    hide: false,
    confirm: {
        confirm: true
    },
    buttons: {
        closer: false,
        sticker: false
    },
    history: {
        history: false
    },
    addclass: 'stack-modal',
    stack: {
        'dir1': 'down',
        'dir2': 'right',
        'modal': true
    }
})).get().on('pnotify.confirm', function() {
    alert('Ok, cool.');
}).on('pnotify.cancel', function() {
    alert('Oh ok. Chicken, I see.');
});
