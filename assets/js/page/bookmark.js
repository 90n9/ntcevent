function eventClickBookmark(){
	$('.btn-bookmark').on('click', function(){
		var courseId = $(this).data('courseid');
		var wishlistStatus = $(this).data('wishliststatus');
		var url = $(this).data('url');
		var redirectUrl = url + ((wishlistStatus == 0)?'/add/':'/remove/') + courseId;
	    var posting = $.post(redirectUrl);
	    var btnBookmark = $(this);
	    posting.done(function(data) {
	    	console.log('result', data);
	    	if(data == 'Please Login'){
          new PNotify({
            title: 'Cannot Add to wishlist',
            text: 'Please login.',
            icon: 'glyphicon glyphicon-exclamation-sign',
            type: 'error',
            styling: 'bootstrap3'
          });
	    	}else if(data == 'success'){
	    		if((wishlistStatus == 0)){
	    			console.log('change to active');
	    			btnBookmark.data('wishliststatus', 1);
	    			btnBookmark.addClass('active');
            new PNotify({
              title: 'Added to your wishlist!',
              icon: 'glyphicon glyphicon-ok-sign',
              type: 'success',
              styling: 'bootstrap3'
            });
	    		}else{
	    			console.log('change to inactive');
	    			btnBookmark.data('wishliststatus', 0);
	    			btnBookmark.removeClass('active');
            new PNotify({
              title: 'Removed from your wishlist!',
              icon: 'glyphicon glyphicon-ok-sign',
              type: 'warning',
              styling: 'bootstrap3'
            });
	    		}
	    	}
	    });
	    posting.fail(function(data){
        new PNotify({
          title: 'Something Wrong!!',
          text: 'Please try again later.',
          icon: 'glyphicon glyphicon-exclamation-sign',
          type: 'error',
          styling: 'bootstrap3'
        });
	    });
	});
}
