var $formLogin = $('#login-form');
var $formLost = $('#lost-form');
var $formLoading = $('#loading-signin');
var $formRegister = $('#register-form');
var $divForms = $('#div-forms');
var $modalAnimateTime = 300;
var $msgAnimateTime = 150;
var $msgShowTime = 2000;
$(document).ready(function(){
  $('#newsletter').validate();
  onLoginFormSubmit();
  onLoginLinkClick();
  initCheckHash();
});
function initCheckHash(){
  if(window.location.hash == '#register'){
    $('.nav-register').click();
  }
  if(window.location.hash == '#login'){
    $('.nav-login').click();
  }
}
function onLoginFormSubmit(){
  $("form").submit(function () {
    switch(this.id) {
      case "login-form":
        submitLogin();
        return false;
        break;
      case "lost-form":
        submitLost();
        return false;
        break;
      case "register-form":
        submitRegister();
        return false;
        break;
      default:
    }
  });
}
function submitLogin(){
  $formLogin.fadeOut();
  $formLoading.fadeIn();
  var $lg_email=$('#login_email').val();
  var $lg_password=$('#login_password').val();
  var posting = $.post($formLogin.attr("action"), {
    email: $lg_email,
    password: $lg_password
  });
  posting.done(function(data) {
    data = JSON.parse(data);
    if (!data.resp_status) {
      $formLogin.fadeIn();
      $formLoading.fadeOut();
      msgChange($('#div-login-msg'), $('#icon-login-msg'), $('#text-login-msg'), "error", "glyphicon-remove", data.resp_msg);
    } else {
      location.reload();
    }
  });
  posting.fail(function(data){
    setTimeout(function() { submitLogin(); }, 5000);
  });
}
function submitLost(){
  $formLost.fadeOut();
  $formLoading.fadeIn();
  var $ls_email=$('#lost_email').val();
  var posting = $.post($formLost.attr("action"), {
    email: $ls_email
  });
  posting.done(function(data) {
    $formLost.fadeIn();
    $formLoading.fadeOut();
    data = JSON.parse(data);
    if (!data.resp_status) {
      msgChange($('#div-lost-msg'), $('#icon-lost-msg'), $('#text-lost-msg'), "error", "glyphicon-remove", data.resp_msg);
    } else {
      //msgChange($('#div-lost-msg'), $('#icon-lost-msg'), $('#text-lost-msg'), "success", "glyphicon-ok", "Send OK");
      showLostComplete();
    }
  });
  posting.fail(function(data){
    setTimeout(function() { submitLost(); }, 5000);
  });
}
function showLostComplete(){
  var $oldForm = $('#lost-form');
  var $newForm = $('#lost-form-complete');
  var $oldH = $oldForm.height();
  var $newH = $newForm.height();
  $divForms.css("height",$oldH);
  $oldForm.fadeToggle($modalAnimateTime, function(){
    $divForms.animate({height: $newH}, $modalAnimateTime, function(){
      $newForm.fadeToggle($modalAnimateTime);
    });
  });
}
function submitRegister(){
  $formRegister.fadeOut();
  $formLoading.fadeIn();
  var $rg_email=$('#register_email').val();
  var $rg_password=$('#register_password').val();
  var $rg_firstname=$('#register_firstname').val();
  var $rg_lastname=$('#register_lastname').val();
  var $rg_mobile=$('#register_mobile').val();
  var $rg_company=$('#register_company').val();
  var posting = $.post($formRegister.attr("action"), {
    email: $rg_email,
    password: $rg_password,
    firstname: $rg_firstname,
    lastname: $rg_lastname,
    mobile: $rg_mobile,
    company: $rg_company
  });
  posting.done(function(data) {
    $formRegister.fadeIn();
    $formLoading.fadeOut();
    data = JSON.parse(data);
    console.log(data, data.resp_status, data.resp_msg);
    if (!data.resp_status) {
      msgChange($('#div-register-msg'), $('#icon-register-msg'), $('#text-register-msg'), "error", "glyphicon-remove", data.resp_msg);
    } else {
      showRegisterComplete();
    }
  });
  posting.fail(function(data){
    setTimeout(function() { submitLogin(); }, 5000);
  });
}
function showRegisterComplete(){
  var $oldForm = $('#register-form');
  var $newForm = $('#register-form-complete');
  var $oldH = $oldForm.height();
  var $newH = $newForm.height();
  $divForms.css("height",$oldH);
  $oldForm.fadeToggle($modalAnimateTime, function(){
    $divForms.animate({height: $newH}, $modalAnimateTime, function(){
      $newForm.fadeToggle($modalAnimateTime);
    });
  });
}
function onLoginLinkClick(){
  $('.nav-register').click(function(){
    $('#register-form-complete').hide();
    $('#lost-form-complete').hide();
    $formLogin.hide();
    $formRegister.show();
    $formLost.hide();
    $divForms.css("height","auto");
  });
  $('.nav-login').click(function(){
    $('#register-form-complete').hide();
    $('#lost-form-complete').hide();
    $formLogin.show();
    $formLost.hide();
    $formRegister.hide();
    $divForms.css("height","auto");
  });
  $('#login_register_btn').click( function () { modalAnimate($formLogin, $formRegister); });
  $('#register_login_btn').click( function () { modalAnimate($formRegister, $formLogin); });
  $('#login_lost_btn').click( function () { modalAnimate($formLogin, $formLost); });
  $('#lost_login_btn').click( function () { modalAnimate($formLost, $formLogin); });
  $('#lost_register_btn').click( function () { modalAnimate($formLost, $formRegister); });
  $('#register_lost_btn').click( function () { modalAnimate($formRegister, $formLost); });
}
function modalAnimate ($oldForm, $newForm) {
  var $oldH = $oldForm.height();
  var $newH = $newForm.height();
  $divForms.css("height",$oldH);
  $divForms.css("width","100%");
  $oldForm.fadeToggle($modalAnimateTime, function(){
    $divForms.animate({height: $newH}, $modalAnimateTime, function(){
      $newForm.fadeToggle($modalAnimateTime);
    });
  });
}

function msgFade ($msgId, $msgText) {
  $msgId.fadeOut($msgAnimateTime, function() {
    $(this).text($msgText).fadeIn($msgAnimateTime);
  });
}

function msgChange($divTag, $iconTag, $textTag, $divClass, $iconClass, $msgText) {
  var $msgOld = $divTag.text();
  msgFade($textTag, $msgText);
  $divTag.addClass($divClass);
  $iconTag.removeClass("glyphicon-chevron-right");
  $iconTag.addClass($iconClass + " " + $divClass);
  setTimeout(function() {
    msgFade($textTag, $msgOld);
    $divTag.removeClass($divClass);
    $iconTag.addClass("glyphicon-chevron-right");
    $iconTag.removeClass($iconClass + " " + $divClass);
  }, $msgShowTime);
}
var maps = [];
var locations = [];
$(document).ready(function() {
  initialize();
  $('.modal').on('shown.bs.modal', function(){
    console.log('modal open');
    maps.forEach(function(item, index){
      google.maps.event.trigger(item, "resize");
      item.setCenter(locations[index]);
    });
  });
});
function initialize() {
  var mapOptions = {
    zoom: 15
  };
  $(".google-map").each(function(index) {
    $(this).height(300);
    var map = new google.maps.Map(this, mapOptions);
    maps.push(map);
    var lad = $(this).data("lad");
    var lon = $(this).data("lon");
    var title = 'Here';
    var initialLocation = new google.maps.LatLng(lad,lon);
    locations.push(initialLocation);
    map.setCenter(initialLocation);
    var marker = new google.maps.Marker({
      position: initialLocation,
      map: map,
      title: title
    });
  });
}
