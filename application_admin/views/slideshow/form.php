<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span>หน้าเวบ</span></li>
        <li><span><?php echo anchor('slideshow','Slideshow'); ?></span></li>
        <li><span><?php echo $title; ?></span></li>
    </ol>
</div>
</header>
<?php echo form_open_multipart('slideshow/form_post/'.$slideshowId, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'slideshow-form')); ?>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group">
            <label for="fileImageTh" class="col-md-3 control-label">รูปภาษาไทย</label>
            <div class="col-md-6">
                <?php
                if($slideshowId != 0 && $slideshow->slideshow_image_th != ''){
                    echo '<img src="'.base_url('uploads/'.$slideshow->slideshow_image_th).'" alt="Slideshow Image Th" class="img-responsive" style="max-width:260px;" />';
                }
                echo '<input type="file" class="form-control" name="fileImageTh" id="fileImageTh" placeholder="Upload file here" value="" />';
                echo '<p class="help-block">recommended 1700x600pixel</p>';
                ?>
            </div>
        </div>
        <div class="form-group">
            <label for="fileImageEn" class="col-md-3 control-label">รูปภาษาอังกฤษ</label>
            <div class="col-md-6">
                <?php
                if($slideshowId != 0 && $slideshow->slideshow_image_en != ''){
                    echo '<img src="'.base_url('uploads/'.$slideshow->slideshow_image_en).'" alt="Slideshow Image En" class="img-responsive" style="max-width:260px;" />';
                }
                echo '<input type="file" class="form-control" name="fileImageEn" id="fileImageEn" placeholder="Upload file here" value="" />';
                echo '<p class="help-block">recommended 1700x600pixel</p>';
                ?>
            </div>
        </div>
        <div class="form-group">
            <label for="txtUrl" class="col-md-3 control-label">URL</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="txtUrl" id="txtUrl" placeholder="http://www..." value="<?php echo ($slideshowId != 0)?$slideshow->slideshow_url:''; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label for="ddlSortPriority" class="col-md-3 control-label">แสดงลำดับที่</label>
            <div class="col-md-6">
                <select name="ddlSortPriority" id="ddlSortPriority" class="form-control">
                    <?php
                    $maxPriority = $this->SlideshowModel->getMaxPriority();
                    $maxPriority += ($slideshowId == 0)?1:0;
                    for($i = 1; $i<=$maxPriority; $i++){
                        $checked = ($slideshowId == 0 || $slideshow->sort_priority == $i)?'selected="selected"':'';
                        echo '<option value="'.$i.'" '.$checked.'>'.$i.'</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="row form-group">
            <label class="col-md-offset-3 col-md-6 xs-margin-bottom">
                <input type="checkbox" name="chkEnableStatus" value="1" <?php echo ($slideshowId == 0 || $slideshow->enable_status == 1)?'checked="checked"':''; ?> />
                แสดงรายการ
            </label>
        </div>
        <div class="form-group">
            <div class="col-md-offset-3 col-md-6">
                <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button> <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>
