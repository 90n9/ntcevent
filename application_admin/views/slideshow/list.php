<header class="page-header">
    <h2>Slideshow</h2>
    <div class="right-wrapper pull-right" style="padding-right:20px;">
        <ol class="breadcrumbs">
            <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
            <li><span>หน้าเวบ</span></li>
            <li><span>Slideshow</span></li>
        </ol>
    </div>
</header>
<div class="search-content">
    <div class="tab-content pt-lg">
        <div class="pb-lg">
            <?php echo anchor('slideshow/form/0', '<i class="fa fa-plus"></i> เพิ่ม Slideshow', array('class'=>'btn btn-success')); ?>
        </div>
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
                <tr>
                    <th>Slideshow ภาษาไทย</th>
                    <th>Slideshow ภาษาอังกฤษ</th>
                    <th>URL</th>
                    <th>สถานะ</th>
                    <th>วันที่สร้าง</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                foreach($slideshowList->result() as $slideshow){ 
                    $detailUrl = 'slideshow/detail/'.$slideshow->slideshow_id;
                    $imageTh = ($slideshow->slideshow_image_th != '')?'<img src="'.base_url('uploads/'.$slideshow->slideshow_image_th).'" width="300" />':'';
                    $imageEn = ($slideshow->slideshow_image_en != '')?'<img src="'.base_url('uploads/'.$slideshow->slideshow_image_en).'" width="300" />':'';
                    ?>
                    <tr>
                        <td>
                            <?php echo ($imageTh != '')?anchor($detailUrl, $imageTh):''; ?>
                        </td>
                        <td>
                            <?php echo ($imageEn != '')?anchor($detailUrl, $imageEn):''; ?>
                        </td>
                        <td>
                            <?php echo anchor($detailUrl, ($slideshow->slideshow_url == '')?'-':$slideshow->slideshow_url); ?>
                        </td>
                        <td>
                            <?php echo anchor($detailUrl, ($slideshow->enable_status == 1)?'แสดง':'ซ่อน'); ?>
                        </td>
                        <td>
                            <?php echo anchor($detailUrl, $slideshow->create_date); ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
          </table>
        </div>
        <br />
    </div>
</div>