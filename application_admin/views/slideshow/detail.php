<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span>หน้าเวบ</span></li>
        <li><span><?php echo anchor('slideshow','Slideshow'); ?></span></li>
        <li><span><?php echo $title; ?></span></li>
    </ol>
</div>
</header>
<div class="btn-group hide-print" style="padding-bottom:10px;">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    การกระทำ <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><?php echo anchor('slideshow', '<i class="glyphicon glyphicon-arrow-left"></i> กลับไปหน้ารายการ'); ?></li>
    <li><?php echo anchor('slideshow/form/0', '<i class="glyphicon glyphicon-plus"></i> เพิ่ม Slideshow'); ?></li>
    <li><?php echo anchor('slideshow/form/'.$slideshow->slideshow_id, '<i class="glyphicon glyphicon-pencil"></i> แก้ไขข้อมูล Slideshow'); ?></li>
    <li class="divider"></li>
    <li><a href="#" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-trash"></i> ลบข้อมูล</a></li>
  </ul>
</div>
<div class="panel panel-default">
    <div class="panel-body">
        <?php echo form_open('#', array('role' => 'form', 'class' => 'form-horizontal')); ?>
        <div class="form-group">
            <label class="col-sm-2 col-print-4 control-label">รูปภาษาไทย</label>
            <div class="col-sm-10 col-print-8">
                <?php
                if($slideshow->slideshow_image_th != ''){
                    echo '<img src="'.base_url('uploads/'.$slideshow->slideshow_image_th).'" alt="Product Image" class="img-responsive" style="max-width:300px;" />';
                }else{
                    echo '<p class="form-control-static">-</p>';
                }
                ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 col-print-4 control-label">รูปภาษาอังกฤษ</label>
            <div class="col-sm-10 col-print-8">
                <?php
                if($slideshow->slideshow_image_en != ''){
                    echo '<img src="'.base_url('uploads/'.$slideshow->slideshow_image_en).'" alt="Product Image" class="img-responsive" style="max-width:300px;" />';
                }else{
                    echo '<p class="form-control-static">-</p>';
                }
                ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 col-print-4 control-label">URL</label>
            <div class="col-sm-10 col-print-8">
                <p class="form-control-static"><a href="<?php echo $slideshow->slideshow_url; ?>" target="_blank"><?php echo $slideshow->slideshow_url; ?></a></p>
            </div>
        </div>
        <div class="form-group">
            <label for="txtMobile" class="col-sm-2 col-print-4 control-label">แสดงลำดับที่</label>
            <div class="col-sm-10 col-print-8">
                <p class="form-control-static mobile"><?php echo $slideshow->sort_priority; ?></p>
            </div>
        </div>
        <fieldset disabled>
        <div class="form-group">
            <label class="col-sm-offset-2 col-sm-4 col-print-6 xs-margin-bottom">
                <input type="checkbox" <?php echo ($slideshow->enable_status == 1)?'checked="checked"':''; ?> />
                แสดงผลหน้าเวบไซต์
            </label>
        </div>
        </fieldset>
        <div class="row form-group">
            <label for="txtBranch" class="col-sm-2 col-print-3 control-label">สร้างวันที่</label>
            <div class="col-sm-4 col-print-3 xs-margin-bottom">
                <p class="form-control-static"><?php echo $slideshow->create_date; ?></p>
            </div>
            <label for="txtZone" class="col-sm-2 col-print-3 control-label">โดย</label>
            <div class="col-sm-4 col-print-3">
                <p class="form-control-static"><?php echo $this->AdminModel->getLoginNameById($slideshow->create_by); ?></p>
            </div>
        </div>
        <div class="row form-group">
            <label for="txtBranch" class="col-sm-2 col-print-3 control-label">ปรับปรุงวันที่</label>
            <div class="col-sm-4 col-print-3 xs-margin-bottom">
                <p class="form-control-static"><?php echo $slideshow->update_date; ?></p>
            </div>
            <label for="txtZone" class="col-sm-2 col-print-3 control-label">โดย</label>
            <div class="col-sm-4 col-print-3">
                <p class="form-control-static"><?php echo $this->AdminModel->getLoginNameById($slideshow->update_by); ?></p>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">ลบข้อมูล</h4>
      </div>
      <div class="modal-body">
        กรุณากดปุ่ม "ลบข้อมูล" เพื่อยืนยันการลบข้อมูลนี้ออกจากระบบ
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button>
        <?php echo anchor('slideshow/delete/'.$slideshow->slideshow_id, '<i class="glyphicon glyphicon-trash"></i> ลบข้อมูล', array('class'=>'btn btn-danger')); ?>
      </div>
    </div>
  </div>
</div>
