<div>
	<form class="form-inline">
		แสดง <?php echo ($totalTransaction < $perPage)?$totalTransaction:$perPage; ?> รายการ จาก <span class="int"><?php echo $totalTransaction; ?></span> รายการ
		<div class="pull-right">หน้า
			<select name="ddlPager" class="ddlPager form-control"
			data-searchval="<?php echo $searchVal; ?>"
			data-startdate="<?php echo $startDate; ?>"
			data-enddate="<?php echo $endDate; ?>"
			data-perpage="<?php echo $perPage; ?>"
			data-action="<?php echo $action; ?>"
			>
			<?php
			for($i = 1; $i <= $totalPage; $i++){
				$selected = ($i == $page)?'selected="selected"':'';
				echo '<option val="'.$i.'" '.$selected.'>'.$i.'</option>';
			}
			?>
			</select>
			/ <span class="int"><?php echo $totalPage; ?></span>
		</div>
	</form>
</div>
