<?php
if($totalTransaction == 0){
    echo '<div class="alert alert-warning" role="alert">ไม่พบรายการตามที่ท่านค้นหา</div>';
}else{
    $totalPage = ceil($totalTransaction/$perPage);
    $pagerData = array();
    $pagerData['totalTransaction'] = $totalTransaction;
    $pagerData['searchVal'] = $searchVal;
    $pagerData['startDate'] = $startDate;
    $pagerData['endDate'] = $endDate;
    $pagerData['perPage'] = $perPage;
    $pagerData['page'] = $page;
    $pagerData['totalPage'] = $totalPage;
    $pagerData['action'] = site_url('subscribe/filter');
?>
<div class="form-group">
    <?php $this->load->view('subscribe/listpager', $pagerData); ?>
</div>
<div class="table-responsive">
  <table class="table table-striped">
    <thead>
        <tr>
            <th>ผู้สมัครจดหมายข่าว</th>
            <th>วันที่สมัคร</th>
            <th>การกระทำ</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach($subscribeList->result() as $subscribe){
            $detailUrl = 'subscribe/form/'.$subscribe->subscribe_id;
            $deleteUrl = 'subscribe/delete/'.$subscribe->subscribe_id;
            ?>
            <tr>
                <td>
                    <?php echo anchor($detailUrl, ($subscribe->email != '')?$subscribe->email:'-'); ?>
                </td>
                <td>
                    <?php echo anchor($detailUrl, $this->DateTimeService->displayOnlyDate($subscribe->create_date)); ?>
                </td>
                <td>
                    <?php echo anchor($deleteUrl, 'ลบรายการ', array('class'=>'btn btn-danger btn-sm btn-delete')); ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
  </table>
</div>
<div class="form-group">
    <?php $this->load->view('subscribe/listpager', $pagerData); ?>
</div>
<?php
}
