<header class="page-header">
<h2>ข้อมูลการชำระเงิน</h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span>ข้อมูลการชำระเงิน</span></li>
    </ol>
</div>
</header>
<div class="search-content">
    <div class="search-control-wrapper">
        <?php echo form_open('payment/filter', array('role' => 'form', 'class' => 'form-inline', 'id' => 'searchForm')); ?>
        <div class="form-group">
            <input type="text" class="form-control" name="txtSearchVal" id="txtSearchVal" placeholder="ข้อความที่ต้องการค้นหา" />
        </div>
        <div class="form-group">
            <select class="form-control" name="ddlPaymentStatus" id="ddlPaymentStatus">
              <option value="all" selected="selected">สถานะทั้งหมด</option>
              <option value="waiting">รอการตรวจสอบ</option>
              <option value="complete">เสร็จสิ้น</option>
              <option value="cancel">ยกเลิก</option>
            </select>
        </div>
        <div class="form-group">
            <select class="form-control" name="ddlPaymentType" id="ddlPaymentType">
              <option value="all" selected="selected">ช่องทางทั้งหมด</option>
              <option value="bank">โอนเงิน</option>
              <option value="creditcard">บัตรเครดิต</option>
            </select>
        </div>
        <div class="form-group" id="create_period">
          <div class="input-daterange input-group">
            <span class="input-group-addon first-input-group">
              <i class="fa fa-calendar"></i>
            </span>
            <input type="text" class="form-control create_range" name="txtStartDate" id="txtStartDate" style="width:100px;" placeholder="แจ้งชำระเงินตั้งแต่วันที่">
            <span class="input-group-addon">to</span>
            <input type="text" class="form-control create_range" name="txtEndDate" id="txtEndDate" style="width:100px;" placeholder="ถึงวันที่">
          </div>
        </div>
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-filter"></i> ค้นหา</button>
        <?php echo form_close(); ?>
    </div>
    <div class="tab-content pt-lg">
        <div id="result">
        </div>
        <br />
    </div>
</div>
