<?php
if($totalTransaction == 0){
    echo '<div class="alert alert-warning" role="alert">ไม่พบรายการตามที่ท่านค้นหา</div>';
}else{
    $totalPage = ceil($totalTransaction/$perPage);
    $pagerData = array();
    $pagerData['totalTransaction'] = $totalTransaction;
    $pagerData['searchVal'] = $searchVal;
    $pagerData['paymentStatus'] = $paymentStatus;
    $pagerData['paymentType'] = $paymentType;
    $pagerData['startDate'] = $startDate;
    $pagerData['endDate'] = $endDate;
    $pagerData['perPage'] = $perPage;
    $pagerData['page'] = $page;
    $pagerData['totalPage'] = $totalPage;
    $pagerData['action'] = site_url('payment/filter');
?>
<div class="form-group">
    <?php $this->load->view('payment/listpager', $pagerData); ?>
</div>
<div class="table-responsive">
  <table class="table table-striped">
    <thead>
        <tr>
            <th>รหัสแจ้งชำระเงิน</th>
            <th>โดย</th>
            <th>สถานะ</th>
            <th>ช่องทาง</th>
            <th>วันที่สร้าง</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach($paymentList->result() as $payment){
            $detailUrl = 'payment/detail/'.$payment->payment_id;
            $arrPaymentStatusClass = array(
              'waiting' => 'info',
              'complete' => 'success',
              'cancel' => 'danger'
            );
            $paymentStatusList = array('waiting', 'complete', 'cancel');
            $paymentStatusClass = (in_array($payment->payment_status, $paymentStatusList))?$arrPaymentStatusClass[$payment->payment_status]:$payment->payment_status;
            $paymentTypeClass = ($payment->payment_type == 'creditcard')?'warning':'primary';
            ?>
            <tr>
                <td><?php echo anchor($detailUrl, $payment->payment_code); ?></td>
                <td><?php echo anchor($detailUrl, $this->MemberModel->getName($payment->member_id)); ?></td>
                <td><?php echo '<span class="label label-'.$paymentStatusClass.'">'.$payment->payment_status.'</span>'; ?></td>
                <td><?php echo '<span class="label label-'.$paymentTypeClass.'">'.$payment->payment_type.'</span>'; ?></td>
                <td><?php echo anchor($detailUrl, $this->DateTimeService->displayOnlyDate($payment->create_date)); ?></td>
            </tr>
        <?php } ?>
    </tbody>
  </table>
</div>
<div class="form-group">
    <?php $this->load->view('payment/listpager', $pagerData); ?>
</div>
<?php
}
