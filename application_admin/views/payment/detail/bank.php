<?php echo form_open('#', array('role' => 'form', 'class' => 'form-horizontal')); ?>
<div class="panel panel-default">
  <div class="panel-body">
      <div class="form-group">
          <label class="col-sm-4 col-print-6 control-label">ยอดชำระเงิน</label>
          <div class="col-sm-8 col-print-6">
              <p class="form-control-static"><?php echo number_format($payment->payment_amount, 2); ?> บาท</p>
          </div>
      </div>
      <div class="form-group">
          <label for="txtBranch" class="col-sm-4 col-print-3 control-label">วันที่ชำระเงิน</label>
          <div class="col-sm-8 col-print-3 xs-margin-bottom">
              <p class="form-control-static"><?php echo $payment->payment_date; ?></p>
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-4 col-print-3 control-label">ไฟล์แนบ</label>
          <div class="col-sm-8 col-print-3">
              <p class="form-control-static"><?php echo ($payment->payment_file != '')?anchor(base_url('uploads/'.$payment->payment_file),'Open file', array('target'=>'_blank')):'ไม่มีไฟล์แนบ'; ?></p>
          </div>
      </div>
  </div>
</div>
<?php echo form_close(); ?>
