<?php
$member = $this->MemberModel->getData($payment->member_id);
if($member){
?>
<style>
  #memberinfo .control-label{
    font-weight:bold;
  }
</style>
<div id="memberinfo" class="panel panel-default">
  <div class="panel-body">
    <div class="form-group">
      <label class="control-label">ชื่อ</label>
      <p class="form-control-static"><?php echo $member->firstname.' '.$member->lastname; ?></p>
    </div>
    <div class="form-group">
      <label class="control-label">อีเมล</label>
      <p class="form-control-static"><?php echo $member->email; ?></p>
    </div>
    <div class="form-group">
      <label class="control-label">เบอร์มือถือ</label>
      <p class="form-control-static"><?php echo $member->mobile; ?></p>
    </div>
    <div class="form-group">
      <label class="control-label">บริษัท</label>
      <p class="form-control-static"><?php echo $member->company; ?></p>
    </div>
  </div>
</div>
<?php
}
