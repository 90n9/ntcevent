<div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="cancelModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="cancelModalLabel">ยกเลิกการแจ้งชำระเงิน</h4>
      </div>
      <div class="modal-body">
        กรุณากดปุ่ม "ยืนยัน" เพื่อยกเลิกรายการแจ้งชำระเงิน
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ปิด</button>
        <?php echo anchor('payment/cancelpayment/'.$payment->payment_id, '<i class="glyphicon glyphicon-pencil"></i> ยืนยัน', array('class'=>'btn btn-danger')); ?>
      </div>
    </div>
  </div>
</div>
