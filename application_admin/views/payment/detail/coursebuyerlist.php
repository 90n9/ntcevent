<?php
$courseBuyerList = $this->CourseBuyerModel->getListByPayment($payment->payment_id);
?>
<div class="table-responsive">
  <table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>รหัสลงทะเบียน</th>
            <th>ที่นั่ง</th>
            <th>สถานะ</th>
            <th>วันที่สั่งซื้อ</th>
            <th>รวม</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $subTotal = 0;
        foreach($courseBuyerList->result() as $courseBuyer){
            $member = $this->MemberModel->getData($courseBuyer->member_id);
            $detailUrl = 'course/order_detail/'.$courseBuyer->course_buyer_id;
            $courseBuyerCost = $courseBuyer->total_amount - $courseBuyer->discount_amount;
            ?>
            <tr>
                <td>
                    <?php echo $courseBuyer->course_buyer_code; ?>
                </td>
                <td>
                    <?php echo anchor($detailUrl, $courseBuyer->total_ticket); ?>
                </td>
                <td><?php echo $courseBuyer->course_buyer_status; ?></td>
                <td>
                    <?php echo anchor($detailUrl, $courseBuyer->create_date); ?>
                </td>
                <td>
                    <?php echo anchor($detailUrl, number_format($courseBuyerCost)); ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
    <tfoot>
      <tr>
        <td colspan="4" class="text-right">Sub Total</td>
        <td style="text-right"><?php echo number_format($payment->order_amount, 2); ?></td>
      </tr>
      <?php if($payment->payment_type == 'creditcard'){ ?>
      <tr>
        <td colspan="4" class="text-right">Surcharge</td>
        <td style="text-right"><?php echo number_format($payment->surcharge_amount, 2); ?></td>
      </tr>
      <?php } ?>
      <tr>
        <td colspan="4" class="text-right">Vat 7%</td>
        <td style="text-right"><?php echo number_format($payment->vat_amount, 2); ?></td>
      </tr>
      <tr>
        <td colspan="4" class="text-right">Grand Total</td>
        <td style="text-right"><?php echo number_format($payment->payment_amount, 2); ?></td>
      </tr>
    </tfoot>
  </table>
</div>
