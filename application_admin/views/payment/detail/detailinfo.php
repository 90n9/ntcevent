<?php echo form_open('#', array('role' => 'form', 'class' => 'form-horizontal')); ?>
<div class="panel panel-default">
  <div class="panel-body">
      <div class="form-group">
          <label class="col-sm-4 col-print-6 control-label">รหัสการแจ้งชำระเงิน</label>
          <div class="col-sm-8 col-print-6">
              <p class="form-control-static"><?php echo $payment->payment_code; ?></p>
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-4 col-print-6 control-label">สถานะ</label>
          <div class="col-sm-8 col-print-6">
              <p class="form-control-static"><span class="label label-<?php echo $paymentStatusClass; ?>"><?php echo $payment->payment_status; ?></span></p>
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-4 col-print-3 control-label">ช่องทางการชำระเงิน</label>
          <div class="col-sm-8 col-print-9">
              <p class="form-control-static"><span class="label label-<?php echo $paymentTypeClass; ?>"><?php echo $payment->payment_type; ?></span></p>
          </div>
      </div>
      <div class="form-group">
          <label for="txtBranch" class="col-sm-4 col-print-3 control-label">มูลค่าการสั่งซื้อ</label>
          <div class="col-sm-8 col-print-3 xs-margin-bottom">
              <p class="form-control-static"><?php echo number_format($payment->order_amount, 2); ?> บาท</p>
          </div>
      </div>
      <?php if($payment->payment_type == 'creditcard'){ ?>
      <div class="form-group">
          <label for="txtBranch" class="col-sm-4 col-print-3 control-label">ค่าธรรมเนียม</label>
          <div class="col-sm-8 col-print-3 xs-margin-bottom">
              <p class="form-control-static"><?php echo number_format($payment->surcharge_amount, 2); ?> บาท</p>
          </div>
      </div>
      <?php } ?>
      <div class="form-group">
          <label for="txtBranch" class="col-sm-4 col-print-3 control-label">VAT 7%</label>
          <div class="col-sm-8 col-print-3 xs-margin-bottom">
              <p class="form-control-static"><?php echo number_format($payment->vat_amount, 2); ?> บาท</p>
          </div>
      </div>
      <div class="form-group">
          <label for="txtBranch" class="col-sm-4 col-print-3 control-label">ยอดที่ชำระ</label>
          <div class="col-sm-8 col-print-3 xs-margin-bottom">
              <p class="form-control-static"><?php echo number_format($payment->payment_amount, 2); ?> บาท</p>
          </div>
      </div>
      <div class="form-group">
          <label for="txtBranch" class="col-sm-4 col-print-3 control-label">แจ้งวันที่</label>
          <div class="col-sm-8 col-print-3 xs-margin-bottom">
              <p class="form-control-static"><?php echo $payment->create_date; ?></p>
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-4 col-print-3 control-label">ปรับปรุงวันที่</label>
          <div class="col-sm-8 col-print-3 xs-margin-bottom">
              <p class="form-control-static"><?php echo $payment->update_date; ?></p>
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-4 col-print-3 control-label">โดย</label>
          <div class="col-sm-8 col-print-3">
              <p class="form-control-static"><?php echo $this->AdminModel->getLoginNameById($payment->update_by); ?></p>
          </div>
      </div>
  </div>
</div>
<?php echo form_close(); ?>
