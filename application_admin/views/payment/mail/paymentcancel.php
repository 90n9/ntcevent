<div>
  {{Course Image}}
</div>
<div>
  <p>FREE ORDER CANCELED</p>
  <p>Event Name: “Building competencies for IT professionals”</p>
  <table cellpadding="0" cellspacing="0" border="0" width="100%" style="boder:1px solid #999;margin:auto;">
    <tr>
      <td>Event Name:</td>
      <td style="text-align:right;"> Baht</td>
    </tr>
    <tr>
      <td>Attendee Name:</td>
      <td style="text-align:right;"></td>
    </tr>
    <tr>
      <td>Attendee Email:</td>
      <td style="text-align:right;"></td>
    </tr>
    <tr>
      <td>Order ID:</td>
      <td style="text-align:right;"></td>
    </tr>
  </table>
  <p>We&#39;re sorry you can&#39;t be there to join us. Of course, you will be most welcome if you change your mind.</p>
  <p>If you have any questions please contact
  support@trainingcenter.co.th or 02 643 7993-4</p>
  <p>We look forward to see you at our next event.</p>
  <p>Cheers!</p>
  <p>The NTC Team</p>
</div>
