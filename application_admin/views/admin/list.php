<header class="page-header">
<h2>เจ้าหน้าที่เวบไซต์</h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span>การจัดการ</span></li>
        <li><span>เจ้าหน้าที่เวบไซต์</span></li>
    </ol>
</div>
</header>
<div class="search-content">
  <div class="tab-content pt-lg">
      <div class="pb-lg">
          <?php echo anchor('admin/form/0', '<i class="fa fa-plus"></i> เพิ่มผู้ใช้', array('class'=>'btn btn-success')); ?>
      </div>
      <div class="pb-lg">
        แสดง <span class="int"><?php echo $totalTransaction; ?></span> รายการ
      </div>
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
              <tr>
                  <th>Login Name</th>
                  <th>ชื่อ</th>
                  <th>อีเมล</th>
                  <th>สถานะ</th>
              </tr>
          </thead>
          <tbody>
              <?php
              foreach($adminList->result() as $admin){
                  $detailUrl = 'admin/detail/'.$admin->admin_id;
                  ?>
                  <tr>
                      <td>
                          <?php echo anchor($detailUrl, $admin->login_name); ?>
                      </td>
                      <td>
                          <?php echo anchor($detailUrl, ($admin->name != '')?$admin->name:'-'); ?>
                      </td>
                      <td>
                          <?php echo anchor($detailUrl, ($admin->email != '')?$admin->email:'-'); ?>
                      </td>
                      <td>
                        <?php echo anchor($detailUrl, ($admin->enable_status == 1)?'<span>ใช้งานได้</span>':'<span>ไม่สามารถใช้งานได้</span>'); ?>
                      </td>
                  </tr>
              <?php } ?>
          </tbody>
        </table>
      </div>
      <br />
  </div>
</div>
