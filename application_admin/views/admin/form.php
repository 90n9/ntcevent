<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span>การจัดการ</span></li>
        <li><span>เจ้าหน้าที่เวบไซต์</span></li>
        <li><span><?php echo $title; ?></span></li>
    </ol>
</div>
</header>
<?php echo form_open('admin/form_post/'.$adminId, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'admin-form')); ?>
<div class="panel panel-default">
    <div class="panel-heading">
    ข้อมูลระบบ
    </div>
    <div class="panel-body">
        <div class="form-group">
            <label for="txtLoginName" class="col-md-3 control-label">Login Name</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="txtLoginName" id="txtLoginName" placeholder="" maxlength="20" value="<?php echo ($adminId != 0)?$admin->login_name:''; ?>">
            </div>
        </div>
        <?php if($adminId == 0){ ?>
        <div class="form-group">
            <label for="txtPassword" class="col-md-3 control-label">Password</label>
            <div class="col-md-6">
                <input type="password" class="form-control" name="txtPassword" id="txtPassword" placeholder="" maxlength="20">
            </div>
        </div>
        <div class="form-group">
            <label for="txtConfirmPassword" class="col-md-3 control-label">Confirm Password</label>
            <div class="col-md-6">
                <input type="password" class="form-control" name="txtConfirmPassword" id="txtConfirmPassword" placeholder="" maxlength="20">
            </div>
        </div>
        <?php } ?>
        <div class="form-group">
            <label class="col-md-offset-3 col-md-6 xs-margin-bottom">
                <input type="checkbox" name="chkEnableStatus" value="1" <?php echo ($adminId == 0 || $admin->enable_status == 1)?'checked="checked"':''; ?> />
                ใช้งานได้
            </label>
        </div>
    </div>
</div>
<div class="panel panel-default" id="editProfile">
    <div class="panel-heading">
    ข้อมูลผู้ใช้งาน
    </div>
    <div class="panel-body">
        <div class="form-group">
            <label for="txtName" class="col-md-3 control-label">ชื่อ</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="txtName" id="txtName" value="<?php echo ($adminId != 0)?$admin->name:''; ?>" />
            </div>
          </div>
          <div class="form-group">
            <label for="txtEmail" class="col-md-3 control-label">อีเมล</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="txtEmail" id="txtEmail" value="<?php echo ($adminId != 0)?$admin->email:''; ?>" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-3 col-md-6">
                <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button> <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>
