<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span>หน้าเวบ</span></li>
        <li><span><?php echo $title; ?></span></li>
    </ol>
</div>
</header>
<?php echo form_open('content/form_post/'.$contentCode, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'content-form')); ?>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group">
            <label for="txtNameTh" class="col-md-3 control-label">ข้อความภาษาไทย</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="txtNameTh" id="txtNameTh" placeholder="" value="<?php echo $content->content_name_th; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="txtNameEn" class="col-md-3 control-label">ข้อความภาษาอังกฤษ</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="txtNameEn" id="txtNameEn" placeholder="" value="<?php echo $content->content_name_en; ?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">เนื้อหาภาษาไทย</label>
            <div class="col-md-9">
                <textarea class="summernote" name="textTh" id="textTh" data-upload="<?php echo site_url('content/uploadImage'); ?>"><?php echo $content->content_text_th; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">เนื้อหาภาษาอังกฤษ</label>
            <div class="col-md-9">
                <textarea class="summernote" name="textEn" id="textEn" data-upload="<?php echo site_url('content/uploadImage'); ?>"><?php echo $content->content_text_en; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-3 col-md-6">
                <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>
