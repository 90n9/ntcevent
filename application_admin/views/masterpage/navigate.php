                <!-- start: sidebar -->
                <aside id="sidebar-left" class="sidebar-left">
                    <div class="sidebar-header">
                        <div class="sidebar-title">
                            Navigation
                        </div>
                        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                        </div>
                    </div>
                    <div class="nano">
                        <div class="nano-content">
                            <nav id="menu" class="nav-main" role="navigation">
                                <ul class="nav nav-main">
                                    <li <?php echo ($nav == 'dashboard')?'class="nav-active"':''; ?>>
                                        <?php echo anchor('dashboard','<i class="fa fa-home" aria-hidden="true"></i>
                                            <span>Dashboard</span>'); ?>
                                    </li>
                                    <li <?php echo ($nav == 'member')?'class="nav-active"':''; ?>>
                                        <?php echo anchor('member','<i class="fa fa-group" aria-hidden="true"></i>
                                            <span>สมาชิกเวบไซต์</span>'); ?>
                                    </li>
                                    <li class="nav-parent <?php echo (in_array($nav, array('course', 'coursecategory', 'speaker', 'pricerange', 'courselocation')))?'nav-expanded nav-active':''; ?>">
                                        <a>
                                            <i class="fa fa-asterisk" aria-hidden="true"></i>
                                            <span>คอร์สเรียน</span>
                                        </a>
                                        <ul class="nav nav-children">
                                            <li <?php echo ($nav == 'course')?'class="nav-active"':''; ?>><?php echo anchor('course','คอร์สเรียน'); ?></li>
                                            <li <?php echo ($nav == 'coursecategory')?'class="nav-active"':''; ?>><?php echo anchor('coursecategory','หมวดหมู่'); ?></li>
                                            <li <?php echo ($nav == 'speaker')?'class="nav-active"':''; ?>><?php echo anchor('speaker','ผู้สอน'); ?></li>
                                            <li <?php echo ($nav == 'courselocation')?'class="nav-active"':''; ?>><?php echo anchor('courselocation','สถานที่อบรม'); ?></li>
                                            <li <?php echo ($nav == 'pricerange')?'class="nav-active"':''; ?>><?php echo anchor('pricerange','ช่วงราคา'); ?></li>
                                        </ul>
                                    </li>
                                    <li <?php echo ($nav == 'payment')?'class="nav-active"':''; ?>>
                                        <?php echo anchor('payment','<i class="fa fa-bank" aria-hidden="true"></i>
                                            <span>ข้อมูลการชำระเงิน</span>'); ?>
                                    </li>
                                    <li <?php echo ($nav == 'coursecontact')?'class="nav-active"':''; ?>>
                                        <?php echo anchor('coursecontact','<i class="fa fa-envelope" aria-hidden="true"></i>
                                            <span>Request of quotation</span>'); ?>
                                    </li>
                                    <li <?php echo ($nav == 'offer')?'class="nav-active"':''; ?>>
                                        <?php echo anchor('offer','<i class="fa fa-asterisk" aria-hidden="true"></i>
                                            <span>Offer</span>'); ?>
                                    </li>
                                    <li <?php echo ($nav == 'subscribe')?'class="nav-active"':''; ?>>
                                        <?php echo anchor('subscribe','<i class="fa fa-newspaper-o" aria-hidden="true"></i>
                                            <span>ผู้สมัครจดหมายข่าว</span>'); ?>
                                    </li>
                                    <li <?php echo ($nav == 'contact')?'class="nav-active"':''; ?>>
                                        <?php echo anchor('contact','<i class="fa fa-envelope" aria-hidden="true"></i>
                                            <span>ติดต่อเรา</span>'); ?>
                                    </li>
                                    <li class="nav-parent <?php echo (in_array($nav, array('slideshow', 'about', 'privacy', 'terms')))?'nav-expanded nav-active':''; ?>">
                                        <a>
                                            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                                            <span>หน้าเวบ</span>
                                        </a>
                                        <ul class="nav nav-children">
                                            <li <?php echo ($nav == 'slideshow')?'class="nav-active"':''; ?>><?php echo anchor('slideshow','Slideshow'); ?></li>
                                            <li <?php echo ($nav == 'about')?'class="nav-active"':''; ?>><?php echo anchor('content/index/about','เกี่ยวกับเรา'); ?></li>
                                            <li <?php echo ($nav == 'configabout')?'class="nav-active"':''; ?>><?php echo anchor('configabout','รูปเกี่ยวกับเรา'); ?></li>
                                            <li <?php echo ($nav == 'privacy')?'class="nav-active"':''; ?>><?php echo anchor('content/index/privacy','Privacy policy'); ?></li>
                                            <li <?php echo ($nav == 'terms')?'class="nav-active"':''; ?>><?php echo anchor('content/index/terms','Terms of Service'); ?></li>
                                        </ul>
                                    </li>
                                    <li class="nav-parent <?php echo (in_array($nav, array('admin', 'popup', 'config')))?'nav-expanded nav-active':''; ?>">
                                        <a>
                                            <i class="fa fa-cog" aria-hidden="true"></i>
                                            <span>การจัดการ</span>
                                        </a>
                                        <ul class="nav nav-children">
                                            <li <?php echo ($nav == 'admin')?'class="nav-active"':''; ?>><?php echo anchor('admin','เจ้าหน้าที่เวบไซต์'); ?></li>
                                            <li <?php echo ($nav == 'popup')?'class="nav-active"':''; ?>><?php echo anchor('popup','ป๊อปอัพ'); ?></li>
                                            <li <?php echo ($nav == 'config')?'class="nav-active"':''; ?>><?php echo anchor('config','ตั้งค่าเวบไซต์'); ?></li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                            <hr class="separator" />
                        </div>
                    </div>
                </aside>
                <!-- end: sidebar -->
