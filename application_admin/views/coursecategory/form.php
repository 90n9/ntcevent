<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span>คอร์สเรียน</span></li>
        <li><span>หมวดหมู่</span></li>
        <li><span><?php echo $title; ?></span></li>
    </ol>
</div>
</header>
<?php echo form_open('coursecategory/form_post/'.$courseCategoryId, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'coursecategory-form')); ?>
<div class="panel panel-default">
    <div class="panel-heading">
    ข้อมูล <?php echo $parentCategory; ?>
    </div>
    <div class="panel-body">
        <div class="form-group">
            <label for="ddlParent" class="col-md-3 control-label">หมวดหมู่</label>
            <div class="col-md-6">
                <select class="form-control" name="ddlParent" id="ddlParent">
                    <?php
                    $maxPriority = $this->CourseCategoryModel->getMaxPriority(0);
                    if($courseCategoryId == 0 || ($courseCategoryId != 0 && $parentCategory != 0)){
                        $maxPriority++;
                    }
                    $currentMaxPriority = $maxPriority;
                    ?>
                    <option value="0" data-maxpriority="<?php echo $maxPriority; ?>">หมวดหมู่หลัก</option>
                    <?php
                    $categoryList = $this->CourseCategoryModel->getList(0);
                    foreach($categoryList->result() as $category){
                        $maxPriority = $this->CourseCategoryModel->getMaxPriority($category->course_category_id);
                        $checked = '';
                        if($courseCategoryId == $category->course_category_id){
                            continue;
                        }
                        if($courseCategoryId == 0 || ($courseCategoryId != 0 && $parentCategory != $category->course_category_id)){
                            $maxPriority++;
                        }
                        if($parentCategory == $category->course_category_id){
                            $currentMaxPriority = $maxPriority;
                            $checked = 'selected="selected"';
                        }
                        echo '<option value="'.$category->course_category_id.'" data-maxpriority="'.$maxPriority.'" '.$checked.'>- '.$category->course_category_name_th.'</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="txtNameTh" class="col-md-3 control-label">ชื่อกลุ่มภาษาไทย</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="txtNameTh" id="txtNameTh" placeholder="" value="<?php echo ($courseCategoryId != 0)?$courseCategory->course_category_name_th:''; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="txtNameEn" class="col-md-3 control-label">ชื่อกลุ่มภาษาอังกฤษ</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="txtNameEn" id="txtNameEn" placeholder="" value="<?php echo ($courseCategoryId != 0)?$courseCategory->course_category_name_en:''; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="ddlSortPriority" class="col-md-3 control-label">แสดงลำดับที่</label>
            <div class="col-md-6">
                <select name="ddlSortPriority" id="ddlSortPriority" class="form-control">
                    <?php
                    for($i = 1; $i<=$currentMaxPriority; $i++){
                        $checked = ($courseCategoryId != 0 && $courseCategory->sort_priority == $i)?'selected="selected"':'';
                        echo '<option value="'.$i.'" '.$checked.'>'.$i.'</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-3 col-md-6">
                <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button> <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>
