<header class="page-header">
<h2>หมวดหมู่</h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span>คอร์สเรียน</span></li>
        <li><span>หมวดหมู่</span></li>
    </ol>
</div>
</header>
<div class="search-content">
    <div class="tab-content pt-lg">
        <div class="pb-lg">
            <?php echo anchor('coursecategory/form/0', '<i class="fa fa-plus"></i> เพิ่มหมวดหมู่', array('class'=>'btn btn-success')); ?>
        </div>
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
                <tr class="primary">
                    <th>ชื่อหมวดหมู่ภาษาไทย</th>
                    <th>ชื่อหมวดหมู่ภาษาอังกฤษ</th>
                    <th width="280" style="min-width:280px;">การกระทำ</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $parentCategoryList = $this->CourseCategoryModel->getList(0);
                foreach($parentCategoryList->result() as $category){ 
                    $detailUrl = 'coursecategory/form/'.$category->course_category_id;
                    $deleteUrl = 'coursecategory/delete/'.$category->course_category_id;
                    $addSub = 'coursecategory/form/0/'.$category->course_category_id;
                    ?>
                    <tr class="info">
                        <td>
                            <?php echo $category->course_category_name_th; ?>
                        </td>
                        <td>
                            <?php echo $category->course_category_name_en; ?>
                        </td>
                        <td>
                            <?php echo anchor($detailUrl, 'แก้ไขข้อมูล', array('class'=>'btn btn-warning btn-sm')); ?>
                            <?php echo anchor($deleteUrl, 'ลบข้อมูล', array('class'=>'btn btn-danger btn-sm btn-delete')); ?>
                            <?php echo anchor($addSub, 'เพิ่มหมวดหมู่ย่อย', array('class'=>'btn btn-success btn-sm')); ?>
                        </td>
                    </tr>
                <?php
                    $categoryList = $this->CourseCategoryModel->getList($category->course_category_id);
                    foreach($categoryList->result() as $categorySub){ 
                        $detailUrl = 'coursecategory/form/'.$categorySub->course_category_id;
                        $deleteUrl = 'coursecategory/delete/'.$categorySub->course_category_id;
                        ?>
                        <tr>
                            <td>
                                <?php echo $categorySub->course_category_name_th; ?>
                            </td>
                            <td>
                                <?php echo $categorySub->course_category_name_en; ?>
                            </td>
                            <td>
                                <?php echo anchor($detailUrl, 'แก้ไขข้อมูล', array('class'=>'btn btn-warning btn-sm')); ?>
                                <?php echo anchor($deleteUrl, 'ลบข้อมูล', array('class'=>'btn btn-danger btn-sm btn-delete')); ?>
                            </td>
                        </tr>
                    <?php
                    }
                }
                ?>
            </tbody>
          </table>
        </div>
    </div>
</div>