<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span>คอร์สเรียน</span></li>
        <li><span><?php echo anchor('courselocation','สถานที่อบรม'); ?></span></li>
        <li><span><?php echo $title; ?></span></li>
    </ol>
</div>
</header>
<?php echo form_open('courselocation/form_post/'.$courseLocationId, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'courselocation-form')); ?>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group">
            <label for="txtNameTh" class="col-md-3 control-label">ชื่อสถานที่(TH)</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="txtNameTh" id="txtNameTh" placeholder="" value="<?php echo ($courseLocationId != 0)?$courseLocation->course_location_name_th:''; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="txtNameEn" class="col-md-3 control-label">ชื่อสถานที่(EN)</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="txtNameEn" id="txtNameEn" placeholder="" value="<?php echo ($courseLocationId != 0)?$courseLocation->course_location_name_en:''; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="txtDetailTh" class="col-md-3 control-label">ที่อยู่(TH)</label>
            <div class="col-md-6">
              <textarea class="form-control" name="txtDetailTh" id="txtDetailTh"><?php echo ($courseLocationId != 0)?$courseLocation->course_location_detail_th:''; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="txtDetailEn" class="col-md-3 control-label">ที่อยู่(EN)</label>
            <div class="col-md-6">
              <textarea class="form-control" name="txtDetailEn" id="txtDetailEn"><?php echo ($courseLocationId != 0)?$courseLocation->course_location_detail_en:''; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="txtLad" class="col-md-3 control-label">lad</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="txtLad" id="txtLad" placeholder="" value="<?php echo ($courseLocationId != 0)?$courseLocation->lad:''; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="txtLon" class="col-md-3 control-label">lon</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="txtLon" id="txtLon" placeholder="" value="<?php echo ($courseLocationId != 0)?$courseLocation->lon:''; ?>">
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <div id="map-canvas" style="width: 100%; height: 100%;min-width:300px; min-height:300px;"></div>
            </div>
        </div>
        <div class="form-group">
            <label for="ddlSortPriority" class="col-md-3 control-label">แสดงลำดับที่</label>
            <div class="col-md-6">
                <select name="ddlSortPriority" id="ddlSortPriority" class="form-control">
                    <?php
                    $maxPriority = $this->CourseLocationModel->getMaxPriority();
                    $maxPriority += ($courseLocationId == 0)?1:0;
                    for($i = 1; $i<=$maxPriority; $i++){
                        $checked = ($courseLocationId != 0 && $courseLocation->sort_priority == $i)?'selected="selected"':'';
                        echo '<option value="'.$i.'" '.$checked.'>'.$i.'</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-3 col-md-6">
                <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button> <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>
