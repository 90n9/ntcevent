<?php
if($totalTransaction == 0){
  echo '<div class="alert alert-warning" role="alert">ไม่พบรายการตามที่ท่านค้นหา</div>';
}else{
  $totalPage = ceil($totalTransaction/$perPage);
  $pagerData = array();
  $pagerData['totalTransaction'] = $totalTransaction;
  $pagerData['perPage'] = $perPage;
  $pagerData['page'] = $page;
  $pagerData['totalPage'] = $totalPage;
  $pagerData['action'] = site_url('courselocation/filter');
?>
<div class="form-group">
    <?php $this->load->view('courselocation/listpager', $pagerData); ?>
</div>
<div class="table-responsive">
  <table class="table table-striped">
    <thead>
      <tr>
        <th>ที่อยู่(EN)</th>
        <th>ที่อยู่(TH)</th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach($courseLocationList->result() as $courseLocation){
        $detailUrl = 'courselocation/detail/'.$courseLocation->course_location_id;
        ?>
        <tr>
          <td>
            <?php echo anchor($detailUrl, ($courseLocation->course_location_name_en != '')?$courseLocation->course_location_name_en:'-'); ?>
            <div><?php echo$courseLocation->course_location_detail_en; ?></div>
          </td>
          <td>
            <?php echo anchor($detailUrl, ($courseLocation->course_location_name_th != '')?$courseLocation->course_location_name_th:'-'); ?>
            <div><?php echo$courseLocation->course_location_detail_th; ?></div>
          </td>
        </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
<div class="form-group">
  <?php $this->load->view('courselocation/listpager', $pagerData); ?>
</div>
<?php
}
