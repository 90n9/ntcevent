<header class="page-header">
<h2>สถานที่อบรม</h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>คอร์สเรียน</span></li>
    <li><span>สถานที่อบรม</span></li>
  </ol>
</div>
</header>
<div class="search-content">
  <?php
  echo form_open('courselocation/filter', array('role' => 'form', 'id' => 'searchForm'));
  echo form_close();
  ?>
  <div class="tab-content pt-lg">
    <div class="pb-lg">
      <?php echo anchor('courselocation/form/0', '<i class="fa fa-plus"></i> เพิ่มสถานที่อบรม', array('class'=>'btn btn-success')); ?>
    </div>
    <div id="result">
    </div>
    <br />
  </div>
</div>
