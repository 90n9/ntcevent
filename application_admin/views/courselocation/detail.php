<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span><?php echo anchor('courselocation','สถานที่อบรม'); ?></span></li>
        <li><span><?php echo $title; ?></span></li>
    </ol>
</div>
</header>
<div class="btn-group hide-print" style="padding-bottom:10px;">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    การกระทำ <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><?php echo anchor('courselocation', '<i class="glyphicon glyphicon-arrow-left"></i> กลับไปหน้ารายการ'); ?></li>
    <li><?php echo anchor('courselocation/form/'.$courseLocation->course_location_id, '<i class="glyphicon glyphicon-pencil"></i> แก้ไขข้อมูล'); ?></li>
    <li class="divider"></li>
    <li><a href="#" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-trash"></i> ลบข้อมูล</a></li>
  </ul>
</div>
<div class="panel panel-default">
    <div class="panel-body">
        <?php echo form_open('#', array('role' => 'form', 'class' => 'form-horizontal')); ?>
        <div class="form-group">
            <label class="col-sm-2 col-print-3 control-label">ชื่อสถานที่(TH)</label>
            <div class="col-sm-10 col-print-9">
                <p class="form-control-static"><?php echo $courseLocation->course_location_name_th; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 col-print-3 control-label">ชื่อสถานที่(EN)</label>
            <div class="col-sm-10 col-print-9">
                <p class="form-control-static"><?php echo $courseLocation->course_location_name_en; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 col-print-3 control-label">ที่อยู่(TH)</label>
            <div class="col-sm-10 col-print-9">
                <p class="form-control-static"><?php echo $courseLocation->course_location_detail_th; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 col-print-3 control-label">ที่อยู่(EN)</label>
            <div class="col-sm-10 col-print-9">
                <p class="form-control-static"><?php echo $courseLocation->course_location_detail_en; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 col-print-3 control-label">แผนที่</label>
            <div class="col-sm-10 col-print-9">
                <input type="hidden" class="form-control" name="txtLad" id="txtLad" placeholder="" value="<?php echo ($courseLocationId != 0)?$courseLocation->lad:''; ?>">
                <input type="hidden" class="form-control" name="txtLon" id="txtLon" placeholder="" value="<?php echo ($courseLocationId != 0)?$courseLocation->lon:''; ?>">
                <div id="map-canvas" style="width: 100%; height: 100%;min-width:300px; min-height:300px;"></div>
            </div>
        </div>
        <div class="row form-group">
            <label for="txtBranch" class="col-sm-2 col-print-3 control-label">แจ้งวันที่</label>
            <div class="col-sm-4 col-print-3 xs-margin-bottom">
                <p class="form-control-static"><?php echo $courseLocation->create_date; ?></p>
            </div>
            <label class="col-sm-2 col-print-3 control-label">โดย</label>
            <div class="col-sm-4 col-print-3">
                <p class="form-control-static"><?php echo $this->AdminModel->getLoginNameById($courseLocation->create_by); ?></p>
            </div>
        </div>
        <div class="row form-group">
            <label class="col-sm-2 col-print-3 control-label">ปรับปรุงวันที่</label>
            <div class="col-sm-4 col-print-3 xs-margin-bottom">
                <p class="form-control-static"><?php echo $courseLocation->update_date; ?></p>
            </div>
            <label class="col-sm-2 col-print-3 control-label">โดย</label>
            <div class="col-sm-4 col-print-3">
                <p class="form-control-static"><?php echo $this->AdminModel->getLoginNameById($courseLocation->update_by); ?></p>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">ลบข้อมูล</h4>
      </div>
      <div class="modal-body">
        กรุณากดปุ่ม "ลบข้อมูล" เพื่อยืนยันการลบข้อมูลนี้ออกจากระบบ
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button>
        <?php echo anchor('courselocation/delete/'.$courseLocation->course_location_id, '<i class="glyphicon glyphicon-trash"></i> ลบข้อมูล', array('class'=>'btn btn-danger')); ?>
      </div>
    </div>
  </div>
</div>
