<?php
if($totalTransaction == 0){
    echo '<div class="alert alert-warning" role="alert">ไม่พบรายการตามที่ท่านค้นหา</div>';
}else{
    $totalPage = ceil($totalTransaction/$perPage);
    $pagerData = array();
    $pagerData['totalTransaction'] = $totalTransaction;
    $pagerData['searchVal'] = $searchVal;
    $pagerData['startDate'] = $startDate;
    $pagerData['endDate'] = $endDate;
    $pagerData['perPage'] = $perPage;
    $pagerData['page'] = $page;
    $pagerData['totalPage'] = $totalPage;
    $pagerData['action'] = site_url('contact/filter');
?>
<div class="form-group">
    <?php $this->load->view('contact/listpager', $pagerData); ?>
</div>
<div class="table-responsive">
  <table class="table table-striped">
    <thead>
        <tr>
            <th>วันเวลา</th>
            <th>ชื่อ</th>
            <th>อีเมล</th>
            <th>เบอร์ติดต่อ</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach($contactList->result() as $contact){
            $detailUrl = 'contact/detail/'.$contact->contact_id;
            ?>
            <tr>
                <td>
                    <?php echo anchor($detailUrl, $this->DateTimeService->displayDateTime($contact->create_date)); ?>
                </td>
                <td>
                    <?php echo anchor($detailUrl, ($contact->contact_firstname.$contact->contact_lastname != '')?$contact->contact_firstname.' '.$contact->contact_lastname:'-'); ?>
                </td>
                <td>
                    <?php echo anchor($detailUrl, ($contact->contact_email != '')?$contact->contact_email:'-'); ?>
                </td>
                <td>
                    <?php echo anchor($detailUrl, ($contact->contact_phone != '')?$contact->contact_phone:'-'); ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
  </table>
</div>
<div class="form-group">
    <?php $this->load->view('contact/listpager', $pagerData); ?>
</div>
<?php
}
