<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span><?php echo anchor('contact','ติดต่อเรา'); ?></span></li>
        <li><span><?php echo $title; ?></span></li>
    </ol>
</div>
</header>
<?php echo form_open('contact/form_post/'.$contactId, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'course-form')); ?>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group">
            <label for="txtFirstname" class="col-sm-2 control-label">ชื่อ</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="txtFirstname" id="txtFirstname" placeholder="" value="<?php echo ($contactId != 0)?$contact->contact_firstname:''; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label for="txtLastname" class="col-sm-2 control-label">นามสกุล</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="txtLastname" id="txtLastname" placeholder="" value="<?php echo ($contactId != 0)?$contact->contact_lastname:''; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label for="txtEmail" class="col-sm-2 control-label">อีเมล</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="txtEmail" id="txtEmail" placeholder="" value="<?php echo ($contactId != 0)?$contact->contact_email:''; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label for="txtPhone" class="col-sm-2 control-label">เบอร์โทร</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="txtPhone" id="txtPhone" placeholder="" value="<?php echo ($contactId != 0)?$contact->contact_phone:''; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label for="txtDetail" class="col-sm-2 control-label">รายละเอียด</label>
            <div class="col-sm-10">
                <textarea class="form-control" name="txtDetail" id="txtDetail" rows="5"><?php echo ($contactId != 0)?$contact->contact_detail:''; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button> <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>