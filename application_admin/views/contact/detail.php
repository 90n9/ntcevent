<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span><?php echo anchor('contact','ติดต่อเรา'); ?></span></li>
        <li><span><?php echo $title; ?></span></li>
    </ol>
</div>
</header>
<div class="btn-group hide-print" style="padding-bottom:10px;">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    การกระทำ <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><?php echo anchor('contact', '<i class="glyphicon glyphicon-arrow-left"></i> กลับไปหน้ารายการ'); ?></li>
    <li><?php echo anchor('contact/form/'.$contact->contact_id, '<i class="glyphicon glyphicon-pencil"></i> แก้ไขข้อมูล'); ?></li>
    <li class="divider"></li>
    <li><a href="#" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-trash"></i> ลบข้อมูล</a></li>
  </ul>
</div>
<div class="panel panel-default">
    <div class="panel-body">
        <?php echo form_open('#', array('role' => 'form', 'class' => 'form-horizontal')); ?>
        <div class="form-group">
            <label class="col-md-3 col-print-3 control-label">ชื่อ</label>
            <div class="col-md-6 col-print-9">
                <p class="form-control-static"><?php echo $contact->contact_firstname.' '.$contact->contact_lastname; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-print-4 control-label">อีเมล</label>
            <div class="col-md-6 col-print-8">
                <p class="form-control-static"><?php echo $contact->contact_email; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-print-4 control-label">เบอร์โทร</label>
            <div class="col-md-6 col-print-8">
                <p class="form-control-static"><?php echo $contact->contact_phone; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-print-4 control-label">รายละเอียด</label>
            <div class="col-md-6 col-print-8">
                <p class="form-control-static"><?php echo $contact->contact_detail; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label for="txtBranch" class="col-md-3 col-print-3 control-label">แจ้งวันที่</label>
            <div class="col-md-6 col-print-3 xs-margin-bottom">
                <p class="form-control-static"><?php echo $contact->create_date; ?></p>
            </div>
        </div>
        <?php if($contact->update_by > 0){ ?>
        <div class="form-group">
            <label for="txtBranch" class="col-md-3 col-print-3 control-label">ปรับปรุงวันที่</label>
            <div class="col-md-6 col-print-3 xs-margin-bottom">
                <p class="form-control-static"><?php echo $contact->update_date; ?></p>
            </div>
          </div>
          <div class="form-group">
            <label for="txtZone" class="col-md-3 col-print-3 control-label">โดย</label>
            <div class="col-md-6 col-print-3">
                <p class="form-control-static"><?php echo $this->AdminModel->getLoginNameById($contact->update_by); ?></p>
            </div>
        </div>
        <?php } ?>
        <?php echo form_close(); ?>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">ลบข้อมูล</h4>
      </div>
      <div class="modal-body">
        กรุณากดปุ่ม "ลบข้อมูล" เพื่อยืนยันการลบข้อมูลนี้ออกจากระบบ
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button>
        <?php echo anchor('contact/delete/'.$contact->contact_id, '<i class="glyphicon glyphicon-trash"></i> ลบข้อมูล', array('class'=>'btn btn-danger')); ?>
      </div>
    </div>
  </div>
</div>
