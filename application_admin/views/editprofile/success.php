<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span><?php echo $title; ?></span></li>
    </ol>
</div>
</header>
<section class="body-error error-inside">
    <div class="center-error">
        <div class="main-error mb-xlg">
            <h3 class="error-code text-dark text-center text-weight-semibold m-none" style="font-size:9rem;">SUCCESS</h3>
            <p class="error-explanation text-center">ได้ทำการแก้ไขข้อมูลส่วนตัวของคุณเรียบร้อยแล้ว ระบบจะทำกลับเข้าสู่หน้าข้อมูลส่วนตัวในอีก 5 วินาที</p>
        </div>
    </div>
</section>