<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span><?php echo $title; ?></span></li>
    </ol>
</div>
</header>
<?php echo form_open('editprofile/form_post/', array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'editprofile-form')); ?>
<div class="panel panel-default">
    <div class="panel-heading">
    ข้อมูลระบบ
    </div>
    <div class="panel-body">
        <div class="form-group">
            <label for="txtLoginName" class="col-sm-2 control-label">Login Name</label>
            <div class="col-sm-6">
                <p class="form-control-static"><?php echo $admin->login_name; ?></p>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-default" id="editProfile">
    <div class="panel-heading">
    ข้อมูลผู้ใช้งาน
    </div>
    <div class="panel-body">
        <div class="row form-group">
            <label for="txtName" class="col-sm-2 control-label">ชื่อ</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="txtName" id="txtName" value="<?php echo $admin->name; ?>" />
            </div>
            <label for="txtEmail" class="col-sm-2 control-label">อีเมล</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="txtEmail" id="txtEmail" value="<?php echo $admin->email; ?>" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button> <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>