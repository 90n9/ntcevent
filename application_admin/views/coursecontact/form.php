<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span><?php echo anchor('coursecontact','Request of quotation'); ?></span></li>
        <li><span><?php echo $title; ?></span></li>
    </ol>
</div>
</header>
<?php echo form_open('coursecontact/form_post/'.$courseContactId, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'course-form')); ?>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group">
          <label class="col-sm-3 control-label">คอร์สเรียน</label>
          <div class="col-sm-6">
            <p class="form-control-static"><?php echo $this->CourseModel->getName($courseContact->course_id); ?></p>
          </div>
        </div>
        <div class="form-group">
            <label for="txtTicket" class="col-sm-3 control-label">จำนวนที่นั่ง</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="txtTicket" id="txtTicket" placeholder="" value="<?php echo ($courseContactId != 0)?$courseContact->total_ticket:'0'; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label for="txtFirstname" class="col-sm-3 control-label">ชื่อ</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="txtFirstname" id="txtFirstname" placeholder="" value="<?php echo ($courseContactId != 0)?$courseContact->contact_firstname:''; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label for="txtLastname" class="col-sm-3 control-label">นามสกุล</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="txtLastname" id="txtLastname" placeholder="" value="<?php echo ($courseContactId != 0)?$courseContact->contact_lastname:''; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label for="txtEmail" class="col-sm-3 control-label">อีเมล</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="txtEmail" id="txtEmail" placeholder="" value="<?php echo ($courseContactId != 0)?$courseContact->contact_email:''; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label for="txtPhone" class="col-sm-3 control-label">เบอร์โทรศัพท์</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="txtPhone" id="txtPhone" placeholder="" value="<?php echo ($courseContactId != 0)?$courseContact->contact_phone:''; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label for="txtMobile" class="col-sm-3 control-label">เบอร์มือถือ</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="txtMobile" id="txtMobile" placeholder="" value="<?php echo ($courseContactId != 0)?$courseContact->contact_mobile:''; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label for="txtCompany" class="col-sm-3 control-label">บริษัท</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="txtCompany" id="txtCompany" placeholder="" value="<?php echo ($courseContactId != 0)?$courseContact->contact_company:''; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label for="txtNote" class="col-sm-3 control-label">รายละเอียด</label>
            <div class="col-sm-6">
                <textarea class="form-control" name="txtNote" id="txtNote" rows="5"><?php echo ($courseContactId != 0)?$courseContact->contact_note:''; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button> <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>
