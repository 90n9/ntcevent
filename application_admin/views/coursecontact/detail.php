<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span><?php echo anchor('coursecontact','Request of Quotation'); ?></span></li>
        <li><span><?php echo $title; ?></span></li>
    </ol>
</div>
</header>
<div class="btn-group hide-print" style="padding-bottom:10px;">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    การกระทำ <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><?php echo anchor('coursecontact', '<i class="glyphicon glyphicon-arrow-left"></i> กลับไปหน้ารายการ'); ?></li>
    <li><?php echo anchor('coursecontact/form/'.$courseContact->course_contact_id, '<i class="glyphicon glyphicon-pencil"></i> แก้ไขข้อมูล'); ?></li>
    <li class="divider"></li>
    <li><a href="#" data-toggle="modal" data-target="#deleteModal"><i class="glyphicon glyphicon-trash"></i> ลบข้อมูล</a></li>
  </ul>
</div>
<div class="panel panel-default">
    <div class="panel-body">
        <?php echo form_open('#', array('role' => 'form', 'class' => 'form-horizontal')); ?>
        <div class="form-group">
            <label class="col-md-3 col-print-4 control-label">ชื่อคอร์ส</label>
            <div class="col-md-6 col-print-8">
                <p class="form-control-static"><?php echo $this->CourseModel->getName($courseContact->course_id); ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-print-4 control-label">จำนวนที่ต้องการ</label>
            <div class="col-md-6 col-print-8">
                <p class="form-control-static"><?php echo $courseContact->total_ticket; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-print-3 control-label">ชื่อ</label>
            <div class="col-md-6 col-print-9">
                <p class="form-control-static"><?php echo $courseContact->contact_firstname.' '.$courseContact->contact_lastname; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-print-4 control-label">อีเมล</label>
            <div class="col-md-6 col-print-8">
                <p class="form-control-static"><?php echo $courseContact->contact_email; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-print-4 control-label">เบอร์โทรศัพท์</label>
            <div class="col-md-6 col-print-8">
                <p class="form-control-static"><?php echo $courseContact->contact_phone; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-print-4 control-label">เบอร์มือถือ</label>
            <div class="col-md-6 col-print-8">
                <p class="form-control-static"><?php echo $courseContact->contact_mobile; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-print-4 control-label">บริษัท</label>
            <div class="col-md-6 col-print-8">
                <p class="form-control-static"><?php echo $courseContact->contact_company; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-print-4 control-label">รายละเอียด</label>
            <div class="col-md-6 col-print-8">
              <p class="form-control-static"><?php echo $courseContact->contact_note; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label for="txtBranch" class="col-md-3 col-print-3 control-label">แจ้งวันที่</label>
            <div class="col-md-6 col-print-3 xs-margin-bottom">
                <p class="form-control-static"><?php echo $courseContact->create_date; ?></p>
            </div>
        </div>
        <?php if($courseContact->update_by > 0){ ?>
        <div class="form-group">
            <label for="txtBranch" class="col-md-3 col-print-3 control-label">ปรับปรุงวันที่</label>
            <div class="col-md-6 col-print-3 xs-margin-bottom">
                <p class="form-control-static"><?php echo $courseContact->update_date; ?></p>
            </div>
          </div>
          <div class="form-group">
            <label for="txtZone" class="col-md-3 col-print-3 control-label">โดย</label>
            <div class="col-md-6 col-print-3">
                <p class="form-control-static"><?php echo $this->AdminModel->getLoginNameById($courseContact->update_by); ?></p>
            </div>
        </div>
        <?php } ?>
        <?php echo form_close(); ?>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="deleteModalLabel">ลบข้อมูล</h4>
      </div>
      <div class="modal-body">
        กรุณากดปุ่ม "ลบข้อมูล" เพื่อยืนยันการลบข้อมูลนี้ออกจากระบบ
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button>
        <?php echo anchor('coursecontact/delete/'.$courseContact->course_contact_id, '<i class="glyphicon glyphicon-trash"></i> ลบข้อมูล', array('class'=>'btn btn-danger')); ?>
      </div>
    </div>
  </div>
</div>
