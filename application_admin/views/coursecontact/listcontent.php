<?php
if($totalTransaction == 0){
  echo '<div class="alert alert-warning" role="alert">ไม่พบรายการตามที่ท่านค้นหา</div>';
}else{
  $totalPage = ceil($totalTransaction/$perPage);
  $pagerData = array();
  $pagerData['totalTransaction'] = $totalTransaction;
  $pagerData['searchVal'] = $searchVal;
  $pagerData['startDate'] = $startDate;
  $pagerData['endDate'] = $endDate;
  $pagerData['perPage'] = $perPage;
  $pagerData['page'] = $page;
  $pagerData['totalPage'] = $totalPage;
  $pagerData['action'] = site_url('coursecontact/filter');
?>
<div class="form-group">
  <?php $this->load->view('coursecontact/listpager', $pagerData); ?>
</div>
<div class="table-responsive">
  <table class="table table-striped">
    <thead>
      <tr>
        <th>วันที่แจ้ง</th>
        <th>คอร์สเรียน</th>
        <th>ผู้อบรม</th>
        <th>ผู้ติดต่อ</th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach($courseContactList->result() as $courseContact){
        $detailUrl = 'coursecontact/detail/'.$courseContact->course_contact_id;
        $course = $this->CourseModel->getData($courseContact->course_id);

        ?>
        <tr>
          <td>
            <?php echo anchor($detailUrl, $this->DateTimeService->displayDateTime($courseContact->create_date)); ?>
          </td>
          <td>
            <?php
            echo '<div>'.$course->course_name.'</div>';
            echo '<span class="label label-info">'.$course->course_code.'</span>';
            ?>
          </td>
          <td>
            <?php echo anchor($detailUrl, $courseContact->total_ticket); ?>
          </td>
          <td>
            <?php echo anchor($detailUrl, $courseContact->contact_firstname.' '.$courseContact->contact_lastname); ?>
            <div><?php echo '<span class="label label-info">'.$courseContact->contact_company.'</span>'; ?></div>
            <div><small><?php echo $courseContact->contact_email; ?></small></div>
          </td>
        </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
<div class="form-group">
  <?php $this->load->view('coursecontact/listpager', $pagerData); ?>
</div>
<?php
}
