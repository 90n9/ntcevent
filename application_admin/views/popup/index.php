<header class="page-header">
  <h2>ป๊อปอัพ</h2>
  <div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
      <li>
        <a href="index.html">
          <i class="fa fa-home"></i>
        </a>
      </li>
      <li><span>การจัดการ</span></li>
      <li><span>ป๊อปอัพ</span></li>
    </ol>
  </div>
</header>
<?php echo form_open_multipart('popup/form_post/', array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'popup-form')); ?>
<div class="panel panel-default">
  <div class="panel-heading">ข้อมูลป็อปอัพ</div>
  <div class="panel-body">
    <div class="form-group">
      <label for="filePopupTh" class="col-md-3 control-label">รูป popup(TH)</label>
      <div class="col-md-6">
        <?php
        $popupImageTh = $this->ConfigModel->getValue('popup_image_th');
        if($popupImageTh != ''){
          echo '<p style="max-width:200px;"><img src="'.base_url('uploads/'.$popupImageTh).'" class="img-responsive" /></p>';
        }
        ?>
        <input type="file" class="form-control" name="filePopupTh" />
      </div>
    </div>
      <div class="form-group">
        <label for="filePopupEn" class="col-md-3 control-label">รูป popup(EN)</label>
        <div class="col-md-6">
          <?php
          $popupImageEn = $this->ConfigModel->getValue('popup_image_en');
          if($popupImageEn != ''){
            echo '<p style="max-width:200px;"><img src="'.base_url('uploads/'.$popupImageEn).'" class="img-responsive" /></p>';
          }
          ?>
            <input type="file" class="form-control" name="filePopupEn" />
        </div>
      </div>
    <div class="form-group">
      <label for="txtUrl" class="col-md-3 control-label">url</label>
      <div class="col-md-6">
          <input type="text" class="form-control" name="txtUrl" id="txtUrl" value="<?php echo $this->ConfigModel->getValue('popup_url'); ?>" />
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">สถานะ</label>
      <div class="col-md-6">
        <div class="radio-inline">
          <label>
            <input type="radio" name="rdoStatus" id="rdoStatusShow" value="show" <?php echo ($this->ConfigModel->getValue('popup_status') == 'show')?'checked=""':''; ?>>
            แสดง
          </label>
        </div>
        <div class="radio-inline">
          <label>
            <input type="radio" name="rdoStatus" id="rdoStatusHide" value="hide" <?php echo ($this->ConfigModel->getValue('popup_status') == 'hide')?'checked=""':''; ?>>
            ซ่อน
          </label>
        </div>
      </div>
    </div>
  </div>
  <div class="panel-footer">
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button>
      </div>
    </div>
  </div>
</div>
<?php echo form_close(); ?>
