<header class="page-header">
  <h2>รูปเกี่ยวกับเรา</h2>
  <div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
      <li>
        <a href="index.html">
          <i class="fa fa-home"></i>
        </a>
      </li>
      <li><span>หน้าเวบ</span></li>
      <li><span>รูปเกี่ยวกับเรา</span></li>
    </ol>
  </div>
</header>
<?php echo form_open_multipart('configabout/form_post/', array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'popup-form')); ?>
<div class="panel panel-default">
  <div class="panel-heading">รูปเกี่ยวกับเรา</div>
  <div class="panel-body">
    <div class="form-group">
      <label class="col-md-3 control-label">ประเภทเนื้อหา</label>
      <div class="col-md-6">
        <div class="radio-inline">
          <label>
            <input type="radio" name="rdoAboutType" id="rdoAboutTypeImg" value="img" <?php echo ($this->ConfigModel->getValue('about_side_type') == 'img')?'checked=""':''; ?>>
            รูปภาพ
          </label>
        </div>
        <div class="radio-inline">
          <label>
            <input type="radio" name="rdoAboutType" id="rdoAboutTypeVideo" value="video" <?php echo ($this->ConfigModel->getValue('about_side_type') == 'video')?'checked=""':''; ?>>
            วีดีโอ
          </label>
        </div>
        <div class="radio-inline">
          <label>
            <input type="radio" name="rdoAboutType" id="rdoAboutTypeNone" value="none" <?php echo ($this->ConfigModel->getValue('about_side_type') == 'none')?'checked=""':''; ?>>
            ไม่แสดง
          </label>
        </div>
      </div>
    </div>
    <div class="form-group" id="form-group-img">
      <label for="filePopupTh" class="col-md-3 control-label">รูปภาพ</label>
      <div class="col-md-6">
        <?php
        $aboutSideImg = $this->ConfigModel->getValue('about_side_img');
        if($aboutSideImg != ''){
          echo '<p style="max-width:200px;"><img src="'.base_url('uploads/'.$aboutSideImg).'" class="img-responsive" /></p>';
        }
        ?>
        <input type="file" class="form-control" name="fileAboutImg" />
        <div>ขนาดรูปภาพที่แนะนำ 345x224 pixel</div>
      </div>
    </div>
    <div class="form-group" id="form-group-video">
      <label for="txtVideo" class="col-md-3 control-label">video url</label>
      <div class="col-md-6">
          <input type="text" class="form-control" name="txtVideo" id="txtVideo" value="<?php echo $this->ConfigModel->getValue('about_side_video'); ?>" />
      </div>
    </div>
  </div>
  <div class="panel-footer">
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button>
      </div>
    </div>
  </div>
</div>
<?php echo form_close(); ?>
