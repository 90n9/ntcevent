<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span><?php echo anchor('offer','Offer'); ?></span></li>
    <li><span><?php echo $title; ?></span></li>
  </ol>
</div>
</header>
<div class="btn-group hide-print" style="padding-bottom:10px;">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    การกระทำ <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><?php echo anchor('offer', '<i class="glyphicon glyphicon-arrow-left"></i> กลับไปหน้ารายการ'); ?></li>
    <li><?php echo anchor('offer/form/'.$offer->offer_id, '<i class="glyphicon glyphicon-pencil"></i> แก้ไขข้อมูล'); ?></li>
    <li class="divider"></li>
    <li><a href="#" data-toggle="modal" data-target="#deleteModal"><i class="glyphicon glyphicon-trash"></i> ลบข้อมูล</a></li>
  </ul>
</div>
<div class="panel panel-default">
  <div class="panel-body">
    <?php echo form_open('#', array('role' => 'form', 'class' => 'form-horizontal')); ?>
    <div class="form-group">
      <label class="col-sm-2 col-print-3 control-label">รูปภาพ</label>
      <div class="col-sm-10 col-print-9">
        <p class="form-control-static"><img src="<?php echo base_url('uploads/'.$offer->offer_image); ?>" class="img-responsive" style="max-width:300px;" /></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 col-print-3 control-label">รายละเอียด (TH)</label>
      <div class="col-sm-10 col-print-9">
        <?php echo $offer->offer_content_th; ?>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 col-print-3 control-label">รายละเอียด (EN)</label>
      <div class="col-sm-10 col-print-9">
        <?php echo $offer->offer_content_en; ?>
      </div>
    </div>
    <div class="row form-group">
      <label for="txtBranch" class="col-sm-2 col-print-3 control-label">แจ้งวันที่</label>
      <div class="col-sm-4 col-print-3 xs-margin-bottom">
        <p class="form-control-static"><?php echo $offer->create_date; ?></p>
      </div>
      <label class="col-sm-2 col-print-3 control-label">โดย</label>
      <div class="col-sm-4 col-print-3">
        <p class="form-control-static"><?php echo $this->AdminModel->getLoginNameById($offer->create_by); ?></p>
      </div>
    </div>
    <?php if($offer->update_by == 0){ ?>
    <div class="row form-group">
      <label class="col-sm-2 col-print-3 control-label">ปรับปรุงวันที่</label>
      <div class="col-sm-4 col-print-3 xs-margin-bottom">
        <p class="form-control-static"><?php echo $offer->update_date; ?></p>
      </div>
      <label class="col-sm-2 col-print-3 control-label">โดย</label>
      <div class="col-sm-4 col-print-3">
        <p class="form-control-static"><?php echo $this->AdminModel->getLoginNameById($offer->update_by); ?></p>
      </div>
    </div>
    <?php } ?>
    <?php echo form_close(); ?>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="deleteModalLabel">ลบข้อมูล</h4>
      </div>
      <div class="modal-body">
        กรุณากดปุ่ม "ลบข้อมูล" เพื่อยืนยันการลบข้อมูลนี้ออกจากระบบ
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button>
        <?php echo anchor('offer/delete/'.$offer->offer_id, '<i class="glyphicon glyphicon-trash"></i> ลบข้อมูล', array('class'=>'btn btn-danger')); ?>
      </div>
    </div>
  </div>
</div>
