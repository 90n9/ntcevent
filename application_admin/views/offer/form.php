<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span><?php echo anchor('offer','Offer'); ?></span></li>
    <li><span><?php echo $title; ?></span></li>
  </ol>
</div>
</header>
<?php echo form_open_multipart('offer/form_post/'.$offer_id, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'offer-form')); ?>
<div class="panel panel-default">
  <div class="panel-heading">ข้อมูล</div>
  <div class="panel-body">
    <div class="form-group">
      <label for="offer_image" class="col-md-3 control-label">รูปภาพ</label>
      <div class="col-md-6">
        <input type="file" name="offer_image" id="offer_image" class="form-control" />
        <div>ขนาดรูปภาพที่แนะนำ 345x224 pixel</div>
      </div>
    </div>
    <div class="form-group">
      <label for="offer_content_th" class="col-md-3 control-label">เนื้อหา</label>
      <div class="col-md-6">
        <textarea class="summernote" name="offer_content_th" id="offer_content_th" data-upload="<?php echo site_url('offer/upload_image'); ?>" required><?php echo ($offer_id != 0)?$offer->offer_content_th:''; ?></textarea>
      </div>
    </div>
    <div class="form-group">
      <label for="offer_content_en" class="col-md-3 control-label">เนื้อหา</label>
      <div class="col-md-6">
        <textarea class="summernote" name="offer_content_en" id="offer_content_en" data-upload="<?php echo site_url('offer/upload_image'); ?>" required><?php echo ($offer_id != 0)?$offer->offer_content_en:''; ?></textarea>
      </div>
    </div>
    <div class="form-group">
      <label for="sort_priority" class="col-md-3 control-label">แสดงลำดับที่</label>
      <div class="col-md-6">
        <select name="sort_priority" id="sort_priority" class="form-control">
          <?php
          $max_priority = $this->Offer_model->get_max_priority();
          $max_priority += ($offer_id == 0)?1:0;
          for($i = 1; $i<=$max_priority; $i++){
            $checked = ($offer_id == 0 || $offer->sort_priority == $i)?'selected="selected"':'';
            echo '<option value="'.$i.'" '.$checked.'>'.$i.'</option>';
          }
          ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">สถานะ</label>
      <div class="col-md-6">
        <div class="radio-inline">
          <label>
            <input type="radio" name="enable_status" value="1" <?php echo ($offer_id == 0 || $offer->enable_status == 1)?'checked="checked"':''; ?>/> แสดง
          </label>
        </div>
        <div class="radio-inline">
          <label>
            <input type="radio" name="enable_status" value="0" <?php echo ($offer_id != 0 && $offer->enable_status == 0)?'checked="checked"':''; ?>/> ซ่อน
          </label>
        </div>
      </div>
    </div>
  </div>
  <div class="panel-footer">
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button> <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
      </div>
    </div>
  </div>
</div>
<?php echo form_close(); ?>
