<header class="page-header">
<h2>Offer</h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>offer</span></li>
  </ol>
</div>
</header>
<div class="search-content">
  <div class="tab-content pt-lg">
    <div class="pb-lg">
      <?php echo anchor('offer/form', '<i class="fa fa-plus"></i> เพิ่ม offer', array('class'=>'btn btn-success')); ?>
    </div>
    <br />
    <div class="table-responsive">
      <table class="table table-striped table-bordered table-hover">
        <thead>
          <tr>
            <th width="200">รูปภาพ</th>
            <th>รายละเอียด (TH)</th>
            <th>รายละเอียด (EN)</th>
            <th width="130">การกระทำ</th>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach($offer_list->result() as $offer){
            $detail_url = 'offer/detail/'.$offer->offer_id;
            ?>
            <tr>
              <td><img src="<?php echo base_url('uploads/'.$offer->offer_image); ?>" class="img-responsive" style="max-width:200px;" /></td>
              <td><?php echo $offer->offer_content_th; ?></td>
              <td><?php echo $offer->offer_content_en; ?></td>
              <td><?php echo anchor($detail_url,'ดูข้อมูลเพิ่มเติม', array('class'=>'btn btn-info')); ?></td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
