<header class="page-header">
    <h2>ช่วงราคา</h2>
    <div class="right-wrapper pull-right" style="padding-right:20px;">
        <ol class="breadcrumbs">
            <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
            <li><span>คอร์สเรียน</span></li>
            <li><span>ช่วงราคา</span></li>
        </ol>
    </div>
</header>
<div class="search-content">
    <div class="tab-content pt-lg">
        <div class="pb-lg">
            <?php echo anchor('pricerange/form/0', '<i class="fa fa-plus"></i> เพิ่มช่วงราคา', array('class'=>'btn btn-success')); ?>
        </div>
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
                <tr>
                    <th>ราคาเริ่มต้น</th>
                    <th>ราคาสิ้นสุด</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach($priceRangeList->result() as $priceRange){
                    $detailUrl = 'pricerange/detail/'.$priceRange->price_range_id;
                    ?>
                    <tr>
                        <td>
                            <?php echo anchor($detailUrl, number_format($priceRange->price_from)); ?>
                        </td>
                        <td>
                            <?php echo anchor($detailUrl, number_format($priceRange->price_to)); ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
          </table>
        </div>
        <br />
    </div>
</div>
