<header class="page-header">
<h2>ข้อมูลช่วงราคา</h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span>คอร์สเรียน</span></li>
        <li><span>ช่วงราคา</span></li>
        <li><span>ข้อมูลช่วงราคา</span></li>
    </ol>
</div>
</header>
<div class="btn-group hide-print" style="padding-bottom:10px;">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    การกระทำ <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><?php echo anchor('pricerange', '<i class="glyphicon glyphicon-arrow-left"></i> กลับไปหน้ารายการ'); ?></li>
    <li><?php echo anchor('pricerange/form/', '<i class="glyphicon glyphicon-plus"></i> เพิ่มช่วงราคาใหม่'); ?></li>
    <li><?php echo anchor('pricerange/form/'.$priceRange->price_range_id, '<i class="glyphicon glyphicon-pencil"></i> แก้ไขข้อมูล'); ?></li>
    <li class="divider"></li>
    <li><a href="#" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-trash"></i> ลบข้อมูล</a></li>
  </ul>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
    ข้อมูลช่วงราคา
    </div>
    <div class="panel-body">
        <?php echo form_open('#', array('role' => 'form', 'class' => 'form-horizontal')); ?>
        <div class="form-group">
            <label class="col-sm-2 col-print-3 control-label">ราคาเริ่มต้น</label>
            <div class="col-sm-10 col-print-9">
                <p class="form-control-static"><?php echo $priceRange->price_from; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 col-print-4 control-label">ราคาสิ้นสุด</label>
            <div class="col-sm-10 col-print-8">
                <p class="form-control-static"><?php echo $priceRange->price_to; ?></p>
            </div>
        </div>
        <div class="row form-group">
            <label for="txtBranch" class="col-sm-2 col-print-3 control-label">สร้างวันที่</label>
            <div class="col-sm-4 col-print-3 xs-margin-bottom">
                <p class="form-control-static"><?php echo $priceRange->create_date; ?></p>
            </div>
            <label for="txtZone" class="col-sm-2 col-print-3 control-label">โดย</label>
            <div class="col-sm-4 col-print-3">
                <p class="form-control-static"><?php echo $this->AdminModel->getLoginNameById($priceRange->create_by); ?></p>
            </div>
        </div>
        <div class="row form-group">
            <label for="txtBranch" class="col-sm-2 col-print-3 control-label">ปรับปรุงวันที่</label>
            <div class="col-sm-4 col-print-3 xs-margin-bottom">
                <p class="form-control-static"><?php echo $priceRange->update_date; ?></p>
            </div>
            <label for="txtZone" class="col-sm-2 col-print-3 control-label">โดย</label>
            <div class="col-sm-4 col-print-3">
                <p class="form-control-static"><?php echo $this->AdminModel->getLoginNameById($priceRange->update_by); ?></p>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">ลบข้อมูล</h4>
      </div>
      <div class="modal-body">
        กรุณากดปุ่ม "ลบข้อมูล" เพื่อยืนยันการลบข้อมูลนี้ออกจากระบบ
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button>
        <?php echo anchor('pricerange/delete/'.$priceRange->price_range_id, '<i class="glyphicon glyphicon-trash"></i> ลบข้อมูล', array('class'=>'btn btn-danger')); ?>
      </div>
    </div>
  </div>
</div>
