<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span>การขาย</span></li>
        <li><span>สินค้าขาย</span></li>
        <li><span><?php echo $title; ?></span></li>
    </ol>
</div>
</header>
<?php echo form_open('pricerange/form_post/'.$priceRangeId, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'pricerange-form')); ?>
<div class="panel panel-default">
    <div class="panel-heading">
    ข้อมูลช่วงราคา
    </div>
    <div class="panel-body">
        <div class="form-group">
            <label for="txtFrom" class="col-md-3 control-label">ราคาเริ่มต้น</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="txtFrom" id="txtFrom" placeholder="" value="<?php echo ($priceRangeId != 0)?$priceRange->price_from:''; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label for="txtTo" class="col-md-3 control-label">ถึง</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="txtTo" id="txtTo" placeholder="" value="<?php echo ($priceRangeId != 0)?$priceRange->price_to:''; ?>" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-3 col-md-6">
                <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button> <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>
