<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span>คอร์สเรียน</span></li>
        <li><span><?php echo anchor('course','คอร์สเรียน'); ?></span></li>
        <li><span><?php echo anchor('course/detail/'.$courseId,$this->CourseModel->getName($courseId)); ?></span></li>
        <li><span><?php echo $title; ?></span></li>
    </ol>
</div>
</header>
<?php echo form_open_multipart('course/document_post/'.$courseId.'/'.$documentId, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'document-form')); ?>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group">
            <label for="txtName" class="col-sm-3 control-label">ชื่อเอกสาร</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="txtName" id="txtName" placeholder="" value="<?php echo ($documentId != 0)?$document->document_name:''; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="file" class="col-sm-3 control-label">ไฟล์เอกสาร</label>
            <div class="col-sm-6">
                <input type="file" class="form-control" name="file" id="file" placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label for="ddlSortPriority" class="col-sm-3 control-label">แสดงลำดับที่</label>
            <div class="col-sm-6">
                <select name="ddlSortPriority" id="ddlSortPriority" class="form-control">
                    <?php
                    $maxPriority = $this->DocumentModel->getMaxPriority($courseId);
                    $maxPriority += ($documentId == 0)?1:0;
                    for($i = 1; $i<=$maxPriority; $i++){
                        $checked = ($documentId == 0 || $document->sort_priority == $i)?'selected="selected"':'';
                        echo '<option value="'.$i.'" '.$checked.'>'.$i.'</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="row form-group">
            <label class="col-sm-offset-2 col-sm-4 xs-margin-bottom">
                <input type="checkbox" name="chkEnableStatus" value="1" <?php echo ($documentId == 0 || $document->enable_status == 1)?'checked="checked"':''; ?> />
                แสดงรายการ
            </label>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button> <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>