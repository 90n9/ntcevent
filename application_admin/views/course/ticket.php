<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span>คอร์สเรียน</span></li>
        <li><span><?php echo anchor('course','คอร์สเรียน'); ?></span></li>
        <li><span><?php echo anchor('course/detail/'.$courseId,$this->CourseModel->getName($courseId)); ?></span></li>
        <li><span><?php echo $title; ?></span></li>
    </ol>
</div>
</header>
<?php echo form_open('course/ticket_post/'.$courseId.'/'.$ticketId, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'ticket-form')); ?>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group">
            <label for="txtName" class="col-sm-3 control-label">ชื่อตั๋ว</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="txtName" id="txtName" placeholder="" value="<?php echo ($ticketId != 0)?$ticket->ticket_name:''; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="txtQty" class="col-sm-3 control-label">จำนวนตั๋วที่ขาย</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="txtQty" id="txtQty" placeholder="0 = ไม่จำกัด" value="<?php echo ($ticketId != 0)?$ticket->ticket_qty:'0'; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="txtQty" class="col-sm-3 control-label">รายละเอียด</label>
            <div class="col-sm-9">
                <label class="xs-margin-bottom">
                    <input type="checkbox" name="chkShowDescription" id="chkShowDescription" value="1" <?php echo ($ticketId != 0 && $ticket->is_show_description == 1)?'checked="checked"':''; ?> />
                    แสดงรายละเอียด
                </label>
                <div id="descriptionBox">
                    <textarea class="summernote" name="txtDescription" id="txtDescription" data-upload="<?php echo site_url('content/uploadImage'); ?>"><?php echo ($ticketId != 0)?$ticket->ticket_description:''; ?></textarea>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="txtQty" class="col-sm-3 control-label">ราคาขาย</label>
            <div class="col-sm-6">
                <select name="ddlPriceType" id="ddlPriceType" class="form-control">
                    <?php
                        $checked = ($ticketId != 0 && $ticket->price_type == 'free')?'selected="selected"':'';
                        echo '<option value="free" '.$checked.'>ฟรี</option>';
                        $checked = ($ticketId != 0 && $ticket->price_type == 'contact')?'selected="selected"':'';
                        echo '<option value="contact" '.$checked.'>ติดต่อฝ่ายขาย</option>';
                        $checked = ($ticketId != 0 && $ticket->price_type == 'amount')?'selected="selected"':'';
                        echo '<option value="amount" '.$checked.'>ระบุราคา</option>';
                    ?>
                </select>
                <div class="pt-sm" id="priceBox" <?php echo ($ticketId == 0 || $ticket->price_type != 'amount')?'style="display:block;"':''; ?>>
                    <input type="text" class="form-control" name="txtPrice" id="txtPrice" placeholder="0" value="<?php echo ($ticketId != 0)?$ticket->price:'0'; ?>">
                </div>
            </div>
        </div>
        <?php
        $startDate = '';
        $startTime = '';
        $endDate = '';
        $endTime = '';
        if($ticketId != 0){
            list($startDate, $startTime) = explode(" ", $ticket->start_date);
            list($endDate, $endTime) = explode(" ", $ticket->end_date);
        }
        ?>
        <div class="form-group">
            <label for="txtStartDate" class="col-xs-12 col-sm-3 control-label">วันที่เริ่ม</label>
            <div class="col-xs-6 col-sm-5 col-md-4">
                <input type="text" class="form-control" name="txtStartDate" id="txtStartDate" placeholder="" value="<?php echo $startDate; ?>">
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3">
                <input type="text" class="form-control" name="txtStartTime" id="txtStartTime" placeholder="" value="<?php echo $startTime; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="txtEndDate" class="col-xs-12 col-sm-3 control-label">วันที่สิ้นสุด</label>
            <div class="col-xs-6 col-sm-5 col-md-4">
                <input type="text" class="form-control" name="txtEndDate" id="txtEndDate" placeholder="" value="<?php echo $endDate; ?>">
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3">
                <input type="text" class="form-control" name="txtEndTime" id="txtEndTime" placeholder="" value="<?php echo $endTime; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="txtBuyMin" class="col-sm-3 control-label">ซื้อตั๋วขั้นต่ำ</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="txtBuyMin" id="txtBuyMin" placeholder="" value="<?php echo ($ticketId != 0)?$ticket->buy_min:'1'; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="txtBuyMax" class="col-sm-3 control-label">ซื้อตั๋วสูงสุด</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="txtBuyMax" id="txtBuyMax" placeholder="" value="<?php echo ($ticketId != 0)?$ticket->buy_max:'0'; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="ddlSortPriority" class="col-sm-3 control-label">แสดงลำดับที่</label>
            <div class="col-sm-6">
                <select name="ddlSortPriority" id="ddlSortPriority" class="form-control">
                    <?php
                    $maxPriority = $this->TicketModel->getMaxPriority($courseId);
                    $maxPriority += ($ticketId == 0)?1:0;
                    for($i = 1; $i<=$maxPriority; $i++){
                        $checked = ($ticketId == 0 || $ticket->sort_priority == $i)?'selected="selected"':'';
                        echo '<option value="'.$i.'" '.$checked.'>'.$i.'</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="row form-group">
            <label class="col-sm-offset-2 col-sm-4 xs-margin-bottom">
                <input type="checkbox" name="chkEnableStatus" value="1" <?php echo ($ticketId == 0 || $ticket->enable_status == 1)?'checked="checked"':''; ?> />
                แสดงหน้าเวบไซต์
            </label>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button> <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>
