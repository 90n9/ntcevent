<div class="btn-group hide-print" style="padding-bottom:10px;">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    การกระทำ <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><?php echo anchor('course', '<i class="glyphicon glyphicon-arrow-left"></i> กลับไปหน้ารายการ'); ?></li>
    <li><?php echo anchor('course/form/0', '<i class="glyphicon glyphicon-plus"></i> เพิ่มคอร์สเรียน'); ?></li>
    <li><?php echo anchor('course/form/'.$course->course_id, '<i class="glyphicon glyphicon-pencil"></i> แก้ไขข้อมูลคอร์สเรียน'); ?></li>
    <li class="divider"></li>
    <li><a href="#" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-trash"></i> ลบข้อมูล</a></li>
  </ul>
</div>
<?php echo form_open('#', array('role' => 'form', 'class' => 'form-horizontal')); ?>
<div class="panel panel-default">
  <div class="panel-heading">
    <div class="panel-actions">
      <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
    </div>
    ข้อมูลคอร์สเรียน
  </div>
    <div class="panel-body">
        <div class="form-group">
            <label class="col-md-3 col-print-4 control-label">รหัสคอร์สเรียน</label>
            <div class="col-md-6 col-print-8">
                <p class="form-control-static"><?php echo $course->course_code; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-print-4 control-label">ชื่อคอร์ส</label>
            <div class="col-md-6 col-print-8">
                <p class="form-control-static"><?php echo $course->course_name; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-print-4 control-label">หมวดหมู่</label>
            <div class="col-md-6 col-print-8">
                <p class="form-control-static"><?php echo $this->CourseCategoryModel->getName($course->course_category_id); ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">วันที่</label>
            <div class="col-md-6">
                <p class="form-control-static"><i class="fa fa-calendar"></i> <?php echo $this->DateTimeService->displayDateList($course->course_date); ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">เวลา</label>
            <div class="col-md-6">
                <p class="form-control-static"><?php echo $course->course_start_time; ?> ถึง <?php echo $course->course_end_time; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-print-4 control-label">สอนภาษา</label>
            <div class="col-md-6 col-print-8">
                <p class="form-control-static"><?php echo $course->course_lang; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">สถานที่อบรม</label>
            <div class="col-md-6">
                <p class="form-control-static"><?php echo $this->CourseLocationModel->getName($course->course_location_id); ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">รูป thumbnail</label>
            <div class="col-md-6">
                <?php
                if($course->thumb_image != ''){
                    echo '<img src="'.base_url('uploads/'.$course->thumb_image).'" alt="Thumbnail Image" class="img-responsive" style="max-width:260px;" />';
                }
                ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">รูป Cover</label>
            <div class="col-md-6">
                <?php
                if($course->cover_image != ''){
                    echo '<img src="'.base_url('uploads/'.$course->cover_image).'" alt="Cover Image" class="img-responsive" style="max-width:260px;" />';
                }
                ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">วีดีโอยูทูป</label>
            <div class="col-md-6">
                <?php
                if($course->cover_video != ''){
                    echo '<iframe width="560" height="315" src="'.$course->cover_video.'" frameborder="0" allowfullscreen></iframe>';
                }
                ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">สถานะคอร์สเรียน</label>
            <div class="col-md-6">
                <p class="form-control-static"><?php echo $this->CourseFullStatusModel->getName($course->course_full_status); ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">ประเภทผู้สอน</label>
            <div class="col-md-6">
                <p class="form-control-static"><?php echo $this->SpeakerTypeModel->getName($course->speaker_type); ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Name</label>
            <div class="col-md-6">
                <p class="form-control-static"><?php echo $this->SpeakerModel->getName($course->speaker_id); ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">รายละเอียดคอร์ส</label>
            <div class="col-md-9">
                <div class="well"><?php echo $course->course_description; ?></div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-print-4 control-label">แสดงลำดับที่</label>
            <div class="col-md-6 col-print-8">
                <p class="form-control-static mobile"><?php echo $course->sort_priority; ?></p>
            </div>
        </div>
        <fieldset disabled>
        <div class="form-group">
            <label class="col-md-offset-3 col-md-6 col-print-6">
                <input type="checkbox" <?php echo ($course->enable_status == 1)?'checked="checked"':''; ?> />
                แสดงผลหน้าเวบไซต์
            </label>
        </div>
        </fieldset>
    </div>
</div>
<?php $this->load->view('course/detail/ticket'); ?>
<div class="panel panel-default">
  <div class="panel-heading">
    <div class="panel-actions">
      <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
    </div>
    ข้อมูลการแก้ไข
  </div>
  <div class="panel-body">
    <div class="form-group">
        <label class="col-md-3 col-print-3 control-label">สร้างวันที่</label>
        <div class="col-sm-4 col-print-3 xs-margin-bottom">
            <p class="form-control-static"><?php echo $this->DateTimeService->displayDateTime($course->create_date); ?></p>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 col-print-3 control-label">โดย</label>
        <div class="col-sm-4 col-print-3">
            <p class="form-control-static"><?php echo $this->AdminModel->getLoginNameById($course->create_by); ?></p>
        </div>
    </div>
    <?php if($course->update_by > 0){ ?>
    <div class="form-group">
        <label class="col-md-3 col-print-3 control-label">ปรับปรุงวันที่</label>
        <div class="col-sm-4 col-print-3 xs-margin-bottom">
            <p class="form-control-static"><?php echo $this->DateTimeService->displayDateTime($course->update_date); ?></p>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 col-print-3 control-label">โดย</label>
        <div class="col-sm-4 col-print-3">
            <p class="form-control-static"><?php echo $this->AdminModel->getLoginNameById($course->update_by); ?></p>
        </div>
    </div>
    <?php } ?>
  </div>
</div>
<?php echo form_close(); ?>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">ลบข้อมูล</h4>
      </div>
      <div class="modal-body">
        กรุณากดปุ่ม "ลบข้อมูล" เพื่อยืนยันการลบข้อมูลนี้ออกจากระบบ
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button>
        <?php echo anchor('course/delete/'.$course->course_id, '<i class="glyphicon glyphicon-trash"></i> ลบข้อมูล', array('class'=>'btn btn-danger')); ?>
      </div>
    </div>
  </div>
</div>
