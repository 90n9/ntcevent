<div class="table-responsive">
  <table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>วันที่ wishlist</th>
            <th>ชื่อ</th>
            <th>อีเมล</th>
            <th>วันที่สมัคร</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach($wishlistList->result() as $wishlist){
            $member = $this->MemberModel->getData($wishlist->member_id);
            if(!$member){
              continue;
            }
            $detailUrl = 'member/detail/'.$member->member_id;
            ?>
            <tr>
                <td>
                    <?php echo $wishlist->create_date; ?>
                </td>
                <td>
                    <?php echo anchor($detailUrl, ($member->firstname != '')?$member->firstname.' '.$member->lastname:'-'); ?>
                </td>
                <td>
                    <?php echo anchor($detailUrl, ($member->email != '')?$member->email:'-'); ?>
                </td>
                <td>
                    <?php echo anchor($detailUrl, $this->DateTimeService->displayDateTime($member->create_date)); ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
  </table>
</div>
