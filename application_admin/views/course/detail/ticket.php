<div class="panel panel-default">
  <div class="panel-heading">
    <div class="panel-actions">
      <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
    </div>
    ข้อมูลตั๋ว
  </div>
  <div class="panel-body">
    <div class="form-group">
        <label class="col-md-3 control-label">ชื่อตั๋ว</label>
        <div class="col-md-6">
          <p class="form-control-static"><?php echo $ticket->ticket_name; ?></p>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">จำนวนตั๋วที่ขาย <span class="required">*</span></label>
        <div class="col-md-6">
          <p class="form-control-static"><?php echo $ticket->ticket_qty; ?></p>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">รายละเอียดตั๋ว</label>
        <div class="col-md-9">
            <label class="xs-margin-bottom control-label">
                <input type="checkbox" name="chkTicketShowDescription" id="chkTicketShowDescription" value="1" <?php echo ($ticket->is_show_description == 1)?'checked="checked"':''; ?> disabled />
                    แสดงรายละเอียด
            </label>
            <?php if($ticket->is_show_description == 1){ ?>
            <div id="descriptionBox">
              <pre><?php echo $ticket->ticket_description; ?></pre>
            </div>
            <?php } ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">ราคาขาย</label>
        <div class="col-md-6">
          <p class="form-control-static"><?php
          switch($ticket->price_type){
            case 'free':
              echo 'Free of charge';
              break;
            case 'contact':
              echo 'Contact us';
              break;
            case 'amount':
              echo 'ระบุราคาขาย';
              break;
          }
          ?></p>
        </div>
    </div>
    <?php if($ticket->price_type == 'amount'){ ?>
    <div class="form-group">
        <label class="col-md-3 control-label">ราคาคอร์สเรียน <span class="required">*</span></label>
        <div class="col-md-6">
            <p class="form-control-static"><?php echo number_format($ticket->price); ?> บาท</p>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">ราคาก่อนหักส่วนลด</label>
        <div class="col-md-6">
            <p class="form-control-static"><?php echo ($course->course_before_discount_price > 0)?number_format($course->course_before_discount_price).' บาท':'ไม่มีราคาก่อนหักส่วนลด'; ?></p>
        </div>
    </div>
    <?php
    }
    $startDate = date('Y-m-d');
    $startTime = '0:00';
    $endDate = '';
    $endTime = '23:59';
    list($startDate, $startTime) = explode(" ", $ticket->start_date);
    list($endDate, $endTime) = explode(" ", $ticket->end_date);
    ?>
    <div class="form-group form-group-filled" id="ticket_event_period">
        <label for="txtTicketStartDate" class="col-md-3 control-label">ช่วงวันที่ขายตั๋ว</label>
        <div class="col-md-6">
          <div class="input-daterange input-group">
            <span class="input-group-addon first-input-group">
              <i class="fa fa-calendar"></i>
            </span>
            <input type="text" class="form-control ticket_actual_range" name="txtTicketStartDate" id="txtTicketStartDate" value="<?php echo $this->DateTimeService->displayDate($startDate); ?>" readonly>
            <span class="input-group-addon">to</span>
            <input type="text" class="form-control ticket_actual_range" name="txtTicketEndDate" id="txtTicketEndDate" value="<?php echo $this->DateTimeService->displayDate($endDate); ?>" readonly>
          </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-offset-3 col-md-6 xs-margin-bottom">
            <input type="checkbox" name="chkTicketEnableStatus" value="1" <?php echo ($ticket->enable_status == 1)?'checked="checked"':''; ?> disabled />
            แสดงตั๋วหน้าเวบไซต์
        </label>
    </div>
  </div>
</div>
