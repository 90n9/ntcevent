<div class="pb-lg">
    <?php echo anchor('course/document/'.$courseId.'/0', '<i class="fa fa-plus"></i> เพิ่มเอกสาร', array('class'=>'btn btn-success')); ?>
</div>
<div class="table-responsive">
  <table class="table table-striped">
    <thead>
        <tr>
            <th>ชื่อเอกสาร</th>
            <th>ชื่อไฟล์</th>
            <th>ดาวน์โหลด</th>
            <th>สถานะ</th>
            <th>การกระทำ</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach($documentList->result() as $document){
            $detailUrl = 'course/document/'.$courseId.'/'.$document->course_document_id;
            $deleteUrl = 'course/document_delete/'.$document->course_document_id;
            ?>
            <tr>
                <td><?php echo anchor($detailUrl, $document->document_name); ?></td>
                <td><?php echo anchor($detailUrl, $document->document_file); ?></td>
                <td><a href="<?php echo base_url('uploads/'.$document->document_file); ?>" target="_blank">Download</a></td>
                <td><?php echo anchor($detailUrl, ($document->enable_status == 1)?'แสดง':'ไม่แสดง'); ?></td>
                <td>
                    <?php echo anchor($deleteUrl, 'ลบเอกสาร', array('class'=>'btn btn-danger btn-sm')); ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
  </table>
</div>
