<div class="table-responsive">
  <table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>รหัสผู้เข้าเรียน</th>
            <th>ชื่อผู้เรียน</th>
            <th>บริษัท</th>
            <th>ติดต่อ</th>
            <th>ประเภทที่นั่ง</th>
            <th>สถานะ</th>
            <th>วันที่าลงทะเบียน</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach($courseRegisterList->result() as $courseRegister){
            $courseBuyer = $this->CourseBuyerModel->getData($courseRegister->course_buyer_id);
            ?>
            <tr>
                <td><?php echo $courseRegister->course_register_id; ?></td>
                <td><?php echo $courseRegister->register_firstname.' '.$courseRegister->register_lastname; ?></td>
                <td><?php echo $courseRegister->register_company; ?></td>
                <td>
                    <div><i class="fa fa-phone"></i>
                        <?php echo $courseRegister->register_mobile; ?>
                    </div>
                    <div><i class="fa fa-envelope"></i>
                        <?php echo $courseRegister->register_email; ?>
                    </div>
                </td>
                <td><?php echo $this->TicketModel->getName($courseRegister->ticket_id); ?></td>
                <?php
                $arrStatusName = array(
                  'waiting' => '<span class="label label-default">Waiting for payment</span>',
                  'payment' => '<span class="label label-primary">Waiting for approval</span>',
                  'complete' => '<span class="label label-success">Complete Order</span>',
                  'cancel' => '<span class="label label-danger">Cancelled Order</span>',
                );
                ?>
                <td><?php echo $arrStatusName[$courseBuyer->course_buyer_status]; ?></td>
                <td><?php echo $this->DateTimeService->displayOnlyDate($courseRegister->create_date); ?></td>
            </tr>
        <?php } ?>
    </tbody>
  </table>
</div>
