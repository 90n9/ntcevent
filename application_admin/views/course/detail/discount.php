<div class="pb-lg">
    <?php echo anchor('course/discount/'.$courseId.'/0', '<i class="fa fa-plus"></i> เพิ่มส่วนลด', array('class'=>'btn btn-success')); ?>
</div>
<div class="table-responsive">
  <table class="table table-striped">
    <thead>
        <tr>
            <th>รหัสส่วนลด</th>
            <th>ขั้นต่ำ</th>
            <th>สูงสุด</th>
            <th>จำนวน</th>
            <th>ส่วนลด</th>
            <th>ช่วงเวลา</th>
            <th>สถานะ</th>
            <th>การกระทำ</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach($discountList->result() as $discount){
            $detailUrl = 'course/discount/'.$courseId.'/'.$discount->discount_id;
            $deleteUrl = 'course/discount_delete/'.$discount->discount_id;
            ?>
            <tr>
                <td><?php echo anchor($detailUrl, $discount->discount_code); ?></td>
                <td>
                  <?php
                  echo anchor($detailUrl, $discount->min_ticket);
                  ?>
                </td>
                <td>
                  <?php
                  echo anchor($detailUrl, ($discount->max_ticket >= $discount->min_ticket)?$discount->max_ticket:'ไม่ระบุ');
                  ?>
                </td>
                <td><?php echo anchor($detailUrl, $discount->discount_qty); ?></td>
                <td>
                    <?php
                    echo $discount->discount_amount;
                    if($discount->discount_type == 'baht'){
                        echo ' บาท';
                    }else{
                        echo '%';
                    }
                    ?>
                </td>
                <td>
                    <?php echo anchor($detailUrl, $this->DateTimeService->displayDateTime($discount->available_from).' - '.$this->DateTimeService->displayDateTime($discount->available_to)); ?>
                </td>
                <td>
                    <?php echo anchor($detailUrl, ($discount->enable_status == 1)?'ใช้ได้':'ปิดการใช้งาน'); ?>
                </td>
                <td>
                    <?php echo anchor($deleteUrl, 'ลบส่วนลด', array('class'=>'btn btn-danger btn-sm')); ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
  </table>
</div>
