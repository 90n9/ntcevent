<div class="table-responsive">
  <table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>รหัสลงทะเบียน</th>
            <th>จำนวนที่นั่ง</th>
            <th>ราคาที่ต้องชำระ</th>
            <th>สถานะ</th>
            <th>วันที่สั่งซื้อ</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach($courseBuyerList->result() as $courseBuyer){
            $member = $this->MemberModel->getData($courseBuyer->member_id);
            $detailUrl = 'course/coursebuyer/'.$courseId.'/'.$courseBuyer->course_buyer_id;
            ?>
            <tr>
                <td>
                    <?php echo anchor($detailUrl,$courseBuyer->course_buyer_code); ?>
                </td>
                <td>
                    <?php echo $courseBuyer->total_ticket; ?>
                </td>
                <td>
                    <?php
                    echo number_format($courseBuyer->paid_amount);
                    if($courseBuyer->discount_code != ''){
                      echo '<div><span class="label label-primary">'.$courseBuyer->discount_code.'</span></div>';
                    }
                    ?>
                </td>
                <?php
                $arrStatusName = array(
                  'waiting' => '<span class="label label-default">Waiting for payment</span>',
                  'payment' => '<span class="label label-primary">Waiting for approval</span>',
                  'complete' => '<span class="label label-success">Complete Order</span>',
                  'cancel' => '<span class="label label-danger">Cancelled Order</span>',
                );
                ?>
                <td><?php echo $arrStatusName[$courseBuyer->course_buyer_status]; ?></td>
                <td>
                    <?php echo $this->DateTimeService->displayDateTime($courseBuyer->create_date); ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
  </table>
</div>
