<header class="page-header">
<h2>รายละเอียดคอร์สเรียน</h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span>คอร์สเรียน</span></li>
        <li><span><?php echo anchor('course','คอร์สเรียน'); ?></span></li>
        <li><span>รายละเอียดคอร์สเรียน</span></li>
    </ol>
</div>
</header>
<div class="search-content" style="background:#ffffff;">
    <div class="search-toolbar">
        <ul class="list-unstyled nav nav-pills">
            <li class="active">
                <a href="#course" data-toggle="tab" aria-expanded="true">ข้อมูลคอร์สเรียน</a>
            </li>
            <li>
                <a href="#discount" data-toggle="tab" aria-expanded="false">ส่วนลด <span class="badge"><?php echo $discountList->num_rows(); ?></span></a>
            </li>
            <li>
                <a href="#document" data-toggle="tab" aria-expanded="false">เอกสาร <span class="badge"><?php echo $documentList->num_rows(); ?></span></a>
            </li>
            <li>
                <a href="#coursebuyer" data-toggle="tab" aria-expanded="false">ผู้สั่งซื้อ <span class="badge"><?php echo $courseBuyerList->num_rows(); ?></span></a>
            </li>
            <li>
                <a href="#courseregister" data-toggle="tab" aria-expanded="false">ที่นั่ง <span class="badge"><?php echo $courseRegisterList->num_rows(); ?></span></a>
            </li>
            <li>
                <a href="#wishlist" data-toggle="tab" aria-expanded="false">Wishlist <span class="badge"><?php echo $wishlistList->num_rows(); ?></span></a>
            </li>
        </ul>
    </div>
    <div class="tab-content">
        <div id="course" class="tab-pane active">
            <?php $this->load->view('course/detail/course'); ?>
        </div>
        <div id="discount" class="tab-pane">
            <?php $this->load->view('course/detail/discount'); ?>
        </div>
        <div id="document" class="tab-pane">
            <?php $this->load->view('course/detail/document'); ?>
        </div>
        <div id="coursebuyer" class="tab-pane">
            <?php $this->load->view('course/detail/coursebuyer'); ?>
        </div>
        <div id="courseregister" class="tab-pane">
            <?php $this->load->view('course/detail/courseregister'); ?>
        </div>
        <div id="wishlist" class="tab-pane">
            <?php $this->load->view('course/detail/wishlist'); ?>
        </div>
    </div>
</div>
