<header class="page-header">
<h2>คอร์สเรียน</h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span>คอร์สเรียน</span></li>
        <li><span>คอร์สเรียน</span></li>
    </ol>
</div>
</header>
<div class="search-content">
    <div class="search-control-wrapper">
        <?php echo form_open('course/filter', array('role' => 'form', 'class' => 'form-inline', 'id' => 'searchForm')); ?>
        <div class="form-group">
            <input type="text" class="form-control" name="txtSearchVal" id="txtSearchVal" placeholder="ข้อความที่ต้องการค้นหา" />
        </div>
        <div class="form-group">
            <select class="form-control" name="ddlCourseCategory" id="ddlCourseCategory">
                <option value="all" selected="selected">หมวดหมู่ทั้งหมด</option>
                <?php
                $courseCategoryList = $this->CourseCategoryModel->getList(0);
                foreach($courseCategoryList->result() as $courseCategory){
                    ?>
                    <option value="<?php echo $courseCategory->course_category_id; ?>"><?php echo $courseCategory->course_category_name_th; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
          <select class="form-control" name="ddlEnableStatus" id="ddlEnableStatus">
            <option value="all" selected="selected">สถานะทั้งหมด</option>
            <option value="1">แสดง</option>
            <option value="0">ซ่อน</option>
          </select>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="txtStartDate" id="txtStartDate" placeholder="ค้นหาคอร์สตั้งแต่วันที่" />
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="txtEndDate" id="txtEndDate" placeholder="ค้นหาคอร์สถึงวันที่" />
        </div>
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-filter"></i> ค้นหา</button>
        <?php echo form_close(); ?>
    </div>
    <div class="tab-content pt-lg">
        <div class="pb-lg">
            <?php echo anchor('course/form/0', '<i class="fa fa-plus"></i> เพิ่มคอร์สเรียน', array('class'=>'btn btn-success')); ?>
        </div>
        <div id="result">
        </div>
        <br />
    </div>
</div>
