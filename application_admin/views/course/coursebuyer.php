<header class="page-header">
  <h2><?php echo $title; ?></h2>
  <div class="right-wrapper pull-right" style="padding-right:20px;">
      <ol class="breadcrumbs">
          <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
          <li><span>คอร์สเรียน</span></li>
          <li><span><?php echo anchor('course','คอร์สเรียน'); ?></span></li>
          <li><span><?php echo anchor('course/detail/'.$course->course_id, $course->course_name); ?></span></li>
          <li><span><?php echo $title; ?></span></li>
      </ol>
  </div>
</header>

<div class="btn-group hide-print" style="padding-bottom:10px;">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    การกระทำ <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><?php echo anchor('course/detail/'.$course->course_id, '<i class="glyphicon glyphicon-arrow-left"></i> กลับไปรายละเอียดคอร์สเรียน'); ?></li>
    <li class="divider"></li>
    <li><a href="#" data-toggle="modal" data-target="#cancelModal"><i class="glyphicon glyphicon-trash"></i> ยกเลิกการสั่งซื้อ</a></li>
  </ul>
</div>
<?php echo form_open('#', array('role' => 'form', 'class' => 'form-horizontal')); ?>
<div class="panel panel-default">
    <div class="panel-heading"><h4>ข้อมูลการสั่งซื้อ</h4></div>
    <div class="panel-body">
        <div class="form-group">
            <label class="col-sm-2 col-print-3 control-label">รหัสสั่งซื้อ</label>
            <div class="col-sm-10 col-print-9">
              <p class="form-control-static"><?php echo $courseBuyer->course_buyer_code; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 col-print-3 control-label">สถานะ</label>
            <div class="col-sm-10 col-print-9">
              <p class="form-control-static"><?php
              $arrStatusName = array(
                'waiting' => '<span class="label label-default">Waiting for payment</span>',
                'payment' => '<span class="label label-primary">Waiting for approval</span>',
                'complete' => '<span class="label label-success">Complete Order</span>',
                'cancel' => '<span class="label label-danger">Cancelled Order</span>',
              );
              echo $arrStatusName[$courseBuyer->course_buyer_status];
              ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 col-print-3 control-label">จำนวนตั๋ว</label>
            <div class="col-sm-10 col-print-9">
              <p class="form-control-static"><?php echo $courseBuyer->total_ticket; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 col-print-3 control-label">sub total</label>
            <div class="col-sm-10 col-print-9">
              <p class="form-control-static"><?php echo $courseBuyer->total_amount; ?></p>
            </div>
        </div>
        <?php if($courseBuyer->discount_id > 0){ ?>
        <div class="form-group">
            <label class="col-sm-2 col-print-3 control-label">Discount (<?php echo $courseBuyer->discount_code; ?>)</label>
            <div class="col-sm-10 col-print-9">
              <p class="form-control-static"><?php echo $courseBuyer->discount_amount; ?></p>
            </div>
        </div>
        <?php } ?>
        <div class="form-group">
            <label class="col-sm-2 col-print-3 control-label">VAT 7%</label>
            <div class="col-sm-10 col-print-9">
              <p class="form-control-static"><?php echo $courseBuyer->vat_amount; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 col-print-3 control-label">Grand Total</label>
            <div class="col-sm-10 col-print-9">
              <p class="form-control-static"><?php echo $courseBuyer->paid_amount; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 col-print-3 control-label">วันที่ลงทะเบียน</label>
            <div class="col-sm-4 col-print-3 xs-margin-bottom">
              <p class="form-control-static"><?php echo $courseBuyer->create_date; ?></p>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading"><h4>ข้อมูลในการออกใบเสร็จรับเงินและใบกำกับภาษี</h4></div>
    <div class="panel-body">
      <?php
      if($courseBuyer->is_company == 1){
        ?>
        <div class="form-group">
          <label class="col-sm-2  col-print-3 control-label">Company Name</label>
          <div class="col-sm-10 col-print-9">
            <p class="form-control-static"><?php echo $courseBuyer->company_name; ?></p>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2  col-print-3 control-label">Branch</label>
          <div class="col-sm-10 col-print-9">
            <p class="form-control-static"><?php echo $courseBuyer->company_branch; ?></p>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2  col-print-3 control-label">Tax</label>
          <div class="col-sm-10 col-print-9">
            <p class="form-control-static"><?php echo $courseBuyer->company_tax; ?></p>
          </div>
        </div>
        <?php
      }
      ?>
        <div class="form-group">
            <label class="col-sm-2 col-print-3 control-label">Billing Address</label>
            <div class="col-sm-10 col-print-9">
              <p class="form-control-static"><?php echo $courseBuyer->company_address; ?></p>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading"><h4>ข้อมูลผู้สั่งซื้อ</h4></div>
    <div class="panel-body">
        <div class="form-group">
            <label class="col-sm-2 col-print-3 control-label">ชื่อผู้ซื้อ</label>
            <div class="col-sm-10 col-print-9">
              <p class="form-control-static"><?php echo $courseBuyer->buyer_firstname.' '.$courseBuyer->buyer_lastname; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 col-print-3 control-label">อีเมล</label>
            <div class="col-sm-10 col-print-9">
              <p class="form-control-static"><?php echo $courseBuyer->buyer_email; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 col-print-3 control-label">เบอร์โทรศัพท์</label>
            <div class="col-sm-10 col-print-9">
              <p class="form-control-static"><?php echo $courseBuyer->buyer_mobile; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 col-print-3 control-label">ชื่อบริษัท</label>
            <div class="col-sm-10 col-print-9">
              <p class="form-control-static"><?php echo $courseBuyer->buyer_company; ?></p>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading"><h4>ข้อมูลผู้เข้าอบรม</h4></div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table">
              <thead>
                <tr>
                  <th>รหัสผู้เรียน</th>
                  <th>ประเภทตั๋ว</th>
                  <th>ข้อมูลผู้เข้าอบรม</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach($courseRegisterList->result() as $courseRegister){
                  $ticket = $this->TicketModel->getData($courseRegister->ticket_id);
                  echo '<tr>
                    <td>'.$courseRegister->course_register_code.'</td>
                    <td>'.$ticket->ticket_name.'</td>
                    <td>
                      <div>'.$courseRegister->register_firstname.' '.$courseRegister->register_lastname.'</div>
                      <div>'.$courseRegister->register_email.'</div>
                      <div>'.$courseRegister->register_mobile.'</div>
                      <div>'.$courseRegister->register_company.'</div>
                    </td>
                  </tr>';
                }
                ?>
              </tbody>
            </table>
        </div>
    </div>
</div>
<? echo form_close(); ?>
<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="deleteModalLabel">ลบข้อมูล</h4>
      </div>
      <div class="modal-body">
        กรุณากดปุ่ม "ลบข้อมูล" เพื่อยืนยันการลบข้อมูลนี้ออกจากระบบ
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button>
        <?php echo anchor('course/coursebuyer_delete/'.$course->course_id.'/'.$courseBuyerId, '<i class="glyphicon glyphicon-trash"></i> ลบข้อมูล', array('class'=>'btn btn-danger')); ?>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="cancelModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="cancelModalLabel">ยกเลิกการสั่งซื้อ</h4>
      </div>
      <div class="modal-body">
        กรุณากดปุ่ม "ยืนยัน" เพื่อยืนยันการยกเลิกการสั่งซื้อนี้
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button>
        <?php echo anchor('course/coursebuyer_cancel/'.$courseBuyerId, '<i class="glyphicon glyphicon-trash"></i> ยืนยัน', array('class'=>'btn btn-danger')); ?>
      </div>
    </div>
  </div>
</div>
