<?php
if($totalTransaction == 0){
    echo '<div class="alert alert-warning" role="alert">ไม่พบรายการตามที่ท่านค้นหา</div>';
}else{
    $totalPage = ceil($totalTransaction/$perPage);
    $pagerData = array();
    $pagerData['totalTransaction'] = $totalTransaction;
    $pagerData['searchVal'] = $searchVal;
    $pagerData['courseCategoryId'] = $courseCategoryId;
    $pagerData['enableStatus'] = $enableStatus;
    $pagerData['startDate'] = $startDate;
    $pagerData['endDate'] = $endDate;
    $pagerData['perPage'] = $perPage;
    $pagerData['page'] = $page;
    $pagerData['totalPage'] = $totalPage;
    $pagerData['action'] = site_url('course/filter');
?>
<div class="form-group">
    <?php $this->load->view('course/listpager', $pagerData); ?>
</div>
<div class="table-responsive">
  <table class="table table-striped">
    <thead>
        <tr>
            <th>คอร์สเรียน</th>
            <th class="text-right">ราคา</th>
            <th class="text-center">ช่วงเวลาขายตั๋ว</th>
            <th class="text-center">สถานะ</th>
            <th class="text-center">วันที่สร้าง</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach($courseList->result() as $course){
            $ticket = $this->TicketModel->getList($course->course_id)->row();
            $detailUrl = 'course/detail/'.$course->course_id;
            ?>
            <tr>
                <td>
                  <div><?php echo anchor($detailUrl, $course->course_name); ?></div>
                  <div style="font-size:13px;">code: <?php echo anchor($detailUrl, $course->course_code); ?></div>
                  <div style="font-size:13px;">วันที่อบรม:
                  <?php
                  echo $this->DateTimeService->displayDateList($course->course_date);
                  echo " ($course->course_start_time - $course->course_end_time)";
                  /*
                  $startDate = '';
                  $startTime = '';
                  $endDate = '';
                  $endTime = '';
                  list($startDate, $startTime) = explode(" ", $course->course_start);
                  list($endDate, $endTime) = explode(" ", $course->course_end);
                  $startTime = substr($startTime, 0, 5);
                  $endTime = substr($endTime, 0, 5);
                  echo '<b>'.$this->DateTimeService->displayDate($startDate).'</b> ถึง <b>'.$this->DateTimeService->displayDate($endDate).'</b> ('.$startTime.' - '.$endTime.')';
                  */
                  ?></div>
                  <div style="font-size:13px;">ภาษา: <b><?php echo $course->course_lang; ?></b> หมวด: <b><?php echo $this->CourseCategoryModel->getName($course->course_category_id); ?></b></div>
                </td>
                <td class="text-right"><?php
                switch($course->course_price_type){
                  case 'contact':
                    echo 'Contact us';
                    break;
                  case 'free':
                    echo 'Free of charge';
                    break;
                  case 'amount':
                    if($course->course_before_discount_price > 0){
                      echo '<div style="text-decoration:line-through;">'.number_format($course->course_before_discount_price).' บาท</div>';
                      echo number_format($course->course_price).' บาท';

                    }else{
                      echo number_format($course->course_price).' บาท';
                    }
                    break;
                }
                ?></td>
                <td class="text-center">
                  <?php
                  if($ticket){
                    list($startDate, $startTime) = explode(" ", $ticket->start_date);
                    list($endDate, $endTime) = explode(" ", $ticket->end_date);
                    echo '<b>'.$this->DateTimeService->displayDate($startDate).'</b> ถึง <b>'.$this->DateTimeService->displayDate($endDate).'</b>';
                  }else{
                    echo 'กรุณาแก้ไขข้อมูลตั๋ว';
                  }
                  ?>
                </td>
                <td class="text-center">
                  <?php echo ($course->enable_status == 1)?'<span class="label label-success">แสดง</span>':'<span class="label label-danger">ไม่แสดง</span>'; ?>
                </td>
                <td>
                    <?php echo $this->DateTimeService->displayOnlyDate($course->create_date); ?>
                    <div style="font-size:13px;">โดย: <b><?php echo $this->AdminModel->getLoginNameById($course->create_by); ?></b></div>
                </td>
            </tr>
        <?php } ?>
    </tbody>
  </table>
</div>
<div class="form-group">
    <?php $this->load->view('course/listpager', $pagerData); ?>
</div>
<?php
}
