<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span>คอร์สเรียน</span></li>
        <li><span><?php echo anchor('course', 'คอร์สเรียน'); ?></span></li>
        <li><span><?php echo $title; ?></span></li>
    </ol>
</div>
</header>
<?php echo form_open_multipart('course/form_post/'.$courseId, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'course-form')); ?>
<div class="panel panel-default">
    <div class="panel-heading">
  		<div class="panel-actions">
  			<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
      </div>
      ข้อมูลคอร์สเรียน
    </div>
    <div class="panel-body">
        <div class="form-group">
            <label for="txtName" class="col-md-3 control-label">ชื่อคอร์ส <span class="required">*</span></label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="txtName" id="txtName" placeholder="" value="<?php echo ($courseId != 0)?$course->course_name:''; ?>" required />
            </div>
        </div>
        <div class="form-group">
            <label for="ddlCourseCategory" class="col-md-3 control-label">หมวดหมู่</label>
            <div class="col-md-6">
                <select class="form-control" name="ddlCourseCategory" id="ddlCourseCategory">
                    <?php $this->CourseCategoryModel->echoOptionList(0,'', ($courseId != 0)?$course->course_category_id:0); ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="txtCourseDate" class="col-md-3 control-label">วันอบรม <span class="required">*</span></label>
            <div class="col-md-6">
              <div id="courseDate" data-date="<?php echo ($courseId != 0)?$course->course_date:''; ?>"></div>
              <input type="text" class="form-control" name="txtCourseDate" id="txtCourseDate" value="<?php echo ($courseId != 0)?$course->course_date:''; ?>" required>
            </div>
        </div>
        <div class="form-group">
            <label for="txtCourseStartTime" class="col-md-3 control-label">ช่วงเวลาอบรม <span class="required">*</span></label>
            <div class="col-md-6">
              <div class="row">
                <div class="col-xs-6">
                    <input type="text" class="form-control timeinput" name="txtCourseStartTime" id="txtCourseStartTime" value="<?php echo ($courseId != 0)?$course->course_start_time:'09:00'; ?>" required />
                </div>
                <div class="col-xs-6">
                    <input type="text" class="form-control timeinput" name="txtCourseEndTime" id="txtCourseEndTime" value="<?php echo ($courseId != 0)?$course->course_end_time:'17:00'; ?>" required />
                </div>
              </div>
            </div>
        </div>
        <div class="form-group">
            <label for="ddlLang" class="col-md-3 control-label">สอนภาษา</label>
            <div class="col-md-6">
                <select class="form-control" name="ddlLang" id="ddlLang">
                    <?php
                    $checked = ($courseId == 0 || $course->course_lang == 'th')?'selected="selected"':'';
                    echo '<option value="th" '.$checked.'>ภาษาไทย</option>';
                    $checked = ($courseId != 0 && $course->course_lang == 'en')?'selected="selected"':'';
                    echo '<option value="en" '.$checked.'>ภาษาอังกฤษ</option>';
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="ddlLocation" class="col-md-3 control-label">สถานที่อบรม</label>
            <div class="col-md-6">
                <select class="form-control" name="ddlLocation" id="ddlLocation">
                    <option value="0">โปรดระบุ</option>
                    <?php
                    $arrLocationList = $this->CourseLocationModel->getList();
                    foreach($arrLocationList->result() as $courseLocation){
                        $checked = ($courseId != 0 && $course->course_location_id == $courseLocation->course_location_id)?'selected="selected"':'';
                        echo '<option value="'.$courseLocation->course_location_id.'" '.$checked.'>'.$courseLocation->course_location_name_th.'</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="fileThumb" class="col-md-3 control-label">รูป thumbnail</label>
            <div class="col-md-6">
                <?php
                if($courseId != 0 && $course->thumb_image != ''){
                    echo '<div class="pb-sm"><img src="'.base_url('uploads/'.$course->thumb_image).'" alt="Thumbnail Image" class="img-responsive" style="max-width:260px;" /></div>';
                }
                echo '<input type="file" class="form-control" name="fileThumb" id="fileThumb" placeholder="Upload file here" value="" />';
                echo '<p class="help-block">recommended 400x400pixel</p>';
                ?>
            </div>
        </div>
        <div class="form-group">
            <label for="fileCover" class="col-md-3 control-label">รูป Cover</label>
            <div class="col-md-6">
                <?php
                if($courseId != 0 && $course->cover_image != ''){
                    echo '<div class="pb-sm"><img src="'.base_url('uploads/'.$course->cover_image).'" alt="Cover Image" class="img-responsive" style="max-width:260px;" /></div>';
                }
                echo '<input type="file" class="form-control" name="fileCover" id="fileCover" placeholder="Upload file here" value="" />';
                echo '<p class="help-block">recommended 1200x420pixel</p>';
                ?>
            </div>
        </div>
        <div class="form-group">
            <label for="txtCoverVideo" class="col-md-3 control-label">วีดีโอยูทูป</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="txtCoverVideo" id="txtCoverVideo" placeholder="http://" value="<?php echo ($courseId != 0)?$course->cover_video:''; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label for="ddlFullStatus" class="col-md-3 control-label">สถานะคอร์สเรียน</label>
            <div class="col-md-6">
                <select class="form-control" name="ddlFullStatus" id="ddlFullStatus">
                    <?php
                    $arrCourseStatus = $this->CourseFullStatusModel->getList();
                    foreach($arrCourseStatus as $key => $value){
                        $checked = ($courseId != 0 && $course->course_full_status == $key)?'selected="selected"':'';
                        echo '<option value="'.$key.'" '.$checked.'>'.$value.'</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="ddlSpeakerType" class="col-md-3 control-label">ประเภทผู้สอน</label>
            <div class="col-md-6">
                <select class="form-control" name="ddlSpeakerType" id="ddlSpeakerType">
                    <option value="">โปรดระบุ</option>
                    <?php
                    $arrSpeakerType = $this->SpeakerTypeModel->getList();
                    foreach($arrSpeakerType as $key => $value){
                        $checked = ($courseId != 0 && $course->speaker_type == $key)?'selected="selected"':'';
                        echo '<option value="'.$key.'" '.$checked.'>'.$value.'</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group speaker-form-group">
            <label for="ddlSpeaker" class="col-md-3 control-label">Name</label>
            <div class="col-md-6">
                <select class="form-control" name="ddlSpeaker" id="ddlSpeaker">
                    <option value="">โปรดระบุ</option>
                    <?php
                    $speakerList = $this->SpeakerModel->getList();
                    foreach($speakerList->result() as $speaker){
                        $checked = ($courseId != 0 && $course->speaker_id == $speaker->speaker_id)?'selected="selected"':'';
                        echo '<option value="'.$speaker->speaker_id.'" '.$checked.'>'.$speaker->speaker_name.'</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="txtDescription" class="col-md-3 control-label">รายละเอียดคอร์ส <span class="required">*</span></label>
            <div class="col-md-9">
                <textarea class="summernote" name="txtDescription" id="txtDescription" data-upload="<?php echo site_url('course/uploadImage'); ?>" required><?php echo ($courseId != 0)?$course->course_description:''; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="ddlSortPriority" class="col-md-3 control-label">แสดงลำดับที่</label>
            <div class="col-md-6">
                <select name="ddlSortPriority" id="ddlSortPriority" class="form-control">
                    <?php
                    $maxPriority = $this->CourseModel->getMaxPriority();
                    $maxPriority += ($courseId == 0)?1:0;
                    for($i = 1; $i<=$maxPriority; $i++){
                        $checked = ($courseId == 0 || $course->sort_priority == $i)?'selected="selected"':'';
                        echo '<option value="'.$i.'" '.$checked.'>'.$i.'</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-offset-3 col-md-6 xs-margin-bottom">
                <input type="checkbox" name="chkEnableStatus" value="1" <?php echo ($courseId == 0 || $course->enable_status == 1)?'checked="checked"':''; ?> />
                แสดงหน้าเวบไซต์
            </label>
        </div>
        <!--- Ticket Form -->
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
    		<div class="panel-actions">
    			<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
        </div>
        ข้อมูลตั๋ว
      </div>
      <div class="panel-body">
        <div class="form-group">
            <label for="txtTicketName" class="col-md-3 control-label">ชื่อตั๋ว <span class="required">*</span></label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="txtTicketName" id="txtTicketName" placeholder="" value="<?php echo ($ticketId != 0)?$ticket->ticket_name:'General Admission'; ?>" required>
            </div>
        </div>
        <div class="form-group">
            <label for="txtTicketQty" class="col-md-3 control-label">จำนวนตั๋วที่ขาย <span class="required">*</span></label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="txtTicketQty" id="txtTicketQty" placeholder="0 = ไม่จำกัด" value="<?php echo ($ticketId != 0)?$ticket->ticket_qty:'1'; ?>" required>
            </div>
        </div>
        <div class="form-group">
            <label for="chkTicketShowDescription" class="col-md-3 control-label">รายละเอียดตั๋ว</label>
            <div class="col-md-9">
                <label class="xs-margin-bottom control-label">
                    แสดงรายละเอียด
                    <input type="checkbox" name="chkTicketShowDescription" id="chkTicketShowDescription" value="1" <?php echo ($ticketId != 0 && $ticket->is_show_description == 1)?'checked="checked"':''; ?> />
                </label>
                <div id="descriptionBox">
                    <textarea class="summernote" name="txtTicketDescription" id="txtTicketDescription" data-upload="<?php echo site_url('content/uploadImage'); ?>"><?php echo ($ticketId != 0)?$ticket->ticket_description:''; ?></textarea>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="ddlTocketPriceType" class="col-md-3 control-label">Price Type</label>
            <div class="col-md-6">
                <select name="ddlTicketPriceType" id="ddlTicketPriceType" class="form-control">
                    <?php
                        $checked = ($ticketId != 0 && $ticket->price_type == 'free')?'selected="selected"':'';
                        echo '<option value="free" '.$checked.'>ฟรี</option>';
                        $checked = ($ticketId != 0 && $ticket->price_type == 'contact')?'selected="selected"':'';
                        echo '<option value="contact" '.$checked.'>ติดต่อฝ่ายขาย</option>';
                        $checked = ($ticketId != 0 && $ticket->price_type == 'amount')?'selected="selected"':'';
                        echo '<option value="amount" '.$checked.'>ระบุราคา</option>';
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group priceBox">
            <label for="txtTicketPrice" class="col-md-3 control-label">ราคาขาย <span class="required">*</span></label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="txtTicketPrice" id="txtTicketPrice" placeholder="ราคาที่ขาย" value="<?php echo ($ticketId != 0)?number_format($ticket->price):'100'; ?>" />
            </div>
        </div>
        <div class="form-group priceBox">
            <label for="txtBeforeDiscountPrice" class="col-md-3 control-label">Price List</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="txtBeforeDiscountPrice" id="txtBeforeDiscountPrice" placeholder="ใส่กรณีที่ต้องการราคาก่อนหักส่วนลด" value="<?php echo ($courseId != 0 && $course->course_before_discount_price != 0)?number_format($course->course_before_discount_price):''; ?>" />
            </div>
        </div>
        <?php
        $startDate = date('Y-m-d');
        $startTime = '0:00';
        $endDate = '';
        $endTime = '23:59';
        if($ticketId != 0){
            list($startDate, $startTime) = explode(" ", $ticket->start_date);
            list($endDate, $endTime) = explode(" ", $ticket->end_date);
        }
        ?>
        <div class="form-group form-group-filled" id="ticket_event_period">
            <label for="txtTicketStartDate" class="col-md-3 control-label">ช่วงวันที่ขายตั๋ว <span class="required">*</span></label>
            <div class="col-md-6">
              <div class="input-daterange input-group">
                <span class="input-group-addon first-input-group">
                  <i class="fa fa-calendar"></i>
                </span>
                <input type="text" class="form-control ticket_actual_range" name="txtTicketStartDate" id="txtTicketStartDate" value="<?php echo $startDate; ?>" required>
                <span class="input-group-addon">to</span>
                <input type="text" class="form-control ticket_actual_range" name="txtTicketEndDate" id="txtTicketEndDate" value="<?php echo $endDate; ?>" required >
              </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-offset-3 col-md-6 xs-margin-bottom">
                <input type="checkbox" name="chkTicketEnableStatus" value="1" <?php echo ($ticketId == 0 || $ticket->enable_status == 1)?'checked="checked"':''; ?> />
                แสดงตั๋วหน้าเวบไซต์
            </label>
        </div>
    </div>
    <footer class="panel-footer">
      <div class="row">
        <div class="col-sm-9 col-sm-offset-3">
            <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button>
            <a class="btn btn-default" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
        </div>
      </div>
    </footer>
</div>
<?php echo form_close(); ?>
