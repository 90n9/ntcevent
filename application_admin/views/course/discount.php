<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span>คอร์สเรียน</span></li>
        <li><span><?php echo anchor('course','คอร์สเรียน'); ?></span></li>
        <li><span><?php echo anchor('course/detail/'.$courseId,$this->CourseModel->getName($courseId)); ?></span></li>
        <li><span><?php echo $title; ?></span></li>
    </ol>
</div>
</header>
<?php echo form_open('course/discount_post/'.$courseId.'/'.$discountId, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'discount-form')); ?>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group">
            <label for="txtCode" class="col-sm-3 control-label">รหัสส่วนลด</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="txtCode" id="txtCode" placeholder="" value="<?php echo ($discountId != 0)?$discount->discount_code:''; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="txtQty" class="col-sm-3 control-label">จำนวนส่วนลดที่ใช้ได้</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="txtQty" id="txtQty" placeholder="0 = ไม่จำกัด" value="<?php echo ($discountId != 0)?$discount->discount_qty:'0'; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="txtMinTicket" class="col-sm-3 control-label">จำนวนสั่งซื้อตั๋วขั้นต่ำ</label>
            <div class="col-sm-6">
                <input type="number" class="form-control" name="txtMinTicket" id="txtMinTicket" value="<?php echo ($discountId != 0)?$discount->min_ticket:1; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label for="txtMaxTicket" class="col-sm-3 control-label">จำนวนสั่งซื้อตั๋วสูงสุด</label>
            <div class="col-sm-6">
                <input type="number" class="form-control" name="txtMaxTicket" id="txtMaxTicket" value="<?php echo ($discountId != 0)?$discount->max_ticket:1; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label for="txtQty" class="col-sm-3 control-label">ประเภทส่วนลด</label>
            <div class="col-sm-6">
                <select name="ddlDiscountType" id="ddlDiscountType" class="form-control">
                    <?php
                        $checked = ($discountId != 0 && $discount->discount_type == 'baht')?'selected="selected"':'';
                        echo '<option value="baht" '.$checked.'>จำนวนบาท</option>';
                        $checked = ($discountId != 0 && $discount->discount_type == 'percent')?'selected="selected"':'';
                        echo '<option value="percent" '.$checked.'>เปอร์เซนต์</option>';
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="txtAmount" class="col-sm-3 control-label">ส่วนลด</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="txtAmount" id="txtAmount" placeholder="" value="<?php echo ($discountId != 0)?$discount->discount_amount:''; ?>">
            </div>
        </div>
        <?php
        $startDate = '';
        $startTime = '';
        $endDate = '';
        $endTime = '';
        if($discountId != 0){
            list($startDate, $startTime) = explode(" ", $discount->available_from);
            list($endDate, $endTime) = explode(" ", $discount->available_to);
        }
        ?>
        <div class="form-group">
            <label for="txtAvailableFromDate" class="col-xs-12 col-sm-3 control-label">วันที่เริ่ม</label>
            <div class="col-xs-6 col-sm-5 col-md-4">
                <input type="text" class="form-control" name="txtAvailableFromDate" id="txtAvailableFromDate" placeholder="" value="<?php echo $startDate; ?>">
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3">
                <input type="text" class="form-control" name="txtAvailableFromTime" id="txtAvailableFromTime" placeholder="" value="<?php echo $startTime; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="txtAvailableToDate" class="col-xs-12 col-sm-3 control-label">วันที่สิ้นสุด</label>
            <div class="col-xs-6 col-sm-5 col-md-4">
                <input type="text" class="form-control" name="txtAvailableToDate" id="txtAvailableToDate" placeholder="" value="<?php echo $endDate; ?>">
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3">
                <input type="text" class="form-control" name="txtAvailableToTime" id="txtAvailableToTime" placeholder="" value="<?php echo $endTime; ?>">
            </div>
        </div>
        <div class="row form-group">
            <label class="col-sm-offset-2 col-sm-4 xs-margin-bottom">
                <input type="checkbox" name="chkEnableStatus" value="1" <?php echo ($discountId == 0 || $discount->enable_status == 1)?'checked="checked"':''; ?> />
                ใช้ได้
            </label>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button> <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>
