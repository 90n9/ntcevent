<header class="page-header">
  <h2>ตั้งค่าเวบไซต์</h2>
  <div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
      <li>
        <a href="index.html">
          <i class="fa fa-home"></i>
        </a>
      </li>
      <li><span>การจัดการ</span></li>
      <li><span>ตั้งค่าเวบไซต์</span></li>
    </ol>
  </div>
</header>
<?php echo form_open_multipart('config/form_post/', array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'config-form')); ?>
<div class="panel panel-default">
  <div class="panel-heading">การตั้งค่า</div>
  <div class="panel-body">
    <div class="form-group" id="form-group-video">
      <label for="email_contact" class="col-md-3 control-label">อีเมล Support</label>
      <div class="col-md-6">
          <input type="text" class="form-control" name="email_contact" id="email_contact" value="<?php echo $this->ConfigModel->getValue('email_contact'); ?>" />
      </div>
    </div>
    <div class="form-group" id="form-group-video">
      <label for="email_sales" class="col-md-3 control-label">อีเมล Sales</label>
      <div class="col-md-6">
          <input type="text" class="form-control" name="email_sales" id="email_sales" value="<?php echo $this->ConfigModel->getValue('email_sales'); ?>" />
      </div>
    </div>
    <div class="form-group" id="form-group-video">
      <label for="email_finance" class="col-md-3 control-label">อีเมล Finance</label>
      <div class="col-md-6">
          <input type="text" class="form-control" name="email_finance" id="email_finance" value="<?php echo $this->ConfigModel->getValue('email_finance'); ?>" />
      </div>
    </div>
  </div>
  <div class="panel-footer">
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button>
      </div>
    </div>
  </div>
</div>
<?php echo form_close(); ?>
