<?php
if($totalTransaction == 0){
    echo '<div class="alert alert-warning" role="alert">ไม่พบรายการตามที่ท่านค้นหา</div>';
}else{
    $totalPage = ceil($totalTransaction/$perPage);
    $pagerData = array();
    $pagerData['totalTransaction'] = $totalTransaction;
    $pagerData['searchVal'] = $searchVal;
    $pagerData['perPage'] = $perPage;
    $pagerData['page'] = $page;
    $pagerData['totalPage'] = $totalPage;
    $pagerData['action'] = site_url('speaker/filter');
?>
<div class="form-group">
    <?php $this->load->view('speaker/listpager', $pagerData); ?>
</div>
<div class="table-responsive">
  <table class="table table-striped">
    <thead>
        <tr>
            <th width="250">ชื่อผู้สอน</th>
            <th>รายละเอียดโดยย่อ</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach($speakerList->result() as $speaker){
            $detailUrl = 'speaker/detail/'.$speaker->speaker_id;
            ?>
            <tr>
                <td><?php echo anchor($detailUrl, ($speaker->speaker_name != '')?$speaker->speaker_name:'-'); ?></td>
                <td><?php echo $speaker->speaker_short_detail; ?></td>
            </tr>
        <?php } ?>
    </tbody>
  </table>
</div>
<div class="form-group">
    <?php $this->load->view('speaker/listpager', $pagerData); ?>
</div>
<?php
}
