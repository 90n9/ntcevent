<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span>คอร์สเรียน</span></li>
        <li><span><?php echo anchor('speaker','ผู้สอน'); ?></span></li>
        <li><span><?php echo $title; ?></span></li>
    </ol>
</div>
</header>
<?php echo form_open_multipart('speaker/form_post/'.$speakerId, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'course-form')); ?>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group">
            <label for="txtName" class="col-md-3 control-label">ชื่อผู้สอน</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="txtName" id="txtName" placeholder="" value="<?php echo ($speakerId != 0)?$speaker->speaker_name:''; ?>" />
            </div>
        </div>
        <div class="form-group">
          <label for="txtShortDetail" class="col-md-3 control-label">รายละเอียดโดยย่อ</label>
          <div class="col-md-6">
            <textarea class="form-control" name="txtShortDetail" id="txtShortDetail" data-upload="<?php echo site_url('speaker/uploadImage'); ?>"><?php echo ($speakerId != 0)?$speaker->speaker_short_detail:''; ?></textarea>
          </div>
        </div>
        <div class="form-group">
            <label for="txtDetail" class="col-md-3 control-label">รายละเอียด</label>
            <div class="col-md-9">
                <textarea class="summernote" name="txtDetail" id="txtDetail" data-upload="<?php echo site_url('speaker/uploadImage'); ?>"><?php echo ($speakerId != 0)?$speaker->speaker_detail:''; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="fileThumb" class="col-md-3 control-label">รูปถ่าย</label>
            <div class="col-md-6">
                <?php
                if($speakerId != 0 && $speaker->speaker_image != ''){
                    echo '<div class="pb-sm"><img src="'.base_url('uploads/'.$speaker->speaker_image).'" alt="Speaker Image" class="img-responsive" style="max-width:200px;" /></div>';
                }
                echo '<input type="file" class="form-control" name="file" id="file" placeholder="Upload file here" value="" />';
                echo '<p class="help-block">recommended 200x200pixel</p>';
                ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-3 col-md-6">
                <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button> <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>
