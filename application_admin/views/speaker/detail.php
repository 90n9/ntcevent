<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span><?php echo anchor('speaker','ผู้สอน'); ?></span></li>
        <li><span><?php echo $title; ?></span></li>
    </ol>
</div>
</header>
<div class="btn-group hide-print" style="padding-bottom:10px;">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    การกระทำ <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><?php echo anchor('speaker', '<i class="glyphicon glyphicon-arrow-left"></i> กลับไปหน้ารายการ'); ?></li>
    <li><?php echo anchor('speaker/form/'.$speaker->speaker_id, '<i class="glyphicon glyphicon-pencil"></i> แก้ไขข้อมูล'); ?></li>
    <li class="divider"></li>
    <li><a href="#" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-trash"></i> ลบข้อมูล</a></li>
  </ul>
</div>
<div class="panel panel-default">
    <div class="panel-body">
        <?php echo form_open('#', array('role' => 'form', 'class' => 'form-horizontal')); ?>
        <div class="form-group">
            <label class="col-sm-2 col-print-3 control-label">ชื่อ</label>
            <div class="col-sm-10 col-print-9">
                <p class="form-control-static"><?php echo $speaker->speaker_name; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 col-print-4 control-label">รายละเอียดโดยย่อ</label>
            <div class="col-sm-10 col-print-8">
                <div class="well"><?php echo $speaker->speaker_short_detail; ?></div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 col-print-4 control-label">รายละเอียด</label>
            <div class="col-sm-10 col-print-8">
                <div class="well"><?php echo $speaker->speaker_detail; ?></div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">รูปถ่าย</label>
            <div class="col-sm-10">
                <?php
                if($speaker->speaker_image != ''){
                    echo '<img src="'.base_url('uploads/'.$speaker->speaker_image).'" alt="Thumbnail Image" class="img-responsive" style="max-width:200px;" />';
                }
                ?>
            </div>
        </div>
        <div class="row form-group">
            <label for="txtBranch" class="col-sm-2 col-print-3 control-label">แจ้งวันที่</label>
            <div class="col-sm-4 col-print-3 xs-margin-bottom">
                <p class="form-control-static"><?php echo $speaker->create_date; ?></p>
            </div>
            <label class="col-sm-2 col-print-3 control-label">โดย</label>
            <div class="col-sm-4 col-print-3">
                <p class="form-control-static"><?php echo $this->AdminModel->getLoginNameById($speaker->create_by); ?></p>
            </div>
        </div>
        <div class="row form-group">
            <label class="col-sm-2 col-print-3 control-label">ปรับปรุงวันที่</label>
            <div class="col-sm-4 col-print-3 xs-margin-bottom">
                <p class="form-control-static"><?php echo $speaker->update_date; ?></p>
            </div>
            <label class="col-sm-2 col-print-3 control-label">โดย</label>
            <div class="col-sm-4 col-print-3">
                <p class="form-control-static"><?php echo $this->AdminModel->getLoginNameById($speaker->update_by); ?></p>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">ลบข้อมูล</h4>
      </div>
      <div class="modal-body">
        กรุณากดปุ่ม "ลบข้อมูล" เพื่อยืนยันการลบข้อมูลนี้ออกจากระบบ
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button>
        <?php echo anchor('speaker/delete/'.$speaker->speaker_id, '<i class="glyphicon glyphicon-trash"></i> ลบข้อมูล', array('class'=>'btn btn-danger')); ?>
      </div>
    </div>
  </div>
</div>
