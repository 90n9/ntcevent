<header class="page-header">
<h2>ผู้สอน</h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span>ผู้สอน</span></li>
    </ol>
</div>
</header>
<div class="search-content">
    <div class="search-control-wrapper">
        <?php echo form_open('speaker/filter', array('role' => 'form', 'class' => 'form-inline', 'id' => 'searchForm')); ?>
        <div class="form-group">
            <input type="text" class="form-control" name="txtSearchVal" id="txtSearchVal" placeholder="ข้อความที่ต้องการค้นหา" />
        </div>
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-filter"></i> ค้นหา</button>
        <?php echo form_close(); ?>
    </div>
    <div class="tab-content pt-lg">
        <div class="pb-lg">
            <?php echo anchor('speaker/form/0', '<i class="fa fa-plus"></i> เพิ่มผู้สอน', array('class'=>'btn btn-success')); ?>
        </div>
        <div id="result">
        </div>
        <br />
    </div>
</div>