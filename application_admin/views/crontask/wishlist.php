<?php
$lang = 'english';
list($startDate, $startTime) = explode(' ', $course->course_start);
?>
<span style="text-align: justify;font-size: 10pt; font-family: tahoma, arial, helvetica, sans-serif;color: #333333;"><br />
  <img src="<?php echo base_url('uploads/'.$course->cover_image); ?>" alt="<?php echo $course->course_name; ?>" width="650" height="227" />
</span>
<table style="width: 650px;" border="0" cellspacing="0">
  <tbody>
    <tr bgcolor="#FFFFFF">
      <td width="650" height="74">
        <p style="text-align: justify;">
          <span style="font-size: 10pt; font-family: tahoma, arial, helvetica, sans-serif;color: #333333;"><br />
            Dear <?php echo $member->firstname; ?>,<br /> <br />
          Expand your possibilities – Start by registering <span style="color: #ff3300;">"<?php echo $course->course_name; ?>"</span>. The training is running on <span style="color: #ff3300;"><?php echo $this->DateTimeService->displayDate($startDate, $lang); ?></span> and registration will be closed in 7 days. Hope to see you there.</span>
        </p>
      </td>
    </tr>
  </tbody>
</table>
<br />
<table style="width: 650px;" border="0" cellspacing="0">
  <tbody>
    <tr bgcolor="#FFFFFF">
      <td width="650" height="74">
        <table style="width: 650px;" border="0" cellspacing="0">
          <tbody>
            <tr>
            <td width="227" height="37"><span style="font-family: tahoma, arial, helvetica, sans-serif;">&nbsp;</span></td>
            <td style="text-align: center;" bgcolor="#f54700" width="169"><span style="color: #ffffff; font-size: 10pt; font-family: tahoma, arial, helvetica, sans-serif;"><strong>
              <?php echo anchor($courseUrl, 'GO TO COURSE DETAIL', array('target'=>'_blank', 'style'=>'color:#fff;text-decoration:none;')); ?>
              </strong></span></td>
            <td width="248"><span style="font-family: tahoma, arial, helvetica, sans-serif;">&nbsp;</span></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
<br />
<table style="width: 650px;" border="0" cellspacing="0">
  <tbody>
    <tr bgcolor="#FFFFFF">
      <td width="650" height="146">
        <p style="text-align: left;"><span style="font-size: 10pt; font-family: tahoma, arial, helvetica, sans-serif;"><span style="color: #333333;">
          If you have any questions please contact<br />
          <span style="text-decoration: underline;"><span class="style1" style="color: #0000ff; text-decoration: underline;">support@trainingcenter.co.th</span></span> or 02 643 7993-4<br /> <br />
          Thank you and see you at the event!<br /><br />
          Cheers!<br />
          The NTC Team </span></span></p>
      </td>
    </tr>
  </tbody>
</table>
