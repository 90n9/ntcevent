<?php
$lang = 'english';
list($startDate, $startTime) = explode(' ', $course->course_start);
?>
<span style="text-align: justify;font-size: 10pt; font-family: tahoma, arial, helvetica, sans-serif;color: #333333;"><br />
  <img src="<?php echo base_url('uploads/'.$course->cover_image); ?>" alt="<?php echo $course->course_name; ?>" width="650" height="227" />
</span>
<table style="width: 650px;" border="0" cellspacing="0">
  <tbody>
    <tr bgcolor="#FFFFFF">
      <td width="650" height="74">
        <p style="text-align: justify;">
          <span style="font-size: 10pt; font-family: tahoma, arial, helvetica, sans-serif;color: #333333;"><br />
            Dear <?php echo $courseBuyer->buyer_firstname; ?>,<br /> <br />
            This is an automatic gentle reminder that your registration for <span style="color:#ff5400;">"<?php echo $course->course_name; ?>"</span> will be expired in 3 days. Please make a payment and submit your pay-in slip. Please find the order information below.
          </span>
        </p>
      </td>
    </tr>
  </tbody>
</table>
<div style="padding-top:20px;padding-bottom:10px;color:#ff5400;font-size: 10pt; font-family: tahoma, arial, helvetica, sans-serif;">Payment Information</div>
<table cellpadding="0" cellspacing="0" border="0">
  <tr>
    <td valign="top" style="v-align:top;font-size: 10pt; font-family: tahoma, arial, helvetica, sans-serif;">Grand Total:</td>
    <td style="font-size: 10pt; font-family: tahoma, arial, helvetica, sans-serif;"><?php echo number_format($courseBuyer->paid_amount, 2); ?> Baht</td>
  </tr>
  <tr>
    <td valign="top" style="v-align:top;font-size: 10pt; font-family: tahoma, arial, helvetica, sans-serif;">Bank Name:</td>
    <td style="font-size: 10pt; font-family: tahoma, arial, helvetica, sans-serif;">Kasikorn Bank</td>
  </tr>
  <tr>
    <td valign="top" style="v-align:top;font-size: 10pt; font-family: tahoma, arial, helvetica, sans-serif;">A/C Name:</td>
    <td style="font-size: 10pt; font-family: tahoma, arial, helvetica, sans-serif;">Network Training Center Co.,Ltd.</td>
  </tr>
  <tr>
    <td valign="top" style="v-align:top;font-size: 10pt; font-family: tahoma, arial, helvetica, sans-serif;">A/C Number:&nbsp;&nbsp;&nbsp;</td>
    <td style="font-size: 10pt; font-family: tahoma, arial, helvetica, sans-serif;">001-1-30904-6</td>
  </tr>
</table>
<p>Please make a payment within 7 days and submit your pay-in slip. Your registration is not completed until your payment has been received and approved.</p>
<br />
<table style="width: 650px;" border="0" cellspacing="0">
  <tbody>
    <tr bgcolor="#FFFFFF">
      <td width="650" height="74">
        <table style="width: 650px;" border="0" cellspacing="0">
          <tbody>
            <tr>
            <td width="227" height="37"><span style="font-family: tahoma, arial, helvetica, sans-serif;">&nbsp;</span></td>
            <td style="text-align: center;" bgcolor="#f54700" width="169"><span style="color: #ffffff; font-size: 10pt; font-family: tahoma, arial, helvetica, sans-serif;"><strong>
              <?php echo anchor(base_url('index.php/payment/bymail/bank/'.$courseBuyerCodeHash), 'SUBMIT PAY-IN SLIP', array('target'=>'_blank', 'style'=>'color:#fff;text-decoration:none;')); ?>
              </strong></span></td>
            <td width="248"><span style="font-family: tahoma, arial, helvetica, sans-serif;">&nbsp;</span></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
<table style="width: 650px;" border="0" cellspacing="0">
  <tbody>
    <tr bgcolor="#FFFFFF">
      <td width="650" height="74">
        <table style="width: 650px;" border="0" cellspacing="0">
          <tbody>
            <tr>
            <td width="227" height="37"><span style="font-family: tahoma, arial, helvetica, sans-serif;">&nbsp;</span></td>
            <td style="text-align: center;" width="169"><span style="font-family: tahoma, arial, helvetica, sans-serif;">OR</span></td>
            <td width="248"><span style="font-family: tahoma, arial, helvetica, sans-serif;">&nbsp;</span></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
<table style="width: 650px;" border="0" cellspacing="0">
  <tbody>
    <tr bgcolor="#FFFFFF">
      <td width="650" height="74">
        <table style="width: 650px;" border="0" cellspacing="0">
          <tbody>
            <tr>
            <td width="227" height="37"><span style="font-family: tahoma, arial, helvetica, sans-serif;">&nbsp;</span></td>
            <td style="text-align: center;" bgcolor="#f54700" width="169"><span style="color: #ffffff; font-size: 10pt; font-family: tahoma, arial, helvetica, sans-serif;"><strong>
              <?php echo anchor(base_url('index.php/payment/bymail/creditcard/'.$courseBuyerCodeHash), 'PAYMENT WITH CREDIT CARD / DEBIT CARD', array('target'=>'_blank', 'style'=>'color:#fff;text-decoration:none;')); ?>
              </strong></span></td>
            <td width="248"><span style="font-family: tahoma, arial, helvetica, sans-serif;">&nbsp;</span></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
<br />
<table style="width: 650px;" border="0" cellspacing="0">
  <tbody>
    <tr bgcolor="#FFFFFF">
      <td width="650" height="146">
        <p style="text-align: left;"><span style="font-size: 10pt; font-family: tahoma, arial, helvetica, sans-serif;"><span style="color: #333333;">
          If you have any questions please contact<br />
          <span style="text-decoration: underline;"><span class="style1" style="color: #0000ff; text-decoration: underline;">support@trainingcenter.co.th</span></span> or 02 643 7993-4<br /> <br />
          Thank you and see you at the event!<br /><br />
          Cheers!<br />
          The NTC Team </span></span></p>
      </td>
    </tr>
  </tbody>
</table>
