<?php
$lang = 'english';
list($startDate, $startTime) = explode(' ', $course->course_start);
?>
<span style="text-align: justify;font-size: 10pt; font-family: tahoma, arial, helvetica, sans-serif;color: #333333;"><br />
  <img src="<?php echo base_url('uploads/'.$course->cover_image); ?>" alt="<?php echo $course->course_name; ?>" width="650" height="227" />
</span>
<table style="width: 650px;" border="0" cellspacing="0">
  <tbody>
    <tr bgcolor="#FFFFFF">
      <td width="650" height="74">
        <p style="text-align: justify;">
          <span style="font-size: 10pt; font-family: tahoma, arial, helvetica, sans-serif;color: #333333;"><br />
            Dear <?php echo $courseBuyer->buyer_firstname; ?>,<br /> <br />
          <span style="color: #ff3300;">"<?php echo $course->course_name; ?>"</span> training is expired. Your order has been cancelled automatically. If you would like to re-order or visit our website, please follow the link below.</span>
        </p>
      </td>
    </tr>
  </tbody>
</table>
<br />
<table style="width: 650px;" border="0" cellspacing="0">
  <tbody>
    <tr bgcolor="#FFFFFF">
      <td width="650" height="74">
        <table style="width: 650px;" border="0" cellspacing="0">
          <tbody>
            <tr>
            <td width="227" height="37"><span style="font-family: tahoma, arial, helvetica, sans-serif;">&nbsp;</span></td>
            <td style="text-align: center;" bgcolor="#f54700" width="169"><span style="color: #ffffff; font-size: 10pt; font-family: tahoma, arial, helvetica, sans-serif;"><strong>
              <?php echo anchor(base_url(), 'GO TO NTC WEBSITE', array('target'=>'_blank', 'style'=>'color:#fff;text-decoration:none;')); ?>
              </strong></span></td>
            <td width="248"><span style="font-family: tahoma, arial, helvetica, sans-serif;">&nbsp;</span></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
<br />
<table style="width: 650px;" border="0" cellspacing="0">
  <tbody>
    <tr bgcolor="#FFFFFF">
      <td width="650" height="146">
        <p style="text-align: left;"><span style="font-size: 10pt; font-family: tahoma, arial, helvetica, sans-serif;"><span style="color: #333333;">
          If you have any questions please contact<br />
          <span style="text-decoration: underline;"><span class="style1" style="color: #0000ff; text-decoration: underline;">support@trainingcenter.co.th</span></span> or 02 643 7993-4<br /> <br />
          Thank you and see you at the event!<br /><br />
          Cheers!<br />
          The NTC Team </span></span></p>
      </td>
    </tr>
  </tbody>
</table>
