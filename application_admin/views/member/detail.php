<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span><?php echo anchor('member','สมาชิกเวบไซต์'); ?></span></li>
        <li><span><?php echo $title; ?></span></li>
    </ol>
</div>
</header>
<div class="search-content" style="background:#ffffff;">
    <div class="search-toolbar">
        <ul class="list-unstyled nav nav-pills">
            <li class="active">
                <a href="#member" data-toggle="tab" aria-expanded="true">ข้อมูลสมาชิก</a>
            </li>
            <li>
                <a href="#wishlist" data-toggle="tab" aria-expanded="false">Wishlist <span class="badge"><?php echo $wishlistList->num_rows(); ?></span></a>
            </li>
        </ul>
    </div>
    <div class="tab-content">
        <div id="member" class="tab-pane active">
            <?php $this->load->view('member/detail/member'); ?>
        </div>
        <div id="wishlist" class="tab-pane">
            <?php $this->load->view('member/detail/wishlist'); ?>
        </div>
    </div>
</div>