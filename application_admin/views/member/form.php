<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span><?php echo anchor('member','สมาชิกเวบไซต์'); ?></span></li>
    <li><span><?php echo $title; ?></span></li>
  </ol>
</div>
</header>
<?php echo form_open('member/form_post/'.$memberId, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'member-form')); ?>
<div class="panel panel-default">
  <div class="panel-heading">
  ข้อมูลระบบ
  </div>
  <div class="panel-body">
    <?php if($memberId == 0){ ?>
    <div class="form-group">
      <label for="txtPassword" class="col-md-3 control-label">Password</label>
      <div class="col-md-6">
        <input type="password" class="form-control" name="txtPassword" id="txtPassword" placeholder="" maxlength="20">
      </div>
    </div>
    <div class="form-group">
      <label for="txtConfirmPassword" class="col-md-3 control-label">Confirm Password</label>
      <div class="col-md-6">
        <input type="password" class="form-control" name="txtConfirmPassword" id="txtConfirmPassword" placeholder="" maxlength="20">
      </div>
    </div>
    <?php } ?>
    <div class="form-group">
      <label class="col-md-3 control-label">Member Status</label>
      <div class="col-md-6">
        <div class="radio-inline">
          <label>
            <input type="radio" name="rdoStatus" value="1" <?php echo ($memberId == 0 || $member->enable_status == 1)?'checked="checked"':''; ?>/> Active
          </label>
        </div>
        <div class="radio-inline">
          <label>
            <input type="radio" name="rdoStatus" value="0" <?php echo ($memberId != 0 && $member->enable_status == 0)?'checked="checked"':''; ?>/> Block
          </label>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">Verify Email</label>
      <div class="col-md-6">
        <div class="radio-inline">
          <label>
            <input type="radio" name="rdoVerify" value="true" <?php echo ($memberId == 0 || $member->is_verify == 'true')?'checked="checked"':''; ?>/> verify
          </label>
        </div>
        <div class="radio-inline">
          <label>
            <input type="radio" name="rdoVerify" value="false" <?php echo ($memberId != 0 && $member->is_verify == 'false')?'checked="checked"':''; ?>/> not verify
          </label>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="panel panel-default" id="editProfile">
    <div class="panel-heading">
    ข้อมูลผู้ใช้งาน
    </div>
    <div class="panel-body">
        <div class="form-group">
            <label for="txtFirstname" class="col-md-3 control-label">ชื่อ</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="txtFirstname" id="txtFirstname" value="<?php echo ($memberId != 0)?$member->firstname:''; ?>" />
            </div>
          </div>
          <div class="form-group">
            <label for="txtLastname" class="col-md-3 control-label">นามสกุล</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="txtLastname" id="txtLastname" value="<?php echo ($memberId != 0)?$member->lastname:''; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label for="txtEmail" class="col-md-3 control-label">อีเมล</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="txtEmail" id="txtEmail" value="<?php echo ($memberId != 0)?$member->email:''; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label for="txtMobile" class="col-md-3 control-label">เบอร์มือถือ</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="txtMobile" id="txtMobile" value="<?php echo ($memberId != 0)?$member->mobile:''; ?>" />
            </div>
        </div>
        <div class="form-group">
            <label for="txtCompany" class="col-md-3 control-label">บริษัท</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="txtCompany" id="txtCompany" value="<?php echo ($memberId != 0)?$member->company:''; ?>" />
            </div>
        </div>
      </div>
    </div>
<div class="panel panel-default">
  <div class="panel-heading">ข้อมูลการชำระเงิน</div>
  <div class="panel-body">
    <div class="form-group">
      <label class="col-md-3 control-label">ประเภทการชำระเงิน</label>
      <div class="col-md-6">
        <div class="radio-inline">
          <label>
            <input type="radio" name="rdoCompany" value="1" <?php echo ($memberId == 0 || $member->is_company == '1')?'checked="checked"':''; ?>/> ในนามบริษัท
          </label>
        </div>
        <div class="radio-inline">
          <label>
            <input type="radio" name="rdoCompany" value="0" <?php echo ($memberId != 0 && $member->is_company == '0')?'checked="checked"':''; ?>/> ในนามบุคคล
          </label>
        </div>
      </div>
    </div>
    <div id="company-payment-form-group">
      <div class="form-group">
        <label for="txtCompanyName" class="col-md-3 control-label">ชื่อบริษัท</label>
        <div class="col-md-6">
          <input type="text" class="form-control" name="txtCompanyName" id="txtCompanyName" value="<?php echo($memberId != 0)?$member->company_name:''; ?>" />
        </div>
      </div>
      <div class="form-group">
        <label for="txtCompanyBranch" class="col-md-3 control-label">สาขา</label>
        <div class="col-md-6">
          <input type="text" class="form-control" name="txtCompanyBranch" id="txtCompanyBranch" value="<?php echo($memberId != 0)?$member->company_branch:''; ?>" />
        </div>
      </div>
      <div class="form-group">
        <label for="txtCompanyTax" class="col-md-3 control-label">เลขประจำตัวผู้เสียภาษี</label>
        <div class="col-md-6">
          <input type="text" class="form-control" name="txtCompanyTax" id="txtCompanyTax" value="<?php echo($memberId != 0)?$member->company_tax:''; ?>" />
        </div>
      </div>
    </div>
    <div class="form-group">
      <label for="txtCompanyAddress" class="col-md-3 control-label">ที่อยู่บริษัท</label>
      <div class="col-md-6">
        <textarea class="form-control" name="txtCompanyAddress" id="txtCompanyAddress"><?php echo ($memberId != 0)?$member->company_address:''; ?></textarea>
      </div>
    </div>
  </div>
  <div class="panel-footer">
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button> <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
      </div>
    </div>
  </div>
</div>
<?php echo form_close(); ?>
