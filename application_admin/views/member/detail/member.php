<div class="btn-group hide-print" style="padding-bottom:10px;">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    การกระทำ <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><?php echo anchor('member', '<i class="glyphicon glyphicon-arrow-left"></i> กลับไปหน้ารายการ'); ?></li>
    <li><?php echo anchor('member/form/', '<i class="glyphicon glyphicon-plus"></i> สร้างสมาชิกเวบไซต์'); ?></li>
    <li><?php echo anchor('member/form/'.$member->member_id, '<i class="glyphicon glyphicon-pencil"></i> แก้ไขข้อมูล'); ?></li>
    <li><a href="#" data-toggle="modal" data-target="#passwordModal"><i class="glyphicon glyphicon-pencil"></i> เปลี่ยนรหัสผ่าน</a></li>
    <li class="divider"></li>
    <li><a href="#" data-toggle="modal" data-target="#deleteModal"><i class="glyphicon glyphicon-trash"></i> ลบข้อมูล</a></li>
  </ul>
</div>
<div class="panel panel-default">
    <div class="panel-body form-horizontal">
        <div class="form-group">
            <label class="col-md-3 control-label">ชื่อ</label>
            <div class="col-md-6">
                <p class="form-control-static"><?php echo $member->firstname; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">นามสกุล</label>
            <div class="col-md-6">
                <p class="form-control-static"><?php echo $member->lastname; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">อีเมล</label>
            <div class="col-md-6">
                <p class="form-control-static"><?php echo $member->email; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">เบอร์มือถือ</label>
            <div class="col-md-6">
                <p class="form-control-static"><?php echo $member->mobile; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">บริษัท</label>
            <div class="col-md-6">
                <p class="form-control-static"><?php echo $member->company; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Member Status</label>
            <div class="col-md-6">
                <p class="form-control-static"><?php echo ($member->enable_status == 1)?'<span class="label label-success">active</span>':'<span class="label label-danger">block</span>'; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Verify Email</label>
            <div class="col-md-6">
                <p class="form-control-static"><?php echo ($member->is_verify == 'true')?'<span class="label label-success">verify</span>':'<span class="label label-danger">not verify</span>'; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">ประเภทการชำระเงิน</label>
            <div class="col-md-6">
                <p class="form-control-static"><?php echo ($member->is_company == '1')?'<span class="label label-primary">ในนามบริษัท</span>':'<span class="label label-default">ในนามบุคคล</span>'; ?></p>
            </div>
        </div>
        <?php if($member->is_company == 1){ ?>
          <div class="form-group">
            <label class="col-md-3 control-label">ชื่อบริษัท</label>
            <div class="col-md-6">
              <p class="form-control-static"><?php echo $member->company_name; ?></p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label">สาขา</label>
            <div class="col-md-6">
              <p class="form-control-static"><?php echo $member->company_branch; ?></p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label">เลขประจำตัวผู้เสียภาษี</label>
            <div class="col-md-6">
              <p class="form-control-static"><?php echo $member->company_tax; ?></p>
            </div>
          </div>
        <?php } ?>
        <div class="form-group">
          <label class="col-md-3 control-label">ที่อยู่บริษัท</label>
          <div class="col-md-6">
            <p class="form-control-static"><?php echo $member->company_address; ?></p>
          </div>
        </div>
        <div class="row form-group">
            <label class="col-md-3 col-print-3 control-label">สร้างวันที่</label>
            <div class="col-md-3 col-print-3 xs-margin-bottom">
                <p class="form-control-static"><?php echo $this->DateTimeService->displayDateTime($member->create_date); ?></p>
            </div>
            <label class="col-md-3 col-print-3 control-label">โดย</label>
            <div class="col-md-3 col-print-3">
                <p class="form-control-static"><?php echo $this->AdminModel->getLoginNameById($member->create_by); ?></p>
            </div>
        </div>
        <?php if($member->update_by > 0){?>
        <div class="row form-group">
            <label class="col-md-3 col-print-3 control-label">ปรับปรุงวันที่</label>
            <div class="col-md-3 col-print-3 xs-margin-bottom">
                <p class="form-control-static"><?php echo $this->DateTimeService->displayDateTime($member->update_date); ?></p>
            </div>
            <label class="col-md-3 col-print-3 control-label">โดย</label>
            <div class="col-md-3 col-print-3">
                <p class="form-control-static"><?php echo $this->AdminModel->getLoginNameById($member->update_by); ?></p>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="deleteModalLabel">ลบข้อมูล</h4>
      </div>
      <div class="modal-body">
        กรุณากดปุ่ม "ลบข้อมูล" เพื่อยืนยันการลบข้อมูลนี้ออกจากระบบ
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button>
        <?php echo anchor('member/delete/'.$member->member_id, '<i class="glyphicon glyphicon-trash"></i> ลบข้อมูล', array('class'=>'btn btn-danger')); ?>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="passwordModal" tabindex="-1" role="dialog" aria-labelledby="passwordModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="passwordModalLabel">เปลี่ยนรหัสผ่าน</h4>
      </div>
      <?php echo form_open('member/form_change_password/'.$member->member_id, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'password-form')); ?>
      <div class="modal-body">
        <div class="form-group">
            <label for="txtPassword" class="col-md-3 control-label">Password</label>
            <div class="col-sm-6">
                <input type="password" class="form-control" name="txtPassword" id="txtPassword" placeholder="" maxlength="20">
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button>
        <button type="submit" class="btn btn-primary">เปลี่ยนรหัสผ่าน</button>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>
