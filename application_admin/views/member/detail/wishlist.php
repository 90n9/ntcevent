<div class="table-responsive">
  <table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>วันที่ wishlist</th>
            <th>ชื่อคอร์ส</th>
            <th>ในหมวด</th>
            <th>ภาษา</th>
            <th>สถานะ</th>
            <th>วันที่สร้าง</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach($wishlistList->result() as $wishlist){
            $course = $this->CourseModel->getData($wishlist->course_id);
            $detailUrl = 'course/detail/'.$course->course_id;
            ?>
            <tr>
                <td>
                    <?php echo $wishlist->create_date; ?>
                </td>
                <td>
                    <?php echo anchor($detailUrl, $course->course_name); ?>
                </td>
                <td>
                    <?php echo anchor($detailUrl, $this->CourseCategoryModel->getName($course->course_category_id)); ?>
                </td>
                <td>
                    <?php echo anchor($detailUrl, $course->course_lang); ?>
                </td>
                <td>
                    <?php echo anchor($detailUrl, ($course->enable_status == 1)?'แสดง':'ไม่แสดง'); ?>
                </td>
                <td>
                    <?php echo anchor($detailUrl, $this->DateTimeService->displayDateTime($course->create_date)); ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
  </table>
</div>
