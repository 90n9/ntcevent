<?php
if($totalTransaction == 0){
    echo '<div class="alert alert-warning" role="alert">ไม่พบรายการตามที่ท่านค้นหา</div>';
}else{
    $totalPage = ceil($totalTransaction/$perPage);
    $pagerData = array();
    $pagerData['totalTransaction'] = $totalTransaction;
    $pagerData['searchVal'] = $searchVal;
    $pagerData['enableStatus'] = $enableStatus;
    $pagerData['perPage'] = $perPage;
    $pagerData['page'] = $page;
    $pagerData['totalPage'] = $totalPage;
    $pagerData['action'] = site_url('member/filter');
?>
<div class="form-group">
    <?php $this->load->view('member/listpager', $pagerData); ?>
</div>
<div class="table-responsive">
  <table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>ชื่อ</th>
            <th>อีเมล</th>
            <th>สถานะ</th>
            <th>วันที่สร้าง</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach($memberList->result() as $member){
            $detailUrl = 'member/detail/'.$member->member_id;
            ?>
            <tr>
                <td>
                    <?php echo anchor($detailUrl, ($member->firstname != '')?$member->firstname.' '.$member->lastname:'-'); ?>
                </td>
                <td>
                    <?php echo anchor($detailUrl, ($member->email != '')?$member->email:'-'); ?>
                </td>
                <td>
                  <?php echo anchor($detailUrl, ($member->enable_status == 1)?'<span style="label label-success">Active</span>':'<span style="label label-danger">Block</span>'); ?>
                </td>
                <td>
                    <?php echo anchor($detailUrl, $this->DateTimeService->displayOnlyDate($member->create_date)); ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
  </table>
</div>
<div class="form-group">
    <?php $this->load->view('member/listpager', $pagerData); ?>
</div>
<?php
}
