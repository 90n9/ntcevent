<?php
class Offer extends CI_Controller{
  function __construct(){
    parent::__construct();
    $this->LoginService->mustLogin();
    $this->load->model(array('offer/Offer_model'));
  }
  public function index(){
    $content = $this->load->view('offer/list', array(
      'offer_list' => $this->Offer_model->get_list()
    ), TRUE);
    $this->MasterpageService->display($content, 'offer', 'offer');
  }
  public function form($offer_id = 0){
    $title = (($offer_id == 0)?'เพิ่ม':'แก้ไข').' Offer';
    $content = $this->load->view('offer/form', array(
      'offer_id' => $offer_id,
      'offer' => $this->Offer_model->get_data($offer_id),
      'title' => $title
    ), TRUE);
    $this->MasterpageService->addCss('assets_admin/vendor/summernote/summernote.css');
    $this->MasterpageService->addCss('assets_admin/vendor/summernote/summernote.css');
    $this->MasterpageService->addJs('assets_admin/vendor/summernote/summernote.min.js');
    $this->MasterpageService->addJs('assets_admin/js/offer/form.js?v=3');
    $this->MasterpageService->display($content, $title, 'offer');
  }
  public function form_post($offer_id){
    $data = array(
      'offer_content_th' => $this->input->post('offer_content_th'),
      'offer_content_en' => $this->input->post('offer_content_en'),
      'offer_image' => '',
      'enable_status' => $this->input->post('enable_status'),
      'sort_priority' => $this->input->post('sort_priority')
    );
    $this->load->model(array('UploadImageModel'));
    $uploadData = $this->UploadImageModel->uploadSingleImage('offer', 'offer_image');
    if($uploadData['success']){
      $data['offer_image'] = $uploadData['imageFile'];
    }
    if($offer_id == 0){
      $offer_id = $this->Offer_model->insert($data);
    }else{
      $this->Offer_model->update($offer_id, $data);
    }
    redirect('offer/detail/'.$offer_id);
  }
  public function detail($offer_id){
    $title = 'รายละเอียด Offer';
    $content = $this->load->view('offer/detail', array(
      'offer_id' => $offer_id,
      'offer' => $this->Offer_model->get_data($offer_id),
      'title' => $title
    ), TRUE);
    $this->MasterpageService->display($content, $title, 'offer');
  }
  public function delete($offer_id){
    $this->Offer_model->delete($offer_id);
    redirect('offer');
  }
  public function upload_image(){
    $contentImage = '';
    $this->load->model('UploadImageModel');
    $uploadImage = $this->UploadImageModel->uploadImage('offer_content', 'file');
    if($uploadImage['success']){
      $contentImage = $uploadImage['imageLarge'];
    }
    echo base_url('uploads/'.$contentImage);
  }
}
