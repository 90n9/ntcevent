<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model(array('course/CourseModel', 'coursecategory/CourseCategoryModel', 'course/DiscountModel', 'course/TicketModel', 'course/DocumentModel', 'coursebuyer/CourseBuyerModel', 'coursebuyer/CourseRegisterModel', 'course/DiscountTicketModel', 'speaker/SpeakerModel', 'courselocation/CourseLocationModel', 'course/SpeakerTypeModel', 'course/CourseFullStatusModel','wishlist/WishlistModel', 'member/MemberModel'));
    }
    public function index() {
        $this->LoginService->mustLogin();
        $dataContent = array();
        $content = $this->load->view('course/list', $dataContent, TRUE);
        $this->MasterpageService->addJs('/assets_admin/js/course/list.js?v=1');
        $this->MasterpageService->display($content, 'รายการอบรม', 'course');
    }
    public function filter(){
        $this->LoginService->mustLogin('js');
        $searchVal = $this->input->post('searchVal');
        $courseCategoryId = $this->input->post('courseCategoryId');
        $enableStatus = $this->input->post('enableStatus');
        $startDate = $this->input->post('startDate');
        $endDate = $this->input->post('endDate');
        $page = $this->input->post('page');
        $this->load->model('course/CourseFilterService');
        $data = $this->CourseFilterService->getDataContent(
            $searchVal
            , $courseCategoryId
            , $enableStatus
            , $startDate
            , $endDate
            , $page
            , 50);
        $this->load->view('course/listcontent', $data);
    }
    public function detail($courseId){
        $this->LoginService->mustLogin();
        $course = $this->CourseModel->getData($courseId);
        $dataContent = array();
        $dataContent['courseId'] = $courseId;
        $dataContent['course'] = $course;
        $dataContent['ticket'] = $this->TicketModel->getList($courseId)->row();
        $dataContent['discountList'] = $this->DiscountModel->getList($courseId);
        $dataContent['documentList'] = $this->DocumentModel->getList($courseId);
        $dataContent['wishlistList'] = $this->WishlistModel->getListByCourse($courseId);
        $dataContent['courseBuyerList'] = $this->CourseBuyerModel->getListByCourse($courseId);
        $dataContent['courseRegisterList'] = $this->CourseRegisterModel->getListByCourse($courseId);
        $title = 'รายละเอียดคอร์สเรียน';
        $dataContent['title'] = $title;
        $content = $this->load->view('course/detail', $dataContent, TRUE);
        $this->MasterpageService->addJs('assets_admin/js/course/detail.js');
        $this->MasterpageService->display($content, $title, 'course');
    }
    public function form($courseId = 0){
        $this->LoginService->mustLogin();
        $dataContent = array();
        $dataContent['courseId'] = $courseId;
        $dataContent['course'] = $this->CourseModel->getData($courseId);
        $dataContent['ticketId'] = $this->TicketModel->getTicketId($courseId);
        $dataContent['ticket'] = $this->TicketModel->getData($dataContent['ticketId']);
        $title = (($courseId == 0)?'เพิ่ม':'แก้ไข').'คอร์สเรียน';
        $dataContent['title'] = $title;
        $content = $this->load->view('course/form', $dataContent, TRUE);
        $this->MasterpageService->addCss('assets_admin/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css');
        $this->MasterpageService->addJs('assets_admin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js');
        $this->MasterpageService->addCss('assets_admin/vendor/bootstrap-timepicker/css/bootstrap-timepicker.min.css');
        $this->MasterpageService->addJs('assets_admin/vendor/bootstrap-timepicker/bootstrap-timepicker.js');
        $this->MasterpageService->addCss('assets_admin/vendor/summernote/summernote.css');

        $this->MasterpageService->addCss('assets_admin/vendor/summernote/summernote.css');
        $this->MasterpageService->addJs('assets_admin/vendor/summernote/summernote.min.js');
        $this->MasterpageService->addJs('assets_admin/js/course/form.js?v=4');
        $this->MasterpageService->display($content, $title, 'course');
    }

    public function form_post($courseId){
        $this->LoginService->mustLogin();
        $courseId = $this->saveCourse($courseId);
        $this->saveTicket($courseId);
        redirect('course/detail/'.$courseId);
    }
    private function saveCourse($courseId){
      $data = array();
      $data['course_category_id'] = $this->input->post('ddlCourseCategory');
      $data['course_name'] = $this->input->post('txtName');
      $arrCourseDate = explode(",", $this->input->post('txtCourseDate'));
      sort($arrCourseDate);
      $courseDate = implode(",", $arrCourseDate);
      $courseLength = count($arrCourseDate);
      $data['course_date'] = $courseDate;
      $data['course_length'] = $courseLength;
      $data['course_start_time'] = $this->input->post('txtCourseStartTime');
      $data['course_end_time'] = $this->input->post('txtCourseEndTime');
      $data['course_start'] = $arrCourseDate[0].' '.$this->input->post('txtCourseStartTime');
      $data['course_end'] = $arrCourseDate[$courseLength -1].' '.$this->input->post('txtCourseEndTime');
      $data['cover_image'] = '';
      $data['cover_video'] = $this->input->post('txtCoverVideo');
      $data['thumb_image'] = '';
      $data['course_description'] = $this->input->post('txtDescription');
      $data['course_lang'] = $this->input->post('ddlLang');
      $data['course_price_type'] = $this->input->post('ddlTicketPriceType');
      $data['course_price'] = str_replace(',', '', $this->input->post('txtTicketPrice'));
      $data['course_before_discount_price'] = str_replace(',', '', $this->input->post('txtBeforeDiscountPrice'));
      $data['course_location_id'] = $this->input->post('ddlLocation');
      $data['course_full_status'] = $this->input->post('ddlFullStatus');
      $data['speaker_type'] = $this->input->post('ddlSpeakerType');
      $data['speaker_id'] = $this->input->post('ddlSpeaker');
      $data['sort_priority'] = $this->input->post('ddlSortPriority');
      $enableStatus = $this->input->post('chkEnableStatus');
      $data['enable_status'] = ($enableStatus != '')?$enableStatus:'0';
      $this->load->model(array('UploadImageModel'));
      $this->UploadImageModel->setThumb(400,400);
      $uploadData = $this->UploadImageModel->uploadImage('course_thumb', 'fileThumb');
      if($uploadData['success']){
          $data['thumb_image'] = $uploadData['imageSmall'];
      }
      $uploadData = $this->UploadImageModel->uploadSingleImage('course_cover', 'fileCover');
      if($uploadData['success']){
          $data['cover_image'] = $uploadData['imageFile'];
      }
      if($courseId == 0){
          $courseId = $this->CourseModel->insert($data);
      }else{
          $this->CourseModel->update($courseId, $data);
      }
      return $courseId;
    }
    private function saveTicket($courseId){
      $ticketId = $this->TicketModel->getTicketId($courseId);
      $data = array();
      $data['ticket_name'] = $this->input->post('txtTicketName');
      $data['ticket_qty'] = $this->input->post('txtTicketQty');
      $data['ticket_description'] = $this->input->post('txtTicketDescription');
      $isShowDescription = $this->input->post('chkTicketShowDescription');
      $data['is_show_description'] = ($isShowDescription != '')?$isShowDescription:'0';
      $data['price_type'] = $this->input->post('ddlTicketPriceType');
      $data['price'] = str_replace(',', '', $this->input->post('txtTicketPrice'));
      $data['start_date'] = $this->input->post('txtTicketStartDate');
      $data['end_date'] = $this->input->post('txtTicketEndDate');
      $data['sort_priority'] = 1;
      $enableStatus = $this->input->post('chkTicketEnableStatus');
      $data['enable_status'] = ($enableStatus != '')?$enableStatus:'0';
      if($ticketId == 0){
          $ticketId = $this->TicketModel->insert($courseId, $data);
      }else{
          $this->TicketModel->update($ticketId, $data);
      }
      return $ticketId;
    }
    public function delete($courseId){
        $this->LoginService->mustLogin();
        $this->CourseModel->delete($courseId);
        redirect('course/');
    }
    public function discount($courseId, $discountId = 0){
        $this->LoginService->mustLogin();
        $dataContent = array();
        $dataContent['courseId'] = $courseId;
        $dataContent['discountId'] = $discountId;
        $dataContent['discount'] = $this->DiscountModel->getData($discountId);
        $title = (($discountId == 0)?'เพิ่ม':'แก้ไข').'ส่วนลด';
        $dataContent['title'] = $title;
        $content = $this->load->view('course/discount', $dataContent, TRUE);
        $this->MasterpageService->addCss('assets_admin/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css');
        $this->MasterpageService->addJs('assets_admin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js');
        $this->MasterpageService->addCss('assets_admin/vendor/bootstrap-timepicker/css/bootstrap-timepicker.min.css');
        $this->MasterpageService->addJs('assets_admin/vendor/bootstrap-timepicker/bootstrap-timepicker.js');
        $this->MasterpageService->addJs('assets_admin/js/course/discount.js?v=1');
        $this->MasterpageService->display($content, $title, 'course');
    }
    public function discount_post($courseId, $discountId){
        $this->LoginService->mustLogin();
        $data = array();
        $data['discount_code'] = $this->input->post('txtCode');
        $data['discount_qty'] = $this->input->post('txtQty');
        $data['discount_type'] = $this->input->post('ddlDiscountType');
        $data['discount_amount'] = $this->input->post('txtAmount');
        $data['available_from'] = $this->input->post('txtAvailableFromDate').' '.$this->input->post('txtAvailableFromTime');
        $data['available_to'] = $this->input->post('txtAvailableToDate').' '.$this->input->post('txtAvailableToTime');
        $enableStatus = $this->input->post('chkEnableStatus');
        $data['min_ticket'] = $this->input->post('txtMinTicket');
        $data['max_ticket'] = $this->input->post('txtMaxTicket');
        $data['enable_status'] = ($enableStatus != '')?$enableStatus:'0';
        if($discountId == 0){
            $discountId = $this->DiscountModel->insert($courseId, $data);
        }else{
            $this->DiscountModel->update($discountId, $data);
        }
        redirect('course/detail/'.$courseId);
    }
    public function discount_delete($discountId){
        $discount = $this->DiscountModel->getData($discountId);
        if($discount){
            $courseId = $discount->course_id;
            $this->DiscountModel->delete($discountId);
            redirect('course/detail/'.$courseId);
        }
        redirect('course');
    }
    public function uploadImage(){
        $contentImage = '';
        $this->load->model('UploadImageModel');
        $uploadImage = $this->UploadImageModel->uploadImage('course_image', 'file');
        if($uploadImage['success']){
            $contentImage = $uploadImage['imageLarge'];
        }
        echo base_url('uploads/'.$contentImage);
    }
    public function document($courseId, $documentId = 0){
        $this->LoginService->mustLogin();
        $dataContent = array();
        $dataContent['courseId'] = $courseId;
        $dataContent['documentId'] = $documentId;
        $dataContent['document'] = $this->DocumentModel->getData($documentId);
        $title = (($documentId == 0)?'เพิ่ม':'แก้ไข').'เอกสาร';
        $dataContent['title'] = $title;
        $content = $this->load->view('course/document', $dataContent, TRUE);
        $this->MasterpageService->addJs('assets_admin/js/course/document.js');
        $this->MasterpageService->display($content, $title, 'course');
    }
    public function document_post($courseId, $documentId){
        $this->LoginService->mustLogin();
        $data = array();
        $data['document_name'] = $this->input->post('txtName');
        $data['document_file'] = $this->input->post('txtFile');
        $this->load->model(array('UploadFileModel'));
        $uploadData = $this->UploadFileModel->upload('course_document_'.$courseId, 'file');
        if($uploadData['success']){
            $data['document_file'] = $uploadData['file_name'];
        }
        $data['sort_priority'] = $this->input->post('ddlSortPriority');
        $enableStatus = $this->input->post('chkEnableStatus');
        $data['enable_status'] = ($enableStatus != '')?$enableStatus:'0';
        if($documentId == 0){
            $documentId = $this->DocumentModel->insert($courseId, $data);
        }else{
            $this->DocumentModel->update($documentId, $data);
        }
        redirect('course/detail/'.$courseId);
    }
    public function document_delete($documentId){
        $this->LoginService->mustLogin();
        $document = $this->DocumentModel->getData($documentId);
        if($document){
            $courseId = $document->course_id;
            $this->DocumentModel->delete($documentId);
            redirect('course/detail/'.$courseId);
        }
        redirect('course');
    }
    public function coursebuyer($courseId, $courseBuyerId){
        $this->LoginService->mustLogin();
        $courseBuyer = $this->CourseBuyerModel->getData($courseBuyerId);
        if(!$courseBuyer){
            redirect('course/detail/'.$courseId);
        }
        $title = 'ข้อมูลการซื้อคอร์ส';
        $course = $this->CourseModel->getData($courseId);
        $member = $this->MemberModel->getData($courseBuyer->member_id);
        $courseRegisterList = $this->CourseRegisterModel->getListByCourseBuyer($courseBuyerId);
        $content = $this->load->view('course/coursebuyer', array(
          'courseId' => $courseId,
          'courseBuyerId' => $courseBuyerId,
          'course' => $course,
          'member' => $member,
          'courseBuyer' => $courseBuyer,
          'courseRegisterList' => $courseRegisterList,
          'title' => $title
        ), TRUE);
        //$this->MasterpageService->addJs('/assets_admin/js/course/coursebuyer.js?v=1');
        $this->MasterpageService->display($content, $title, 'course');
    }
    public function coursebuyer_cancel($courseBuyerId){
      $this->LoginService->mustLogin();
      $courseBuyer = $this->CourseBuyerModel->getData($courseBuyerId);
      if($courseBuyer){
        $courseBuyer = $this->CourseBuyerModel->updateStatus($courseBuyerId, 'cancel');
      }
      redirect('course/coursebuyer/'.$courseBuyer->course_id.'/'.$courseBuyerId);
    }
}
