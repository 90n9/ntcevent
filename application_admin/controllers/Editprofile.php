<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EditProfile extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model(array('admin/AdminModel'));
    }
    public function index() {
        $this->LoginService->mustLogin();
        $dataContent = array();
        $this->session->userdata('admin_id');
        $dataContent['admin'] = $this->AdminModel->getData($this->session->userdata('admin_id'));
        $title = $dataContent['title'] = 'แก้ไขข้อมูลส่วนตัว';
        $content = $this->load->view('editprofile/index', $dataContent, TRUE);
        $this->MasterpageService->addJs('/assets/js/admin/editprofile/index.js');
        $this->MasterpageService->display($content, $title, 'editprofile');
    }

    public function form_post() {
        $this->LoginService->mustLogin();
        $data['name'] = $this->input->post('txtName');
        $data['email'] = $this->input->post('txtEmail');
        $this->AdminModel->editProfile($data);
        $this->LoginService->updateSession();
        redirect('editprofile/success');
    }

    public function success() {
        $this->LoginService->mustLogin();
        $dataContent = array();
        $title = $dataContent['title'] = 'แก้ไขข้อมูลส่วนตัว';
        $content = $this->load->view('editprofile/success', $dataContent, TRUE);
        $this->MasterpageService->addJs('/assets/js/admin/editprofile/success.js');
        $this->MasterpageService->display($content, $title, 'editprofile');
    }
}