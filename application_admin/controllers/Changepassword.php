<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ChangePassword extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model(array('admin/AdminModel'));
    }
    public function index() {
        $this->LoginService->mustLogin();
        $dataContent = array();
        $this->session->userdata('admin_id');
        $dataContent['admin'] = $this->AdminModel->getData($this->session->userdata('admin_id'));
        $title = $dataContent['title'] = 'เปลี่ยนรหัสผ่าน';
        $content = $this->load->view('changepassword/index', $dataContent, TRUE);
        $this->MasterpageService->addJs('/assets/js/admin/changepassword/index.js');
        $this->MasterpageService->display($content, $title, 'changepassword');
    }

    public function form_post() {
        $this->LoginService->mustLogin();
        $password = $this->input->post('txtPassword');
        $this->AdminModel->updatePassword($this->session->userdata('admin_id'),$password);
        $this->LoginService->updateSession();
        redirect('changepassword/success');
    }

    public function success() {
        $this->LoginService->mustLogin();
        $dataContent = array();
        $title = $dataContent['title'] = 'เปลี่ยนรหัสผ่าน';
        $content = $this->load->view('changepassword/success', $dataContent, TRUE);
        $this->MasterpageService->addJs('/assets/js/admin/changepassword/success.js');
        $this->MasterpageService->display($content, $title, 'changepassword');
    }
}