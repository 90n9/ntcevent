<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PriceRange extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model(array('pricerange/PriceRangeModel'));
    }
    public function index(){
        $this->LoginService->mustLogin();
        $data = array();
        $data['priceRangeList'] = $this->PriceRangeModel->getList();
        $content = $this->load->view('pricerange/list', $data, true);
        $this->MasterpageService->addJs('assets_admin/js/pricerange/list.js');
        $this->MasterpageService->display($content, 'Price Range', 'pricerange');
    }
    public function detail($priceRangeId){
        $this->LoginService->mustLogin();
        $this->MasterpageService->addJs('assets_admin/js/pricerange/detail.js');
        $dataContent = array();
        $dataContent['priceRangeId'] = $priceRangeId;
        $dataContent['priceRange'] = $this->PriceRangeModel->getData($priceRangeId);
        $content = $this->load->view('pricerange/detail', $dataContent, TRUE);
        $this->MasterpageService->display($content, 'ช่วงราคา', 'pricerange');
    }
    public function form($priceRangeId = 0){
        $this->LoginService->mustLogin();
        $this->MasterpageService->addJs('assets_admin/js/pricerange/form.js');
        $title = ($priceRangeId == 0)?'เพิ่มช่วงราคา':'แก้ไขช่วงราคา';
        $dataContent = array();
        $dataContent['priceRangeId'] = $priceRangeId;
        $dataContent['priceRange'] = $this->PriceRangeModel->getData($priceRangeId);
        $dataContent['title'] = $title;
        $content = $this->load->view('pricerange/form', $dataContent, TRUE);
        $this->MasterpageService->display($content, $title, 'pricerange');
    }
    public function form_post($priceRangeId){
        $this->LoginService->mustLogin();
        $data = array();
        $data['price_from'] = $this->input->post('txtFrom');
        $data['price_to'] = $this->input->post('txtTo');
        if($priceRangeId == 0){
            $priceRangeId = $this->PriceRangeModel->insert($data);
        }else{
            $this->PriceRangeModel->update($priceRangeId, $data);
        }
        redirect('pricerange');
    }
    public function delete($priceRangeId){
    	$this->PriceRangeModel->delete($priceRangeId);
        redirect('pricerange');
    }
}