<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Course_migration extends CI_Controller {
  public function index() {
    $this->LoginService->mustLogin();
    $this->db->where('is_delete', 0);
    $course_list = $this->db->get('tbl_course');
    foreach($course_list->result() as $course){
      if($course->course_length > 0){
        continue;
      }
      $start_date = '';
      $end_date = '';
      list($start_date, $start_time) = explode(" ", $course->course_start);
      list($end_date, $end_time) = explode(" ", $course->course_end);
      $date_list = $this->convert_date_range_to_list($start_date, $end_date);
      $course_date = implode(",", $date_list);
      $course_length = count($date_list);
      var_dump($course->course_id, $course_date, $course_length, $start_time, $end_time);
      echo '<hr />';
      $this->db->set('course_date', $course_date);
      $this->db->set('course_length', $course_length);
      $this->db->set('course_start_time', $start_time);
      $this->db->set('course_end_time', $end_time);
      $this->db->where('course_id', $course->course_id);
      $this->db->update('tbl_course');
    }
  }
  private function convert_date_range_to_list($str_start_date, $str_end_date){
    $arr_date_list = array();
    $end_date = new DateTime($str_end_date);
    $end_date->modify('+1 day');
    $period = new DatePeriod(
      new DateTime($str_start_date),
      new DateInterval('P1D'),
      $end_date
    );
    foreach ($period as $savedDate) {
      $arr_date_list[] = $savedDate->format('Y-m-d');
    }
    return $arr_date_list;
  }
}
