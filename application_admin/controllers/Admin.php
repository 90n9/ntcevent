<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model(array('admin/AdminModel'));
  }
  public function index(){
    $this->LoginService->mustLogin();
    $this->load->model('admin/AdminFilterService');
    $data = $this->AdminFilterService->getDataContent();
    $content = $this->load->view('admin/list', $data, TRUE);
    $this->MasterpageService->display($content, 'รายการเจ้าหน้าที่', 'admin');
  }
  public function form($adminId = 0){
    $this->LoginService->mustLogin();
    $dataContent = array();
    $dataContent['adminId'] = $adminId;
    $dataContent['admin'] = $this->AdminModel->getData($adminId);
    $title = (($adminId == 0)?'เพิ่ม':'แก้ไข').'ข้อมูลเจ้าหน้าที่';
    $dataContent['title'] = $title;
    $content = $this->load->view('admin/form', $dataContent, TRUE);
    $this->MasterpageService->addJs('assets_admin/js/admin/form.js');
    $this->MasterpageService->display($content, $title, 'admin');
  }
  public function form_post($adminId){
    $this->LoginService->mustLogin();
    $data = array();
    $data['name'] = $this->input->post('txtName');
    $data['email'] = $this->input->post('txtEmail');
    $data['login_name'] = $this->input->post('txtLoginName');
    $enableStatus = $this->input->post('chkEnableStatus');
    $data['enable_status'] = ($enableStatus != '')?$enableStatus:'0';
    if($adminId == 0){
      $data['login_password'] = $this->input->post('txtPassword');
      $adminId = $this->AdminModel->insert($data);
    }else{
      $this->AdminModel->update($adminId, $data);
    }
    redirect('admin/detail/'.$adminId);
  }
  public function detail($adminId){
    $this->LoginService->mustLogin();
    $admin = $this->AdminModel->getData($adminId);
    $dataContent = array();
    $dataContent['adminId'] = $adminId;
    $dataContent['admin'] = $admin;
    $title = 'ข้อมูลเจ้าหน้าที่';
    $dataContent['title'] = $title;
    $content = $this->load->view('admin/detail', $dataContent, TRUE);
    $this->MasterpageService->addJs('assets_admin/js/admin/detail.js');
    $this->MasterpageService->display($content, $title, 'admin');
  }
  public function form_change_password($adminId){
    $this->LoginService->mustLogin();
    $loginPassword = $this->input->post('txtPassword');
    $this->AdminModel->updatePassword($adminId, $loginPassword);
    redirect('admin/detail/'.$adminId);
  }
  public function delete($adminId){
    $this->LoginService->mustLogin();
    $this->AdminModel->delete($adminId);
    redirect('admin/');
  }
}
