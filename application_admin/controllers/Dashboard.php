<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function index(){
		$this->LoginService->mustLogin();
        $content = $this->load->view('dashboard/index', array(), true);
        $this->MasterpageService->display($content, 'Dashboard', 'dashboard');
	}
}
