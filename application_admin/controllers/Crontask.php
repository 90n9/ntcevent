<?php
class Crontask extends CI_Controller{
  public function index(){
    $this->wait_for_payment();
    $this->complete_payment();
    $this->wishlist();
  }
  public function testmail(){
    $this->load->model('SendMailService');
    $mailTo = 'tholop@gmail.com';
    //$content = $this->send_expireorder();
    //$this->SendMailService->send_mail($mailTo, '', '“Big Data Foundation” Registration has been cancelled', $content);
    //$content = $this->send_remindercourse();
    //$this->SendMailService->send_mail($mailTo, '', 'Gentle Reminder “Big Data Foundation” ', $content);
    //$content = $this->send_wishlist();
    //$this->SendMailService->send_mail($mailTo, '', 'Last week to register for “Big Data Foundation” ', $content);
    $content = $this->send_waitforpayment();
    $this->SendMailService->send_mail($mailTo, '', 'We’re waiting for your final payment for “Big Data Foundation” ', $content);

    $content = $this->load->view('mailtemplate/mail_masterpage', array(
      'content' => $content,
      'mail_to' => $mailTo
    ));
  }
  public function wait_for_payment(){
    $this->load->model('crontask/Wait_for_payment_task_model');
    $this->Wait_for_payment_task_model->payment_reminder();
    $this->Wait_for_payment_task_model->expire_7_day();
  }
  private function wishlist(){
    $this->load->model('crontask/Wishlist_task_model');
    $this->Wishlist_task_model->before_7_day();
  }
  private function complete_payment(){
    $this->load->model('crontask/Complete_payment_task_model');
    $this->Complete_payment_task_model->before_3_day();
  }
}
