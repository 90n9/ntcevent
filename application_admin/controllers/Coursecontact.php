<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CourseContact extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model(array('coursecontact/CourseContactModel', 'member/MemberModel', 'course/CourseModel'));
  }
  public function index() {
    $this->LoginService->mustLogin();
    $dataContent = array();
    $content = $this->load->view('coursecontact/list', $dataContent, TRUE);
    $this->MasterpageService->addJs('/assets_admin/js/coursecontact/list.js');
    $this->MasterpageService->display($content, 'Request of quotation', 'coursecontact');
  }
  public function filter(){
    $this->LoginService->mustLogin('js');
    $searchVal = $this->input->post('searchVal');
    $startDate = $this->input->post('startDate');
    $endDate = $this->input->post('endDate');
    $page = $this->input->post('page');
    $this->load->model('coursecontact/CourseContactFilterService');
    $data = $this->CourseContactFilterService->getDataContent(
      $searchVal
      , $startDate
      , $endDate
      , $page
      , 50);
    $this->load->view('coursecontact/listcontent', $data);
  }
  public function detail($courseContactId){
    $this->LoginService->mustLogin();
    $dataContent = array();
    $dataContent['courseContactId'] = $courseContactId;
    $dataContent['courseContact'] = $this->CourseContactModel->getData($courseContactId);
    $title = 'Request of quotation Detail';
    $dataContent['title'] = $title;
    $content = $this->load->view('coursecontact/detail', $dataContent, TRUE);
    $this->MasterpageService->display($content, $title, 'coursecontact');
  }
  public function form($courseContactId = 0){
    $this->LoginService->mustLogin();
    $dataContent = array();
    $dataContent['courseContactId'] = $courseContactId;
    $dataContent['courseContact'] = $this->CourseContactModel->getData($courseContactId);
    $title = (($courseContactId == 0)?'Add':'Edit').' request of quotation';
    $dataContent['title'] = $title;
    $content = $this->load->view('coursecontact/form', $dataContent, TRUE);
    $this->MasterpageService->addJs('assets_admin/js/coursecontact/form.js');
    $this->MasterpageService->display($content, $title, 'coursecontact');
  }

  public function form_post($courseContactId){
    $this->LoginService->mustLogin();
    $data = array(
      'total_ticket' => $this->input->post('txtTicket'),
      'contact_firstname' => $this->input->post('txtFirstname'),
      'contact_lastname' => $this->input->post('txtLastname'),
      'contact_phone' => $this->input->post('txtPhone'),
      'contact_mobile' => $this->input->post('txtMobile'),
      'contact_email' => $this->input->post('txtEmail'),
      'contact_company' => $this->input->post('txtCompany'),
      'contact_note' => $this->input->post('txtNote'),
    );
    if($courseContactId != 0){
      $this->CourseContactModel->update($courseContactId, $data);
    }
    redirect('coursecontact/detail/'.$courseContactId);
  }
  public function delete($courseContactId){
    $this->LoginService->mustLogin();
    $this->CourseContactModel->delete($courseContactId);
    redirect('coursecontact/');
  }
}
