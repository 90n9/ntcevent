<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slideshow extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model(array('slideshow/SlideshowModel'));
    }
    public function index(){
        $this->LoginService->mustLogin();
        $data = array();
        $data['slideshowList'] = $this->SlideshowModel->getList();
        $content = $this->load->view('slideshow/list', $data, true);
        $this->MasterpageService->display($content, 'Slideshow', 'slideshow');
    }
    public function form($slideshowId = 0){
        $this->LoginService->mustLogin();
        $this->MasterpageService->addJs('assets_admin/js/slideshow/form.js');
        $title = ($slideshowId == 0)?'เพิ่ม Slideshow':'แก้ไข Slideshow';
        $dataContent = array();
        $dataContent['slideshowId'] = $slideshowId;
        $dataContent['slideshow'] = $this->SlideshowModel->getData($slideshowId);
        $dataContent['title'] = $title;
        $content = $this->load->view('slideshow/form', $dataContent, TRUE);
        $this->MasterpageService->display($content, $title, 'slideshow');
    }
    public function form_post($slideshowId){
      $data = array();
      $data['slideshow_image_th'] = '';
      $data['slideshow_image_en'] = '';
      $data['slideshow_url'] = $this->input->post('txtUrl');
      $data['sort_priority'] = $this->input->post('ddlSortPriority');
      $enableStatus = $this->input->post('chkEnableStatus');
      $data['enable_status'] = ($enableStatus != '')?$enableStatus:'0';
      $this->load->model(array('UploadImageModel'));
      $uploadData = $this->UploadImageModel->uploadSingleImage('slideshow_th', 'fileImageTh');
      if($uploadData['success']){
          $data['slideshow_image_th'] = $uploadData['imageFile'];
      }
      $uploadData = $this->UploadImageModel->uploadSingleImage('slideshow_en', 'fileImageEn');
      if($uploadData['success']){
          $data['slideshow_image_en'] = $uploadData['imageFile'];
      }
      if($slideshowId == 0){
      	$slideshowId = $this->SlideshowModel->insert($data);
      }else{
      	$this->SlideshowModel->update($slideshowId, $data);
      }
      redirect('slideshow/detail/'.$slideshowId);
    }
    public function detail($slideshowId){
        $title = 'ข้อมูล Slideshow';
        $dataContent = array();
        $dataContent['slideshowId'] = $slideshowId;
        $dataContent['slideshow'] = $this->SlideshowModel->getData($slideshowId);
        $dataContent['title'] = $title;
        $content = $this->load->view('slideshow/detail', $dataContent, TRUE);
        $this->MasterpageService->display($content, $title, 'slideshow');
    }
    public function delete($slideshowId){
    	$this->SlideshowModel->delete($slideshowId);
    	redirect('slideshow');
    }
}
