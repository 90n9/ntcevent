<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model(array('contact/ContactModel'));
    }
    public function index() {
        $this->LoginService->mustLogin();
        $dataContent = array();
        $content = $this->load->view('contact/list', $dataContent, TRUE);
        $this->MasterpageService->addJs('/assets_admin/js/contact/list.js');
        $this->MasterpageService->display($content, 'ติดต่อเรา', 'contact');
    }
    public function filter(){
        $this->LoginService->mustLogin('js');
        $searchVal = $this->input->post('searchVal');
        $startDate = $this->input->post('startDate');
        $endDate = $this->input->post('endDate');
        $page = $this->input->post('page');
        $this->load->model('contact/ContactFilterService');
        $data = $this->ContactFilterService->getDataContent(
            $searchVal
            , $startDate
            , $endDate
            , $page
            , 50);
        $this->load->view('contact/listcontent', $data);
    }
    public function detail($contactId){
        $this->LoginService->mustLogin();
        $dataContent = array();
        $dataContent['contactId'] = $contactId;
        $dataContent['contact'] = $this->ContactModel->getData($contactId);
        $title = 'แก้ไขข้อมูลติดต่อเรา';
        $dataContent['title'] = $title;
        $content = $this->load->view('contact/detail', $dataContent, TRUE);
        $this->MasterpageService->addJs('assets_admin/js/contact/form.js');
        $this->MasterpageService->display($content, $title, 'contact');
    }
    public function form($contactId){
        $this->LoginService->mustLogin();
        $dataContent = array();
        $dataContent['contactId'] = $contactId;
        $dataContent['contact'] = $this->ContactModel->getData($contactId);
        $title = 'แก้ไขข้อมูลติดต่อเรา';
        $dataContent['title'] = $title;
        $content = $this->load->view('contact/form', $dataContent, TRUE);
        $this->MasterpageService->addJs('assets_admin/js/contact/form.js');
        $this->MasterpageService->display($content, $title, 'contact');
    }

    public function form_post($contactId){
        $this->LoginService->mustLogin();
        $data = array();
        $data['contact_firstname'] = $this->input->post('txtFirstname');
        $data['contact_lastname'] = $this->input->post('txtLastname');
        $data['contact_email'] = $this->input->post('txtEmail');
        $data['contact_phone'] = $this->input->post('txtPhone');
        $data['contact_detail'] = $this->input->post('txtDetail');
        $this->ContactModel->update($contactId, $data);
        redirect('contact/');
    }
    public function delete($contactId){
        $this->LoginService->mustLogin();
        $this->ContactModel->delete($contactId);
        redirect('contact/');
    }
}
