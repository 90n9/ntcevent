<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model(array('member/MemberModel', 'wishlist/WishlistModel', 'course/CourseModel', 'coursecategory/CourseCategoryModel', 'coursebuyer/CourseBuyerModel'));
    }
    public function index() {
        $this->LoginService->mustLogin();
        $dataContent = array();
        $content = $this->load->view('member/list', $dataContent, TRUE);
        $this->MasterpageService->addJs('/assets_admin/js/member/list.js');
        $this->MasterpageService->display($content, 'สมาชิกเวบไซต์', 'member');
    }
    public function filter(){
        $this->LoginService->mustLogin('js');
        $searchVal = $this->input->post('searchVal');
        $enableStatus = $this->input->post('enableStatus');
        $page = $this->input->post('page');
        $perPage = $this->input->post('perPage');
        $this->load->model('member/MemberFilterService');
        $data = $this->MemberFilterService->getDataContent(
            $searchVal
            , $enableStatus
            , $page
            , 50);
        $this->load->view('member/listcontent', $data);
    }
    public function detail($memberId){
        $this->LoginService->mustLogin();
        $member = $this->MemberModel->getData($memberId);
        $dataContent = array();
        $dataContent['memberId'] = $memberId;
        $dataContent['member'] = $member;
        $dataContent['wishlistList'] = $this->WishlistModel->getListByMember($memberId);
        $dataContent['courseBuyerList'] = $this->CourseBuyerModel->getListByMember($memberId);
        $title = 'รายละเอียดสมาชิก';
        $dataContent['title'] = $title;
        $content = $this->load->view('member/detail', $dataContent, TRUE);
        $this->MasterpageService->addJs('assets_admin/js/member/detail.js');
        $this->MasterpageService->display($content, $title, 'member');
    }
    public function form($memberId = 0){
        $this->LoginService->mustLogin();
        $dataContent = array();
        $dataContent['memberId'] = $memberId;
        $dataContent['member'] = $this->MemberModel->getData($memberId);
        $title = (($memberId == 0)?'เพิ่ม':'แก้ไข').'สมาชิกเวบไซต์';
        $dataContent['title'] = $title;
        $content = $this->load->view('member/form', $dataContent, TRUE);
        $this->MasterpageService->addJs('assets_admin/js/member/form.js');
        $this->MasterpageService->display($content, $title, 'member');
    }

    public function form_post($memberId){
        $this->LoginService->mustLogin();
        $data['login_name'] = $this->input->post('txtLoginName');
        $data['login_password'] = $this->input->post('txtPassword');
        $data['firstname'] = $this->input->post('txtFirstname');
        $data['lastname'] = $this->input->post('txtLastname');
        $data['email'] = $this->input->post('txtEmail');
        $data['mobile'] = $this->input->post('txtMobile');
        $data['company'] = $this->input->post('txtCompany');
        $data['is_company'] = $this->input->post('rdoCompany');
        $data['company_name']  = $this->input->post('txtCompanyName');
        $data['company_address'] = $this->input->post('txtCompanyAddress');
        $data['company_branch'] = $this->input->post('txtCompanyBranch');
        $data['company_tax'] = $this->input->post('txtCompanyTax');
        $data['enable_status'] = $this->input->post('rdoStatus');
        $data['is_verify'] = $this->input->post('rdoVerify');
        if($memberId == 0){
            $memberId = $this->MemberModel->insert($data);
        }else{
            $this->MemberModel->update($memberId, $data);
        }
        redirect('member/detail/'.$memberId);
    }
    public function delete($memberId){
        $this->LoginService->mustLogin();
        $this->MemberModel->delete($memberId);
        redirect('member/');
    }
    public function form_change_password($memberId){
        $this->LoginService->mustLogin();
        $loginPassword = trim($this->input->post('txtPassword'));
        if($loginPassword != ''){
          $this->MemberModel->update_password($memberId, $loginPassword);
        }
        redirect('member/');
    }
}
