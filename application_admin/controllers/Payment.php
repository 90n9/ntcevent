<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model(array('payment/PaymentModel', 'member/MemberModel'));
    }
    public function index() {
        $this->LoginService->mustLogin();
        $dataContent = array();
        $content = $this->load->view('payment/list', $dataContent, TRUE);
        $this->MasterpageService->addJs('/assets_admin/js/payment/list.js?v=1');
        $this->MasterpageService->display($content, 'ข้อมูลการชำระเงิน', 'payment');
    }
    public function filter(){
        $this->LoginService->mustLogin('js');
        $searchVal = $this->input->post('searchVal');
        $paymentStatus = $this->input->post('paymentStatus');
        $paymentType = $this->input->post('paymentType');
        $startDate = $this->input->post('startDate');
        $endDate = $this->input->post('endDate');
        $page = $this->input->post('page');
        $this->load->model('payment/PaymentFilterService');
        $data = $this->PaymentFilterService->getDataContent(
            $searchVal
            , $paymentStatus
            , $paymentType
            , $startDate
            , $endDate
            , $page
            , 50);
        $this->load->view('payment/listcontent', $data);
    }
    public function detail($paymentId){
        $this->LoginService->mustLogin();
        $dataContent = array();
        $dataContent['paymentId'] = $paymentId;
        $dataContent['payment'] = $this->PaymentModel->getData($paymentId);
        $dataContent['arrPaymentStatusClass'] = array(
          'waiting' => 'info',
          'complete' => 'success',
          'cancel' => 'danger'
        );
        $paymentStatusList = array('waiting', 'complete', 'cancel');
        $dataContent['paymentStatusClass'] = (in_array($dataContent['payment']->payment_status, $paymentStatusList))?$dataContent['arrPaymentStatusClass'][$dataContent['payment']->payment_status]:$dataContent['payment']->payment_status;
        $dataContent['paymentTypeClass'] = ($dataContent['payment']->payment_type == 'creditcard')?'warning':'primary';
        $title = 'รายละเอียดข้อมูลการชำระเงิน';
        $dataContent['title'] = $title;
        $content = $this->load->view('payment/detail', $dataContent, TRUE);
        $this->MasterpageService->display($content, $title, 'payment');
    }
    public function form($paymentId = 0){
        $this->LoginService->mustLogin();
        $dataContent = array();
        $dataContent['paymentId'] = $paymentId;
        $dataContent['payment'] = $this->PaymentModel->getData($paymentId);
        $title = (($paymentId == 0)?'เพิ่ม':'แก้ไข').'ข้อมูลการชำระเงิน';
        $dataContent['title'] = $title;
        $content = $this->load->view('payment/form', $dataContent, TRUE);
        $this->MasterpageService->addJs('assets_admin/js/payment/form.js');
        $this->MasterpageService->display($content, $title, 'payment');
    }

    public function form_post($paymentId){
        $this->LoginService->mustLogin();
        $data = array();
        if($paymentId == 0){
            $paymentId = $this->PaymentModel->insert($data);
        }else{
            $this->PaymentModel->update($paymentId, $data);
        }
        redirect('payment/');
    }
    public function delete($paymentId){
        $this->LoginService->mustLogin();
        $this->PaymentModel->delete($paymentId);
        redirect('payment/');
    }
    public function waitingPayment($paymentId){
        $this->LoginService->mustLogin();
        $this->PaymentModel->setToWaiting($paymentId);
        //$this->sendMailUpdateStatus($paymentId, 'waiting');
        redirect('payment/detail/'.$paymentId);
    }
    public function confirmPayment($paymentId){
        $this->LoginService->mustLogin();
        $this->PaymentModel->setToComplete($paymentId);
        $this->sendmail_payment_complete($paymentId);
        //$this->sendMailUpdateStatus($paymentId, 'complete');
        redirect('payment/detail/'.$paymentId);
    }
    public function sendmail_payment_complete($paymentId){
      $this->load->model(array('SendMailService', 'EncodeService', 'coursebuyer/CourseBuyerModel', 'coursebuyer/CourseRegisterModel', 'courselocation/CourseLocationModel', 'course/CourseModel'));
      $paymentItemList = $this->PaymentModel->getPaymentItemList($paymentId);
      foreach($paymentItemList->result() as $paymentItem){
        $courseBuyer = $this->CourseBuyerModel->getData($paymentItem->course_buyer_id);
        $course = $this->CourseModel->getData($courseBuyer->course_id);
        $courseLocation = $this->CourseLocationModel->getData($course->course_location_id);
        $subject = '"'.$course->course_name.'" Registration Confirmation';
        $courseRegisterList = $this->CourseRegisterModel->getListByCourseBuyer($courseBuyer->course_buyer_id);
        $mapUrl = base_url('index.php/map/index/'.$courseLocation->course_location_id);
        foreach($courseRegisterList->result() as $courseRegister){
          $hashCode = $this->EncodeService->encode_number($courseRegister->course_register_id);
          $ticketUrl = base_url('index.php/mycourse/ticket/'.$hashCode);
          $content = $this->load->view('payment/mail/paymentsuccess', array(
            'course' => $course,
            'courseRegister' => $courseRegister,
            'courseLocation' => $courseLocation,
            'courseUrl' => base_url('index.php/course/detail/'.$course->course_id),
            'ticketUrl' => $ticketUrl,
            'mapUrl' => $mapUrl
          ), true);
          $this->SendMailService->send_mail($courseBuyer->buyer_email, '', $subject, $content);
        }
      }
    }
    public function cancelPayment($paymentId){
        $this->LoginService->mustLogin();
        $this->PaymentModel->setToCancel($paymentId);
        $this->sendMailUpdateStatus($paymentId, 'cancel');
        redirect('payment/detail/'.$paymentId);
    }
    private function sendMailUpdateStatus($paymentId, $paymentStatus){
      $this->load->model('SendMailService');
      $payment = $this->PaymentModel->getData($paymentId);
      $member = $this->MemberModel->getData($payment->member_id);
      $paymentItemList = $this->PaymentModel->getPaymentItemList($paymentId);
      foreach($paymentItemList->result() as $paymentItem){
        list ($subject, $content) = $this->getMailUpdateStatusContent($paymentStatus, $paymentItem, $payment, $member);
        if($subject != ''){
          $this->SendMailService->send_mail($member->email, '', $subject, $content);
        }
      }
    }
    private function getMailUpdateStatusContent($paymentStatus, $paymentItem, $payment, $member){
      $courseId = $paymentItem->course_id;
      switch ($paymentStatus) {
        case 'complete':
          $subject = '“ITIL® Service Strategy Lifecycle (SS)” Registration Confirmation';
          $content = '';
          break;
        case 'cancel':
          $subject = 'Free Order Canceled for “Building competencies for IT professionals”';
          $content = '';
          break;
        default:
          $subject = '';
          $content = '';
          break;
      }
      return array($subject, $content);
    }
}
