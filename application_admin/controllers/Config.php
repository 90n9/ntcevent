<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config extends CI_Controller {
    function __construct() {
      parent::__construct();
      $this->load->model(array('ConfigModel'));
    }
    public function index(){
      $this->LoginService->mustLogin();
      $content = $this->load->view('config/index', array(), true);
      $this->MasterpageService->display($content, 'Config', 'config');
    }
    public function form_post(){
      $this->ConfigModel->update('email_contact', $this->input->post('email_contact'));
      $this->ConfigModel->update('email_sales', $this->input->post('email_sales'));
      $this->ConfigModel->update('email_finance', $this->input->post('email_finance'));
      redirect('config/index');
    }
}
