<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CourseCategory extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model(array('coursecategory/CourseCategoryModel'));
    }
	public function index(){
        $this->LoginService->mustLogin();
        $content = $this->load->view('coursecategory/list', array(), TRUE);
        $this->MasterpageService->addJs('/assets_admin/js/coursecategory/list.js');
        $this->MasterpageService->display($content, 'หมวดหมู่', 'coursecategory');
	}
	public function form($courseCategoryId = 0, $parentId = 0){
        $this->LoginService->mustLogin();
        $dataContent = array();
        $dataContent['courseCategoryId'] = $courseCategoryId;
        $courseCategory = $dataContent['courseCategory'] = $this->CourseCategoryModel->getData($courseCategoryId);
        $parentCategory = -1;
        if($courseCategory){
            $parentCategory = $courseCategory->parent_id;
        }else{
            $parentCategory = $parentId;
        }
        $dataContent['parentCategory'] = $parentCategory;
        $title = (($courseCategoryId == 0)?'เพิ่ม':'แก้ไข').'หมวดหมู่';
        $dataContent['title'] = $title;
        $content = $this->load->view('coursecategory/form', $dataContent, TRUE);
        $this->MasterpageService->addJs('assets_admin/js/coursecategory/form.js');
        $this->MasterpageService->display($content, $title, 'coursecategory');
	}
	public function form_post($courseCategoryId){
        $this->LoginService->mustLogin();
        $data = array(
          'parent_id' => $this->input->post('ddlParent'),
          'course_category_name_th' => $this->input->post('txtNameTh'),
          'course_category_name_en' => $this->input->post('txtNameEn'),
          'sort_priority' => $this->input->post('ddlSortPriority')
        );
        if($courseCategoryId == 0){
            $courseCategoryId = $this->CourseCategoryModel->insert($data);
        }else{
            $this->CourseCategoryModel->update($courseCategoryId, $data);
        }
        redirect('coursecategory/');
	}
    public function delete($courseCategoryId){
        $this->LoginService->mustLogin();
        $this->CourseCategoryModel->delete($courseCategoryId);
        redirect('coursecategory/');
    }
}
