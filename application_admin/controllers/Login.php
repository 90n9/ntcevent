<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    
    public function index() {
        $this->LoginService->mustNotLogin();
        $data['title'] = 'เข้าสู่ระบบ';
        $this->load->view('login/index', $data);
    }

    public function login_post() {
        $loginName = $this->input->post('login_name');
        $loginPassword = $this->input->post('login_password');
        $this->LoginService->login($loginName, $loginPassword);
        redirect('dashboard');
    }

    public function logout() {
        $this->LoginService->logout();
        redirect('login');
    }
}