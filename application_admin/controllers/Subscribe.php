<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscribe extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model(array('subscribe/SubscribeModel'));
    }
    public function index() {
        $this->LoginService->mustLogin();
        $dataContent = array();
        $content = $this->load->view('subscribe/list', $dataContent, TRUE);
        $this->MasterpageService->addJs('/assets_admin/js/subscribe/list.js');
        $this->MasterpageService->display($content, 'ผู้สมัครจดหมายข่าว', 'subscribe');
    }
    public function filter(){
        $this->LoginService->mustLogin('js');
        $searchVal = $this->input->post('searchVal');
        $startDate = $this->input->post('startDate');
        $endDate = $this->input->post('endDate');
        $page = $this->input->post('page');
        $this->load->model('subscribe/SubscribeFilterService');
        $data = $this->SubscribeFilterService->getDataContent(
            $searchVal
            , $startDate
            , $endDate
            , $page
            , 50);
        $this->load->view('subscribe/listcontent', $data);
    }
    public function form($subscribeId = 0){
        $this->LoginService->mustLogin();
        $dataContent = array();
        $dataContent['subscribeId'] = $subscribeId;
        $dataContent['subscribe'] = $this->SubscribeModel->getData($subscribeId);
        $title = (($subscribeId == 0)?'เพิ่ม':'แก้ไข').'ผู้สมัครจดหมายข่าว';
        $dataContent['title'] = $title;
        $content = $this->load->view('subscribe/form', $dataContent, TRUE);
        $this->MasterpageService->addJs('assets_admin/js/subscribe/form.js');
        $this->MasterpageService->display($content, $title, 'subscribe');
    }

    public function form_post($subscribeId){
        $this->LoginService->mustLogin();
        $email = $this->input->post('txtEmail');
        if($subscribeId == 0){
            $subscribeId = $this->SubscribeModel->insert($email);
        }else{
            $this->SubscribeModel->update($subscribeId, $email);
        }
        redirect('subscribe/');
    }
    public function delete($subscribeId){
        $this->LoginService->mustLogin();
        $this->SubscribeModel->delete($subscribeId);
        redirect('subscribe/');
    }
}
