<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ConfigAbout extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model(array('ConfigModel'));
  }
  public function index(){
    $this->LoginService->mustLogin();
    $content = $this->load->view('configabout/index', array(), true);
    $this->MasterpageService->addJs('/assets_admin/js/configabout/index.js');
    $this->MasterpageService->display($content, 'รูปเกี่ยวกับเรา', 'configabout');
  }
  public function form_post(){
    $type = $this->input->post('rdoAboutType');
    $this->ConfigModel->update('about_side_type', $type);
    if($type == 'img'){
      $this->load->model(array('UploadImageModel'));
      $uploadData = $this->UploadImageModel->uploadSingleImage('about_img', 'fileAboutImg');
      if($uploadData['success']){
        $this->ConfigModel->update('about_side_img', $uploadData['imageFile']);
      }
    }
    if($type == 'video'){
      $this->ConfigModel->update('about_side_video', $this->input->post('txtVideo'));
    }
    redirect('configabout/index');
  }
}
