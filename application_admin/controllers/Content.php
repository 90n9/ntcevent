<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model(array('content/ContentModel'));
    }
    public function index($contentCode = ''){
        $this->LoginService->mustLogin();
        $dataContent = array();
        $content = $this->ContentModel->getData($contentCode);
        $dataContent['contentCode'] = $contentCode;
        $dataContent['content'] = $content;
        $title = (($content)?$content->content_name_th:'Page not found');
        $dataContent['title'] = $title;
        $content = $this->load->view('content/form', $dataContent, TRUE);
        $this->MasterpageService->addCss('assets_admin/vendor/summernote/summernote.css');
        $this->MasterpageService->addJs('assets_admin/vendor/summernote/summernote.min.js');
        $this->MasterpageService->addJs('assets_admin/js/content/form.js?v=1');
        $this->MasterpageService->display($content, $title, $contentCode);
    }

    public function form_post($contentCode){
        $this->LoginService->mustLogin();
        $data = array();
        $data['content_name_th'] = $this->input->post('txtNameTh');
        $data['content_name_en'] = $this->input->post('txtNameEn');
        $data['content_text_th'] = $this->input->post('textTh');
        $data['content_text_en'] = $this->input->post('textEn');
        $this->ContentModel->update($contentCode, $data);
        redirect('content/index/'.$contentCode);
    }
    public function uploadImage(){
        $contentImage = '';
        $this->load->model('UploadImageModel');
        $uploadImage = $this->UploadImageModel->uploadImage('content_image', 'file');
        if($uploadImage['success']){
            $contentImage = $uploadImage['imageLarge'];
        }
        echo base_url('uploads/'.$contentImage);
    }
}
