<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Popup extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model(array('ConfigModel'));
  }
  public function index(){
    $this->LoginService->mustLogin();
    $content = $this->load->view('popup/index', array(), true);
    $this->MasterpageService->display($content, 'Popup', 'popup');
  }
  public function form_post(){
    $this->ConfigModel->update('popup_url', $this->input->post('txtUrl'));
    $this->ConfigModel->update('popup_status', $this->input->post('rdoStatus'));
    $this->load->model(array('UploadImageModel'));
    $uploadData = $this->UploadImageModel->uploadSingleImage('popup_th', 'filePopupTh');
    if($uploadData['success']){
      $this->ConfigModel->update('popup_image_th', $uploadData['imageFile']);
    }
    $uploadData = $this->UploadImageModel->uploadSingleImage('popup_en', 'filePopupEn');
    if($uploadData['success']){
      $this->ConfigModel->update('popup_image_en', $uploadData['imageFile']);
    }
    redirect('popup/index');
  }
}
