<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Courselocation extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model(array('courselocation/CourseLocationModel'));
    }
    public function index() {
        $this->LoginService->mustLogin();
        $dataContent = array();
        $content = $this->load->view('courselocation/list', $dataContent, TRUE);
        $this->MasterpageService->addJs('/assets_admin/js/courselocation/list.js');
        $this->MasterpageService->display($content, 'สถานที่อบรม', 'courselocation');
    }
    public function filter(){
        $this->LoginService->mustLogin('js');
        $page = $this->input->post('page');
        $this->load->model('courselocation/CourseLocationFilterService');
        $data = $this->CourseLocationFilterService->getDataContent(
            $page
            , 50);
        $this->load->view('courselocation/listcontent', $data);
    }
    public function detail($courseLocationId){
        $this->LoginService->mustLogin();
        $dataContent = array();
        $dataContent['courseLocationId'] = $courseLocationId;
        $dataContent['courseLocation'] = $this->CourseLocationModel->getData($courseLocationId);
        $title = 'รายละเอียดสถานที่อบรม';
        $dataContent['title'] = $title;
        $content = $this->load->view('courselocation/detail', $dataContent, TRUE);
        $this->MasterpageService->addJs('assets_admin/js/courselocation/detail.js');
        $this->MasterpageService->display($content, $title, 'courselocation');
    }
    public function form($courseLocationId = 0){
        $this->LoginService->mustLogin();
        $dataContent = array();
        $dataContent['courseLocationId'] = $courseLocationId;
        $dataContent['courseLocation'] = $this->CourseLocationModel->getData($courseLocationId);
        $title = (($courseLocationId == 0)?'เพิ่ม':'แก้ไข').'สถานที่อบรม';
        $dataContent['title'] = $title;
        $content = $this->load->view('courselocation/form', $dataContent, TRUE);
        $this->MasterpageService->addJs('assets_admin/js/courselocation/form.js');
        $this->MasterpageService->display($content, $title, 'courselocation');
    }

    public function form_post($courseLocationId){
        $this->LoginService->mustLogin();
        $data = array();
        $data['course_location_name_th'] = $this->input->post('txtNameTh');
        $data['course_location_name_en'] = $this->input->post('txtNameEn');
        $data['course_location_detail_th'] = $this->input->post('txtDetailTh');
        $data['course_location_detail_en'] = $this->input->post('txtDetailEn');
        $data['lad'] = $this->input->post('txtLad');
        $data['lon'] = $this->input->post('txtLon');
        $data['sort_priority'] = $this->input->post('ddlSortPriority');
        if($courseLocationId == 0){
            $courseLocationId = $this->CourseLocationModel->insert($data);
        }else{
            $this->CourseLocationModel->update($courseLocationId, $data);
        }
        redirect('courselocation/');
    }
    public function delete($courseLocationId){
        $this->LoginService->mustLogin();
        $this->CourseLocationModel->delete($courseLocationId);
        redirect('courselocation/');
    }
}
