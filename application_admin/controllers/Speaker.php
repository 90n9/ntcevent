<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Speaker extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model(array('speaker/SpeakerModel'));
    }
    public function index() {
        $this->LoginService->mustLogin();
        $dataContent = array();
        $content = $this->load->view('speaker/list', $dataContent, TRUE);
        $this->MasterpageService->addJs('/assets_admin/js/speaker/list.js');
        $this->MasterpageService->display($content, 'ผู้สอน', 'speaker');
    }
    public function filter(){
        $this->LoginService->mustLogin('js');
        $searchVal = $this->input->post('searchVal');
        $page = $this->input->post('page');
        $this->load->model('speaker/SpeakerFilterService');
        $data = $this->SpeakerFilterService->getDataContent(
            $searchVal
            , $page
            , 50);
        $this->load->view('speaker/listcontent', $data);
    }
    public function detail($speakerId){
        $this->LoginService->mustLogin();
        $dataContent = array();
        $dataContent['speakerId'] = $speakerId;
        $dataContent['speaker'] = $this->SpeakerModel->getData($speakerId);
        $title = 'รายละเอียดผู้สอน';
        $dataContent['title'] = $title;
        $content = $this->load->view('speaker/detail', $dataContent, TRUE);
        $this->MasterpageService->display($content, $title, 'speaker');
    }
    public function form($speakerId = 0){
        $this->LoginService->mustLogin();
        $dataContent = array();
        $dataContent['speakerId'] = $speakerId;
        $dataContent['speaker'] = $this->SpeakerModel->getData($speakerId);
        $title = (($speakerId == 0)?'เพิ่ม':'แก้ไข').'ผู้สอน';
        $dataContent['title'] = $title;
        $content = $this->load->view('speaker/form', $dataContent, TRUE);
        $this->MasterpageService->addCss('assets_admin/vendor/summernote/summernote.css');
        $this->MasterpageService->addCss('assets_admin/vendor/summernote/summernote.css');
        $this->MasterpageService->addJs('assets_admin/vendor/summernote/summernote.min.js');
        $this->MasterpageService->addJs('assets_admin/js/speaker/form.js?v=2');
        $this->MasterpageService->display($content, $title, 'speaker');
    }

    public function form_post($speakerId){
        $this->LoginService->mustLogin();
        $data = array();
        $data['speaker_name'] = $this->input->post('txtName');
        $data['speaker_short_detail'] = $this->input->post('txtShortDetail');
        $data['speaker_detail'] = $this->input->post('txtDetail');
        $data['speaker_image'] = '';
        $this->load->model(array('UploadImageModel'));
        $this->UploadImageModel->setThumb(200,200);
        $uploadData = $this->UploadImageModel->uploadImage('speaker_image', 'file');
        if($uploadData['success']){
            $data['speaker_image'] = $uploadData['imageSmall'];
        }
        if($speakerId == 0){
            $speakerId = $this->SpeakerModel->insert($data);
        }else{
            $this->SpeakerModel->update($speakerId, $data);
        }
        redirect('speaker/detail/'.$speakerId);
    }
    public function delete($speakerId){
        $this->LoginService->mustLogin();
        $this->SpeakerModel->delete($speakerId);
        redirect('speaker/');
    }
    public function uploadImage(){
        $contentImage = '';
        $this->load->model('UploadImageModel');
        $uploadImage = $this->UploadImageModel->uploadImage('speaker_image', 'file');
        if($uploadImage['success']){
            $contentImage = $uploadImage['imageLarge'];
        }
        echo base_url('uploads/'.$contentImage);
    }
}
