<?php
class SlideshowModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function getList(){
        $this->db->where('is_delete', 0);
        $this->db->order_by('sort_priority', 'ASC');
        return $this->db->get('tbl_slideshow');
    }
    public function getData($slideshowId){
        $this->db->where('slideshow_id', $slideshowId);
        $this->db->where('is_delete', 0);
        $query = $this->db->get('tbl_slideshow');
        if($query->num_rows() > 0){
            return $query->row();
        }
        return false;
    }
    public function getMaxPriority(){
        $this->db->select('slideshow_id');
        $this->db->where('is_delete', 0);
        $query = $this->db->get('tbl_slideshow');
        return $query->num_rows();
    }
    public function insert($data){
        $this->moveUpPriority($data['sort_priority']);
        $this->setToDb($data);
        $this->db->set('create_date', 'now()', false);
        $this->db->set('create_by', $this->session->userdata('admin_id'));
        $this->db->insert('tbl_slideshow');
        $slideshowId = $this->db->insert_id();
        $this->LogActionModel->insertLog('slideshow',$slideshowId,'insert', 'insert new slideshow', $data, $this->db->last_query());
        return $slideshowId;
    }
    public function update($slideshowId, $data){
        $slideshow = $this->getData($slideshowId);
        if(!$slideshow){
            return false;
        }
        $this->moveDownPriority($slideshow->sort_priority);
        $this->moveUpPriority($data['sort_priority']);
        $this->setToDb($data);
        $this->db->set('update_date', 'now()', false);
        $this->db->set('update_by', $this->session->userdata('admin_id'));
        $this->db->where('slideshow_id', $slideshowId);
        $this->db->update('tbl_slideshow');
        $this->LogActionModel->insertLog('slideshow',$slideshowId,'update', 'update slideshow', $data, $this->db->last_query());
    }
    public function delete($slideshowId){
        $slideshow = $this->getData($slideshowId);
        if(!$slideshow){
            return false;
        }
        $this->moveDownPriority($slideshow->sort_priority);
        $this->db->set('is_delete', 1);
        $this->db->where('slideshow_id', $slideshowId);
        $this->db->update('tbl_slideshow');
        $this->LogActionModel->insertLog('slideshow',$slideshowId,'delete', 'delete slideshow', array(), $this->db->last_query());
    }
    private function setToDb($data){
        if($data['slideshow_image_th'] != ''){
            $this->db->set('slideshow_image_th', $data['slideshow_image_th']);
        }
        if($data['slideshow_image_en'] != ''){
            $this->db->set('slideshow_image_en', $data['slideshow_image_en']);
        }
        $this->db->set('slideshow_url', $data['slideshow_url']);
        $this->db->set('enable_status', $data['enable_status']);
        $this->db->set('sort_priority', $data['sort_priority']);
    }
    private function moveUpPriority($sortPriority){
        $this->db->set('sort_priority', 'sort_priority + 1', false);
        $this->db->where('sort_priority >=', $sortPriority);
        $this->db->update('tbl_slideshow');
    }
    private function moveDownPriority($sortPriority){
        $this->db->set('sort_priority', 'sort_priority - 1', false);
        $this->db->where('sort_priority >', $sortPriority);
        $this->db->update('tbl_slideshow');
    }
}
