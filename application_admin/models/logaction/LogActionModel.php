<?php
class LogActionModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function getData($logActionId){
        $this->db->where('log_action_id', $logActionId);
        $query = $this->db->get('tbl_log_action');
        if($query->num_rows() > 0){
            return $query->row();
        }
        return false;
    }
    public function insertLog($page, $pageId, $actionType, $actionName, $data, $lastQuery){
        $data = array();
        $data['log_action_page'] = $page;
        $data['page_id'] = $page;
        $data['log_action_type'] = $actionType;
        $data['log_action_name'] = $actionName;
        $data['log_action_detail'] = $this->concatLogData($data);
        $data['log_action_query'] = $lastQuery;
        $this->insert($data);
    }
    public function insert($data){
        $this->db->set('admin_id', $this->session->userdata('admin_id'));
        $this->db->set('log_action_page', $data['log_action_page']);
        $this->db->set('page_id', $data['page_id']);
        $this->db->set('log_action_type', $data['log_action_type']);
        $this->db->set('log_action_name', $data['log_action_name']);
        $this->db->set('log_action_detail', $data['log_action_detail']);
        $this->db->set('log_action_query', $data['log_action_query']);
        $this->db->set('create_date', 'NOW()', false);
        $this->db->insert('tbl_log_action');
    }
    public function concatLogData($data){
        $str = '';
        foreach($data as $key=>$value){
            if($key != 'login_password'){
                $str .= '<br />'.$key.' : '.$value;
            }
        }
        return $str;
    }
}