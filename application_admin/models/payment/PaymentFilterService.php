<?php
class PaymentFilterService extends CI_Model {
  function __construct() {
    parent::__construct();
  }
  public function getDataContent($searchVal, $paymentStatus, $paymentType, $startDate, $endDate, $page = 1, $perPage = 20) {
    $data = array();
    $data['searchVal'] = $searchVal;
    $data['paymentStatus'] = $paymentStatus;
    $data['paymentType'] = $paymentType;
    $data['startDate'] = $startDate;
    $data['endDate'] = $endDate;
    $data['page'] = $page;
    $data['perPage'] = $perPage;
    $data['paymentList'] = $this->getList($searchVal, $paymentStatus, $paymentType, $startDate, $endDate, $page, $perPage);
    $data['totalTransaction'] = $this->getCount($searchVal, $paymentStatus, $paymentType, $startDate, $endDate);
    return $data;
  }
  private function getCount($searchVal, $paymentStatus, $paymentType, $startDate, $endDate){
    $this->whereTransaction($searchVal, $paymentStatus, $paymentType, $startDate, $endDate);
    $this->db->select('tbl_payment.payment_id');
    $query = $this->db->get('tbl_payment');
    return $query->num_rows();
  }
  private function getList($searchVal, $paymentStatus, $paymentType, $startDate, $endDate, $page, $perPage){
    $this->whereTransaction($searchVal, $paymentStatus, $paymentType, $startDate, $endDate);
    $this->db->select('tbl_payment.*');
    $this->db->order_by('tbl_payment.payment_id', 'DESC');
    $offset = ($page - 1) * $perPage;
    return $this->db->get('tbl_payment', $perPage, $offset);
  }
  private function whereTransaction($searchVal, $paymentStatus, $paymentType, $startDate, $endDate){
    if($searchVal != ''){
      $strWhere = "(payment_code like '%$searchVal%' ";
      $strWhere .= "OR note like '%$searchVal%' ";
      $strWhere .= ')';
      $this->db->where($strWhere, NULL,FALSE);
    }
    if($paymentStatus != 'all'){
      $this->db->where('payment_status', $paymentStatus);
    }
    if($paymentType != 'all'){
      $this->db->where('payment_type', $paymentType);
    }
    if($startDate != ''){
      $this->db->where('create_date >=', $startDate);
    }
    if($endDate != ''){
      $this->db->where('create_date <=', $endDate);
    }
    $this->db->where('is_delete', 0);
  }
}
