<?php
class PaymentModel extends CI_Model {
  function __construct() {
      parent::__construct();
      $this->load->model(array('coursebuyer/CourseBuyerModel'));
  }
  public function getData($paymentId){
      $this->db->where('payment_id', $paymentId);
      $this->db->where('is_delete', 0);
      $query = $this->db->get('tbl_payment');
      if($query->num_rows() > 0){
          return $query->row();
      }
      return false;
  }
  public function insert($data){
    $preCode = $this->getPreCode();
    $runningNumber = $this->getNextRunningNumber($preCode);
    $paymentCode = 'INV'. $preCode.str_pad($runningNumber,3,"0",STR_PAD_LEFT);
    $this->setToDb($data);
    $this->db->set('payment_code', $paymentCode);
    $this->db->set('payment_pre_code', $preCode);
    $this->db->set('running_number', $runningNumber);
    $this->db->set('create_date', 'now()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_payment');
    $paymentId = $this->db->insert_id();
    $this->LogActionModel->insertLog('payment',$paymentId,'insert', 'insert new payment', $data, $this->db->last_query());
    return $paymentId;
  }
  public function update($paymentId, $data){
    $this->setToDb($data);
    $this->db->set('update_date', 'now()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('payment_id', $paymentId);
    $this->db->update('tbl_payment');
    $this->LogActionModel->insertLog('payment',$paymentId,'update', 'update payment', $data, $this->db->last_query());
  }
  public function delete($paymentId){
    $this->db->set('is_delete', 1);
    $this->db->where('payment_id', $paymentId);
    $this->db->update('tbl_payment');
    $this->LogActionModel->insertLog('payment', $paymentId, 'delete', 'delete payment', '', $this->db->last_query());
  }
  public function setToWaiting($paymentId){
    $this->updateStatus($paymentId, 'waiting');
    $payment = $this->getData($paymentId);
    $paymentItemList = $this->getPaymentItemList($paymentId);
    foreach($paymentItemList->result() as $paymentItem){
      $this->CourseBuyerModel->updateStatus($paymentItem->course_buyer_id, 'payment');
    }
  }
  public function setToComplete($paymentId){
    $this->updateStatus($paymentId, 'complete');
    $paymentItemList = $this->getPaymentItemList($paymentId);
    foreach($paymentItemList->result() as $paymentItem){
      $this->CourseBuyerModel->updateStatus($paymentItem->course_buyer_id, 'complete');
    }
  }
  public function setToCancel($paymentId){
    $this->updateStatus($paymentId, 'cancel');
    $paymentItemList = $this->getPaymentItemList($paymentId);
    foreach($paymentItemList->result() as $paymentItem){
      $courseBuyer = $this->CourseBuyerModel->getData($paymentItem->course_buyer_id);
      if($courseBuyer && $courseBuyer->course_buyer_status == 'payment'){
        $this->CourseBuyerModel->updateStatus($paymentItem->course_buyer_id, 'waiting');
      }
    }
  }
  public function getPaymentItemList($paymentId){
    $this->db->where('payment_id', $paymentId);
    return $this->db->get('tbl_payment_item');
  }
  private function updateStatus($paymentId, $paymentStatus){
    $this->db->set('payment_status', $paymentStatus);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('payment_id', $paymentId);
    $this->db->update('tbl_payment');
  }
  private function setToDb($data){
  }
  private function getPreCode(){
    return date('ym');
  }
  private function getNextRunningNumber($preCode){
    $this->db->select('MAX(running_number) AS max_running_number');
    $this->db->where('payment_pre_code', $preCode);
    $query = $this->db->get('tbl_payment');
    if($query->num_rows() > 0){
      return $query->row()->max_running_number + 1;
    }
    return 1;
  }
}
