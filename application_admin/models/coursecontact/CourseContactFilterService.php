<?php
class CourseContactFilterService extends CI_Model {
  function __construct() {
    parent::__construct();
  }

  public function getDataContent($searchVal, $startDate, $endDate, $page = 1, $perPage = 20) {
    $data = array();
    $data['searchVal'] = $searchVal;
    $data['startDate'] = $startDate;
    $data['endDate'] = $endDate;
    $data['page'] = $page;
    $data['perPage'] = $perPage;
    $data['courseContactList'] = $this->getList($searchVal, $startDate, $endDate, $page, $perPage);
    $data['totalTransaction'] = $this->getCount($searchVal, $startDate, $endDate);
    return $data;
  }
  private function getCount($searchVal, $startDate, $endDate){
    $this->whereTransaction($searchVal, $startDate, $endDate);
    $this->db->select('tbl_course_contact.course_contact_id');
    $query = $this->db->get('tbl_course_contact');
    return $query->num_rows();
  }
  private function getList($searchVal, $startDate, $endDate, $page, $perPage){
    $this->whereTransaction($searchVal, $startDate, $endDate);
    $this->db->select('tbl_course_contact.*');
    $this->db->order_by('tbl_course_contact.course_contact_id', 'DESC');
    $offset = ($page - 1) * $perPage;
    return $this->db->get('tbl_course_contact', $perPage, $offset);
  }
  private function whereTransaction($searchVal, $startDate, $endDate){
    if($searchVal != ''){
      $strWhere = "(contact_firstname like '%$searchVal%' ";
      $strWhere .= "OR contact_lastname like '%$searchVal%' ";
      $strWhere .= "OR contact_mobile like '%$searchVal%' ";
      $strWhere .= "OR contact_email like '%$searchVal%' ";
      $strWhere .= "OR contact_company like '%$searchVal%' ";
      $strWhere .= "OR contact_note like '%$searchVal%' ";
      $strWhere .= ')';
      $this->db->where($strWhere, NULL,FALSE);
    }
    if($startDate != ''){
      $this->db->where('create_date >=', $startDate);
    }
    if($endDate != ''){
      $this->db->where('create_date <=', $endDate.' 23:59:59');
    }
    $this->db->where('is_delete', 0);
  }
}
