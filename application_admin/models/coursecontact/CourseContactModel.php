<?php
class CourseContactModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function getData($courseContactId){
        $this->db->where('course_contact_id', $courseContactId);
        $query = $this->db->get('tbl_course_contact');
        if($query->num_rows() > 0){
            return $query->row();
        }
        return false;
    }
    public function getListByMember($memberId){
        $this->db->where('member_id', $memberId);
        $this->db->where('is_delete', 0);
        return $this->db->get('tbl_course_contact');
    }
    public function getListByCourse($courseId){
        $this->db->where('course_id', $courseId);
        $this->db->where('is_delete', 0);
        return $this->db->get('tbl_course_contact');
    }
    public function update($courseContactId, $data){
      $this->setToDb($data);
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('course_contact_id', $courseContactId);
      $this->db->update('tbl_course_contact');
    }
    public function delete($courseContactId){
      $this->db->set('is_delete', 1);
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('course_contact_id', $courseContactId);
      $this->db->update('tbl_course_contact');
    }
    private function setToDb($data){
      $this->db->set('total_ticket', $data['total_ticket']);
      $this->db->set('contact_firstname', $data['contact_firstname']);
      $this->db->set('contact_lastname', $data['contact_lastname']);
      $this->db->set('contact_email', $data['contact_email']);
      $this->db->set('contact_phone', $data['contact_phone']);
      $this->db->set('contact_mobile', $data['contact_mobile']);
      $this->db->set('contact_company', $data['contact_company']);
      $this->db->set('contact_note', $data['contact_note']);
    }
}
