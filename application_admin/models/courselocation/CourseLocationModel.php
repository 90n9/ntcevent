<?php
class CourseLocationModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function getList(){
        $this->db->where('is_delete', 0);
        $this->db->order_by('sort_priority', 'ASC');
        return $this->db->get('tbl_course_location');
    }
    public function getData($courseLocationId){
        $this->db->where('course_location_id', $courseLocationId);
        $this->db->where('is_delete', 0);
        $query = $this->db->get('tbl_course_location');
        if($query->num_rows() > 0){
            return $query->row();
        }
        return false;
    }
    public function getName($courseLocationId){
        $courseLocation = $this->getData($courseLocationId);
        if($courseLocation){
            return $courseLocation->course_location_name_th;
        }
        return '-';
    }
    public function getMaxPriority(){
        $this->db->select('course_location_id');
        $this->db->where('is_delete', 0);
        $query = $this->db->get('tbl_course_location');
        return $query->num_rows();
    }
    public function insert($data){
        $this->moveUpPriority($data['sort_priority']);
        $this->setToDb($data);
        $this->db->set('create_date', 'now()', false);
        $this->db->set('create_by', $this->session->userdata('admin_id'));
        $this->db->insert('tbl_course_location');
        $courseLocationId = $this->db->insert_id();
        $this->LogActionModel->insertLog('courseLocation',$courseLocationId,'insert', 'insert new course location ', $data, $this->db->last_query());
        return $courseLocationId;
    }
    public function update($courseLocationId, $data){
        $courseLocation = $this->getData($courseLocationId);
        if(!$courseLocation){
            return false;
        }
        $this->moveDownPriority($courseLocation->sort_priority);
        $this->moveUpPriority($data['sort_priority']);
        $this->setToDb($data);
        $this->db->set('update_date', 'now()', false);
        $this->db->set('update_by', $this->session->userdata('admin_id'));
        $this->db->where('course_location_id', $courseLocationId);
        $this->db->update('tbl_course_location');
        $this->LogActionModel->insertLog('courseLocation',$courseLocationId,'update', 'update course location', $data, $this->db->last_query());
    }
    public function delete($courseLocationId){
        $courseLocation = $this->getData($courseLocationId);
        if(!$courseLocation){
            return false;
        }
        $this->moveDownPriority($courseLocation->sort_priority);
        $this->db->where('course_location_id', $courseLocationId);
        $this->db->delete('tbl_course_location');
        $this->LogActionModel->insertLog('courseLocation',$courseLocationId,'delete', 'delete course location', array(), $this->db->last_query());
    }
    private function setToDb($data){
        $this->db->set('course_location_name_th', $data['course_location_name_th']);
        $this->db->set('course_location_name_en', $data['course_location_name_en']);
        $this->db->set('course_location_detail_th', $data['course_location_detail_th']);
        $this->db->set('course_location_detail_en', $data['course_location_detail_en']);
        $this->db->set('lad', $data['lad']);
        $this->db->set('lon', $data['lon']);
        $this->db->set('sort_priority', $data['sort_priority']);
    }
    private function moveUpPriority($sortPriority){
        $this->db->set('sort_priority', 'sort_priority + 1', false);
        $this->db->where('sort_priority >=', $sortPriority);
        $this->db->update('tbl_course_location');
    }
    private function moveDownPriority($sortPriority){
        $this->db->set('sort_priority', 'sort_priority - 1', false);
        $this->db->where('sort_priority >=', $sortPriority);
        $this->db->update('tbl_course_location');
    }
}
