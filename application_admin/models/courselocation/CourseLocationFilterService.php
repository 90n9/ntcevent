<?php
class CourseLocationFilterService extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getDataContent($page = 1, $perPage = 20) {
        $data = array();
        $data['page'] = $page;
        $data['perPage'] = $perPage;
        $data['courseLocationList'] = $this->getList($page, $perPage);
        $data['totalTransaction'] = $this->getCount();
        return $data;
    }
    private function getCount(){
        $this->db->select('tbl_course_location.course_location_id');
        $this->whereTransaction();
        $query = $this->db->get('tbl_course_location');
        return $query->num_rows();
    }
    private function getList($page, $perPage){
        $this->db->select('tbl_course_location.*');
        $this->whereTransaction();
        $this->db->order_by('tbl_course_location.sort_priority', 'ASC');
        $offset = ($page - 1) * $perPage;
        return $this->db->get('tbl_course_location', $perPage, $offset);
    }
    private function whereTransaction(){
        $this->db->where('is_delete', 0);
    }
}
