<?php

class ContactFilterService extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getDataContent($searchVal, $startDate, $endDate, $page = 1, $perPage = 20) {
        $data = array();
        $data['searchVal'] = $searchVal;
        $data['startDate'] = $startDate;
        $data['endDate'] = $endDate;
        $data['page'] = $page;
        $data['perPage'] = $perPage;
        $data['contactList'] = $this->getList($searchVal, $startDate, $endDate, $page, $perPage);
        $data['totalTransaction'] = $this->getCount($searchVal, $startDate, $endDate);
        return $data;
    }
    private function getCount($searchVal, $startDate, $endDate){
        $this->db->select('tbl_contact.contact_id');
        $this->whereTransaction($searchVal, $startDate, $endDate);
        $query = $this->db->get('tbl_contact');
        return $query->num_rows();
    }
    private function getList($searchVal, $startDate, $endDate, $page, $perPage){
        $this->db->select('tbl_contact.*');
        $this->whereTransaction($searchVal, $startDate, $endDate);
        $offset = ($page - 1) * $perPage;
        return $this->db->get('tbl_contact', $perPage, $offset);
    }
    private function whereTransaction($searchVal, $startDate, $endDate){
        if($searchVal != ''){
            $strWhere = "(contact_first_name like '%$searchVal%' ";
            $strWhere .= "OR contact_last_name like '%$searchVal%' ";
            $strWhere .= "OR contact_email like '%$searchVal%' ";
            $strWhere .= "OR contact_phone like '%$searchVal%' ";
            $strWhere .= "OR contact_subject like '%$searchVal%' ";
            $strWhere .= "OR contact_detail like '%$searchVal%' ";
            $strWhere .= ')';
            $this->db->where($strWhere, NULL,FALSE);
        }
        if($startDate != ''){
            $this->db->where('tbl_contact.create_date >=', $startDate);
        }
        if($startDate != ''){
            $this->db->where('tbl_contact.create_date >=', $endDate);
        }
        $this->db->where('is_delete', 0);
    }
}