<?php
class ContactModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function getData($contactId){
        $this->db->where('contact_id', $contactId);
        $this->db->where('is_delete', 0);
        $query = $this->db->get('tbl_contact');
        if($query->num_rows() > 0){
            return $query->row();
        }
        return false;
    }
    public function update($contactId, $data){
        $contact = $this->getData($contactId);
        if(!$contact){
            return false;
        }
        $this->setToDb($data);
        $this->db->set('update_date', 'now()', false);
        $this->db->set('update_by', $this->session->userdata('admin_id'));
        $this->db->where('contact_id', $contactId);
        $this->db->update('tbl_contact');
        $this->LogActionModel->insertLog('contact',$contactId,'update', 'update contact us', $data, $this->db->last_query());
    }
    public function delete($contactId){
        $this->db->set('is_delete', 1);
        $this->db->where('contact_id', $contactId);
        $this->db->update('tbl_contact');
        $this->LogActionModel->insertLog('contact',$contactId,'delete', 'delete contact us', array(), $this->db->last_query());
    }
    private function setToDb($data){
        $this->db->set('contact_firstname', $data['contact_firstname']);
        $this->db->set('contact_lastname', $data['contact_lastname']);
        $this->db->set('contact_email', $data['contact_email']);
        $this->db->set('contact_phone', $data['contact_phone']);
        $this->db->set('contact_detail', $data['contact_detail']);
    }
}