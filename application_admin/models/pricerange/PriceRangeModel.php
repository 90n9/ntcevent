<?php
class PriceRangeModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function getList(){
        $this->db->where('is_delete', 0);
        $this->db->order_by('price_from', 'ASC');
        return $this->db->get('tbl_price_range');
    }
    public function getData($priceRangeId){
        $this->db->where('price_range_id', $priceRangeId);
        $this->db->where('is_delete', 0);
        $query = $this->db->get('tbl_price_range');
        if($query->num_rows() > 0){
            return $query->row();
        }
        return false;
    }
    public function insert($data){
        $this->setToDb($data);
        $this->db->set('create_date', 'now()', false);
        $this->db->set('create_by', $this->session->userdata('admin_id'));
        $this->db->insert('tbl_price_range');
        $priceRangeId = $this->db->insert_id();
        $this->LogActionModel->insertLog('pricerange',$priceRangeId,'insert', 'insert new price range', $data, $this->db->last_query());
        return $priceRangeId;
    }
    public function update($priceRangeId, $data){
        $this->setToDb($data);
        $this->db->set('update_date', 'now()', false);
        $this->db->set('update_by', $this->session->userdata('admin_id'));
        $this->db->where('price_range_id', $priceRangeId);
        $this->db->update('tbl_price_range');
        $this->LogActionModel->insertLog('pricerange',$priceRangeId,'update', 'update price range', $data, $this->db->last_query());
    }
    public function delete($priceRangeId){
        $this->db->set('is_delete', 1);
        $this->db->where('price_range_id', $priceRangeId);
        $this->db->update('tbl_price_range');
        $this->LogActionModel->insertLog('pricerange',$pricerangeId,'delete', 'delete price range', $data, $this->db->last_query());
    }
    private function setToDb($data){
        $this->db->set('price_from', $data['price_from']);
        $this->db->set('price_to', $data['price_to']);
    }
}