<?php
class ContentModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function getData($contentCode){
        $this->db->where('content_code', $contentCode);
        $query = $this->db->get('tbl_content');
        if($query->num_rows() > 0){
            return $query->row();
        }
        return false;
    }
    public function getName($contentCode){
        $content = $this->getData($contentCode);
        if($content){
            return $content->content_name_th;
        }
        return '-';
    }
    public function update($contentCode, $data){
        $this->setToDb($data);
        $this->db->set('update_date', 'now()', false);
        $this->db->set('update_by', $this->session->userdata('admin_id'));
        $this->db->where('content_code', $contentCode);
        $this->db->update('tbl_content');
        $this->LogActionModel->insertLog('content/index/'.$contentCode,0,'update', 'update content '.$contentCode, $data, $this->db->last_query());
    }
    public function setToDb($data){
        $this->db->set('content_name_th', $data['content_name_th']);
        $this->db->set('content_name_en', $data['content_name_en']);
        $this->db->set('content_text_th', $data['content_text_th']);
        $this->db->set('content_text_en', $data['content_text_en']);
    }
}