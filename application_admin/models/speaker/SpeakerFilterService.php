<?php

class SpeakerFilterService extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getDataContent($searchVal, $page = 1, $perPage = 20) {
        $data = array();
        $data['searchVal'] = $searchVal;
        $data['page'] = $page;
        $data['perPage'] = $perPage;
        $data['speakerList'] = $this->getList($searchVal, $page, $perPage);
        $data['totalTransaction'] = $this->getCount($searchVal);
        return $data;
    }
    private function getCount($searchVal){
        $this->db->select('tbl_speaker.speaker_id');
        $this->whereTransaction($searchVal);
        $query = $this->db->get('tbl_speaker');
        return $query->num_rows();
    }
    private function getList($searchVal, $page, $perPage){
        $this->db->select('tbl_speaker.*');
        $this->whereTransaction($searchVal);
        $this->db->order_by('speaker_name', 'ASC');
        $offset = ($page - 1) * $perPage;
        return $this->db->get('tbl_speaker', $perPage, $offset);
    }
    private function whereTransaction($searchVal){
        if($searchVal != ''){
            $strWhere = "(speaker_name like '%$searchVal%' ";
            $strWhere = "OR speaker_short_detail like '%$searchVal%' ";
            $strWhere = "OR speaker_detail like '%$searchVal%' ";
            $strWhere .= ')';
            $this->db->where($strWhere, NULL,FALSE);
        }
        $this->db->where('is_delete', 0);
    }
}
