<?php
class SpeakerModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function getList(){
        $this->db->where('is_delete', 0);
        $this->db->order_by('speaker_name', 'ASC');
        return $this->db->get('tbl_speaker');
    }
    public function getData($speakerId){
        $this->db->where('speaker_id', $speakerId);
        $this->db->where('is_delete', 0);
        $query = $this->db->get('tbl_speaker');
        if($query->num_rows() > 0){
            return $query->row();
        }
        return false;
    }
    public function getName($speakerId){
        $speaker = $this->getData($speakerId);
        if($speaker){
            return $speaker->speaker_name;
        }
        return '';
    }
    public function insert($data){
        $this->setToDb($data);
        $this->db->set('create_date', 'now()', false);
        $this->db->set('create_by', $this->session->userdata('admin_id'));
        $this->db->insert('tbl_speaker');
        $speakerId = $this->db->insert_id();
        $this->LogActionModel->insertLog('speaker',$speakerId,'insert', 'insert new speaker', $data, $this->db->last_query());
        return $speakerId;
    }
    public function update($speakerId, $data){
        $this->setToDb($data);
        $this->db->set('update_date', 'now()', false);
        $this->db->set('update_by', $this->session->userdata('admin_id'));
        $this->db->where('speaker_id', $speakerId);
        $this->db->update('tbl_speaker');
        $this->LogActionModel->insertLog('speaker',$speakerId,'update', 'update speaker', $data, $this->db->last_query());
    }
    public function delete($speakerId){
        $this->db->set('is_delete', 1);
        $this->db->where('speaker_id', $speakerId);
        $this->db->update('tbl_speaker');
        $this->LogActionModel->insertLog('speaker',$speakerId,'delete', 'delete speaker', $data, $this->db->last_query());
    }
    public function setToDb($data){
        $this->db->set('speaker_name', $data['speaker_name']);
        $this->db->set('speaker_detail', $data['speaker_detail']);
        $this->db->set('speaker_short_detail', $data['speaker_short_detail']);
        if($data['speaker_image'] != ''){
            $this->db->set('speaker_image', $data['speaker_image']);
        }
    }
}
