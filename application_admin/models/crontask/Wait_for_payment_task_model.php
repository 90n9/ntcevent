<?php
class Wait_for_payment_task_model extends CI_Model{
  public function expire_7_day(){
    $this->db->where('tbl_course_buyer.create_date <=','NOW() - INTERVAL 1 WEEK', FALSE);
    $this->db->where('tbl_course_buyer.course_buyer_status', 'waiting');
    $query = $this->db->get('tbl_course_buyer');
    foreach($query->result() as $course_buyer){
      $this->cancelRegister($course_buyer);
      $this->mail_expireorder($course_buyer);
    }
  }
  private function cancelRegister($course_buyer){
    $this->load->model(array('coursebuyer/CourseBuyerModel'));
    $this->CourseBuyerModel->updateStatus($course_buyer->course_buyer_id, 'cancel');
  }
  private function mail_expireorder($course_buyer){
    $this->load->model(array('SendMailService','course/CourseModel', 'member/MemberModel'));
    $course = $this->CourseModel->getData($course_buyer->course_id);
    $member = $this->MemberModel->getData($course_buyer->member_id);
    $content = $this->load->view('crontask/expireorder',array(
      'course' => $course,
      'courseBuyer' => $course_buyer,
      'member' => $member
    ), true);
    $this->SendMailService->send_mail($member->email, '', 'Your order has been cancelled automatically', $content);
  }
  public function payment_reminder(){
    $this->db->where('DATE(tbl_course_buyer.create_date)','CURDATE() - INTERVAL 3 DAY', FALSE);
    $this->db->where('tbl_course_buyer.course_buyer_status', 'waiting');
    $query = $this->db->get('tbl_course_buyer');
    foreach($query->result() as $course_buyer){
      $this->mail_waitforpayment($course_buyer);
    }
  }

  public function mail_waitforpayment($course_buyer){
    $this->load->model(array('SendMailService', 'EncodeService', 'course/CourseModel', 'courselocation/CourseLocationModel'));
    $course = $this->CourseModel->getData($course_buyer->course_id);
    $courseBuyerCodeHash = $this->EncodeService->encode_number($course_buyer->course_buyer_code);
    $content = $this->load->view('crontask/waitforpayment',array(
      'course' => $course,
      'courseBuyer' => $course_buyer,
      'courseBuyerCodeHash' => 'courseBuyerCodeHash'
    ), true);
    $this->SendMailService->send_mail($member->email, '', 'your registration for "'.$course->course_name.'"</span> will be expired in 3 days', $content);
  }
}
