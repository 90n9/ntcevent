<?php
class Wishlist_task_model extends CI_Model{
  public function before_7_day(){
    $this->load->model(array('course/CourseModel', 'coursebuyer/CourseBuyerModel','coursebuyer/CourseRegisterModel'));
    $this->db->where('DATE(tbl_ticket.end_date) =','CURDATE() + INTERVAL 1 WEEK', FALSE);
    $this->db->where('enable_status', 1);
    $this->db->where('is_delete', 0);
    $ticketList = $this->db->get('tbl_ticket');
    foreach($ticketList->result() as $ticket){
      $this->db->where('enable_status', 1);
      $this->db->where('is_delete', 0);
      $course = $this->CourseModel->getData($ticket->course_id);
      $this->db->where('course_id', $ticket->course_id);
      $wishlist_list = $this->db->get('tbl_member_wishlist');
      foreach($wishlist_list->result() as $wishlist){
        $this->db->where('enable_status', 1);
        $this->db->where('is_delete', 0);
        $member = $this->MemberModel->getData($wishlist->member_id);
        $this->db->where('course_id', $course->course_id);
        $this->db->where('member_id', $wishlist->member_id);
        $this->db->where('course_buyer_status !=', 'cancel');
        $this->db->where('is_delete', 0);
        $courseBuyer = $this->db->get('tbl_course_buyer');
        if($member && $courseBuyer->num_rows() == 0){
          $this->email_wishlist($course, $member);
        }
      }
    }
  }
  public function email_wishlist($course, $member){
    $this->load->model(array('SendMailService', 'course/CourseModel', 'courselocation/CourseLocationModel'));
    $content = $this->load->view('crontask/wishlist',array(
      'course' => $course,
      'member' => $member,
      'courseUrl' => base_url('index.php/course/detail/'.$course->course_id)
    ), true);
    $this->SendMailService->send_mail($member->email, '', 'Reminder for the "'.$course->course_name.'" registration will be closed in 7 days', $content);
  }
}
