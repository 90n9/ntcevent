<?php
class Complete_payment_task_model extends CI_Model{
  public function before_3_day(){
    $this->load->model(array('course/CourseModel', 'coursebuyer/CourseBuyerModel','coursebuyer/CourseRegisterModel'));
    $this->db->where('DATE(tbl_course.course_start) =','CURDATE() + INTERVAL 3 DAY', FALSE);
    $courseList = $this->db->get('tbl_course');
    foreach($courseList->result() as $course){
      $this->db->where('course_buyer_status', 'complete');
      $courseBuyerList = $this->CourseBuyerModel->getListByCourse($course->course_id);
      foreach($courseBuyerList->result() as $courseBuyer){
        $courseRegisterList = $this->CourseRegisterModel->getListByCourseBuyer($courseBuyer->course_buyer_id);
        foreach($courseRegisterList->result() as $courseRegister){
          $this->send_remindercourse($course, $courseBuyer, $courseRegister);
        }
      }
    }
  }
  private function send_remindercourse($course, $course_buyer, $course_register){
    $this->load->model(array('SendMailService','course/CourseModel', 'member/MemberModel', 'courselocation/CourseLocationModel', 'coursebuyer/CourseBuyerModel','coursebuyer/CourseRegisterModel'));
    $course_location = $this->CourseLocationModel->getData($course->course_location_id);
    $mapUrl = base_url('index.php/map/index/'.$course->course_location_id);
    $content = $this->load->view('crontask/remindercourse',array(
      'course' => $course,
      'courseLocation' => $course_location,
      'courseBuyer' => $course_buyer,
      'courseRegister' => $course_register,
      'mapUrl' => $mapUrl,
      'ticketUrl' => 'ticket'
    ), true);
    $this->SendMailService->send_mail($member->email, '', 'Reminder for the "'.$course->course_name.'"', $content);
  }
}
