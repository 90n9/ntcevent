<?php
class CourseRegisterModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function getListByMember($memberId){
        $this->db->where('member_id', $memberId);
        $this->db->where('is_delete', 0);
        $this->db->order_by('course_register_id', 'DESC');
        return $this->db->get('tbl_course_register');
    }
    public function getListByCourseBuyer($courseBuyerId){
        $this->db->where('course_buyer_id', $courseBuyerId);
        $this->db->where('is_delete', 0);
        $this->db->order_by('course_register_id', 'DESC');
        return $this->db->get('tbl_course_register');
    }
    public function getListByCourse($courseId){
        $this->db->where('course_id', $courseId);
        $this->db->where('is_delete', 0);
        $this->db->order_by('course_register_id', 'DESC');
        return $this->db->get('tbl_course_register');
    }
    public function getData($courseRegisterId){
        $query = $this->db->get('tbl_course_register');
        if($query->num_rows() > 0){
            return $query->row();
        }
        return false;
    }
}
