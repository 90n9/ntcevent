<?php
class CourseBuyerModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function getData($courseBuyerId){
      $this->db->where('course_buyer_id', $courseBuyerId);
      $query = $this->db->get('tbl_course_buyer');
      if($query->num_rows() > 0){
        return $query->row();
      }
      return false;
    }
    public function getListByMember($memberId){
      $this->db->where('member_id', $memberId);
      $this->db->where('is_delete', 0);
      $this->db->order_by('course_buyer_id', 'DESC');
      return $this->db->get('tbl_course_buyer');
    }
    public function getListByCourse($courseId){
      $this->db->where('course_id', $courseId);
      $this->db->where('is_delete', 0);
      $this->db->order_by('course_buyer_id', 'DESC');
      return $this->db->get('tbl_course_buyer');
    }
    public function getListByPayment($paymentId){
      $this->db->select('tbl_course_buyer.*');
      $this->db->where('tbl_payment_item.payment_id', $paymentId);
      $this->db->where('tbl_course_buyer.is_delete', 0);
      $this->db->join('tbl_payment_item', 'tbl_payment_item.course_buyer_id = tbl_course_buyer.course_buyer_id');
      $this->db->order_by('course_buyer_id', 'DESC');
      return $this->db->get('tbl_course_buyer');
    }
    public function updateStatus($courseBuyerId, $courseBuyerStatus){
      $this->db->set('course_buyer_status', $courseBuyerStatus);
      $this->db->where('course_buyer_id', $courseBuyerId);
      $this->db->update('tbl_course_buyer');
    }
}
