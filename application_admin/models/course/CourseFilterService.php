<?php
class CourseFilterService extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->model(array('coursecategory/CourseCategoryModel'));
    }

    public function getDataContent($searchVal, $courseCategoryId, $enableStatus, $startDate, $endDate, $page = 1, $perPage = 20) {
        $data = array();
        $data['searchVal'] = $searchVal;
        $data['courseCategoryId'] = $courseCategoryId;
        $data['enableStatus'] = $enableStatus;
        $data['startDate'] = $startDate;
        $data['endDate'] = $endDate;
        $data['page'] = $page;
        $data['perPage'] = $perPage;
        $data['courseList'] = $this->getList($searchVal, $courseCategoryId, $enableStatus, $startDate, $endDate, $page, $perPage);
        $data['totalTransaction'] = $this->getCount($searchVal, $courseCategoryId, $enableStatus, $startDate, $endDate);
        return $data;
    }
    private function getCount($searchVal, $courseCategoryId, $enableStatus, $startDate, $endDate){
        $this->whereTransaction($searchVal, $courseCategoryId, $enableStatus, $startDate, $endDate);
        $this->db->select('tbl_course.course_id');
        $query = $this->db->get('tbl_course');
        return $query->num_rows();
    }
    private function getList($searchVal, $courseCategoryId, $enableStatus, $startDate, $endDate, $page, $perPage){
        $this->whereTransaction($searchVal, $courseCategoryId, $enableStatus, $startDate, $endDate);
        $this->db->select('tbl_course.*');
        $this->db->order_by('tbl_course.sort_priority', 'ASC');
        $offset = ($page - 1) * $perPage;
        return $this->db->get('tbl_course', $perPage, $offset);
    }
    private function whereTransaction($searchVal, $courseCategoryId, $enableStatus, $startDate, $endDate){
        if($courseCategoryId != 'all'){
            $arrCourseCategoryId = $this->CourseCategoryModel->getUnderId($courseCategoryId);
            $this->db->where_in('tbl_course.course_category_id', $arrCourseCategoryId);
        }
        if($searchVal != ''){
            $strWhere = "(course_name like '%$searchVal%' ";
            $strWhere .= "OR course_description like '%$searchVal%' ";
            $strWhere .= ')';
            $this->db->where($strWhere, NULL,FALSE);
        }
        if($enableStatus != 'all'){
          $this->db->where('tbl_course.enable_status', $enableStatus);
        }
        if($startDate != ''){
            $this->db->where('course_start >=', $startDate);
        }
        if($endDate != ''){
            $this->db->where('course_start <=', $endDate.' 23:59:59');
        }
        $this->db->where('is_delete', 0);
    }
}
