<?php
class DocumentModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function getList($courseId){
        $this->db->where('course_id', $courseId);
        $this->db->where('is_delete', 0);
        $this->db->order_by('sort_priority', 'ASC');
        return $this->db->get('tbl_course_document');
    }
    public function getData($documentId){
        $this->db->where('course_document_id', $documentId);
        $this->db->where('is_delete', 0);
        $query = $this->db->get('tbl_course_document');
        if($query->num_rows() > 0){
            return $query->row();
        }
        return false;
    }
    public function getName($documentId){
        $document = $this->getData($documentId);
        if($document){
            return $document->document_name;
        }
        return '-';
    }
    public function getMaxPriority($courseId){
        $this->db->select('course_document_id');
        $this->db->where('course_id', $courseId);
        $this->db->where('is_delete', 0);
        $query = $this->db->get('tbl_course_document');
        return $query->num_rows();
    }
    public function insert($courseId, $data){
        $this->moveUpPriority($courseId, $data['sort_priority']);
        $this->setToDb($data);
        $this->db->set('course_id', $courseId);
        $this->db->set('create_date', 'now()', false);
        $this->db->set('create_by', $this->session->userdata('admin_id'));
        $this->db->insert('tbl_course_document');
        $documentId = $this->db->insert_id();
        $this->LogActionModel->insertLog('course/document',$documentId,'insert', 'insert new course document', $data, $this->db->last_query());
        return $documentId;
    }
    public function update($documentId, $data){
        $document = $this->getData($documentId);
        if(!$document){
            return false;
        }
        $this->moveDownPriority($document->course_id, $document->sort_priority);
        $this->moveUpPriority($document->course_id, $data['sort_priority']);
        $this->setToDb($data);
        $this->db->set('update_date', 'now()', false);
        $this->db->set('update_by', $this->session->userdata('admin_id'));
        $this->db->where('course_document_id', $documentId);
        $this->db->update('tbl_course_document');
        $this->LogActionModel->insertLog('course/document',$documentId,'update', 'update course document', $data, $this->db->last_query());
    }
    public function delete($documentId){
        $document = $this->getData($documentId);
        if(!$document){
            return false;
        }
        $this->moveDownPriority($document->course_id, $document->sort_priority);
        $this->db->set('is_delete', 1);
        $this->db->where('course_document_id', $documentId);
        $this->db->update('tbl_course_document');
        $this->LogActionModel->insertLog('course/document',$documentId,'delete', 'delete course document', array(), $this->db->last_query());
    }
    private function setToDb($data){
        $this->db->set('document_name', $data['document_name']);
        if($data['document_file'] != ''){
            $this->db->set('document_file', $data['document_file']);
        }
        $this->db->set('sort_priority', $data['sort_priority']);
        $this->db->set('enable_status', $data['enable_status']);
    }
    private function moveUpPriority($courseId, $sortPriority){
        $this->db->set('sort_priority', 'sort_priority + 1', false);
        $this->db->where('sort_priority >=', $sortPriority);
        $this->db->where('course_id', $courseId);
        $this->db->update('tbl_course_document');
    }
    private function moveDownPriority($courseId, $sortPriority){
        $this->db->set('sort_priority', 'sort_priority - 1', false);
        $this->db->where('sort_priority >', $sortPriority);
        $this->db->where('course_id', $courseId);
        $this->db->update('tbl_course_document');
    }
}