<?php
class DiscountModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function getData($discountId){
        $this->db->where('discount_id', $discountId);
        $this->db->where('is_delete', 0);
        $query = $this->db->get('tbl_discount');
        if($query->num_rows() > 0){
            return $query->row();
        }
        return false;
    }
    public function getName($dicountId){
        $discount = $this->getData($discountId);
        if($discount){
            return $discount->discount_code;
        }
        return '-';
    }
    public function getList($courseId){
        $this->db->where('course_id', $courseId);
        $this->db->where('is_delete', 0);
        return $this->db->get('tbl_discount');
    }
    public function insert($courseId, $data){
        $this->setToDb($data);
        $this->db->set('course_id', $courseId);
        $this->db->set('create_date', 'now()', false);
        $this->db->set('create_by', $this->session->userdata('admin_id'));
        $this->db->insert('tbl_discount');
        $discountId = $this->db->insert_id();
        $this->LogActionModel->insertLog('course/discount/',$discountId,'insert', 'insert new course discount ', $data, $this->db->last_query());
        return $discountId;
    }
    public function update($discountId, $data){
        $this->setToDb($data);
        $this->db->set('update_date', 'now()', false);
        $this->db->set('update_by', $this->session->userdata('admin_id'));
        $this->db->where('discount_id', $discountId);
        $this->db->update('tbl_discount');
        $this->LogActionModel->insertLog('course/discount/',$discountId,'update', 'update course discount ', $data, $this->db->last_query());
    }
    public function delete($discountId){
        $this->db->set('is_delete', 1);
        $this->db->where('discount_id', $discountId);
        $this->db->update('tbl_discount');
        $this->LogActionModel->insertLog('course/discount/',$discountId,'delete', 'delete course discount ', array(), $this->db->last_query());
    }
    private function setToDb($data){
        $this->db->set('discount_code', $data['discount_code']);
        $this->db->set('discount_qty', $data['discount_qty']);
        $this->db->set('discount_type', $data['discount_type']);
        $this->db->set('discount_amount', $data['discount_amount']);
        $this->db->set('available_from', $data['available_from']);
        $this->db->set('available_to', $data['available_to']);
        $this->db->set('enable_status', $data['enable_status']);
        $this->db->set('min_ticket', $data['min_ticket']);
        $this->db->set('max_ticket', $data['max_ticket']);
    }
}
