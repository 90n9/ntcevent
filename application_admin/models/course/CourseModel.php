<?php
class CourseModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function getData($courseId){
        $this->db->where('course_id', $courseId);
        $this->db->where('is_delete', 0);
        $query = $this->db->get('tbl_course');
        if($query->num_rows() > 0){
            return $query->row();
        }
        return false;
    }
    public function getName($courseId){
        $course = $this->getData($courseId);
        if($course){
            return $course->course_name;
        }
        return '-';
    }
    public function getMaxPriority(){
        $this->db->select('course_id');
        $this->db->where('is_delete', 0);
        $query = $this->db->get('tbl_course');
        return $query->num_rows();
    }
    public function insert($data){
      $this->load->model('coursecategory/CourseCategoryModel');
      $coursePreCode = $this->getPreCode();
      $runningNumber = $this->getNextRunningNumber($coursePreCode);
      $courseCode = 'NTC'. $this->CourseCategoryModel->getCourseCategoryCode($data['course_category_id']).$coursePreCode.str_pad($runningNumber,2,"0",STR_PAD_LEFT);
      $this->moveUpPriority($data['sort_priority']);
      $this->setToDb($data);
      $this->db->set('course_pre_code', $coursePreCode);
      $this->db->set('running_number', $runningNumber);
      $this->db->set('course_code', $courseCode);
      $this->db->set('create_date', 'now()', false);
      $this->db->set('create_by', $this->session->userdata('admin_id'));
      $this->db->insert('tbl_course');
      $courseId = $this->db->insert_id();
      $this->LogActionModel->insertLog('course',$courseId,'insert', 'insert new course ', $data, $this->db->last_query());
      return $courseId;
    }
    public function update($courseId, $data){
        $course = $this->getData($courseId);
        if(!$course){
            return false;
        }
        $courseCode = 'NTC'. $this->CourseCategoryModel->getCourseCategoryCode($data['course_category_id']).$course->course_pre_code.str_pad($course->running_number,2,"0",STR_PAD_LEFT);
        $this->moveDownPriority($course->sort_priority);
        $this->moveUpPriority($data['sort_priority']);
        $this->setToDb($data);
        $this->db->set('course_code', $courseCode);
        $this->db->set('update_date', 'now()', false);
        $this->db->set('update_by', $this->session->userdata('admin_id'));
        $this->db->where('course_id', $courseId);
        $this->db->update('tbl_course');
        $this->LogActionModel->insertLog('course',$courseId,'update', 'update course ', $data, $this->db->last_query());
    }
    public function delete($courseId){
        $course = $this->getData($courseId);
        if(!$course){
            return false;
        }
        $this->moveDownPriority($course->sort_priority);
        $this->db->set('is_delete', 1);
        $this->db->where('course_id', $courseId);
        $this->db->update('tbl_course');
        $this->LogActionModel->insertLog('course',$courseId,'delete', 'delete course ', array(), $this->db->last_query());
    }
    private function setToDb($data){
        $this->db->set('course_category_id', $data['course_category_id']);
        $this->db->set('course_name', $data['course_name']);
        $this->db->set('course_date', $data['course_date']);
        $this->db->set('course_length', $data['course_length']);
        $this->db->set('course_start_time', $data['course_start_time']);
        $this->db->set('course_end_time', $data['course_end_time']);
        $this->db->set('course_start', $data['course_start']);
        $this->db->set('course_end', $data['course_end']);
        if($data['cover_image'] != ''){
            $this->db->set('cover_image', $data['cover_image']);
        }
        $this->db->set('cover_video', $data['cover_video']);
        if($data['thumb_image'] != ''){
            $this->db->set('thumb_image', $data['thumb_image']);
        }
        $this->db->set('course_description', $data['course_description']);
        $this->db->set('course_lang', $data['course_lang']);
        $this->db->set('course_price_type', $data['course_price_type']);
        $this->db->set('course_price', $data['course_price']);
        $this->db->set('course_before_discount_price', $data['course_before_discount_price']);
        $this->db->set('course_location_id', $data['course_location_id']);
        $this->db->set('course_full_status', $data['course_full_status']);
        $this->db->set('speaker_type', $data['speaker_type']);
        $this->db->set('speaker_id', $data['speaker_id']);
        $this->db->set('sort_priority', $data['sort_priority']);
        $this->db->set('enable_status', $data['enable_status']);
    }
    private function moveUpPriority($sortPriority){
        $this->db->set('sort_priority', 'sort_priority + 1', false);
        $this->db->where('sort_priority >=', $sortPriority);
        $this->db->update('tbl_course');
    }
    private function moveDownPriority($sortPriority){
        $this->db->set('sort_priority', 'sort_priority - 1', false);
        $this->db->where('sort_priority >', $sortPriority);
        $this->db->update('tbl_course');
    }
    private function getPreCode(){
      return date('ym');
    }
    private function getNextRunningNumber($coursePreCode){
        $this->db->select('MAX(running_number) AS max_running_number');
        $this->db->where('course_pre_code', $coursePreCode);
        $query = $this->db->get('tbl_course');
        if($query->num_rows() > 0){
            return $query->row()->max_running_number + 1;
        }
        return 1;
    }
}
