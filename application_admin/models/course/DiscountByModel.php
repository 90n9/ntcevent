<?php
class DiscountByModel extends CI_Model{
  var $type_list = array(
    'buyer' => array(
      'type_code' => 'buyer',
      'type_name' => 'ต่อการสั่งซื้อ',
      'label_class' => 'label label-warning'
    ),
    'ticket' => array(
      'type_code' => 'ticket',
      'type_name' => 'ต่อผู้เข้าเรียน',
      'label_class' => 'label label-primary'
    )
  );
  public function get_list(){
    return $this->type_list;
  }
  public function get_data($type_code){
    return $this->type_list[$type_code];
  }
  public function get_name($type_code){
    $type = $this->get_data($type_code);
    return $type['type_name'];
  }
  public function get_label($type_code){
    $type = $this->get_data($type_code);
    return '<span class="'.$type['label_class'].'">'.$type['type_name'].'</span>';
  }
}
