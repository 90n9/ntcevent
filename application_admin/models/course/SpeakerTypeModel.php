<?php
class SpeakerTypeModel extends CI_Model {
    var $typeList = array('speaker'=>'Speaker', 'instructure'=>'Instructor', 'none'=>'None');
    var $typeCodeList = array('speaker', 'instructure', 'none');
    function __construct() {
        parent::__construct();
    }
    public function getList(){
        return $this->typeList;
    }
    public function getName($speakerTypeCode){
        return (in_array($speakerTypeCode, $this->typeCodeList))?$this->typeList[$speakerTypeCode]:$speakerTypeCode;
    }
}
