<?php
class DiscountTicketModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function getDiscountList($ticketId){
        $this->db->select('tbl_discount.*');
        $this->db->join('tbl_discount', 'tbl_discount.discount_id = tbl_discount_ticket.discount_id');
        $this->db->where('tbl_discount_ticket.ticket_id', $ticketId);
        $this->db->where('tbl_discount_ticket.is_delete', 0);
        return $this->db->get('tbl_discount_ticket');
    }
    public function getTicketList($discountId){
        $this->db->select('tbl_ticket.*');
        $this->db->join('tbl_ticket', 'tbl_ticket.ticket_id = tbl_discount_ticket.ticket_id');
        $this->db->where('tbl_discount_ticket.discount_id', $discountId);
        return $this->db->get('tbl_discount_ticket');
    }
    public function insert($discountId, $ticketId){
        $this->db->set('discount_id', $discountId);
        $this->db->set('ticket_id', $ticketId);
        $this->db->insert('tbl_discount_ticket');
        $discountTicketId = $this->db->insert_id();
        $this->LogActionModel->insertLog('course/discountticket',$discountTicketId,'insert', 'insert new discount ticket', $data, $this->db->last_query());
        return $discountTicketId;
    }
    public function delete($discountTicketId){
        $this->db->where('discount_ticket_id', $discountTicketId);
        $this->db->delete('tbl_discount_ticket');
        $this->LogActionModel->insertLog('course/discountticket',$discountTicketId,'delete', 'delete discount ticket', array(), $this->db->last_query());
    }
    public function deleteByTicket($ticketId){
        $this->db->where('ticket_id', $ticketId);
        $this->db->delete('tbl_discount_ticket');
    }
    public function deleteByDiscount($discountId){
        $this->db->where('discount_id', $discountId);
        $this->db->delete('tbl_discount_ticket');
    }
}