<?php
class TicketModel extends CI_Model {
    function __construct() {
      parent::__construct();
    }
    public function getTicketId($courseId){
      $tickets = $this->getList($courseId);
      if($tickets->num_rows() > 0){
        return $tickets->row()->ticket_id;
      }else{
        return 0;
      }
    }
    public function getList($courseId){
      $this->db->where('course_id', $courseId);
      $this->db->where('is_delete', 0);
      $this->db->order_by('sort_priority', 'ASC');
      return $this->db->get('tbl_ticket');
    }
    public function getData($ticketId){
      $this->db->where('ticket_id', $ticketId);
      $this->db->where('is_delete', 0);
      $query = $this->db->get('tbl_ticket');
      if($query->num_rows() > 0){
        return $query->row();
      }
      return false;
    }
    public function getName($ticketId){
      $ticket = $this->getData($ticketId);
      if($ticket){
        return $ticket->ticket_name;
      }
      return '-';
    }
    public function getMaxPriority($courseId){
      $this->db->select('ticket_id');
      $this->db->where('course_id', $courseId);
      $this->db->where('is_delete', 0);
      $query = $this->db->get('tbl_ticket');
      return $query->num_rows();
    }
    public function insert($courseId, $data){
      $this->moveUpPriority($courseId, $data['sort_priority']);
      $this->setToDb($data);
      $this->db->set('course_id', $courseId);
      $this->db->set('create_date', 'now()', false);
      $this->db->set('create_by', $this->session->userdata('admin_id'));
      $this->db->insert('tbl_ticket');
      $ticketId = $this->db->insert_id();
      $this->LogActionModel->insertLog('course/ticket/',$ticketId,'insert', 'insert new course ticket ', $data, $this->db->last_query());
      return $ticketId;
    }
    public function update($ticketId, $data){
      $ticket = $this->getData($ticketId);
      if(!$ticket){
        return false;
      }
      $this->moveDownPriority($ticket->course_id, $ticket->sort_priority);
      $this->moveUpPriority($ticket->course_id, $data['sort_priority']);
      $this->setToDb($data);
      $this->db->set('update_date', 'now()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('ticket_id', $ticketId);
      $this->db->update('tbl_ticket');
      $this->LogActionModel->insertLog('course/ticket',$ticketId,'update', 'update course ticket', $data, $this->db->last_query());
    }
    public function delete($ticketId){
      $ticket = $this->getData($ticketId);
      if(!$ticket){
        return false;
      }
      $this->moveDownPriority($ticket->course_id, $ticket->sort_priority);
      $this->db->set('is_delete', 1);
      $this->db->where('ticket_id', $ticketId);
      $this->db->update('tbl_ticket');
      $this->LogActionModel->insertLog('course/ticket',$ticketId,'delete', 'delete course ticket', array(), $this->db->last_query());
    }
    private function setToDb($data){
      $this->db->set('ticket_name', $data['ticket_name']);
      $this->db->set('ticket_qty', $data['ticket_qty']);
      $this->db->set('ticket_description', $data['ticket_description']);
      $this->db->set('is_show_description', $data['is_show_description']);
      $this->db->set('price_type', $data['price_type']);
      $this->db->set('price', $data['price']);
      $this->db->set('start_date', $data['start_date']);
      $this->db->set('end_date', $data['end_date']);
      $this->db->set('sort_priority', $data['sort_priority']);
      $this->db->set('enable_status', $data['enable_status']);
    }
    private function moveUpPriority($courseId, $sortPriority){
      $this->db->set('sort_priority', 'sort_priority + 1', false);
      $this->db->where('sort_priority >=', $sortPriority);
      $this->db->where('course_id', $courseId);
      $this->db->update('tbl_ticket');
    }
    private function moveDownPriority($courseId, $sortPriority){
      $this->db->set('sort_priority', 'sort_priority - 1', false);
      $this->db->where('sort_priority >', $sortPriority);
      $this->db->where('course_id', $courseId);
      $this->db->update('tbl_ticket');
    }
}
