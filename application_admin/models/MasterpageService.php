<?php

class MasterpageService extends CI_Model {
    var $cssList;
    var $jsList;
    function __construct() {
        parent::__construct();
        $this->cssList = array();
        $this->jsList = array();
    }
    function display($content, $title = '', $nav = ''){
        $data['content'] = $content;
        $data['title'] = $title;
        $data['nav'] = $nav;
        $this->load->view('masterpage/masterpage', $data);
    }
    function addCss($cssFile){
        $this->cssList[] = base_url($cssFile);
    }
    function addJs($jsFile){
        $this->jsList[] = base_url($jsFile);
    }
    function addOuterCss($cssFile){
        $this->cssList[] = $cssFile;
    }
    function addOuterJs($jsFile){
        $this->jsList[] = $jsFile;
    }
    public function printCss(){
        return $this->load->view('masterpage/cssloader', array('cssList'=>$this->cssList), TRUE);
    }
    public function printJs(){
        return $this->load->view('masterpage/jsloader', array('jsList'=>$this->jsList), TRUE);
    }
}