<?php
class LoginService extends CI_Model {

    var $NO_USER_NAME = 'require_user_name';
    var $NO_PASSWORD = 'require_password';
    var $NO_USER_FOUND = 'user_and_password_miss_match';
    var $USERTYPE_INVALID = 'unenable_user';

    function __construct() {
        parent::__construct();
    }

    public function mustLogin() {
        if (!$this->getLoginStatus()) {
            redirect('login');
        }
    }
    public function mustNotLogin() {
        if ($this->getLoginStatus()) {
            redirect('dashboard');
        }
    }

    public function logout() {
        $this->session->sess_destroy();
    }

    public function formatUserName($loginName) {
        return ($loginName !== 'null') ? $loginName : '';
    }

    public function getErrorMessage($error) {
        $errorMessage = '';
        if ($error != '') {
            $arrMessage[$this->NO_USER_NAME] = 'กรุณาระบุชื่อผู้ใช้';
            $arrMessage[$this->NO_PASSWORD] = 'กรุณาระบุรหัสผ่าน';
            $arrMessage[$this->NO_USER_FOUND] = 'ไม่พบข้อมูล กรุณาตรวจสอบชื่อผู้ใช้ และรหัสผ่านอีกครั้ง';
            $arrMessage[$this->USERTYPE_INVALID] = 'บัญชีนี้ไม่สามารถใช้ในระบบนี้ได้ กรุณาติดต่อเจ้าหน้าที่เพื่อสอบถามข้อมูลเพิ่มเติม';
            $errorMessage = (isset($arrMessage[$error])) ? $arrMessage[$error] : $error;
        }
        return $errorMessage;
    }

    public function login($loginName, $loginPassword) {
        $this->checkUserNameInput($loginName);
        $this->checkUserPasswordInput($loginName, $loginPassword);
        $user = $this->checkUserLogin($loginName, $loginPassword);
        $this->checkUserAllow($user);
        $this->createSession($user);
    }

    private function getLoginStatus() {
        if ($this->session->userdata('logged_in')) {
            return true;
        }
        return false;
    }

    private function checkUserNameInput($loginName) {
        if (!$loginName) {
            redirect('login/index/null/' . $this->NO_USER_NAME);
        }
    }

    private function checkUserPasswordInput($loginName, $loginPassword) {
        if (!$loginPassword) {
            redirect('login/index/' . $loginName . '/' . $this->NO_PASSWORD);
        }
    }

    private function checkUserLogin($loginName, $loginPassword) {
        $this->load->model('admin/AdminModel');
        $data = $this->AdminModel->getLogin($loginName, $loginPassword);
        if (!$data) {
            redirect('login/index/' . $loginName . '/' . $this->NO_USER_FOUND);
        }
        return $data;
    }

    private function checkUserAllow($admin) {
        if ($admin->enable_status != 1) {
            redirect('login/index/' . $admin->login_name . '/' . $this->USERTYPE_INVALID);
        }
    }

    public function updateSession(){
        $admin = $this->AdminModel->getData($this->session->userdata('admin_id'));
        $this->createSession($admin);
    }
    private function createSession($admin) {
        $userdata = array();
        $userdata['admin_id'] = $admin->admin_id;
        $userdata['login_name'] = $admin->login_name;
        $userdata['name'] = $admin->name;
        $userdata['email'] = $admin->email;
        $userdata['logged_in'] = TRUE;
        $this->session->set_userdata($userdata);
    }

}
