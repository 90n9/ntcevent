<?php

class NumberService extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function formatInt($number){
    	return number_format($number);
    }
    public function formatFloat($number){
        return number_format($number, 2, '.', ',');;
    }
}