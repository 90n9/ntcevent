<?php
class CourseCategoryModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function getCourseCategoryCode($courseCategoryId){
      $retVal = '';
      do{
        $courseCategory = $this->getData($courseCategoryId);
        $courseCategoryId = $courseCategory->parent_id;
        if($courseCategoryId == 0){
          $retVal = $courseCategory->course_category_code;
        }
      }while($courseCategoryId != 0);
      return $retVal;
    }
    public function echoOptionList($parentId, $prefix = '', $selectId = 0){
        $categoryList = $this->getList($parentId);
        foreach($categoryList->result() as $category){
            $selected = ($category->course_category_id == $selectId)?'selected="selected"':'';
            echo '<option value="'.$category->course_category_id.'" '.$selected.' >'.$prefix.$category->course_category_name_th.'</option>';
            $this->echoOptionList($category->course_category_id, $prefix.'&nbsp;&nbsp;', $selectId);
        }
    }
    public function getUnderId($courseCategoryId, $retVal = array()){
        array_push($retVal, $courseCategoryId);
        $courseCategoryList = $this->getList($courseCategoryId);
        foreach($courseCategoryList->result() as $courseCategory){
            $retVal = $this->getUnderId($courseCategory->course_category_id, $retVal);
        }
        return $retVal;
    }
    public function getList($parentId = 0){
        $this->db->where('parent_id', $parentId);
        $this->db->where('is_delete', 0);
        $this->db->order_by('sort_priority', 'ASC');
        return $this->db->get('tbl_course_category');
    }
    public function getData($courseCategoryId){
        $this->db->where('course_category_id', $courseCategoryId);
        $this->db->where('is_delete', 0);
        $query = $this->db->get('tbl_course_category');
        if($query->num_rows() > 0){
            return $query->row();
        }
        return false;
    }
    public function getName($courseCategoryId){
        if($courseCategoryId == 0){
            return 'กลุ่มหลัก';
        }
        $courseCategory = $this->getData($courseCategoryId);
        if($courseCategory){
            return $courseCategory->course_category_name_th;
        }
        return '-';
    }
    public function getMaxPriority($parentId){
        $this->db->select('course_category_id');
        $this->db->where('parent_id', $parentId);
        $query = $this->db->get('tbl_course_category');
        return $query->num_rows();
    }
    public function insert($data){
        $this->moveUpPriority($data['parent_id'], $data['sort_priority']);
        $this->setToDb($data);
        $this->db->set('create_date', 'now()', false);
        $this->db->set('create_by', $this->session->userdata('admin_id'));
        $this->db->insert('tbl_course_category');
        $courseCategoryId = $this->db->insert_id();
        $this->LogActionModel->insertLog('coursecategory',$courseCategoryId,'insert', 'insert new course category ', $data, $this->db->last_query());
        return $courseCategoryId;
    }
    public function update($courseCategoryId, $data){
        $courseCategory = $this->getData($courseCategoryId);
        if(!$courseCategory){
            return false;
        }
        $this->moveDownPriority($courseCategory->parent_id, $courseCategory->sort_priority);
        $this->moveUpPriority($data['parent_id'], $data['sort_priority']);
        $this->setToDb($data);
        $this->db->set('update_date', 'now()', false);
        $this->db->set('update_by', $this->session->userdata('admin_id'));
        $this->db->where('course_category_id', $courseCategoryId);
        $this->db->update('tbl_course_category');
        $this->LogActionModel->insertLog('coursecategory',$courseCategoryId,'update', 'update course category ', $data, $this->db->last_query());
    }
    public function delete($courseCategoryId){
        $courseCategory = $this->getData($courseCategoryId);
        if(!$courseCategory){
            return false;
        }
        $this->moveDownPriority($courseCategory->parent_id ,$courseCategory->sort_priority);
        $this->db->where('course_category_id', $courseCategoryId);
        $this->db->delete('tbl_course_category');
        $this->LogActionModel->insertLog('coursecategory',$courseCategoryId,'delete', 'delete course category ', array(), $this->db->last_query());
    }
    private function setToDb($data){
        $this->db->set('parent_id', $data['parent_id']);
        $this->db->set('course_category_name_th', $data['course_category_name_th']);
        $this->db->set('course_category_name_en', $data['course_category_name_en']);
        $this->db->set('sort_priority', $data['sort_priority']);
    }
    private function moveUpPriority($parentId, $sortPriority){
        $this->db->set('sort_priority', 'sort_priority + 1', false);
        $this->db->where('sort_priority >=', $sortPriority);
        $this->db->where('parent_id', $parentId);
        $this->db->update('tbl_course_category');
    }
    private function moveDownPriority($parentId, $sortPriority){
        $this->db->set('sort_priority', 'sort_priority - 1', false);
        $this->db->where('sort_priority >=', $sortPriority);
        $this->db->where('parent_id', $parentId);
        $this->db->update('tbl_course_category');
    }
}
