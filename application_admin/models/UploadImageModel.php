<?php

class UploadImageModel extends CI_Model {
    var $uploadPath = './uploads/';
    var $thumbWidth = 200;
    var $thumbHeight = 200;
    function __construct() {
        parent::__construct();
    }
    public function setThumb($width = 200, $height = 200){
        $this->thumbWidth = $width;
        $this->thumbHeight = $height;
    }
    public function uploadSingleImage($prefix, $fieldName = 'file'){
        $retVal = array();
        $config['file_name'] = $this->genImageName($prefix);
        $config['upload_path'] = $this->uploadPath;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '3000';
        $config['max_width']  = '2048';
        $config['max_height']  = '2048';
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($fieldName)){
            $retVal = array('success'=>false, 'message'=>$this->upload->display_errors(), 'imageData'=>'');
        }else{
            $imageData = $this->upload->data();
            $retVal = array(
                'success' => true,
                'message' => 'success',
                'imageFile' => $imageData['file_name']
                );
        }
        return $retVal;
    }
    public function uploadImage($prefix, $fieldName = 'file'){
        $retVal = array();
        $config['file_name'] = $this->genImageName($prefix);
        $config['upload_path'] = $this->uploadPath;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '3000';
        $config['max_width']  = '2048';
        $config['max_height']  = '2048';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($fieldName)){
            $retVal = array('success'=>false, 'message'=>$this->upload->display_errors(), 'imageData'=>'');
        }else{
            $imageData = $this->upload->data();
            $imageWidth = $imageData['image_width'];
            $imageHeight = $imageData['image_height'];
            $imageMedium = $imageData['raw_name'].'_m'.$imageData['file_ext'];
            $imageSmall = $imageData['raw_name'].'_s'.$imageData['file_ext'];
            $this->resizeImage($this->uploadPath.$imageData['file_name'], $this->uploadPath.$imageMedium, 800, 600);
            $this->resizeAndCropImage($this->uploadPath.$imageData['file_name'], $this->uploadPath.$imageSmall, $imageWidth, $imageHeight, $this->thumbWidth, $this->thumbHeight);
            $retVal = array(
                'success'=>true, 
                'message'=>'success', 
                'imageLarge'=>$imageData['file_name'],
                'imageMedium'=>$imageMedium,
                'imageSmall'=>$imageSmall,
                'imageName'=>$imageData['client_name']
                );
        }
        return $retVal;
    }
    private function genImageName($prefix){
        $thedate = date('Y/m/d H:i:s');
        $replace = array(":"," ","/");
        return $prefix.'_'.str_ireplace($replace, "", $thedate);
    }
    private function resizeImage($sourceImage, $newImage, $width, $height){
        $this->load->library('image_lib');
        $config['source_image'] = $sourceImage;
        $config['new_image'] = $newImage;
        $config['width'] = $width;
        $config['height'] = $height;
        $this->image_lib->initialize($config); 
        $this->image_lib->resize();
    }
    private function resizeAndCropImage($sourceImage, $newImage, $sourceWidth, $sourceHeight, $width, $height){
        $this->load->library('image_lib');
        $configResize['source_image'] = $sourceImage;
        $configResize['new_image'] = $newImage;
        $configResize['width'] = $width;
        $configResize['height'] = $height;
        $dim = (intval($sourceWidth) / intval($sourceHeight)) - ($width / $height);
        $configResize['master_dim'] = ($dim > 0)? "height" : "width";
        $this->image_lib->initialize($configResize);
        $this->image_lib->resize();
        $configCrop['source_image'] = $newImage;
        $configCrop['new_image'] = $newImage;
        $configCrop['width'] = $width;
        $configCrop['height'] = $height;
        $configCrop['maintain_ratio'] = false;
        $configCrop['x_axis'] = ($dim > 0)?(($sourceWidth * $height/$sourceHeight) - $width)/2:0;
        $configCrop['y_axis'] = ($dim > 0)?0:(($sourceHeight * $width/$sourceWidth) - $height)/2;
        $this->image_lib->initialize($configCrop);
        $this->image_lib->crop();
    }
    public function deleteImage($fileName){
        $this->load->helper('file');
        $path = FCPATH.'uploads/'.$fileName;
        if (file_exists($path)) {
            unlink($path) or die('failed deleting: ' . $path);
        }
    }
}