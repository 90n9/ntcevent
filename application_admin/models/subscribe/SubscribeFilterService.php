<?php

class SubscribeFilterService extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getDataContent($searchVal, $startDate, $endDate, $page = 1, $perPage = 20) {
        $data = array();
        $data['searchVal'] = $searchVal;
        $data['startDate'] = $startDate;
        $data['endDate'] = $endDate;
        $data['page'] = $page;
        $data['perPage'] = $perPage;
        $data['subscribeList'] = $this->getList($searchVal, $startDate, $endDate, $page, $perPage);
        $data['totalTransaction'] = $this->getCount($searchVal, $startDate, $endDate);
        return $data;
    }
    private function getCount($searchVal, $startDate, $endDate){
        $this->db->select('tbl_subscribe.subscribe_id');
        $this->whereTransaction($searchVal, $startDate, $endDate);
        $query = $this->db->get('tbl_subscribe');
        return $query->num_rows();
    }
    private function getList($searchVal, $startDate, $endDate, $page, $perPage){
        $this->db->select('tbl_subscribe.*');
        $this->whereTransaction($searchVal, $startDate, $endDate);
        $offset = ($page - 1) * $perPage;
        return $this->db->get('tbl_subscribe', $perPage, $offset);
    }
    private function whereTransaction($searchVal, $startDate, $endDate){
        if($searchVal != ''){
            $strWhere = "(email like '%$searchVal%' ";
            $strWhere .= ')';
            $this->db->where($strWhere, NULL,FALSE);
        }
        if($startDate != ''){
            $this->db->where('tbl_subscribe.create_date >=', $startDate);
        }
        if($endDate != ''){
            $this->db->where('tbl_subscribe.create_date <=', $endDate.' 23:59:59');
        }
        $this->db->where('is_delete', 0);
    }
}