<?php
class SubscribeModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function getData($subscribeId){
        $this->db->where('subscribe_id', $subscribeId);
        $this->db->where('is_delete', 0);
        $query = $this->db->get('tbl_subscribe');
        if($query->num_rows() > 0){
            return $query->row();
        }
        return false;
    }
    public function insert($data){
        $this->setToDb($data);
        $this->db->set('create_date', 'now()', false);
        $this->db->insert('tbl_subscribe');
        $subscribeId = $this->db->insert_id();
        $this->LogActionModel->insertLog('subscribe',$subscribeId,'insert', 'insert new subscribe', $data, $this->db->last_query());
        return $subscribeId;
    }
    public function update($subscribeId, $data){
        $this->setToDb($data);
        $this->db->where('subscribe_id', $subscribeId);
        $this->db->update('tbl_subscribe');
        $this->LogActionModel->insertLog('subscribe',$subscribeId,'update', 'update subscribe', $data, $this->db->last_query());
    }
    public function delete($subscribeId){
        $this->db->set('is_delete', 1);
        $this->db->where('subscribe_id', $subscribeId);
        $this->db->update('tbl_subscribe');
        $this->LogActionModel->insertLog('subscribe',$subscribeId,'delete', 'delete subscribe', array(), $this->db->last_query());
    }
    private function setToDb($data){
        $this->db->set('email', $data['email']);
    }
}