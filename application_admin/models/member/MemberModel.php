<?php
class MemberModel extends CI_Model {
    function __construct() {
      parent::__construct();
    }
    public function getData($memberId){
      $this->db->where('member_id', $memberId);
      $query = $this->db->get('tbl_member');
      if($query->num_rows() > 0){
          return $query->row();
      }
      return false;
    }
    public function getName($memberId){
      $member = $this->getData($memberId);
      if($member){
        return $member->firstname.' '.$member->lastname;
      }
      return '-';
    }
    public function insert($data){
      $this->db->set('login_password', md5($data['login_password']));
      $this->setToDb($data);
      $this->db->set('create_date', 'now()', false);
      $this->db->set('create_by', $this->session->userdata('admin_id'));
      $this->db->insert('tbl_member');
      $memberId = $this->db->insert_id();
      $this->LogActionModel->insertLog('member',$memberId,'insert', 'insert new member ', $data, $this->db->last_query());
      return $memberId;
    }
    public function update($memberId, $data){
      $member = $this->getData($memberId);
      if(!$member){
        return false;
      }
      $this->setToDb($data);
      $this->db->set('update_date', 'now()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('member_id', $memberId);
      $this->db->update('tbl_member');
      $this->LogActionModel->insertLog('member', $memberId,'update', 'update member data', $data, $this->db->last_query());
    }
    public function delete($memberId){
      $this->db->set('is_delete', 1);
      $this->db->set('update_date', 'now()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('member_id', $memberId);
      $this->db->update('tbl_member');
      $this->LogActionModel->insertLog('member',$memberId,'insert', 'delete member ', array(), $this->db->last_query());
    }
    public function update_password($memberId, $loginPassword){
      $this->db->set('login_password', md5($loginPassword));
      $this->db->set('update_date', 'now()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('member_id', $memberId);
      $this->db->update('tbl_member');
      $this->LogActionModel->insertLog('member',$memberId,'changepassword', 'change password member ', array(), $this->db->last_query());
    }
    private function setToDb($data){
      $this->db->set('firstname', $data['firstname']);
      $this->db->set('lastname', $data['lastname']);
      $this->db->set('email', $data['email']);
      $this->db->set('mobile', $data['mobile']);
      $this->db->set('company', $data['company']);
      $this->db->set('is_company', $data['is_company']);
      $this->db->set('company_name', $data['company_name']);
      $this->db->set('company_address', $data['company_address']);
      $this->db->set('company_branch', $data['company_branch']);
      $this->db->set('company_tax', $data['company_tax']);
      $this->db->set('enable_status', $data['enable_status']);
      $this->db->set('is_verify', $data['is_verify']);
    }
}
