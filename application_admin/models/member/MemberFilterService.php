<?php
class MemberFilterService extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function getDataContent($searchVal, $enableStatus, $page = 1, $perPage = 20) {
        $data = array();
        $data['searchVal'] = $searchVal;
        $data['enableStatus'] = $enableStatus;
        $data['page'] = $page;
        $data['perPage'] = $perPage;
        $data['memberList'] = $this->getList($searchVal, $enableStatus, $page, $perPage);
        $data['totalTransaction'] = $this->getCount($searchVal, $enableStatus);
        return $data;
    }
    private function getCount($searchVal, $enableStatus){
        $this->db->select('tbl_member.member_id');
        $this->whereTransaction($searchVal, $enableStatus);
        $query = $this->db->get('tbl_member');
        return $query->num_rows();
    }
    private function getList($searchVal, $enableStatus, $page, $perPage){
        $this->db->select('tbl_member.*');
        $this->whereTransaction($searchVal, $enableStatus);
        $this->db->order_by('tbl_member.member_id', 'DESC');
        $offset = ($page - 1) * $perPage;
        return $this->db->get('tbl_member', $perPage, $offset);
    }
    private function whereTransaction($searchVal, $enableStatus){
        if($searchVal != ''){
            $strWhere = "(email like '%$searchVal%' ";
            $strWhere .= "OR firstname like '%$searchVal%' ";
            $strWhere .= "OR lastname like '%$searchVal%' ";
            $strWhere .= "OR mobile like '%$searchVal%' ";
            $strWhere .= "OR company like '%$searchVal%' ";
            $strWhere .= "OR company_name like '%$searchVal%' ";
            $strWhere .= "OR company_address like '%$searchVal%' ";
            $strWhere .= "OR company_branch like '%$searchVal%' ";
            $strWhere .= "OR company_tax like '%$searchVal%' ";
            $strWhere .= ')';
            $this->db->where($strWhere, NULL,FALSE);
        }
        if($enableStatus != 'all'){
            $this->db->where('tbl_member.enable_status', $enableStatus);
        }
        $this->db->where('is_delete', 0);
    }
}
