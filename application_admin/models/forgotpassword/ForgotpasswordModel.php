<?php
class ForgotpasswordModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function getData($paymentId){
        $this->db->where('payment_id', $paymentId);
        $this->db->where('is_delete', 0);
        $query = $this->db->get('tbl_payment');
        if($query->num_rows() > 0){
            return $query->row();
        }
        return false;
    }
    public function insert($data){
        $this->setToDb($data);
        $this->db->set('create_date', 'now()', false);
        $this->db->set('create_by', $this->session->userdata('admin_id'));
        $this->db->insert('tbl_payment');
        $subscribeId = $this->db->insert_id();
        return $subscribeId;
    }
    public function update($paymentId, $data){
        $this->setToDb($data);
        $this->db->set('update_date', 'now()', false);
        $this->db->set('update_by', $this->session->userdata('admin_id'));
        $this->db->where('payment_id', $paymentId);
        $this->db->update('tbl_payment');
    }
    public function delete($paymentId){
        $this->db->set('is_delete', 1);
        $this->db->where('payment_id', $paymentId);
        $this->db->update('tbl_payment');
    }
    public function setToDb($data){
        $this->db->set('speaker_name', $data['speaker_name']);
        $this->db->set('speaker_detail', $data['speaker_detail']);
    }
}