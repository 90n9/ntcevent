<?php
class Offer_model extends CI_Model{
  public function get_list(){
    $this->db->where('is_delete', 0);
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_offer');
  }
  public function get_data($offer_id){
    $this->db->where('offer_id', $offer_id);
    $query = $this->db->get('tbl_offer');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function get_max_priority(){
    $this->db->select('offer_id');
    $this->db->where('is_delete', 0);
    $query = $this->db->get('tbl_offer');
    return $query->num_rows();
  }
  public function insert($data){
    $this->move_up_priority($data['sort_priority']);
    $this->set_to_db($data);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_offer');
    return $this->db->insert_id();
  }
  public function update($offer_id, $data){
    $offer = $this->get_data($offer_id);
    if(!$offer){
      return false;
    }
    $this->move_down_priority($offer->sort_priority);
    $this->move_up_priority($data['sort_priority']);
    $this->set_to_db($data);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('offer_id', $offer_id);
    $this->db->update('tbl_offer');
  }
  public function delete($offer_id){
    $offer = $this->get_data($offer_id);
    if(!$offer){
      return false;
    }
    $this->move_down_priority($offer->sort_priority);
    $this->db->set('is_delete', 1);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('offer_id', $offer_id);
    $this->db->update('tbl_offer');
  }
  public function set_to_db($data){
    if($data['offer_image'] != ''){
      $this->db->set('offer_image', $data['offer_image']);
    }
    $this->db->set('offer_content_th', $data['offer_content_th']);
    $this->db->set('offer_content_en', $data['offer_content_en']);
    $this->db->set('enable_status', $data['enable_status']);
    $this->db->set('sort_priority', $data['sort_priority']);
  }
  private function move_up_priority($sort_priority){
      $this->db->set('sort_priority', 'sort_priority + 1', false);
      $this->db->where('sort_priority >=', $sort_priority);
      $this->db->where('is_delete', 0);
      $this->db->update('tbl_offer');
  }
  private function move_down_priority($sort_priority){
      $this->db->set('sort_priority', 'sort_priority - 1', false);
      $this->db->where('sort_priority >', $sort_priority);
      $this->db->where('is_delete', 0);
      $this->db->update('tbl_offer');
  }
}
