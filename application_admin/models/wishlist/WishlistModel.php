<?php
class WishlistModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function getListByCourse($courseId){
        $this->db->where('course_id', $courseId);
        $this->db->order_by('tbl_member_wishlist.create_date', 'ASC');
        return $this->db->get('tbl_member_wishlist');
    }
    public function getListByMember($memberId){
        $this->db->where('member_id', $memberId);
        $this->db->order_by('tbl_member_wishlist.create_date', 'ASC');
        return $this->db->get('tbl_member_wishlist');
    }
}