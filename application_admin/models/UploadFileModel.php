<?php

class UploadFileModel extends CI_Model {
    var $uploadPath = './uploads/';
    function __construct() {
        parent::__construct();
    }
    public function upload($prefix, $fieldName = 'file'){
        $retVal = array();
        $config['file_name'] = $this->genImageName($prefix);
        $config['upload_path'] = $this->uploadPath;
        $config['allowed_types'] = 'csv|pdf|xls|xlsx|ppt|pptx|gtar|gz|gzip|zip|rar|txt|rtx|rtf|doc|docx|word|xl|7zip|gif|jpg|png|jpeg';
        $config['max_size'] = '1000000';
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($fieldName)){
            $retVal = array('success'=>false, 'message'=>$this->upload->display_errors(), 'file_name'=>'');
        }else{
            $uploadData = $this->upload->data();
            $retVal = array(
                'success' => true,
                'message' => 'success',
                'file_name' => $uploadData['file_name']
                );
        }
        return $retVal;
    }
    private function genImageName($prefix){
        $thedate = date('Y/m/d H:i:s');
        $replace = array(":"," ","/");
        return $prefix.'_'.str_ireplace($replace, "", $thedate);
    }
    public function deleteFile($fileName){
        $this->load->helper('file');
        $path = FCPATH.'uploads/'.$fileName;
        if (file_exists($path)) {
            unlink($path) or die('failed deleting: ' . $path);
        }
    }
}