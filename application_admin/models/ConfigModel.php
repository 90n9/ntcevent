<?php
class ConfigModel extends CI_Model{
  public function getData($code){
    $this->db->select('config_value');
    $this->db->where('config_code', $code);
    $query = $this->db->get('tbl_config');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function getValue($code){
    $this->db->select('config_value');
    $this->db->where('config_code', $code);
    $query = $this->db->get('tbl_config');
    if($query->num_rows() == 0){
      return '-';
    }
    return $query->row()->config_value;
  }
  public function update($code, $value){
    $this->db->set('config_value', $value);
    $this->db->where('config_code', $code);
    $this->db->update('tbl_config');
  }
}
