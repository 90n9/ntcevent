<?php
class AdminModel extends CI_Model {
    var $loginKey = 'ntc3v3nt';
    function __construct() {
        parent::__construct();
    }
    public function getLoginNameById($adminId){
        $admin = $this->getData($adminId);
        if($admin){
            return $admin->name;
        }
        return '-';
    }
    public function getData($adminId){
        $this->db->where('admin_id', $adminId);
        $query = $this->getAdminDb();
        return ($query->num_rows() > 0)?$query->row():false;
    }
    public function getLogin($loginName, $loginPassword){
        $this->db->where('login_name', $loginName);
        $this->db->where('login_password', $this->encryptPassword($loginPassword));
        $query = $this->getAdminDb();
        return ($query->num_rows() > 0)?$query->row():false;
    }
    private function getAdminDb(){
        $this->db->select('tbl_admin.*');
        $this->db->where('is_delete', 0);
        return $this->db->get('tbl_admin');
    }
    public function setLastLogin($adminId){
        $this->db->set('last_login_date', 'NOW()', false);
        $this->db->where('admin_id', $adminId);
        $this->db->update('tbl_admin');
    }
    public function insert($data){
        $this->setToDb($data);
        $this->db->set('login_password', $this->encryptPassword($data['login_password']));
        $this->db->set('create_date', 'now()', false);
        $this->db->set('create_by', $this->session->userdata('admin_id'));
        $this->db->insert('tbl_admin');
        $adminId = $this->db->insert_id();
        $this->LogActionModel->insertLog('admin',$adminId,'insert', 'create new admin', $data, $this->db->last_query());
        return $adminId;
    }
    public function update($adminId, $data){
        $this->setToDb($data);
        $this->db->set('update_date', 'now()', false);
        $this->db->set('update_by', $this->session->userdata('admin_id'));
        $this->db->where('admin_id', $adminId);
        $this->db->update('tbl_admin');
        $this->LogActionModel->insertLog('admin',$adminId,'update', 'update admin', $data, $this->db->last_query());
    }
    public function delete($adminId){
        $this->db->set('is_delete', 1);
        $this->db->where('admin_id', $adminId);
        $this->db->update('tbl_admin');
        $this->LogActionModel->insertLog('admin',$adminId,'delete', 'delete admin', array(), $this->db->last_query());
    }
    public function updatePassword($adminId, $loginPassword){
        $this->db->set('login_password', $this->encryptPassword($loginPassword));
        $this->db->set('update_date', 'now()', false);
        $this->db->set('update_by', $this->session->userdata('admin_id'));
        $this->db->where('admin_id', $adminId);
        $this->db->update('tbl_admin');
        $this->LogActionModel->insertLog('admin',$adminId,'change password', 'change password admin', array(), $this->db->last_query());
    }
    private function setToDb($data){
        $this->db->set('name', $data['name']);
        $this->db->set('email', $data['email']);
        $this->db->set('login_name', $data['login_name']);
        $this->db->set('enable_status', $data['enable_status']);
    }
    public function editProfile($data){
        $this->db->set('name', $data['name']);
        $this->db->set('email', $data['email']);
        $this->db->set('update_date', 'now()', false);
        $this->db->set('update_by', $this->session->userdata('admin_id'));
        $this->db->where('admin_id', $this->session->userdata('admin_id'));
        $this->db->update('tbl_admin');
        $this->LogActionModel->insertLog('admin',$adminId,'editprofile', 'edit profile', $data, $this->db->last_query());
    }
    private function encryptPassword($loginPassword){
        return md5($this->loginKey.$loginPassword);
    }
}