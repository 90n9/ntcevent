<?php
class AdminFilterService extends CI_Model {
  function __construct() {
    parent::__construct();
  }

  public function getDataContent() {
    $data = array();
    $data['adminList'] = $this->getList();
    $data['totalTransaction'] = $this->getCount();
    return $data;
  }
  private function getCount(){
    $this->db->select('tbl_admin.admin_id');
    $this->whereTransaction();
    $query = $this->db->get('tbl_admin');
    return $query->num_rows();
  }
  private function getList(){
    $this->db->select('tbl_admin.*');
    $this->whereTransaction();
    return $this->db->get('tbl_admin');
  }
  private function whereTransaction(){
    $this->db->where('is_delete', 0);
  }
}
