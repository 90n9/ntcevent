$(document).ready(function() {
    $("#searchForm").submit(function(event) {
        event.preventDefault();
        var action = $(this).attr("action");
        onSearchSubmit(action);
    });
    $('#create_period').datepicker({
      inputs: $('.create_range'),
      format: 'yyyy-mm-dd',
      todayHighlight: true,
      todayBtn: true
    });
    $("#searchForm").submit();
});
function onSearchSubmit(action) {
    $("#result").empty().append('Loading ...');
    var searchVal = $('#txtSearchVal').val();
    var paymentStatus = $('#ddlPaymentStatus').val();
    var paymentType = $('#ddlPaymentType').val();
    var startDate = $('#txtStartDate').val();
    var endDate = $('#txtEndDate').val();
    getContent(action, searchVal, paymentStatus, paymentType, startDate, endDate, 1);
}
function getContent(action, searchVal, paymentStatus, paymentType, startDate, endDate, page){
    $("#result").empty().append('Loading ...');
    var posting = $.post(action, {
        searchVal: searchVal,
        paymentStatus: paymentStatus,
        paymentType: paymentType,
        startDate: startDate,
        endDate: endDate,
        page: page
    });
    posting.done(function(data) {
        $("#result").empty().append(data);
        setOnChangePage();
    });
    posting.fail(function(data){
        setTimeout(function() { getContent(action, searchVal, paymentStatus, paymentType, startDate, endDate, page); }, 5000);
    });
}
function setOnChangePage(){
    $('.ddlPager').change(function(){
        var page = $(this).val();
        var action = $(this).data('action');
        var searchVal = $(this).data('searchval');
        var paymentStatus = $(this).data('paymentstatus');
        var paymentType = $(this).data('paymenttype');
        var startDate = $(this).data('startdate');
        var endDate = $(this).data('enddate');
        getContent(action, searchVal, paymentStatus, paymentType, startDate, endDate, page);
    });
}
