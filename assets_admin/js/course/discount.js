$(document).ready(function(){
    addEventListener();
});
function addEventListener(){
    $('#txtAvailableFromDate').datepicker({format: 'yyyy-mm-dd'});
    $('#txtAvailableFromTime').timepicker({showMeridian: false});
    $('#txtAvailableToDate').datepicker({format: 'yyyy-mm-dd'});
    $('#txtAvailableToTime').timepicker({showMeridian: false});
}
