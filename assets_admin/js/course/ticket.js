$(document).ready(function(){
    addEventListener();
    setupTextarea();
    setDescription();
    setPrice();
});
function addEventListener(){
    $('#chkShowDescription').on('change', setDescription);
    $('#ddlPriceType').on('change', setPrice);
    $('#txtStartDate').datepicker({format: 'yyyy-mm-dd'});
    $('#txtStartTime').timepicker({showMeridian: false});
    $('#txtEndDate').datepicker({format: 'yyyy-mm-dd'});
    $('#txtEndTime').timepicker({showMeridian: false});
}
function setDescription(){
    if($('#chkShowDescription').is(':checked')){
        $('#descriptionBox').slideDown();
    }else{
        $('#descriptionBox').slideUp();
    }
}
function setPrice(){
    var priceType = $('#ddlPriceType').val();
    if(priceType == 'amount'){
        $('#priceBox').slideDown();
    }else{
        $('#priceBox').slideUp();
    }
}
function setupTextarea(){
    $('.summernote').summernote({
        height: 300,
        minHeight: null,
        maxHeight: null,
        focus: true ,
        callbacks: {
            onImageUpload: function(files) {
                console.log('call on Image Load')
                url = $(this).data('upload');
                sendFile(files[0], url, $(this));
            }
        }
    });
}
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            var preview = $(input).attr('data-target');
            $(preview).attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function sendFile(file, url, editor) {
    data = new FormData();
    data.append("file", file);
    $.ajax({
        data: data,
        type: "POST",
        url:url,
        cache: false,
        contentType: false,
        processData: false,
        success: function(url) {
            console.log('upload url :'+url);
            editor.summernote('insertImage', url);
            console.log('upload complete');
        }
    });
}
