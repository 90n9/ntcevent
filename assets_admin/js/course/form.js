$(document).ready(function(){
  $('#courseDate').datepicker({
    format: 'yyyy-mm-dd',
    multidate: true,
    todayHighlight: true,
    todayBtn: true
  });
  $('#courseDate').on('changeDate', function() {
    $('#txtCourseDate').val(
      $('#courseDate').datepicker('getFormattedDate')
    );
});
  $('#ticket_event_period').datepicker({
    inputs: $('.ticket_actual_range'),
    format: 'yyyy-mm-dd',
    todayHighlight: true,
    todayBtn: true
  });
  addEventListener();
  setupTextarea();
  setCoverType();
  setTicketDescription();
  setTicketPrice();
  setSpeakerType();
  validateForm();
});
function validateForm(){
	$("form").validate({
		highlight: function( label ) {
			$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function( label ) {
			$(label).closest('.form-group').removeClass('has-error');
			label.remove();
		},
		errorPlacement: function( error, element ) {
			var placement = element.closest('.input-group');
			if (!placement.get(0)) {
				placement = element;
			}
			if (error.text() !== '') {
				placement.after(error);
			}
		}
	});
}
function addEventListener(){
    $('#chkTicketShowDescription').on('change', setTicketDescription);
    $('#ddlTicketPriceType').on('change', setTicketPrice);
    $('#ddlCoverType').on('change', setCoverType);
    $('#ddlSpeakerType').on('change', setSpeakerType);
    $('.dateinput').datepicker({format: 'yyyy-mm-dd'});
    $('.timeinput').timepicker({showMeridian: false});
}
function setTicketDescription(){
    if($('#chkTicketShowDescription').is(':checked')){
        $('#ticketDescriptionBox').slideDown();
    }else{
        $('#ticketDescriptionBox').slideUp();
    }
}
function setTicketPrice(){
    var priceType = $('#ddlTicketPriceType').val();
    if(priceType == 'amount'){
        $('.priceBox').slideDown();
    }else{
        $('.priceBox').slideUp();
    }
}
function setCoverType(){
	var coverType = $('#ddlCoverType').val();
    if(coverType == 'image'){
        $('#coverImageBox').slideDown();
        $('#coverVideoBox').slideUp();
    }else{
        $('#coverImageBox').slideUp();
        $('#coverVideoBox').slideDown();
    }
}
function setSpeakerType(){
  var speakerType = $('#ddlSpeakerType').val();
  if(speakerType == '' || speakerType == 'none'){
    $('.speaker-form-group').hide();
  }else{
    $('.speaker-form-group').show();
  }
}
function setupTextarea(){
  $('#txtDescription').summernote({
    height: 300,
    toolbar: [
      ['style', ['style']],
      ['font', ['bold', 'underline', 'clear']],
      ['fontname', ['fontname']],
      ['fontsize', ['fontsize']],
      ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['height', ['height']],
      ['table', ['table']],
      ['insert', ['link', 'picture', 'video']],
      ['view', ['fullscreen', 'codeview', 'help']]
    ],
    minHeight: null,
    maxHeight: null,
    callbacks: {
      onImageUpload: function(files) {
        url = $(this).data('upload');
        sendFile(files[0], url, $(this));
      }
    }
  });
  $('#txtTicketDescription').summernote({
    height: 100,
    toolbar: [
      ['style', ['style']],
      ['font', ['bold', 'underline', 'clear']],
      ['fontname', ['fontname']],
      ['fontsize', ['fontsize']],
      ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['height', ['height']],
      ['table', ['table']],
      ['insert', ['link', 'picture', 'video']],
      ['view', ['fullscreen', 'codeview', 'help']]
    ],
    minHeight: null,
    maxHeight: null,
    callbacks: {
      onImageUpload: function(files) {
        url = $(this).data('upload');
        sendFile(files[0], url, $(this));
      }
    }
  });
}
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            var preview = $(input).attr('data-target');
            $(preview).attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function sendFile(file, url, editor) {
    data = new FormData();
    data.append("file", file);
    $.ajax({
        data: data,
        type: "POST",
        url:url,
        cache: false,
        contentType: false,
        processData: false,
        success: function(url) {
            console.log('upload url :'+url);
            editor.summernote('insertImage', url);
            console.log('upload complete');
        }
    });
}
