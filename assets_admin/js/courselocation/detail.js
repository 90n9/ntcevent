function initialize() {
  var mapOptions = {
    zoom: 15
  };
  console.log('call map canvas');
  var map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);
  var lad = document.getElementById("txtLad").value;
  var lon = document.getElementById("txtLon").value;
  initialLocation = new google.maps.LatLng(lad,lon);
  map.setCenter(initialLocation);
  var marker = new google.maps.Marker({
      position: initialLocation,
      map: map,
      title:"Location!"
  });
}

initialize();