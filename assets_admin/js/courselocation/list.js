$(document).ready(function() {
    $("#searchForm").submit(function(event) {
        event.preventDefault();
        var action = $(this).attr("action");
        onSearchSubmit(action);
    });
    $("#searchForm").submit();
});
function onSearchSubmit(action) {
    $("#result").empty().append('Loading ...');
    getContent(action, 1);
}
function getContent(action, page){
    $("#result").empty().append('Loading ...');
    var posting = $.post(action, {
        page: page
    });
    posting.done(function(data) {
        $("#result").empty().append(data);
        setOnChangePage();
    });
    posting.fail(function(data){
        setTimeout(function() { getContent(action, page); }, 5000);
    });
}
function setOnChangePage(){
    $('.ddlPager').change(function(){
        var page = $(this).val();
        var action = $(this).data('action');
        getContent(action, page);
    });
}
