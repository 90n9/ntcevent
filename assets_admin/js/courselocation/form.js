/* Google Map */
var map;
var marker;
function initialize() {
  var mapOptions = {
    zoom: 15
  };
  map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);
  var lad = document.getElementById("txtLad").value;
  var lon = document.getElementById("txtLon").value;
  initialLocation = new google.maps.LatLng(lad,lon);
  map.setCenter(initialLocation);
  marker = new google.maps.Marker({
      position: initialLocation,
      map: map,
      draggable:true,
      title:"Location!"
  });
  google.maps.event.addListener(marker, 'dragend', function(evt){
    document.getElementById("txtLad").value = evt.latLng.lat();
    document.getElementById("txtLon").value = evt.latLng.lng();
  });
}
function updateMarker(){
    var lad = $('#txtLad').val();
    var lon = $('#txtLon').val();
    if(isNaN(lad) || isNaN(lon)){
        console.log('input lad and lon location is not valid');
        return false;
    }
    var updateLocation = new google.maps.LatLng(lad,lon);
    map.setCenter(updateLocation);
    marker.setPosition(updateLocation);
}


initialize();