$(document).ready(function(){
	onChangeParent();
});
function onChangeParent(){
	$('#ddlParent').on('change', function(){
		var maxPriority = $(this).find(':selected').data('maxpriority');
		console.log('change max priority: '+maxPriority);
		$('#ddlSortPriority').html('');
		for(var i = 1; i<= maxPriority; i++){
			$('#ddlSortPriority').append('<option value="'+i+'">'+i+'</option>')
		}
	});
}