$(document).ready(function(){
    setupTextarea();
});
function setupTextarea(){
    $('.summernote').summernote({
        height: 200,
        toolbar: [
          ['style', ['style']],
          ['font', ['bold', 'underline', 'clear']],
          ['fontname', ['fontname']],
          ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['height', ['height']],
          ['table', ['table']],
          ['insert', ['link', 'picture', 'video']],
          ['view', ['fullscreen', 'codeview', 'help']]
        ],
        minHeight: null,
        maxHeight: null,
        focus: true ,
        callbacks: {
            onImageUpload: function(files) {
            	console.log('call on Image Load')
                url = $(this).data('upload');
                sendFile(files[0], url, $(this));
            }
        }
    });
}
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            var preview = $(input).attr('data-target');
            $(preview).attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function sendFile(file, url, editor) {
    data = new FormData();
    data.append("file", file);
    $.ajax({
        data: data,
        type: "POST",
        url:url,
        cache: false,
        contentType: false,
        processData: false,
        success: function(url) {
            console.log('upload url :'+url);
            editor.summernote('insertImage', url);
            console.log('upload complete');
        }
    });
}
