$(document).ready(function() {
    $("#searchForm").submit(function(event) {
        event.preventDefault();
        var action = $(this).attr("action");
        onSearchSubmit(action);
    });
    $("#searchForm").submit();
});
function onSearchSubmit(action) {
    $("#result").empty().append('Loading ...');
    var searchVal = $('#txtSearchVal').val();
    getContent(action, searchVal, 1);
}
function getContent(action, searchVal, page){
    $("#result").empty().append('Loading ...');
    var posting = $.post(action, {
        searchVal: searchVal,
        page: page
    });
    posting.done(function(data) {
        $("#result").empty().append(data);
        setOnChangePage();
    });
    posting.fail(function(data){
        setTimeout(function() { getContent(action, searchVal, page); }, 5000);
    });
}
function setOnChangePage(){
    $('.ddlPager').change(function(){
        var page = $(this).val();
        var action = $(this).data('action');
        var searchVal = $(this).data('searchval');
        getContent(action, searchVal, page);
    });
}
