$(document).ready(function() {
    $("#searchForm").submit(function(event) {
        event.preventDefault();
        var action = $(this).attr("action");
        onSearchSubmit(action);
    });
    $('#txtStartDate').datepicker({format: 'yyyy-mm-dd'});
    $('#txtEndDate').datepicker({format: 'yyyy-mm-dd'});
    $("#searchForm").submit();
});
function onSearchSubmit(action) {
    $("#result").empty().append('Loading ...');
    var searchVal = $('#txtSearchVal').val();
    var startDate = $('#txtStartDate').val();
    var endDate = $('#txtEndDate').val();
    getContent(action, searchVal, startDate, endDate, 1);
}
function getContent(action, searchVal, startDate, endDate, page){
    $("#result").empty().append('Loading ...');
    var posting = $.post(action, {
        searchVal: searchVal,
        startDate: startDate,
        endDate: endDate,
        page: page
    });
    posting.done(function(data) {
        $("#result").empty().append(data);
        setOnChangePage();
    });
    posting.fail(function(data){
        setTimeout(function() { getContent(action, searchVal, startDate, endDate, page); }, 5000);
    });
}
function setOnChangePage(){
    $('.ddlPager').change(function(){
        var page = $(this).val();
        var action = $(this).data('action');
        var searchVal = $(this).data('searchval');
        var startDate = $(this).data('startdate');
        var endDate = $(this).data('enddate');
        getContent(action, searchVal, startDate, endDate, page);
    });
}
