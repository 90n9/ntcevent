$(document).ready(function() {
    validationForm();
});
function validationForm(){
    $('#admin-form').validate({
        rules: {
            txtLoginName: {
                required: true,
                minlength: 4
            },
            txtPassword: {
                required: true,
                minlength: 6
            },
            txtConfirmPassword: {
                required: true,
                minlength: 6,
                equalTo: "#txtPassword"
            },
            txtEmail: {
                email: true
            },
        },
        onfocusout: injectTrim($.validator.defaults.onfocusout),
    });
}
function injectTrim(handler) {
    return function (element, event) {
        if (element.tagName === "TEXTAREA" || (element.tagName === "INPUT" && element.type !== "password")) {
            element.value = $.trim(element.value);
        }
        return handler.call(this, element, event);
    };
}