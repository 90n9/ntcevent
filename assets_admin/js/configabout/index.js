$(document).ready(function(){
  onChangeType();
  $('input[name=rdoAboutType]').on('click', onChangeType);
});
function onChangeType(){
  var $rdoAboutType = $('input[name=rdoAboutType]:checked').val();
  $('#form-group-img').hide();
  $('#form-group-video').hide();
  if($rdoAboutType == 'img'){
    $('#form-group-img').show();
  }else if($rdoAboutType == 'video'){
    $('#form-group-video').show();
  }
}
