$(document).ready(function() {
    $("#searchForm").submit(function(event) {
        event.preventDefault();
        var action = $(this).attr("action");
        onSearchSubmit(action);
    });
    $("#searchForm").submit();
});
function onSearchSubmit(action) {
    $("#result").empty().append('Loading ...');
    var searchVal = $('#txtSearchVal').val();
    var enableStatus = $('#ddlEnableStatus').val();
    getContent(action, searchVal, enableStatus, 1);
}
function getContent(action, searchVal, enableStatus, page){
    $("#result").empty().append('Loading ...');
    var posting = $.post(action, {
        searchVal: searchVal,
        enableStatus: enableStatus,
        page: page
    });
    posting.done(function(data) {
        $("#result").empty().append(data);
        setOnChangePage();
    });
    posting.fail(function(data){
        setTimeout(function() { getContent(action, searchVal, enableStatus, page); }, 5000);
    });
}
function setOnChangePage(){
    $('.ddlPager').change(function(){
        var page = $(this).val();
        var action = $(this).data('action');
        var searchVal = $(this).data('searchval');
        var enableStatus = $(this).data('enablestatus');
        getContent(action, searchVal, enableStatus, page);
    });
}
