$(document).ready(function() {
  checkDisplayCompany();
  $('input[name=rdoCompany]').on('change', checkDisplayCompany);
  validationForm();
});
function checkDisplayCompany(){
  var isCompany = $('input[name=rdoCompany]:checked').val()
  if(isCompany == 1){
    $('#company-payment-form-group').slideDown();
  }else{
    $('#company-payment-form-group').slideUp();
  }
}
function validationForm(){
    $('#admin-form').validate({
        rules: {
            txtLoginName: {
                required: true,
                minlength: 4
            },
            txtPassword: {
                required: true,
                minlength: 6
            },
            txtConfirmPassword: {
                required: true,
                minlength: 6,
                equalTo: "#txtPassword"
            },
            txtEmail: {
                email: true
            },
        },
        onfocusout: injectTrim($.validator.defaults.onfocusout),
    });
}
function injectTrim(handler) {
    return function (element, event) {
        if (element.tagName === "TEXTAREA" || (element.tagName === "INPUT" && element.type !== "password")) {
            element.value = $.trim(element.value);
        }
        return handler.call(this, element, event);
    };
}
