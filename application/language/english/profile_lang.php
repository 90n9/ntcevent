<?php
$lang['profile_my_profile'] = 'My Profile';
$lang['profile_first_name'] = 'First Name';
$lang['profile_last_name'] = 'Last Name';
$lang['profile_email'] = 'E-Mail';
$lang['profile_mobile'] = 'Mobile';
$lang['profile_company_name'] = 'Company';

$lang['profile_payment_information'] = 'Payment Information';
$lang['profile_payment_on_behalf'] = 'Payments on behalf';
$lang['profile_payment_company'] = 'Company';
$lang['profile_payment_individual'] = 'Individual';
$lang['profile_payment_company_name'] = 'Company';
$lang['profile_payment_detail'] = '* The follow information will be on Receipt/Tax Invoice.';
$lang['profile_payment_branch'] = 'Branch';
$lang['profile_payment_company_tax'] = 'Tax ID';
$lang['profile_payment_company_address'] = 'Company address';
$lang['profile_payment_individual_address'] = 'Billing address';

$lang['profile_payment_save'] = 'Save';
