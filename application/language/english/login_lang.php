<?php
$lang['login_error_no_user_name'] = 'Please fill your email';
$lang['login_error_no_password'] = 'Please fill your password';
$lang['login_error_no_user_found'] = 'Invalid Email or Password';
$lang['login_error_not_verify'] = 'Please verify your email';
$lang['login_error_usertype_invalid'] = 'Please contact us';
