<?php
/*** Form  ****/
$lang['contact_form_header'] = 'Contact Us';
$lang['contact_form_firstname'] = 'First name';
$lang['contact_form_lastname'] = 'Last name';
$lang['contact_form_email'] = 'Email';
$lang['contact_form_phone'] = 'Phone number';
$lang['contact_form_message'] = 'Message';
$lang['contact_form_submit'] = 'SEND MESSAGE';
/*** End Form  ****/
/*** Location Detail  ****/
$lang['contact_bts_detail'] = 'Take the Silom line and get off at Chong-Nonsi Station and go out through Exit No. 1 or 3.';
$lang['contact_mrt_detail'] = 'Take the MRT toward Hua lamphong station and get off at Sam Yan Station and go out through Exit No. 1.';
$lang['contact_bus_detail'] = '<p>Surawongse Road: Bus number 16, 93
<br />Silom Road: Bus number 77, 177, 15, 115, 76</p>';
$lang['contact_express_detail'] = '<p style="text-align:justify;">Follow Sirat Express(toll road) to Silom road and take Exit 2-11, continue on Sirom road. Then turn left onto Decho road and turn right to Surawong road. Go straight for 100 meters, turn right on to Anuman Rajdhon Alley. NTC is located at BUI building on the right.</p>';
/*** End Location Detail  ****/
