<?php
$lang['register_error_form_not_complete'] = 'Please fill out the field.';
$lang['register_error_invalid_email'] = 'Invalid email';
$lang['register_error_duplicate_email'] = 'This email address is already registered.';
