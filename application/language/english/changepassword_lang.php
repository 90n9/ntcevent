<?php
$lang['changepassword_head'] = 'Change Password';
$lang['changepassword_password'] = 'New password';
$lang['changepassword_confirm_password'] = 'Confirm new password';

$lang['changepassword_save'] = 'Save new password';
