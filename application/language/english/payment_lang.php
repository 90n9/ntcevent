<?php
$lang['baht'] = 'Baht';
$lang['payment_note'] = '
<h3>IMPORTANT</h3>
<ul style="padding-left:30px;">
  <li>Please wait 1-2 business days after <br>submit Pay-in slip for confirmation<br>email.</br></li>
  <li>If you no longer need the order as <br>placed, please cancel your order on your<br>order cart of contact 02-634-7993-4 <br>opt 12.</li>
  <li>For other payment questions contact <br>02-634-7993-4 opt 24.</li>
</ul>
';
$lang['payment_bank_header'] = 'Submit Pay-in slip';
$lang['payment_creditcard_header'] = 'Creditcard Payment';
$lang['payment_form_note'] = 'Leave us a message';
$lang['payment_submit'] = 'Submit';
$lang['payment_back'] = 'Back';

$lang['payment_order_id'] = 'Order ID';
$lang['payment_course_name'] = 'Course Name';
$lang['payment_quantity'] = 'Quantity';
$lang['payment_total_baht'] = 'Total (Baht)';
$lang['payment_total'] = 'Total';
$lang['payment_surcharge'] = 'Surcharge';
$lang['payment_vat'] = 'VAT 7%';
$lang['payment_grand_total'] = 'Grand Total';

$lang['payment_bank_transfer_instruction'] = 'Transfer Instruction';
$lang['payment_bank_account_name'] = 'A/C Name:';
$lang['payment_bank_account_name_value'] = 'Network training Center Co.,Ltd.';
$lang['payment_bank_account_number'] = 'A/C No.:';
$lang['payment_bank_amount'] = 'Amount (Baht)';
$lang['payment_bank_value_date'] = 'Value Date';
$lang['payment_bank_upload'] = 'Upload bank transfer slip';
