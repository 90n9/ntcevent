<?php
$lang['baht'] = 'Baht';
$lang['course_ticket_type'] = 'Ticket Type';
$lang['course_unit_price'] = 'Unit Price (Baht)';
$lang['course_quantity'] = 'Quantity';
$lang['course_total_baht'] = 'Total (Baht)';
$lang['course_discount'] = 'Promo Code';
$lang['course_sub_total'] = 'SubTotal';
$lang['course_vat'] = 'VAT 7%';
$lang['course_deduct_withholding_tax'] = 'Withholding Tax 3%';
$lang['course_grand_total'] = 'Grand Total';

$lang['course_billing_for_company'] = 'Billing on behalf of a company';
$lang['course_request_withholding_tax'] = 'Request Withholding Tax Deduction 3%';
$lang['course_company_information'] = 'Company Information';
$lang['course_individual_information'] = 'Billing Information';
$lang['course_company_note'] = 'The follow information will be on Receipt/Tax Invoice.';

$lang['course_ticket_buyer_information'] = 'Ticket Buyer\'s information';
$lang['course_ticket_form_header'] = 'Ticket';
$lang['course_ticket_form_same_buyer'] = 'Use Ticket Buyer\'s information.';
$lang['course_payment_header'] = 'Payment';
$lang['course_choose_payment'] = 'Choose payment method';
$lang['course_payment_bank_transfer'] = 'Bank Transfer';
$lang['course_payment_creditcard'] = 'Credit Card/ Debit Card';
$lang['course_bank_header'] = 'Bank Account Information';
$lang['course_bank_account_name'] = 'A/C Name:';
$lang['course_bank_account_name_value'] = 'Network Training Center Co.,Ltd.';
$lang['course_bank_account_number'] = 'A/C No.:';
$lang['course_bank_note'] = 'Please submit a copy of your Pay-in Slip to confirm your registration within 7 days.<br />If any issue occurs, please contact us at 02-6347993#12. Otherwise, your registration will be cancelled.';
$lang['course_creditcard_header'] = 'Credit Card / Debit Card Payment';
$lang['course_creditcard_surcharges'] = 'Surcharges';

$lang['course_contact_request'] = 'Request for quotation';
$lang['course_contact_request_firstname'] = 'First Name';
$lang['course_contact_request_lastname'] = 'Last Name';
$lang['course_contact_request_phone'] = 'Phone';
$lang['course_contact_request_mobile'] = 'Mobile';
$lang['course_contact_request_company'] = 'Company';
$lang['course_contact_request_attendee'] = 'Number of attendee';
$lang['course_contact_request_message'] = 'Leave us a message';

$lang['no_ticket_found'] = 'For more information, please call +66 (0) 2634-7993-4';

$lang['promo_error_condition_not_met'] = 'Promo code does not meet the condition specified.';
$lang['promo_error_expired'] = 'Promo code is past the expiration date.';
$lang['promo_error_one_time_use'] = 'Promo code has already been applied once.';
