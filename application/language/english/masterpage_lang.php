<?php
$lang['main_menu_home'] = 'Home';
$lang['main_menu_course'] = 'Courses';
$lang['main_menu_about'] = 'About Us';
$lang['main_menu_contact'] = 'Contact Us';
$lang['main_menu_privacy'] = 'Privacy Policy';
$lang['main_menu_terms'] = 'Term of service';
$lang['main_menu_register'] = 'Sign up';
$lang['main_menu_signin'] = 'Login';

$lang['main_manu_promotion'] = 'Promotions';
$lang['main_manu_howtobuy'] = 'How to Buy';
$lang['user_menu_mycourse'] = 'My Courses';
$lang['user_menu_wishlist'] = 'WISHLIST';
$lang['user_menu_profile'] = 'My Profile';
$lang['user_menu_changepassword'] = 'Change Password';
$lang['user_menu_logout'] = 'Logout';
//footer
$lang['link_main'] = 'Main Website';
$lang['link_tms'] = 'Test Management System (TMS)';
$lang['working_hours'] = 'Mon - Fri / 9 AM - 5 PM';
$lang['questions?'] = 'Email:';
//popup
$lang['popup_link_login'] = 'Login';
$lang['popup_link_sign_up'] = 'Sign up';
$lang['popup_link_forgotpassword'] = 'Forgot Password?';
//popup login
$lang['popup_login_header'] = 'LOGIN';
$lang['popup_login_button'] = 'SUBMIT';
$lang['popup_login_input_email'] = 'Email';
$lang['popup_login_input_password'] = 'Password';
//popup Signup
$lang['popup_sign_up_header'] = 'SIGN UP';
$lang['popup_sign_up_button'] = 'SUBMIT';
$lang['popup_sign_up_input_email'] = 'Email *';
$lang['popup_sign_up_input_password'] = 'Password *';
$lang['popup_sign_up_input_firstname'] = 'First name *';
$lang['popup_sign_up_input_lastname'] = 'Last name *';
$lang['popup_sign_up_input_mobile'] = 'Mobile phone number';
$lang['popup_sign_up_input_company'] = 'Company';
//popup forgotpassword
$lang['popup_forgotpassword_header'] = 'FORGOT PASSWORD';
$lang['popup_forgotpassword_button'] = 'SUBMIT';
$lang['popup_forgotpassword_input_email'] = 'Email';
