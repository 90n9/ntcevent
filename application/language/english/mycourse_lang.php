<?php
$lang['baht'] = 'Baht';
$lang['price'] = 'price';
$lang['mycourse_order_id'] = 'Order ID';
$lang['mycourse_status'] = 'Status';
$lang['mycourse_quantity'] = 'Quantity';
$lang['mycourse_total'] = 'Total';
$lang['mycourse_discount'] = 'Discount';
$lang['mycourse_vat'] = 'VAT 7%';
$lang['mycourse_withholding_tax'] = 'Withholding Tax 3%';
$lang['mycourse_sub_total'] = 'Sub Total';
$lang['mycourse_grand_total'] = 'Grand Total';
$lang['mycourse_buyer_name'] = 'Buyer Name';
$lang['mycourse_email'] = 'Email';
$lang['mycourse_mobile'] = 'Mobile Phone Number';
$lang['mycourse_company_name'] = 'Company Name';
$lang['mycourse_course_detail'] = 'Course Detail';
$lang['mycourse_submit_payin'] = 'Submit Pay-in Slip';
$lang['mycourse_payment_creditcard'] = 'Online Payment';
$lang['mycourse_ticket_type'] = 'Ticket Type';
$lang['mycourse_attendee_information'] = 'Attendee Information';
$lang['mycourse_print'] = 'PRINT';


$lang['mycourse_list_view_all_orders'] = 'View all orders';
$lang['mycourse_list_waiting_for_payment'] = 'Waiting for Payment';
$lang['mycourse_list_waiting_for_approval'] = 'Waiting for Approval';
$lang['mycourse_list_completed_orders'] = 'Completed Orders';
$lang['mycourse_list_cancelled_orders'] = 'Cancelled Orders';

$lang['mycourse_list_all_orders_detail'] = 'View all orders';
$lang['mycourse_list_waiting_for_payment_detail'] = 'Choose your method: Bank Transfer or Online Payment';
$lang['mycourse_list_waiting_for_approval_detail'] = 'After administrator proof of payment, order status will be changed automatically within 1-2 business days. International wire transfer may take 3-5 business days to complete.';
$lang['mycourse_list_completed_orders_detail'] = 'Please print your ticket.';


$lang['mycourse_list_waiting_for_payment_bank'] = 'Submit Pay-in Silp';
$lang['mycourse_list_waiting_for_payment_creditcard'] = 'Online Payment';

$lang['mycourse_list_order_id'] = 'Order ID';
$lang['mycourse_list_course_name'] = 'Course Name';
$lang['mycourse_list_date_of_purchase'] = 'Date of Purchase';
$lang['mycourse_list_schedule'] = 'Schedule';
$lang['mycourse_list_status'] = 'Status';
$lang['mycourse_list_grand_total'] = 'Grand Total (Baht)';
$lang['mycourse_list_order_details'] = 'Order Details';
$lang['mycourse_list_submit_pay_in_silp'] = 'Submit Pay-in Silp';
$lang['mycourse_list_cancel_order'] = 'Cancel Order';
$lang['mycourse_list_status_cancel'] = 'Cancelled';
$lang['mycourse_list_status_waiting'] = 'Waiting for Payment';
$lang['mycourse_list_status_payment'] = 'Waiting for Approval';
$lang['mycourse_list_status_complete'] = 'Completed';

$lang['mycourse_list_all_orders_noshow_detail'] = 'We can\'t find any registered courses.';
$lang['mycourse_list_waiting_for_payment_noshow_detail'] = 'We can\'t find any "Waiting for Payment" orders.';
$lang['mycourse_list_waiting_for_approval_noshow_detail'] = 'We can\'t find any "Waiting for Approval" orders.';
$lang['mycourse_list_completed_orders_noshow_detail'] = 'We can\'t find any completed orders.';
$lang['mycourse_list_cancelled_orders_noshow_detail'] = 'We can\'t find any cancelled orders.';

$lang['mycourse_detail_cancel_name'] = 'Cancel Order';
$lang['mycourse_detail_cancel_detail'] = 'Are you sure you want to cancel this order?';
$lang['mycourse_detail_cancel_yes'] = 'Yes, cancel this order';
$lang['mycourse_detail_cancel_no'] = 'No, never mind';
