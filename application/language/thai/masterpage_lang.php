<?php
$lang['main_menu_home'] = 'หน้าแรก';
$lang['main_menu_course'] = 'คอร์สเรียน';
$lang['main_menu_about'] = 'เกี่ยวกับเรา';
$lang['main_menu_contact'] = 'ติดต่อเรา';
$lang['main_menu_privacy'] = 'นโยบายความเป็นส่วนตัว';
$lang['main_menu_terms'] = 'เงื่อนไขการใช้บริการ';
$lang['main_menu_register'] = 'สมัครสมาชิก';
$lang['main_menu_signin'] = 'เข้าสู่ระบบ';
$lang['main_manu_promotion'] = 'โปรโมชั่น';
$lang['main_manu_howtobuy'] = 'วิธีการสั่งซื้อ';
$lang['user_menu_mycourse'] = 'คอร์สเรียนของฉัน';
$lang['user_menu_wishlist'] = 'WISHLIST';
$lang['user_menu_profile'] = 'ข้อมูลส่วนตัว';
$lang['user_menu_changepassword'] = 'เปลี่ยนรหัสผ่าน';
$lang['user_menu_logout'] = 'ออกจากระบบ';
//footer
$lang['link_main'] = 'เว็บไซต์หลัก';
$lang['link_tms'] = 'เว็บไซต์ฝึกทำข้อสอบ (TMS)';
$lang['working_hours'] = 'จันทร์ - ศุกร์ 9.00 - 17.00 น.';
$lang['questions?'] = 'ติดต่อสอบถาม';
//popup
$lang['popup_link_login'] = 'เข้าสู่ระบบ';
$lang['popup_link_sign_up'] = 'สมัครสมาชิก';
$lang['popup_link_forgotpassword'] = 'ลืมรหัสผ่าน?';
//popup login
$lang['popup_login_header'] = '<span style="font-size:24px;">เข้าสู่ระบบ</span>';
$lang['popup_login_button'] = 'ยืนยัน';
$lang['popup_login_input_email'] = 'อีเมล';
$lang['popup_login_input_password'] = 'รหัสผ่าน';
//popup Signup
$lang['popup_sign_up_header'] = '<span style="font-size:24px;">สมัครสมาชิก</span>';
$lang['popup_sign_up_button'] = 'ลงทะเบียน';
$lang['popup_sign_up_input_email'] = 'อีเมล *';
$lang['popup_sign_up_input_password'] = 'รหัสผ่าน *';
$lang['popup_sign_up_input_firstname'] = 'ชื่อ *';
$lang['popup_sign_up_input_lastname'] = 'นามสกุล *';
$lang['popup_sign_up_input_mobile'] = 'เบอร์มือถือ';
$lang['popup_sign_up_input_company'] = 'ชื่อบริษัท';
//popup forgotpassword
$lang['popup_forgotpassword_header'] = '<span style="font-size:24px;">ลืมรหัสผ่าน</span>';
$lang['popup_forgotpassword_button'] = 'ยืนยัน';
$lang['popup_forgotpassword_input_email'] = 'อีเมล';
