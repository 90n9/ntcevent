<?php
/*** Form  ****/
$lang['contact_form_header'] = 'ติดต่อเรา';
$lang['contact_form_firstname'] = 'ชื่อ';
$lang['contact_form_lastname'] = 'นามสกุล';
$lang['contact_form_email'] = 'อีเมล';
$lang['contact_form_phone'] = 'เบอร์โทรศัพท์';
$lang['contact_form_message'] = 'ข้อความ';
$lang['contact_form_submit'] = 'ส่งข้อความ';
/*** End Form  ****/
/*** Location Detail  ****/
$lang['contact_bts_detail'] = 'ลงสถานีช่องนนทรี(สายสีลม) ใช้ทางออกที่ 1 หรือ 3';
$lang['contact_mrt_detail'] = 'ลงสถานีสามย่าน ใช้ทางออกที่ 1';
$lang['contact_bus_detail'] = '<p>ถนนสุรวงศ์: รถเมล์หมายเลข 16, 93
<br />ถนนสีลม: รถเมล์หมายเลข 77, 177, 15, 115, 76</p>';
$lang['contact_express_detail'] = '<p style="line-height:1.8em;">ใช้ทางพิเศษศรีรัชมุ่งสู่ถนนสีลม ออกทางออกที่ 2-11 เลี้ยวซ้ายเข้าสู่ถนนสีลมและขับตรงไปประมาณ 800 เมตร จากนั้นเลี้ยวซ้ายเขาถนนเดโช ขับตรงไปอีกประมาณ 300 เมตร และเลี้ยวขวาเข้าถนนสุรวงศ์ ให้ขับตรงไปอีกเพียง 100 เมตร เลี้ยวขวาอีกครั้ง เข้าซอยอนุมานราชธน สามารถจอดรถที่ตึก BUI ชั้นใต้ติน</p>';
/*** End Location Detail  ****/
