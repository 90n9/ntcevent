<?php
$lang['profile_my_profile'] = 'ข้อมูลส่วนตัว';
$lang['profile_first_name'] = 'ชื่อ';
$lang['profile_last_name'] = 'นามสกุล';
$lang['profile_email'] = 'อีเมล์';
$lang['profile_mobile'] = 'เบอร์โทรศัพท์มือถือ';
$lang['profile_company_name'] = 'บริษัท';

$lang['profile_payment_information'] = 'ข้อมูลการชำระเงิน';
$lang['profile_payment_on_behalf'] = 'ชำระเงินในนาม';
$lang['profile_payment_company'] = 'ในนามบริษัท';
$lang['profile_payment_individual'] = 'ในนามบุคคล';
$lang['profile_payment_company_name'] = 'ชื่อบริษัท';
$lang['profile_payment_detail'] = '* รายละเอียดนี้ใช้ในการออกใบเสร็จรับเงินและใบกำกับภาษี';
$lang['profile_payment_branch'] = 'สาขา';
$lang['profile_payment_company_tax'] = 'เลขประจำตัวผู้เสียภาษี';
$lang['profile_payment_company_address'] = 'ที่อยู่บริษัท';
$lang['profile_payment_individual_address'] = 'ที่อยู่ออกใบเสร็จ';

$lang['profile_payment_save'] = 'บันทึกข้อมูล';
