<?php
$lang['baht'] = 'บาท';
$lang['course_ticket_type'] = 'ประเภทตั๋ว';
$lang['course_unit_price'] = 'ราคา (บาท)';
$lang['course_quantity'] = 'จำนวน';
$lang['course_total_baht'] = 'รวม (บาท)';
$lang['course_discount'] = 'Promo Code';
$lang['course_sub_total'] = 'รวม';
$lang['course_vat'] = 'ภาษีมูลค่าเพิ่ม 7%';
$lang['course_deduct_withholding_tax'] = 'ภาษีหัก ณ ที่จ่าย 3%';
$lang['course_grand_total'] = 'ยอดรวม';

$lang['course_billing_for_company'] = 'ชำระเงินในนามบริษัท';
$lang['course_request_withholding_tax'] = 'ต้องการหักภาษี ณ ที่จ่าย 3%';
$lang['course_company_information'] = 'ข้อมูลบริษัท';
$lang['course_individual_information'] = 'ข้อมูลออกใบเสร็จรับเงิน';
$lang['course_company_note'] = 'รายละเอียดนี้ใช้ในการออกใบเสร็จรับเงินและใบกำกับภาษี';

$lang['course_ticket_buyer_information'] = 'ผู้ซื้อตั๋ว';
$lang['course_ticket_form_header'] = 'ผู้เข้าอบรมที่';
$lang['course_ticket_form_same_buyer'] = 'ใช้ข้อมูลเดียวกับผู้ซื้อตั๋ว';
$lang['course_payment_header'] = 'ข้อมูลการชำระเงิน';
$lang['course_choose_payment'] = 'ช่องทางการชำระเงิน';
$lang['course_payment_bank_transfer'] = 'โอนเงินผ่านธนาคาร';
$lang['course_payment_creditcard'] = 'ชำระด้วยบัตรเครดิต';
$lang['course_bank_header'] = 'ข้อมูลการโอนเงินผ่านทางธนาคาร';
$lang['course_bank_account_name'] = 'ชื่อบัญชี:';
$lang['course_bank_account_name_value'] = 'บริษัท เน็ตเวิร์ก เทรนนิ่ง เซ็นเตอร์ จำกัด';
$lang['course_bank_account_number'] = 'เลขที่บัญชี:';
$lang['course_bank_note'] = 'กรุณาแจ้งหลักฐานการชำระเงินภายใน 7 วันหลังการลงทะเบียน<br/>
  หากไม่สามารถแจ้งในเวลาที่กำหนด กรุณาติดต่อเจ้าหน้าที่ มิเช่นนั้นรายการลงทะเบียนของท่านจะถูกยกเลิกโดยอัตโนมัติ';
$lang['course_creditcard_header'] = 'ข้อมูลการชำระด้วยบัตรเครดิต';
$lang['course_creditcard_surcharges'] = 'ค่าธรรมเนียมธนาคาร';

$lang['course_contact_request'] = 'ขอใบเสนอราคา';
$lang['course_contact_request_firstname'] = 'ชื่อ';
$lang['course_contact_request_lastname'] = 'นามสกุล';
$lang['course_contact_request_phone'] = 'โทรศัพท์';
$lang['course_contact_request_mobile'] = 'มือถือ';
$lang['course_contact_request_company'] = 'บริษัท';
$lang['course_contact_request_attendee'] = 'จำนวนผู้เรียน';
$lang['course_contact_request_message'] = 'ข้อความ';

$lang['no_ticket_found'] = 'สอบถามรายละเอียดเพิ่มเติมที่ (0) 2634-7993-4';


$lang['promo_error_condition_not_met'] = 'รหัสโปรโมชั่นไม่ตรงกับเงื่อนไข';
$lang['promo_error_expired'] = 'รหัสโปรโมชั่นหมดอายุ';
$lang['promo_error_one_time_use'] = 'รหัสโปรโมชั่นสามารถใช้ได้ครั้งเดียว';
