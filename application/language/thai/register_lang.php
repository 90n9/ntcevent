<?php
$lang['register_error_form_not_complete'] = 'กรุณากรอกข้อมูลให้ครบถ้วน';
$lang['register_error_invalid_email'] = 'อีเมลไม่ถูกต้อง';
$lang['register_error_duplicate_email'] = 'อีเมลนี้มีการลงทะเบียนแล้ว';
