<?php
$lang['login_error_no_user_name'] = 'กรุณาระบุอีเมล';
$lang['login_error_no_password'] = 'กรุณาระบุรหัสผ่าน';
$lang['login_error_no_user_found'] = 'อีเมลหรือรหัสผ่านไม่ถูกต้อง';
$lang['login_error_not_verify'] = 'กรุณายืนยันอีเมล';
$lang['login_error_usertype_invalid'] = 'กรุณาติดต่อเจ้าหน้าที่';
