<?php
$lang['baht'] = 'บาท';
$lang['price'] = 'ราคา';
$lang['mycourse_order_id'] = 'รหัสสั่งซื้อ';
$lang['mycourse_status'] = 'สถานะ';
$lang['mycourse_quantity'] = 'จำนวน';
$lang['mycourse_total'] = 'รวม';
$lang['mycourse_discount'] = 'ส่วนลด';
$lang['mycourse_vat'] = 'VAT 7%';
$lang['mycourse_withholding_tax'] = 'ภาษีหัก ณ ที่จ่าย 3%';
$lang['mycourse_sub_total'] = 'รวม';
$lang['mycourse_grand_total'] = 'ยอดรวม (บาท)';
$lang['mycourse_buyer_name'] = 'ชื่อผู้ซื้อ';
$lang['mycourse_email'] = 'อีเมล';
$lang['mycourse_mobile'] = 'เบอร์โทรศัพท์';
$lang['mycourse_company_name'] = 'ชื่อบริษัท';
$lang['mycourse_course_detail'] = 'รายละเอียดคอร์สเรียน';
$lang['mycourse_submit_payin'] = 'แจ้งยืนยันการโอนเงิน';
$lang['mycourse_payment_creditcard'] = 'ชำระด้วยบัตรเครดิต';
$lang['mycourse_ticket_type'] = 'ประเภทตั๋ว';
$lang['mycourse_attendee_information'] = 'ข้อมูลผู้เข้าอบรม';
$lang['mycourse_print'] = 'พิมพ์ตั๋ว';

$lang['mycourse_list_view_all_orders'] = 'รายการทั้งหมด';
$lang['mycourse_list_waiting_for_payment'] = 'รายการรอชำระเงิน';
$lang['mycourse_list_waiting_for_approval'] = 'รายการรอตรวจสอบ';
$lang['mycourse_list_completed_orders'] = 'รายการชำระเงินแล้ว';
$lang['mycourse_list_cancelled_orders'] = 'รายการยกเลิก';

$lang['mycourse_list_all_orders_detail'] = 'รายการที่สมัครเรียนทั้งหมด';
$lang['mycourse_list_waiting_for_payment_detail'] = 'ท่านสามารถชำระเงินโดยการโอนเงินผ่านทางธนาคาร หรือจ่ายออนไลน์ผ่านบัตรเครดิต';
$lang['mycourse_list_waiting_for_approval_detail'] = 'รายการรอการตรวจสอบการชำระเงินผ่านธนาคาร หลังจากเจ้าหน้าที่ตรวจสอบการโอนเงิน สถานะรายการจะปรับโดยอัตโนมัติ';
$lang['mycourse_list_completed_orders_detail'] = 'กรุณาพิมพ์ตั๋วเพื่อยืนยันการสั่งซื้อ';


$lang['mycourse_list_waiting_for_payment_bank'] = 'แจ้งยืนยันการโอนเงิน';
$lang['mycourse_list_waiting_for_payment_creditcard'] = 'ชำระด้วยบัตรเครดิต';

$lang['mycourse_list_order_id'] = 'รหัสสั่งซื้อ';
$lang['mycourse_list_course_name'] = 'คอร์สเรียน';
$lang['mycourse_list_date_of_purchase'] = 'วันที่สั่งซื้อ';
$lang['mycourse_list_schedule'] = 'ตารางเรียน';
$lang['mycourse_list_status'] = 'สถานะ';
$lang['mycourse_list_grand_total'] = 'ยอดรวม (บาท)';
$lang['mycourse_list_order_details'] = 'รายละเอียดการสั่งซื้อ';
$lang['mycourse_list_submit_pay_in_silp'] = 'แจ้งยืนยันการโอนเงิน';
$lang['mycourse_list_cancel_order'] = 'ยกเลิกรายการ';
$lang['mycourse_list_status_cancel'] = 'ยกเลิก';
$lang['mycourse_list_status_waiting'] = 'รอการชำระ';
$lang['mycourse_list_status_payment'] = 'รอการตรวจสอบ';
$lang['mycourse_list_status_complete'] = 'ชำระเงินแล้ว';

$lang['mycourse_list_all_orders_noshow_detail'] = 'ไม่พบรายการลงทะเบียน';
$lang['mycourse_list_waiting_for_payment_noshow_detail'] = 'ไม่พบรายการรอการชำระเงิน';
$lang['mycourse_list_waiting_for_approval_noshow_detail'] = 'ไม่พบรายการรอการตรวจสอบ';
$lang['mycourse_list_completed_orders_noshow_detail'] = 'ไม่พบรายการที่ชำระเงิน';
$lang['mycourse_list_cancelled_orders_noshow_detail'] = 'ไม่พบรายการยกเลิก';

$lang['mycourse_detail_cancel_name'] = 'ยกเลิกรายการ';
$lang['mycourse_detail_cancel_detail'] = 'กดปุ่มยืนยัน ถ้าคุณต้องการลบรายการสั่งซื้อนี้';
$lang['mycourse_detail_cancel_yes'] = 'ยืนยันการยกเลิก';
$lang['mycourse_detail_cancel_no'] = 'กลับสู่หน้ารายการ';
