<?php
$lang['baht'] = 'บาท';
$lang['payment_note'] = '
<h3>หมายเหตุ</h3>
<ul style="padding-left:30px;">
  <li>หลังจากแจ้งชำระเงิน โดยแนบรูป<br>หลักฐานการโอนเงิน กรุณารอ 1-2 วันทำการ<br>เพื่อตรวจสอบยอดชำระ จดหมายยืนยันการ<br>ลงทะเบียนจะถูกจัดส่งตามอีเมลที่ได้แจ้งไว้</li>
  <li>หากต้องการยกเลิกการสั่งซื้อ กรุณากดยกเลิกที่<br>หน้ารายการสมัครเรียน หรือติดต่อ<br>02-634-7993-4 ต่อ 12</li>
  <li>หากมีคำถามในการชำระเงิน กรุณาติดต่อ<br>02-634-7993 ต่อ 24 ในเวลาทำการ</li>
</ul>
';
$lang['payment_bank_header'] = 'แจ้งยืนยันการโอนเงิน';
$lang['payment_creditcard_header'] = 'ชำระเงินด้วยบัตรเครดิต';
$lang['payment_form_note'] = 'ข้อความถึงเจ้าหน้าที่';
$lang['payment_submit'] = 'ยืนยัน';
$lang['payment_back'] = 'กลับไปหน้ารายการ';

$lang['payment_order_id'] = 'รหัสสั่งซื้อ';
$lang['payment_course_name'] = 'ชื่อคอร์สเรียน';
$lang['payment_quantity'] = 'จำนวนที่นั่ง';
$lang['payment_total_baht'] = 'รวม (บาท)';
$lang['payment_total'] = 'รวม';
$lang['payment_surcharge'] = 'ค่าธรรมเนียมธนาคาร';
$lang['payment_vat'] = 'VAT 7%';
$lang['payment_grand_total'] = 'ยอดรวม';

$lang['payment_bank_transfer_instruction'] = 'รายละเอียดการโอนเงิน';
$lang['payment_bank_account_name'] = 'ชื่อบัญชี:';
$lang['payment_bank_account_name_value'] = 'บริษัท เน็ตเวิร์ก เทรนนิ่ง เซ็นเตอร์ จำกัด';
$lang['payment_bank_account_number'] = 'เลขที่บัญชี:';
$lang['payment_bank_amount'] = 'จำนวนเงิน (บาท)';
$lang['payment_bank_value_date'] = 'วันที่โอนเงิน';
$lang['payment_bank_upload'] = 'หลักฐานการโอนเงิน';
