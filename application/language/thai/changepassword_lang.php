<?php
$lang['changepassword_head'] = 'เปลี่ยนรหัสผ่าน';
$lang['changepassword_password'] = 'รหัสผ่านใหม่';
$lang['changepassword_confirm_password'] = 'ยืนยันรหัสผ่าน';

$lang['changepassword_save'] = 'บันทึกการเปลี่ยนรหัสผ่าน';
