<?php
class ContactService extends CI_Model {
  function __construct() {
    parent::__construct();
    $this->load->model(array('contact/ContactModel', 'SendMailService'));
  }
  public function contact($data){
    $this->ContactModel->insert($data);
    $this->send_contact_email($data);
  }
  private function send_contact_email($data){
    $content = $this->load->view('contact/emailadmin', $data, true);
    $subject = '[Message] Contact us message from online';
    $this->SendMailService->send_mail($this->ConfigModel->getData('email_contact'), '', $subject, $content);
  }
}
