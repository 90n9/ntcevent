<?php
class ContactModel extends CI_Model {
  function __construct() {
    parent::__construct();
  }
  public function insert($data){
    $memberId = 0;
    if($this->session->userdata('member_logged_in')){
      $this->session->userdata('member_id');
    }
    $this->db->set('contact_firstname', $data['contact_firstname']);
    $this->db->set('contact_lastname', $data['contact_lastname']);
    $this->db->set('contact_email', $data['contact_email']);
    $this->db->set('contact_phone', $data['contact_phone']);
    $this->db->set('contact_detail', $data['contact_detail']);
    $this->db->set('is_read', 1);
    $this->db->set('member_id', $memberId);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->insert('tbl_contact');
  }
}
