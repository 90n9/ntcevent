<?php
class SlideshowModel extends CI_Model {
  function __construct() {
    parent::__construct();
  }
  public function getList(){
    $this->db->where('is_delete', 0);
    $this->db->where('enable_status', 1);
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_slideshow');
  }
  public function showImage($slideshow, $lang){
    $retStr = '';
    $langImage = ($lang == 'thai')?$slideshow->slideshow_image_th:$slideshow->slideshow_image_en;
    $retStr = ($langImage != '')?'<img src="'.base_url('uploads/'.$langImage).'" style="max-width:100%" />':'';
    $retStr = ($slideshow->slideshow_url != '')?'<a href="'.$slideshow->slideshow_url.'">'.$retStr.'</a>':$retStr;
    return $retStr;
  }
}
