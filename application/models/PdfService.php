<?php
class PdfService extends CI_Model {
  function __construct() {
    parent::__construct();
  }
  public function createPdfFile($html, $stylesheet, $header, $footer, $folder, $filename){
    $mpdf = $this->setupPdf($html, $stylesheet, $header, $footer);
    $fullPath = $folder.$filename;
    $mpdf->Output($fullPath,'F');
    return $fullPath;
  }
  public function viewPdfFile($html, $stylesheet, $header, $footer, $filename){
    $mpdf = $this->setupPdf($html, $stylesheet, $header, $footer);
    $mpdf->Output($filename,'I');
  }
  private function setupPdf($html, $stylesheet, $header, $footer){
    include(FCPATH."mpdf/mpdf.php");
    $mpdf=new mPDF('th','A4','','',0,0,0,0,0,0);
    $mpdf->SetHTMLHeader($header);
    $mpdf->SetHTMLFooter($footer);
    $mpdf->SetDisplayMode('fullpage');
    $mpdf->list_indent_first_level = 0;
    $mpdf->WriteHTML($stylesheet,1);
    $mpdf->WriteHTML($html,2);
    return $mpdf;
  }
}
