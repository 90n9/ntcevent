<?php
class WishlistModel extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function getList(){
        $this->db->where('member_id', $this->session->userdata('member_id'));
        $this->db->order_by('create_date', 'DESC');
        return $this->db->get('tbl_member_wishlist');
    }
    public function getStatus($courseId){
        if(!$this->LoginService->getLoginStatus()){
            return false;
        }
        $this->db->where('member_id', $this->session->userdata('member_id'));
        $this->db->where('course_id', $courseId);
        $query = $this->db->get('tbl_member_wishlist');
        if($query->num_rows() > 0){
            return true;
        }
        return false;
    }
    public function insert($courseId){
        if(!$this->getStatus($courseId)){
            $this->db->set('member_id', $this->session->userdata('member_id'));
            $this->db->set('course_id', $courseId);
            $this->db->set('create_date', 'NOW()', false);
            $this->db->insert('tbl_member_wishlist');
        }
    }
    public function delete($courseId){
        $this->db->where('member_id', $this->session->userdata('member_id'));
        $this->db->where('course_id', $courseId);
        $this->db->delete('tbl_member_wishlist');
    }
}