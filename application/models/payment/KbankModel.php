<?php
class KbankModel extends CI_Model {
  function __construct() {
    parent::__construct();
    $this->load->model('ConfigModel');
  }
  public function getData($paymentId, $paidAmount, $coursePaymentName){
    $data = array();
    $data['merchant2'] = $this->ConfigModel->getData('kbank_merchant2');
    $data['term2'] = $this->ConfigModel->getData('kbank_term2');
    $data['amount2'] = str_pad(number_format ( $paidAmount , 2 , '', ''), 12, '0', STR_PAD_LEFT);
    $data['url2'] = site_url('payment/creditcardsuccess/'.$paymentId);
    $data['respurl'] = site_url('payment/confirm/'.$paymentId);
    $data['ipcust2'] = $this->ConfigModel->getData('kbank_ipcust2');;
    $data['detail2'] = $coursePaymentName;
    $data['invmerchant'] = str_pad($paymentId, 12, '0', STR_PAD_LEFT);
    $md5Code = $this->ConfigModel->getData('kbank_md5key');;
    $preEnCode = implode('',$data).$md5Code;
    $data['checkSum'] = md5($preEnCode);
    return $data;
  }
  public function insertResponse($paymentId, $pmgwresp2){
    $this->db->set('pmgwresp', $pmgwresp2);
    $this->db->set('payment_id', $paymentId);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->insert('tbl_kbank_response');
    $kbankResponseId = $this->db->insert_id();
    $pmgwData = $this->update($kbankResponseId, $pmgwresp2);
    $pmgwData['kbank_response_id'] = $kbankResponseId;
    return $pmgwData;
  }
  public function update($kbankResponseId, $pmgwresp2){
    $pmgwData = $this->convertPmgwrespToData($pmgwresp2);
    foreach($pmgwData as $key => $value){
      $this->db->set($key, $value);
    }
    $this->db->where('kbank_response_id', $kbankResponseId);
    $this->db->update('tbl_kbank_response');
    return $pmgwData;
  }
  private function convertPmgwrespToData($pmgwresp2){
    return array(
      'pmgwresp'          => $pmgwresp2,
      'transcode'         => substr($pmgwresp2, 0, 4),
      'merchant_id'       => substr($pmgwresp2, 4, 15),
      'terminal_id'       => substr($pmgwresp2, 19, 8),
      'shop_no'           => substr($pmgwresp2, 27, 2),
      'currency_code'     => substr($pmgwresp2, 29, 3),
      'invoice_no'        => substr($pmgwresp2, 32, 12),
      'date'              => substr($pmgwresp2, 44, 8),
      'time'              => substr($pmgwresp2, 52, 6),
      'card_no'           => substr($pmgwresp2, 58, 19),
      'expired_date'      => substr($pmgwresp2, 77, 4),
      'cvv2'              => substr($pmgwresp2, 81, 4),
      'transamount'       => substr($pmgwresp2, 85, 12),
      'response_code'     => substr($pmgwresp2, 97, 2),
      'approval_code'     => substr($pmgwresp2, 99, 6),
      'card_type'         => substr($pmgwresp2, 105, 3),
      'reference1'        => substr($pmgwresp2, 108, 20),
      'plan_id'           => substr($pmgwresp2, 128, 3),
      'pay_month'         => substr($pmgwresp2, 131, 2),
      'interest_type'     => substr($pmgwresp2, 133, 1),
      'interest_rate'     => substr($pmgwresp2, 134, 6),
      'amount_per_month'  => substr($pmgwresp2, 140, 9),
      'total_amount'      => substr($pmgwresp2, 149, 12),
      'management_fee'    => substr($pmgwresp2, 161, 5),
      'interest_mode'     => substr($pmgwresp2, 166, 2),
      'fx_rate'           => substr($pmgwresp2, 168, 20),
      'thb_amount'        => substr($pmgwresp2, 188, 20),
      'customer_email'    => substr($pmgwresp2, 208, 100),
      'description'       => substr($pmgwresp2, 308, 150),
      'payer_ip_address'  => substr($pmgwresp2, 458, 18),
      'warning_light'     => substr($pmgwresp2, 476, 1),
      'selected_bank'     => substr($pmgwresp2, 477, 60),
      'issuer_bank'       => substr($pmgwresp2, 537, 60),
      'selected_country'  => substr($pmgwresp2, 597, 45),
      'ip_country'        => substr($pmgwresp2, 642, 45),
      'issuer_country'    => substr($pmgwresp2, 687, 45),
      'eci'               => substr($pmgwresp2, 732, 4),
      'xid'               => substr($pmgwresp2, 736, 40),
      'cavv'              => substr($pmgwresp2, 776, 40)
    );
  }
}
