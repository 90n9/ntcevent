<?php
class PaymentModel extends CI_Model {
  public function getData($paymentId){
    $this->db->where('payment_id', $paymentId);
    $query = $this->db->get('tbl_payment');
    if($query->num_rows() > 0){
      return $query->row();
    }
    return false;
  }
  public function insert($data){
    $preCode = $this->getPreCode();
    $runningNumber = $this->getNextRunningNumber($preCode);
    $paymentCode = 'INV'. $preCode.str_pad($runningNumber,3,"0",STR_PAD_LEFT);
    $this->db->set('member_id', $this->session->userdata('member_id'));
    $this->db->set('payment_code', $paymentCode);
    $this->db->set('payment_pre_code', $preCode);
    $this->db->set('running_number', $runningNumber);
    $this->db->set('payment_status', 'waiting');
    $this->db->set('payment_type', $data['payment_type']);
    $this->db->set('order_amount', $data['order_amount']);
    $this->db->set('vat_amount', $data['vat_amount']);
    $this->db->set('surcharge_amount', $data['surcharge_amount']);
    $this->db->set('payment_amount', $data['payment_amount']);
    $this->db->set('payment_date', $data['payment_date']);
    $this->db->set('payment_time', $data['payment_time']);
    $this->db->set('payment_file', $data['payment_file']);
    $this->db->set('note', $data['note']);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', 0);
    $this->db->insert('tbl_payment');
    $paymentId = $this->db->insert_id();
    return $paymentId;
  }
  public function updatePrice($paymentId, $data){
    $this->db->set('order_amount', $data['order_amount']);
    $this->db->set('surcharge_amount', $data['surcharge_amount']);
    $this->db->set('vat_amount', $data['vat_amount']);
    $this->db->set('payment_amount', $data['payment_amount']);
    $this->db->where('payment_id', $paymentId);
    $this->db->update('tbl_payment');
  }
  public function updateComplete($paymentId){
    $this->load->model(array('payment/PaymentItemModel', 'course/CourseBuyerModel', 'course/CourseRegisterModel'));
    $paymentItemList = $this->PaymentItemModel->getList($paymentId);
    $this->updatePaymentStatus($paymentId, 'complete');
    foreach($paymentItemList->result() as $paymentItem){
      $this->CourseBuyerModel->updateStatus($paymentItem->course_buyer_id, 'complete');
    }
  }
  public function updateWarning($paymentId){
    $this->load->model(array('payment/PaymentItemModel', 'course/CourseBuyerModel', 'course/CourseRegisterModel'));
    $paymentItemList = $this->PaymentItemModel->getList($paymentId);
    $this->updatePaymentStatus($paymentId, 'warning');
    foreach($paymentItemList->result() as $paymentItem){
      $this->CourseBuyerModel->updateStatus($paymentItem->course_buyer_id, 'complete');
    }
  }
  public function updateCancel($paymentId){
    $this->load->model(array('payment/PaymentItemModel', 'course/CourseBuyerModel', 'course/CourseRegisterModel'));
    $paymentItemList = $this->PaymentItemModel->getList($paymentId);
    $this->updatePaymentStatus($paymentId, 'cancel');
    foreach($paymentItemList->result() as $paymentItem){
      $this->CourseBuyerModel->updateStatus($paymentItem->course_buyer_id, 'waiting');
    }
  }
  public function updatePaymentStatus($paymentId, $paymentStatus){
    $this->db->set('payment_status', $paymentStatus);
    $this->db->where('payment_id', $paymentId);
    $this->db->where('payment_type', 'creditcard');
    $this->db->update('tbl_payment');
  }
  private function getPreCode(){
    return date('ym');
  }
  private function getNextRunningNumber($preCode){
    $this->db->select('MAX(running_number) AS max_running_number');
    $this->db->where('payment_pre_code', $preCode);
    $query = $this->db->get('tbl_payment');
    if($query->num_rows() > 0){
      return $query->row()->max_running_number + 1;
    }
    return 1;
  }
}
