<?php
class PaymentItemModel extends CI_Model {
  public function getList($paymentId){
    $this->db->where('payment_id', $paymentId);
    return $this->db->get('tbl_payment_item');
  }
  public function insert($data){
    $this->db->set('payment_id', $data['payment_id']);
    $this->db->set('course_buyer_id', $data['course_buyer_id']);
    $this->db->set('course_buyer_amount', $data['course_buyer_amount']);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', 0);
    $this->db->insert('tbl_payment_item');
    $paymentItemId = $this->db->insert_id();
    return $paymentItemId;
  }
}
