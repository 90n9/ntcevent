<?php
class Offer_model extends CI_Model{
  public function get_list(){
    $this->db->where('is_delete', 0);
    $this->db->where('enable_status', 1);
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_offer');
  }
}
