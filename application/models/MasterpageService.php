<?php

class MasterpageService extends CI_Model {
  var $cssList;
  var $jsList;
  function __construct() {
    parent::__construct();
    $this->cssList = array();
    $this->jsList = array();
  }
  public function getPageLang(){
    $idiom = $this->session->userdata('language');
    $lang = ($idiom != 'thai')?'english':$idiom;
    $this->lang->load('masterpage', $idiom);
    return $lang;
  }
  function display($content, $title = '', $nav = ''){
    $this->session->set_userdata('referred_from', current_url().'?'.$_SERVER['QUERY_STRING']);
    $idiom = $this->getPageLang();
    $data['content'] = $content;
    $data['title'] = $title;
    $data['nav'] = $nav;
    $data['lang'] = $idiom;
    $this->load->view('masterpage/masterpage', $data);
  }
  function addCss($cssFile){
    $this->cssList[] = base_url($cssFile);
  }
  function addJs($jsFile){
    $this->jsList[] = base_url($jsFile);
  }
  function addOuterCss($cssFile){
    $this->cssList[] = $cssFile;
  }
  function addOuterJs($jsFile){
    $this->jsList[] = $jsFile;
  }
  function printCss(){
    return $this->load->view('masterpage/cssloader', array('cssList'=>$this->cssList), FALSE);
  }
  function printJs(){
    return $this->load->view('masterpage/jsloader', array('jsList'=>$this->jsList), FALSE);
  }
}
