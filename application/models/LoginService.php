<?php
class LoginService extends CI_Model {

  var $NO_USER_NAME = 'require_user_name';
  var $NO_PASSWORD = 'require_password';
  var $NO_USER_FOUND = 'user_and_password_miss_match';
  var $NOT_VERIFY = 'not_verify';
  var $USERTYPE_INVALID = 'unenable_user';
  var $response_status = true;
  var $response_msg = '';

  function __construct() {
    parent::__construct();
  }

  public function mustLogin() {
    if (!$this->getLoginStatus()) {
      redirect('home');
    }
  }
  public function mustNotLogin() {
    if ($this->getLoginStatus()) {
      redirect('home');
    }
  }

  public function logout() {
    $this->session->sess_destroy();
  }
  public function getErrorMessage($error, $lang = 'english') {
    $errorMessage = '';
		$this->lang->load('login', $lang);
    if ($error != '') {
      $arrMessage[$this->NO_USER_NAME] = $this->lang->line('login_error_no_user_name');
      $arrMessage[$this->NO_PASSWORD] = $this->lang->line('login_error_no_password');
      $arrMessage[$this->NO_USER_FOUND] = $this->lang->line('login_error_no_user_found');
      $arrMessage[$this->NOT_VERIFY] = $this->lang->line('login_error_not_verify');
      $arrMessage[$this->USERTYPE_INVALID] = $this->lang->line('login_error_usertype_invalid');
      $errorMessage = (isset($arrMessage[$error])) ? $arrMessage[$error] : $error;
    }
    return $errorMessage;
  }

  public function login($loginName, $loginPassword) {
    $this->checkUserNameInput($loginName);
    if($this->response_status){
      $this->checkUserPasswordInput($loginPassword);
    }
    if($this->response_status){
      $user = $this->checkUserLogin($loginName, $loginPassword);
    }
    if($this->response_status){
      $this->checkUserAllow($user);
    }
    if($this->response_status){
      $this->createSession($user);
    }
    return array(
      'resp_status' => $this->response_status,
      'resp_msg' => $this->getErrorMessage($this->response_msg)
    );
  }

  public function getLoginStatus() {
    if ($this->session->userdata('member_logged_in')) {
      return true;
    }
    return false;
  }

  private function checkUserNameInput($loginName) {
    if (!$loginName) {
      $this->response_status = false;
      $this->response_msg = $this->NO_USER_NAME;
    }
  }

  private function checkUserPasswordInput($loginPassword) {
    if (!$loginPassword) {
      $this->response_status = false;
      $this->response_msg = $this->NO_PASSWORD;
    }
  }

  private function checkUserLogin($loginName, $loginPassword) {
    $this->load->model('member/MemberModel');
    $data = $this->MemberModel->getLogin($loginName, $loginPassword);
    if (!$data) {
      $this->response_status = false;
      $this->response_msg = $this->NO_USER_FOUND;
      return false;
    }
    return $data;
  }

  private function checkUserAllow($member) {
    if ($member->is_verify != 'true') {
      $this->response_status = false;
      $this->response_msg = $this->NOT_VERIFY;
    }
    if ($member->enable_status != 1) {
      $this->response_status = false;
      $this->response_msg = $this->USERTYPE_INVALID;
    }
  }

  public function createSession($member) {
    $userdata = array();
    $userdata['member_id'] = $member->member_id;
    $userdata['firstname'] = $member->firstname;
    $userdata['lastname'] = $member->lastname;
    $userdata['email'] = $member->email;
    $userdata['member_logged_in'] = TRUE;
    $this->session->set_userdata($userdata);
  }
}
