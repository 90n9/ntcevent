<?php
class ConfigModel extends CI_Model {
  function __construct() {
    parent::__construct();
  }
  public function getData($code){
    $this->db->where('config_code', $code);
    $query = $this->db->get('tbl_config');
    if($query->num_rows() > 0){
      return $query->row()->config_value;
    }
    return '';
  }
}
