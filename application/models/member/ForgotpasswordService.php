<?php

class ForgotpasswordService extends CI_Model {
  var $NO_USERNAME = 'require_email';
  var $NOT_FOUND = 'email_not_found';
  var $INVALID_ACCOUNT = 'invalid_account';

  function __construct() {
    parent::__construct();
    $this->load->model(array('member/MemberModel'));
  }
  public function getErrorMessage($error) {
    $errorMessage = '';
    if ($error != '') {
      $arrMessage[$this->NO_USERNAME] = 'Please enter the email address that you use to sign into website.';
      $arrMessage[$this->NOT_FOUND] = 'No user was found with that email address.';
      $arrMessage[$this->INVALID_ACCOUNT] = 'This account has been disabled, please contact admin';
      $errorMessage = (isset($arrMessage[$error])) ? $arrMessage[$error] : $error;
    }
    return $errorMessage;
  }
  public function forgot($email) {
    if(trim($email) == ''){
      return array(
        'resp_status' => false,
        'resp_msg' => $this->getErrorMessage($this->NO_USERNAME)
      );
    }
    $member = $this->getMemberData($email);
    if(!$member){
      return array(
        'resp_status' => false,
        'resp_msg' => $this->getErrorMessage($this->NOT_FOUND)
      );
    }
    if ($member->enable_status != 1) {
      return array(
        'resp_status' => false,
        'resp_msg' => $this->getErrorMessage($this->INVALID_ACCOUNT)
      );
    }
    $this->sendChangepasswordEmail($member);
    return array(
      'resp_status' => true,
      'resp_msg' => 'Send mail complete'
    );
  }
  private function getMemberData($email){
    $this->db->where('email', $email);
    $this->db->where('is_delete', 0);
    $query = $this->db->get('tbl_member');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  private function sendChangepasswordEmail($member){
    $hashCode = $this->EncodeService->encode_number($member->member_id);
    $linkUrl = 'forgotpassword/changepassword/'.$hashCode;
    $this->load->model('SendMailService');
    $content = $this->load->view('forgotpassword/forgotpasswordemail', array(
      'member' => $member,
      'linkUrl' => $linkUrl
    ), true);
    $subject = 'Reset Password';
    $this->SendMailService->send_mail($member->email, '', $subject, $content);
  }
}
