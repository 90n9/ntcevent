<?php
class MemberModel extends CI_Model {
  function __construct() {
    parent::__construct();
  }
  public function getData($memberId){
    $this->db->where('member_id', $memberId);
    $this->db->where('is_delete', 0);
    $query = $this->db->get('tbl_member');
    if($query->num_rows() > 0){
      return $query->row();
    }
    return false;
  }
  public function getDataByEmail($memberEmail){
    $this->db->where('email', $memberEmail);
    $this->db->where('is_delete', 0);
    $query = $this->db->get('tbl_member');
    if($query->num_rows() > 0){
      return $query->row();
    }
    return false;
  }
  public function getLogin($email, $loginPassword){
    $this->db->where('email', $email);
    $this->db->where('login_password', md5($loginPassword));
    $this->db->where('is_delete', 0);
    $query = $this->db->get('tbl_member');
    if($query->num_rows() > 0){
      return $query->row();
    }
    return false;
  }
  public function insert($data){
    $this->db->set('login_password', md5($data['login_password']));
    $this->db->set('firstname', $data['firstname']);
    $this->db->set('lastname', $data['lastname']);
    $this->db->set('company', $data['company']);
    $this->db->set('mobile', $data['mobile']);
    $this->db->set('email', $data['email']);
    $this->db->set('company_name', $data['company']);
    $this->db->set('enable_status', 1);
    $this->db->set('is_verify', 'false');
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', 0);
    $this->db->insert('tbl_member');
    $memberId = $this->db->insert_id();
    return $memberId;
  }
  public function update($memberId, $data){
    $this->db->set('firstname', $data['firstname']);
    $this->db->set('lastname', $data['lastname']);
    $this->db->set('company', $data['company']);
    $this->db->set('mobile', $data['mobile']);
    $this->db->set('is_company', $data['is_company']);
    $this->db->set('company_name', $data['company_name']);
    $this->db->set('company_address', $data['company_address']);
    $this->db->set('company_branch', $data['company_branch']);
    $this->db->set('company_tax', $data['company_tax']);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', 0);
    $this->db->where('member_id', $memberId);
    $this->db->where('is_delete', 0);
    $this->db->update('tbl_member');
  }
  public function changePassword($memberId, $password){
    $this->db->set('login_password', md5($password));
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', 0);
    $this->db->where('member_id', $memberId);
    $this->db->where('is_delete', 0);
    $this->db->update('tbl_member');
  }
  public function updateTaxInfo($memberId, $data){
    $this->db->set('is_company', $data['is_company']);
    $this->db->set('company_name', $data['company_name']);
    $this->db->set('company_address', $data['company_address']);
    $this->db->set('company_branch', $data['company_branch']);
    $this->db->set('company_tax', $data['company_tax']);
    $this->db->where('member_id', $memberId);
    $this->db->where('is_delete', 0);
    $this->db->update('tbl_member');
  }
  public function verify($memberId){
    $this->db->set('is_verify', 'true');
    $this->db->where('member_id', $memberId);
    $this->db->where('is_delete', 0);
    $this->db->update('tbl_member');
  }
}
