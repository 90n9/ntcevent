<?php
class SendMailService extends CI_Model {
  function __construct() {
    parent::__construct();
  }
  public function send_mail($mail_to, $mail_bcc, $subject, $content){
    $content = $this->load->view('mailtemplate/mail_masterpage', array(
      'content' => $content,
      'mail_to' => $mail_to
    ),true);
    if (strpos( $mail_to, '@trainingcenter.co.th' ) !== false ) {
      $this->mail_server($mail_to, $subject, $content);
    } else {
      $this->mail_gun($mail_to, $subject, $content);
    }
  }
  public function mail_server($mail_to, $subject, $content){
    $config = Array(
    'protocol' => 'smtp',
    'smtp_host' => 'ssl://netway14.netway.co.th',
    'smtp_port' => 465,
    'smtp_user' => 'noreply@trainingcenter.co.th',
    'smtp_pass' => 'n0rep1y',
    'mailtype'  => 'html',
    'charset'   => 'utf-8'
    );
    $this->load->library('email', $config);
    $this->email->set_newline("\r\n");
    $this->email->from('noreply@trainingcenter.co.th', 'Network Training Center');
    $this->email->to($mail_to);

    $this->email->subject($subject);
    $this->email->message($content);

    $this->email->send();
  }
  public function mail_gun($mail_to, $subject, $content){
    $config = array();
    $config['api_key'] = "key-7cb7696b48202b1fc12dccc804002285";
    $config['api_url'] = "https://api.mailgun.net/v3/mg.trainingcenter.co.th/messages";
    $message = array();
    $message['from'] = "Network Training Center <sales@trainingcenter.co.th>";
    $message['to'] = $mail_to;
    $message['h:Reply-To'] = "<sales@trainingcenter.co.th>";
    $message['subject'] = $subject;
    $message['html'] = $content;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $config['api_url']);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, "api:{$config['api_key']}");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$message);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
  }
}
