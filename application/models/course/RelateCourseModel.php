<?php
class RelateCourseModel extends CI_Model{
  public function getRelateList($courseId, $courseCategoryId){
    $courseCategory = $this->CourseCategoryModel->getData($courseCategoryId);
    $mainCategoryId = ($courseCategory->parent_id > 0)?$courseCategory->parent_id:$courseCategoryId;
    $arrCourseCategoryId = $this->CourseCategoryModel->getUnderId($mainCategoryId);
    $sameCatList = $this->getSameCategoryList($courseId, $arrCourseCategoryId);
    $remainAmount = 3 - $sameCatList->num_rows();
    $otherCatList = $this->getOtherCategoryList($arrCourseCategoryId, $remainAmount);
    return array(
      'sameCatList' => $sameCatList,
      'otherCatList' => $otherCatList
    );
  }
  private function getSameCategoryList($courseId, $arrCourseCategoryId){
    $this->db->where_in('tbl_course.course_category_id', $arrCourseCategoryId);
    $this->db->where('tbl_course.course_id !=', $courseId);
    $this->queryWhere();
    $this->db->order_by('rand()');
    return $this->db->get('tbl_course', 3, 0);
  }
  private function getOtherCategoryList($arrCourseCategoryId, $maxCourse){
    $this->db->where_not_in('tbl_course.course_category_id', $arrCourseCategoryId);
    $this->queryWhere();
    $this->db->order_by('rand()');
    return $this->db->get('tbl_course', $maxCourse, 0);
  }
  private function queryWhere(){
    $this->db->where('tbl_course.enable_status', 1);
    $this->db->where('tbl_course.is_delete', 0);
  }
}
