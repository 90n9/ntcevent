<?php
class CourseModel extends CI_Model {
  function __construct() {
    parent::__construct();
    $this->load->model(array('course/CourseCategoryModel'));
  }
  public function getPrice($courseId, $lang){
    $this->db->where('course_id', $courseId);
    $this->db->order_by('sort_priority', 'ASC');
    $query = $this->db->get('tbl_ticket', 1);
    if($query->num_rows() > 0){
      $ticket = $query->row();
      if($ticket->price_type == 'amount'){
        return $this->NumberService->formatInt($ticket->price).(($lang == 'thai')?' บาท':' Baht');
      }elseif($ticket->price_type == 'contact'){
        return ($lang == 'thai')?'ติดต่อฝ่ายขาย':'contact';
      }elseif($ticket->price_type == 'free'){
        return ($lang == 'thai')?'ฟรี':'free';
      }else{
        return '-';
      }
    }
    return ($lang == 'thai')?'ไม่ระบุ':'undefined';
  }
  public function getLastest(){
    $this->db->where('enable_status', 1);
    $this->db->where('is_delete', 0);
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_course', 4, 0);
  }
  public function getNext(){
    $this->db->where('enable_status', 1);
    $this->db->where('is_delete', 0);
    $this->db->where('course_start >', 'NOW()', false);
    $this->db->order_by('course_start', 'ASC');
    return $this->db->get('tbl_course', 4, 0);
  }
  public function getList($categoryGroupId = 0, $perPage = 4, $page = 1){
    if($categoryGroupId > 0){
      $arrCourseCategoryId = $this->CourseCategoryModel->getUnderId($categoryGroupId);
      $this->db->where_in('course_category_id', $arrCourseCategoryId);
    }
    $this->db->where('is_delete', 0);
    $this->db->where('enable_status', 1);
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_course', 4, 0);
  }
  public function getData($courseId){
    $this->db->where('course_id', $courseId);
    //$this->db->where('enable_status', 1);
    $query = $this->db->get('tbl_course');
    if($query->num_rows() > 0){
      return $query->row();
    }
    return false;
  }
  public function hasCoupon($courseId){
    $this->db->where('course_id', $courseId);
    $this->db->where('available_from <', 'NOW()', false);
    $this->db->where('available_to >', 'NOW()', false);
    $this->db->where('enable_status', 1);
    $this->db->where('is_delete', 0);
    $query = $this->db->get('tbl_discount');
    if($query->num_rows() > 0){
      return true;
    }
    return false;
  }
}
