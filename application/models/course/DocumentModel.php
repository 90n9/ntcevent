<?php
class DocumentModel extends CI_Model {
  function __construct() {
    parent::__construct();
  }
  public function getList($courseId){
    $this->db->where('is_delete', 0);
    $this->db->where('enable_status', 1);
    $this->db->where('course_id', $courseId);
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_course_document');
  }
  public function getData($documentId){
    $this->db->where('course_document_id', $documentId);
    $this->db->where('is_delete', 0);
    $this->db->where('enable_status', 1);
    $query = $this->db->get('tbl_course_document');
    if($query->num_rows() > 0){
        return $query->row();
    }
    return false;
  }
}
