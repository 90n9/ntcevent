<?php
class CourseRegisterModel extends CI_Model {
  public function getData($courseRegisterId){
    $this->db->where('course_register_id', $courseRegisterId);
    $query = $this->db->get('tbl_course_register');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function getList($courseBuyerId){
    $this->db->where('course_buyer_id', $courseBuyerId);
    $this->db->where('is_delete', 0);
    return $this->db->get('tbl_course_register');
  }
  public function insert($data){
    $this->load->model(array('course/CourseModel', 'course/CourseCategoryModel'));
    $course = $this->CourseModel->getData($data['course_id']);
    $courseCategoryCode = $this->CourseCategoryModel->getCourseCategoryCode($course->course_category_id);
    $courseCode = $courseCategoryCode.$course->course_pre_code.str_pad($course->running_number,2,"0",STR_PAD_LEFT);
    $runningNumber = $this->getNextRunningNumber($data['course_id']);
    $courseRegisterCode = 'NTC'.$courseCode.str_pad($runningNumber,3,"0",STR_PAD_LEFT);
    $this->db->set('course_buyer_id', $data['course_buyer_id']);
    $this->db->set('member_id', $this->session->userdata('member_id'));
    $this->db->set('course_id', $data['course_id']);
    $this->db->set('ticket_id', $data['ticket_id']);
    $this->db->set('course_register_code', $courseRegisterCode);
    $this->db->set('running_number', $runningNumber);
    $this->db->set('register_firstname', $data['register_firstname']);
    $this->db->set('register_lastname', $data['register_lastname']);
    $this->db->set('register_email', $data['register_email']);
    $this->db->set('register_mobile', $data['register_mobile']);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', 0);
    $this->db->insert('tbl_course_register');
    $courseRegisterId = $this->db->insert_id();
    return $courseRegisterId;
  }
  private function getNextRunningNumber($courseId){
    $this->db->select('MAX(running_number) AS max_running_number');
    $this->db->where('course_id', $courseId);
    $query = $this->db->get('tbl_course_register');
    if($query->num_rows() > 0){
      return $query->row()->max_running_number + 1;
    }
    return 1;
  }
}
