<?php
class CourseLocationModel extends CI_Model {
  public function getList(){
    $this->db->where('is_delete', 0);
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_course_location');
  }
  public function getName($courseLocationId, $lang = 'thai'){
    $courseLocation = $this->getData($courseLocationId);
    if($courseLocation){
      if($lang == 'thai'){
        return $courseLocation->course_location_name_th;
      }else{
        return $courseLocation->course_location_name_en;
      }
    }
    return '-';
  }
  public function getDetail($courseLocationId, $lang = 'thai'){
    $courseLocation = $this->getData($courseLocationId);
    if($courseLocation){
      if($lang == 'thai'){
        return $courseLocation->course_lcoation_detail_th;
      }else{
        return $courseLocation->course_location_detail_en;
      }
    }
  }
  public function getData($courseLocationId){
    $this->db->where('course_location_id', $courseLocationId);
    $this->db->where('is_delete', 0);
    $query = $this->db->get('tbl_course_location');
    if($query->num_rows() > 0){
        return $query->row();
    }
    return false;
  }
}
