<?php
class TicketModel extends CI_Model {
  function __construct() {
    parent::__construct();
  }
  public function getList($courseId){
    $this->db->where('is_delete', 0);
    $this->db->where('course_id', $courseId);
    $this->db->where('start_date <=', 'CURDATE()', false);
    $this->db->where('end_date >=', 'CURDATE()', false);
    $this->db->where('enable_status', 1);
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_ticket');
  }
  public function getData($ticketId){
    $this->db->where('ticket_id', $ticketId);
    $this->db->where('is_delete', 0);
    $query = $this->db->get('tbl_ticket');
    if($query->num_rows() > 0){
      return $query->row();
    }
    return false;
  }
}
