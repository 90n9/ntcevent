<?php
class PriceRangeModel extends CI_Model {
  function __construct() {
    parent::__construct();
  }
  public function getList(){
    $this->db->where('is_delete', 0);
    $this->db->order_by('price_from', 'ASC');
    return $this->db->get('tbl_price_range');
  }
  public function getData($priceRangeId){
    $this->db->where('price_range_id', $priceRangeId);
    $this->db->where('is_delete', 0);
    $query = $this->db->get('tbl_price_range');
    if($query->num_rows() > 0){
        return $query->row();
    }
    return false;
  }
  public function getMaxPrice(){
    $this->db->where('is_delete',0);
    $this->db->get('tbl_price_range');
  }
}
