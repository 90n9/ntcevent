<?php
class CourseCategoryModel extends CI_Model {
  public function getUnderId($courseCategoryId, $retVal = array()){
    array_push($retVal, $courseCategoryId);
    $courseCategoryList = $this->getList($courseCategoryId);
    foreach($courseCategoryList->result() as $courseCategory){
      $retVal = $this->getUnderId($courseCategory->course_category_id, $retVal);
    }
    return $retVal;
  }
  public function getCourseCategoryCode($courseCategoryId){
    $retVal = '';
    do{
      $courseCategory = $this->getData($courseCategoryId);
      $courseCategoryId = $courseCategory->parent_id;
      if($courseCategoryId == 0){
        $retVal = $courseCategory->course_category_code;
      }
    }while($courseCategoryId != 0);
    return $retVal;
  }
  public function getList($parentId){
    $this->db->where('parent_id', $parentId);
    $this->db->where('is_delete', 0);
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_course_category');
  }
  public function getName($courseCategoryId, $lang = 'thai'){
    $courseCategory = $this->getData($courseCategoryId);
    if($courseCategory){
      if($lang == 'thai'){
        return $courseCategory->course_category_name_th;
      }else{
        return $courseCategory->course_category_name_en;
      }
    }
    return '-';
  }
  public function getData($courseCategoryId){
    $this->db->where('course_category_id', $courseCategoryId);
    $this->db->where('is_delete', 0);
    $query = $this->db->get('tbl_course_category');
    if($query->num_rows() > 0){
      return $query->row();
    }
    return false;
  }
}
