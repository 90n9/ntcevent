<?php
class CourseBuyerModel extends CI_Model {
  public function getList($courseBuyerStatus = ''){
    if($courseBuyerStatus != ''){
      $this->db->where('course_buyer_status', $courseBuyerStatus);
    }else{
      $this->db->where('course_buyer_status !=', 'cancel');
    }

    $this->db->where('member_id', $this->session->userdata('member_id'));
    $this->db->where('is_delete', 0);
    $this->db->order_by('course_buyer_id', 'DESC');
    return $this->db->get('tbl_course_buyer');
  }
  public function getDataByCode($courseBuyerCode){
    $this->db->where('course_buyer_code', $courseBuyerCode);
    $this->db->where('is_delete', 0);
    $query = $this->db->get('tbl_course_buyer');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function getData($courseBuyerId){
    $this->db->where('course_buyer_id', $courseBuyerId);
    $query = $this->db->get('tbl_course_buyer');
    if($query->num_rows() > 0){
      return $query->row();
    }
    return false;
  }
  public function insert($data){
    $this->load->model(array('course/CourseModel', 'course/CourseCategoryModel'));
    $course = $this->CourseModel->getData($data['course_id']);
    $courseCategoryCode = $this->CourseCategoryModel->getCourseCategoryCode($course->course_category_id);
    $courseCode = $courseCategoryCode.$course->course_pre_code.str_pad($course->running_number,2,"0",STR_PAD_LEFT);
    $runningNumber = $this->getNextRunningNumber($data['course_id']);
    $courseBuyerCode = 'OD'.$courseCode.str_pad($runningNumber,3,"0",STR_PAD_LEFT);
    $this->db->set('course_id', $data['course_id']);
    $this->db->set('member_id', $this->session->userdata('member_id'));
    $this->db->set('course_buyer_code', $courseBuyerCode);
    $this->db->set('running_number', $runningNumber);
    $this->db->set('course_buyer_status', 'waiting');
    $this->db->set('discount_code', $data['discount_code']);
    $this->db->set('discount_id', $data['discount_id']);
    $this->db->set('is_company', $data['is_company']);
    $this->db->set('want_payment_type', $data['want_payment_type']);
    $this->db->set('total_ticket', $data['total_ticket']);
    $this->db->set('company_name', $data['company_name']);
    $this->db->set('company_address', $data['company_address']);
    $this->db->set('company_branch', $data['company_tax']);
    $this->db->set('company_tax', $data['company_tax']);
    $this->db->set('buyer_firstname', $data['buyer_firstname']);
    $this->db->set('buyer_lastname', $data['buyer_lastname']);
    $this->db->set('buyer_email', $data['buyer_email']);
    $this->db->set('buyer_mobile', $data['buyer_mobile']);
    $this->db->set('buyer_company', $data['buyer_company']);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', 0);
    $this->db->insert('tbl_course_buyer');
    $courseBuyerId = $this->db->insert_id();
    return $courseBuyerId;
  }
  public function updateCost($courseBuyerId, $data){
    $this->db->set('total_amount',$data['total_amount']);
    $this->db->set('discount_amount', $data['discount_amount']);
    $this->db->set('surcharge_amount', $data['surcharge_amount']);
    $this->db->set('vat_amount',$data['vat_amount']);
    $this->db->set('paid_amount',$data['paid_amount']);
    $this->db->where('course_buyer_id', $courseBuyerId);
    $this->db->update('tbl_course_buyer');
    if($data['paid_amount'] == 0){
      $this->payment($courseBuyerId);
    }
  }
  public function updateStatus($courseBuyerId, $courseBuyerStatus){
    $this->db->set('course_buyer_status', $courseBuyerStatus);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', 0);
    $this->db->where('course_buyer_id', $courseBuyerId);
    $this->db->update('tbl_course_buyer');
  }
  public function cancel($courseBuyerId){
    $this->db->set('course_buyer_status', 'cancel');
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', 0);
    $this->db->where('course_buyer_id', $courseBuyerId);
    $this->db->where('member_id', $this->session->userdata('member_id'));
    $this->db->update('tbl_course_buyer');
  }
  public function payment($courseBuyerId){
    $this->db->set('course_buyer_status', 'payment');
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', 0);
    $this->db->where('course_buyer_id', $courseBuyerId);
    $this->db->where('member_id', $this->session->userdata('member_id'));
    $this->db->update('tbl_course_buyer');
  }
  private function getNextRunningNumber($courseId){
    $this->db->select('MAX(running_number) AS max_running_number');
    $this->db->where('course_id', $courseId);
    $query = $this->db->get('tbl_course_buyer');
    if($query->num_rows() > 0){
      return $query->row()->max_running_number + 1;
    }
    return 1;
  }
}
