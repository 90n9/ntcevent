<?php
class SpeakerModel extends CI_Model {
  function __construct() {
    parent::__construct();
  }
  public function getData($speakerId){
    $this->db->where('speaker_id', $speakerId);
    $this->db->where('is_delete', 0);
    $query = $this->db->get('tbl_speaker');
    if($query->num_rows() > 0){
      return $query->row();
    }
    return false;
  }
}
