<?php
class DiscountModel extends CI_Model {
  function __construct() {
    parent::__construct();
  }
  public function getDataByCode($courseId, $discountCode){
    $this->db->where('course_id', $courseId);
    $this->db->where('discount_code', $discountCode);
    $this->db->where('is_delete', 0);
    $this->db->where('enable_status', 1);
    $query = $this->db->get('tbl_discount');
    if($query->num_rows() > 0){
      return $query->row();
    }
    return false;
  }
  public function getData($discountId){
    $this->db->where('discount_id', $discountId);
    $this->db->where('is_delete', 0);
    $this->db->where('enable_status', 1);
    $query = $this->db->get('tbl_discount');
    if($query->num_rows() > 0){
      return $query->row();
    }
    return false;
  }
}
