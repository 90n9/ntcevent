<?php
class CourseContactModel extends CI_Model {
  public function getData($courseContactId){
    $this->db->where('course_contact_id', $courseContactId);
    $query = $this->db->get('tbl_course_contact');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function insert($data){
    $this->db->set('course_id', $data['course_id']);
    if($this->LoginService->getLoginStatus()){
      $this->db->set('member_id', $this->session->userdata('member_id'));
    }else{
      $this->db->set('member_id', 0);
    }
    $this->db->set('total_ticket', $data['total_ticket']);
    $this->db->set('contact_firstname', $data['contact_firstname']);
    $this->db->set('contact_lastname', $data['contact_lastname']);
    $this->db->set('contact_email', $data['contact_email']);
    $this->db->set('contact_phone', $data['contact_phone']);
    $this->db->set('contact_mobile', $data['contact_mobile']);
    $this->db->set('contact_company', $data['contact_company']);
    $this->db->set('contact_note', $data['contact_note']);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', 0);
    $this->db->insert('tbl_course_contact');
    return $this->db->insert_id();
  }
}
