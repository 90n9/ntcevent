<?php
class CourseFilterService extends CI_Model {
  function __construct() {
    parent::__construct();
    $this->load->model(array('course/CourseCategoryModel', 'course/CourseModel', 'course/PriceRangeModel', 'course/DocumentModel', 'course/TicketModel'));
  }
  public function getContent($searchData){
    if($searchData['type'] == 'upcoming'){
      $searchData['order'] = 'course_start';
      $searchData['sort'] = 'ASC';
    }elseif($searchData['type'] == 'lastest'){
      $searchData['order'] = 'sort_priority';
      $searchData['sort'] = 'ASC';
    }
    $searchData['catid'] = ($searchData['catid'])?$searchData['catid']:0;
    $data = $searchData;
    $data['courseUrl'] = $this->getListUrl($searchData);
    $data['courseCount'] = $this->getCount($searchData);
    $data['courseActiveCount'] = $this->getCountActive($searchData);
    $data['courseList'] = $this->getList($searchData);
    $data['courseCloseList'] = $this->getCloseList($searchData, $data['courseCount'], $data['courseActiveCount']);
    return $data;
  }
  private function getCount($data){
    $this->queryWhere($data);
    $this->db->select('tbl_course.course_id');
    $query = $this->db->get('tbl_course');
    return $query->num_rows();
  }
  private function getCountActive($data){
    $this->queryWhere($data);
    $this->db->select('tbl_course.course_id');
    $this->db->where('tbl_course.course_start >=','NOW()', false);
    $query = $this->db->get('tbl_course');
    return $query->num_rows();
  }
  private function getList($data){
    $sort = (isset($data['sort']))?$data['sort']:'ASC';
    $order = (isset($data['order']))?$data['order']:'tbl_course.course_start';
    $page = (isset($data['page']))?$data['page']:1;
    $perPage = (isset($data['perpage']))?$data['perpage']:15;
    $this->queryWhere($data);
    $this->db->where('tbl_course.course_start >=','NOW()', false);
    $this->db->select('tbl_course.*');
    $this->db->order_by($order, $sort);
    $offset = ($page - 1) * $perPage;
    return $this->db->get('tbl_course', $perPage, $offset);
  }
  private function getCloseList($data, $totalCourse, $activeCourse){
    $page = (isset($data['page']))?$data['page']:1;
    $perPage = (isset($data['perpage']))?$data['perpage']:15;
    $this->queryWhere($data);
    $this->db->select('tbl_course.*');
    $this->db->where('tbl_course.course_start <','NOW()', false);
    $this->db->order_by('tbl_course.course_start', 'DESC');
    $offset = ($page - 1) * $perPage;
    $showActiveCourse = $activeCourse - $offset;
    $showActiveCourse = ($showActiveCourse < 0)?0:$showActiveCourse;
    $offsetClose = $offset - $activeCourse;
    $offsetClose = ($offsetClose < 0)?0:$offsetClose;
    $perPageClose = $perPage - $showActiveCourse;
    $perPageClose = ($perPageClose < 0)?0:$perPageClose;
    return $this->db->get('tbl_course', $perPageClose, $offsetClose);
  }
  private function queryWhere($data){
    $price = false;
    if(isset($data['price']) && $data['price'] != '' && $price != 'free' && $price != 'contact'){
      $price = $this->PriceRangeModel->getData($data['price']);
    }
    if(isset($data['catid'])){
      $categoryId = $data['catid'];
      if($categoryId > 0){
        $arrCourseCategoryId = $this->CourseCategoryModel->getUnderId($categoryId);
        $this->db->where_in('tbl_course.course_category_id', $arrCourseCategoryId);
      }
    }
    if(isset($data['price']) && $data['price'] != ''){
      if($data['price'] == 'free' || $data['price'] == 'contact'){
        $this->db->where('tbl_ticket.price_type', $data['price']);
      }else{
        $this->db->where('tbl_ticket.price_type', 'amount');
        $this->db->where('tbl_ticket.price >=', $price->price_from);
        $this->db->where('tbl_ticket.price <=', $price->price_to);
      }
      $this->db->join('tbl_ticket', 'tbl_ticket.course_id = tbl_course.course_id AND tbl_ticket.end_date > NOW() AND tbl_ticket.enable_status = 1 AND tbl_ticket.is_delete = 0', 'left');
    }
    if(isset($data['text']) && $data['text'] != ''){
      $strWhere = "(course_name like '%".$data['text']."%' ";
      $strWhere .= "OR course_description like '%".$data['text']."%' ";
      $strWhere .= ')';
      $this->db->where($strWhere, NULL,FALSE);
    }
    if($data['type'] == 'upcoming'){
      $this->db->where('tbl_course.course_start >', 'NOW()', false);
    }
    $this->db->where('tbl_course.enable_status', 1);
    $this->db->where('tbl_course.is_delete', 0);
    $this->db->distinct();
  }
  private function getListUrl($data){
    $retVal = 'course?sh=1';
    $retVal .= (isset($data['catid']))?'&catid='.$data['catid']:'';
    $retVal .= (isset($data['type']))?'&type='.$data['type']:'';
    $retVal .= (isset($data['price']))?'&price='.$data['price']:'';
    $retVal .= (isset($data['sort']))?'&sort='.$data['sort']:'';
    $retVal .= (isset($data['order']))?'&order='.$data['order']:'';
    $retVal .= (isset($data['text']))?'&text='.$data['text']:'';
    return $retVal;
  }
}
