<?php

class DateTimeService extends CI_Model {

  function __construct() {
    parent::__construct();
  }
  public function displayDateList($strDateList, $lang= 'thai'){
    if($strDateList == ''){
      return '';
    }
    $dateList = explode(",", $strDateList);
    $total_date = count($dateList);
    if($total_date <= 1){
      return $this->displayDate($strDateList, $lang);
    }
    $currentDate = $dateList[0];
    $strDate = $this->displayDateSmall($dateList[0], $lang);
    $lastPrintDate = $dateList[0];
    foreach($dateList as $key => $dateData){
      if($key == 0){
        continue;
      }
      $nextCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($currentDate)));
      if($nextCurrentDate != $dateData){
        if($lastPrintDate != $currentDate){
          $strDate .= ' - '.$this->displayDateSmall($currentDate, $lang);
        }
        $strDate .= ', '.$this->displayDateSmall($dateData, $lang);
        $lastPrintDate = $dateData;
      }
      if($dateData == $dateList[$total_date - 1] && $dateData != $lastPrintDate){
        if($nextCurrentDate == $dateData){
          $strDate .= '-';
        }else{
          $strDate .= ',';
        }
        $strDate .= $this->displayDateSmall($dateData, $lang);
      }
      $currentDate = $dateData;
    }
    return $strDate;
  }
  public function displayDateSmall($strDate, $lang = 'thai'){
    list($year,$month,$day) = explode('-', $strDate);
    return $day.' '.$this->getMonthName($month, $lang).' '.$year;
  }
  public function displayOnlyDate($strDateTime, $lang='thai'){
    list($strDate, $strTime) = explode(' ', $strDateTime);
    return $this->displayDate($strDate, $lang);
  }
  public function displayTime($strTime, $lang='thai'){
    if($strTime == ''){
      return '';
    }
    $arrTimeList = explode(':', $strTime);
    $hour = $arrTimeList[0];
    $min = $arrTimeList[1];
    if($lang == 'thai'){
      return $hour.':'.$min.' น.';
    }else{
      $suffix = ($hour >= 12)?'PM':'AM';
      return (($hour>12)?$hour % 12:$hour).':'.$min.' '.$suffix;
    }
  }
  public function displayDate($strDate, $lang = 'thai'){
    list($year,$month,$day) = explode('-', $strDate);
    return $day.' '.$this->getMonthName($month, $lang).' '.$year;
  }
  public function displayDateTime($strDateTime, $lang = 'thai'){
    if($strDateTime == ''){
      return '';
    }
    list($strDate, $strTime) = explode(' ', $strDateTime);
    return '<i class="icon-calendar"></i> '.$this->displayDate($strDate, $lang).' <i class=" icon-clock"></i> '.$this->displayTime($strTime, $lang);
  }
  public function getMonthName($monthId, $lang){
    $monthId = intval($monthId);
    $monthTh = array('','ม.ค.','ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.');
    $monthEn = array('','JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC');
    return ($lang == 'thai')?$monthTh[$monthId]:$monthEn[$monthId];
  }
  public function getDateDiff($startDateTime, $endDateTime, $lang = 'thai'){
    $startDate = strtotime($startDateTime);
    $endDate = strtotime($endDateTime);
    $datediff = $endDate - $startDate;
    $dateDiff = ceil($datediff/(60*60*24));
    return $dateDiff.' '.(($lang=='thai')?'วัน':(($dateDiff > 1)?'Days':'Day'));
  }
}
