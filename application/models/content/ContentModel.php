<?php
class ContentModel extends CI_Model {
  function __construct() {
    parent::__construct();
  }
  public function getText($contentCode, $lang){
    $this->db->where('content_code', $contentCode);
    $query = $this->db->get('tbl_content');
    if($query->num_rows() > 0){
      $content = $query->row();
      if($lang == 'thai'){
        return $content->content_text_th;
      }else{
        return $content->content_text_en;
      }
    }
    return '';
  }
}
