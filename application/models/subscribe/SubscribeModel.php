<?php
class SubscribeModel extends CI_Model {
  function __construct() {
    parent::__construct();
  }
  public function isDuplicate($email){
    $this->db->where('email', trim($email));
    $this->db->where('is_delete', 0);
    $query = $this->db->get('tbl_subscribe');
    if($query->num_rows() > 0){
      return true;
    }
    return false;
  }
  public function insert($email){
    $this->db->set('email', $email);
    $this->db->set('is_delete', 0);
    $this->db->set('create_date', 'now()', false);
    $this->db->insert('tbl_subscribe');
    $subscribeId = $this->db->insert_id();
    return $subscribeId;
  }
}
