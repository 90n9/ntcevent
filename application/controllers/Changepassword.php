<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Changepassword extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model(array('member/MemberModel'));
    $idiom = $this->MasterpageService->getPageLang();
    $this->lang->load('changepassword', $idiom);
  }
  public function index(){
    $this->LoginService->mustLogin();
    $idiom = $this->MasterpageService->getPageLang();
    $this->lang->load('profile', $idiom);
    $data = array();
    $data['member'] = $this->MemberModel->getData($this->session->userdata('member_id'));
    $content = $this->load->view('changepassword/form', $data, true);
    $this->MasterpageService->addJs('assets/js/page/changepassword/form.js');
    $this->MasterpageService->display($content, 'Change Password', 'changepassword');
  }
  public function form_post(){
    $this->LoginService->mustLogin();
    $password = trim($this->input->post('password'));
    $confirm_password = trim($this->input->post('confirm_password'));
    if($password == '' || $password != $confirm_password){
      redirect('changepassword/index/error');
    }
    $this->MemberModel->changePassword($this->session->userdata('member_id'), $password);
    redirect('changepassword/success');
  }
  public function success(){
    $this->LoginService->mustLogin();
    $data = array();
    $content = $this->load->view('changepassword/success', $data, true);
    $this->MasterpageService->display($content, 'Change Password', 'changepassword');
  }
}
