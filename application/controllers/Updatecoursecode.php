<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UpdateCourseCode extends CI_Controller {
	public function index(){
    $this->db->set('course_code', '');
    $this->db->set('course_pre_code', '');
    $this->db->set('running_number', 0);
    $this->db->update('tbl_course');
    $this->updateCode();
    redirect('updatebuyercode');
	}
  private function updateCode(){
    $this->db->order_by('course_id', 'ASC');
    $query = $this->db->get('tbl_course');
    foreach($query->result() as $course){
      $coursePreCode = $this->getPreCodeFromCreatedate($course->create_date);
      $runningNumber = $this->getNextRunningNumber($coursePreCode);
      $courseCode = 'NTC'. $this->getCourseCategoryCode($course->course_category_id).$coursePreCode.str_pad($runningNumber,2,"0",STR_PAD_LEFT);
      $this->updateToDb($course->course_id, $courseCode, $coursePreCode, $runningNumber);
	  }
  }
  private function getCourseCategoryCode($courseCategoryId){
    $retVal = '';
    do{
      $courseCategory = $this->getCourseCategoryData($courseCategoryId);
    	if($courseCategory){
      	$courseCategoryId = $courseCategory->parent_id;
    		if($courseCategoryId == 0){
        	$retVal = $courseCategory->course_category_code;
      	}
    	}else{
      	return '';
      }
    }while($courseCategoryId != 0);
    return $retVal;
  }
  private function getCourseCategoryData($courseCategoryId){
    $this->db->where('course_category_id', $courseCategoryId);
    $this->db->where('is_delete', 0);
    $query = $this->db->get('tbl_course_category');
    if($query->num_rows() > 0){
      return $query->row();
    }
    return false;
  }
  private function getPreCodeFromCreatedate($createDate){
    return date('ym', strtotime($createDate));
  }
  private function getNextRunningNumber($coursePreCode){
    $this->db->select('MAX(running_number) AS max_running_number');
    $this->db->where('course_pre_code', $coursePreCode);
    $query = $this->db->get('tbl_course');
    if($query->num_rows() > 0){
      return $query->row()->max_running_number + 1;
    }
    return 1;
  }
  private function updateToDb($courseId, $courseCode, $coursePreCode, $runningNumber){
    $this->db->set('course_code', $courseCode);
    $this->db->set('course_pre_code', $coursePreCode);
    $this->db->set('running_number', $runningNumber);
    $this->db->where('course_id', $courseId);
    $this->db->update('tbl_course');
  }
}
