<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Terms extends CI_Controller {
	public function index(){
		$this->load->model(array('content/ContentModel'));
		$idiom = $this->MasterpageService->getPageLang();
		$this->lang->load('home', $idiom);
		$data = array();
		$data['lang'] = $idiom;
		$data['contentText'] = $this->ContentModel->getText('terms', $idiom);
		$content = $this->load->view('content/terms', $data, true);
		$this->MasterpageService->display($content, 'Terms of conditions', 'terms');
	}
}
