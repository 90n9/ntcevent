<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Privacy extends CI_Controller {
	public function index(){
		$this->load->model(array('content/ContentModel'));
		$idiom = $this->MasterpageService->getPageLang();
		$this->lang->load('home', $idiom);
		$data = array();
		$data['lang'] = $idiom;
		$data['contentText'] = $this->ContentModel->getText('privacy', $idiom);
		$content = $this->load->view('content/privacy', $data, true);
		$this->MasterpageService->display($content, 'About', 'privacy');
	}
}
