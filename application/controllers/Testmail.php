<?php
class Testmail extends CI_Controller{
  public function index(){
    $this->load->model('SendMailService');
    //$mailTo = 'noreply@trainingcenter.co.th';
    //$mailTo = 'tholop@gmail.com';
    $mailTo = 'web-eiSBqV@mail-tester.com';
    $content = $this->test_course_bank_payment();
    $this->SendMailService->send_mail($mailTo, '', 'test forgotpassword content', $content);
    //echo 'complete';
    echo $content;
  }
  private function test_course_free(){
    $course = new stdClass();
    $course->cover_image = '';
    $course->course_name = 'ITIL SYSTEM MANAGEMENT';
    $course->course_start = '2016-11-13 20:10:10000';
    $courseBuyer = new stdClass();
    $courseBuyer->buyer_firstname = 'Narathip';
    $content = $this->load->view('course/mail/coursefree',array(
      'course' => $course,
      'courseBuyer' => $courseBuyer,
      'ticketUrl' => 'testverify'
    ), true);
    return $content;
  }
  private function test_course_bank_payment(){
    $course = new stdClass();
    $course->cover_image = '';
    $course->course_name = 'ITIL SYSTEM MANAGEMENT';
    $course->course_start = '2016-11-13 20:10:10000';
    $courseBuyer = new stdClass();
    $courseBuyer->buyer_firstname = 'Narathip';
    $courseBuyer->paid_amount = 16500;
    $content = $this->load->view('course/mail/coursebankpayment',array(
      'course' => $course,
      'courseBuyer' => $courseBuyer,
      'courseBuyerCodeHash' => 'testverify'
    ), true);
    return $content;
  }
  private function test_forgotpassword(){
    $member = new stdClass();
    $member->firstname = 'Narathip';
    $content = $this->load->view('forgotpassword/forgotpasswordemail',array(
      'member' => $member,
      'linkUrl' => 'testverify'
    ), true);
    return $content;
  }
}
