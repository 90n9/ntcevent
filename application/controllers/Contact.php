<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {
  function __construct() {
    parent::__construct();
    $idiom = $this->MasterpageService->getPageLang();
    $this->lang->load('contact', $idiom);
  }
  public function index($showSucess = ''){
    $data = array();
    $content = $this->load->view('contact/index', array(), true);
    $this->MasterpageService->addCss('assets/js/fancybox/source/jquery.fancybox.css');
    $this->MasterpageService->addJs('assets/js/fancybox/source/jquery.fancybox.js');
    $this->MasterpageService->addJs('assets/js/page/contact.js');
    $this->MasterpageService->display($content, 'Contact', 'contact');
  }
  public function form_post(){
    $data = array();
    $data['contact_firstname'] = $this->input->post('txtFirstname');
    $data['contact_lastname'] = $this->input->post('txtLastname');
    $data['contact_email'] = $this->input->post('txtEmail');
    $data['contact_phone'] = $this->input->post('txtPhone');
    $data['contact_detail'] = $this->input->post('txtDetail');
    $this->load->model(array('contact/ContactService'));
    $this->ContactService->contact($data);
    redirect('contact/success');
  }
  public function success(){
    $content = $this->load->view('contact/complete', array(), true);
    $this->MasterpageService->display($content, 'Contact', 'contact');
  }
}
