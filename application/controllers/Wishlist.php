<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Wishlist extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model(array('member/MemberModel', 'wishlist/WishlistModel', 'course/CourseModel', 'course/CourseLocationModel'));
    $idiom = $this->MasterpageService->getPageLang();
    $this->lang->load('home', $idiom);
  }
  public function index(){
    $idiom = $this->MasterpageService->getPageLang();
    $data = array();
    $data['wishlistList'] = $this->WishlistModel->getList();
    $data['lang'] = $idiom;
    $content = $this->load->view('wishlist/index', $data, true);
    $this->MasterpageService->addJs('assets/js/page/wishlist.js');
    $this->MasterpageService->display($content, 'Wishlist', 'wishlist');
  }
  public function add($courseId){
    if($this->LoginService->getLoginStatus()){
      $this->WishlistModel->insert($courseId);
      echo 'success';
    }else{
      echo 'Please Login';
    }
  }
  public function remove($courseId){
    if($this->LoginService->getLoginStatus()){
      $this->WishlistModel->delete($courseId);
      echo 'success';
    }else{
      echo 'Please Login';
    }
  }
}
