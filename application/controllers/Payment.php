<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model(array('course/CourseBuyerModel', 'course/CourseModel', 'payment/PaymentModel', 'payment/PaymentItemModel', 'member/MemberModel'));
        $idiom = $this->MasterpageService->getPageLang();
        $this->lang->load('payment', $idiom);
        $this->lang->load('course', $idiom);
    }
    public function index($courseBuyerId = 0, $paymentType='bank'){
      $this->LoginService->mustLogin();
      $data = array();
      $data['courseBuyerId'] = $courseBuyerId;
      if($courseBuyerId > 0){
        $data['courseBuyerList'] = array($courseBuyerId);
        $data['paymentType'] = $paymentType;
      }else{
        if(!empty($_POST)){
          $this->session->set_flashdata('courseBuyerList', $this->input->post('chkCourseBuyerId'));
          $this->session->set_flashdata('paymentType', $this->input->post('payment'));
        }else{
          $this->session->keep_flashdata('courseBuyerList');
          $this->session->keep_flashdata('paymentType');
        }
        $data['courseBuyerList']  = $this->session->flashdata('courseBuyerList');;
        $data['paymentType'] = $this->session->flashdata('paymentType');;
      }
      if(sizeof($data['courseBuyerList']) < 1){
        redirect('mycourse/index/waiting');
      }
      $this->show_payment($data);
    }
    public function bymail($paymentType, $hashCode){
      $this->load->model('EncodeService');
      $courseBuyerCode = ltrim($this->EncodeService->decode_number($hashCode), '0');
      $courseBuyer = $this->CourseBuyerModel->getDataByCode($courseBuyerCode);
      if($courseBuyer->course_buyer_status == 'waiting'){
        $member = $this->MemberModel->getData($courseBuyer->member_id);
        $this->LoginService->createSession($member);
        $this->show_payment(array(
          'courseBuyerList' => array($courseBuyer->course_buyer_id),
          'paymentType' => $paymentType,
          'courseBuyerId' => $courseBuyer->course_buyer_id
        ));
      }else{
        echo 'something broken!';
      }
    }
    private function show_payment($data){
      $data['paymentType'] = ($data['paymentType'] == 'bank')?'bank':'creditcard';
      $totalAmount = 0;
      foreach($data['courseBuyerList'] as $courseBuyerId){
        $courseBuyer = $this->CourseBuyerModel->getData($courseBuyerId);
        $totalAmount += $courseBuyer->paid_amount;
      }
      $data['totalAmount'] = $totalAmount;
      $data['member'] = $this->MemberModel->getData($this->session->userdata('member_id'));
      $title = $data['title'] = 'แจ้งชำระเงิน';
      $content = $this->load->view('payment/index', $data, true);
      $this->MasterpageService->addCss('assets_admin/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css');
      $this->MasterpageService->addJs('assets_admin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js?v=2');
      $this->MasterpageService->addCss('assets/css/single_course.css');
      $this->MasterpageService->addJs('assets/js/page/payment/index.js?v=2');
      $this->MasterpageService->display($content, $title, 'payment');
    }
    public function choosepaid(){
      $this->LoginService->mustLogin();
      $data = array();
      $data['courseBuyerList'] = $this->CourseBuyerModel->getUnpaidList();
      $title = $data['title'] = 'แจ้งชำระเงิน';
      $content = $this->load->view('payment/index', $data, true);
      $this->MasterpageService->addCss('assets/css/single_course.css');
      $this->MasterpageService->addCss('assets/check_radio/skins/square/aero.css');
      $this->MasterpageService->addJs('assets/check_radio/jquery.icheck.js');
      $this->MasterpageService->addJs('assets/js/page/payment/offline.js');
      $this->MasterpageService->display($content, $title, 'payment');
    }
    public function form_post(){
        $this->LoginService->mustLogin();
        $arrOrderId = $this->input->post('chkBuyerId');
        $paymentData = array();
        $paymentData['payment_type'] = $this->input->post('paymentChannel');
        $paymentData['order_amount'] = 0;
        $paymentData['surcharge_amount'] = 0;
        $paymentData['vat_amount'] = 0;
        if($paymentData['payment_type'] == 'creditcard'){
          $paymentData['payment_amount'] = str_replace(',', '', $this->input->post('txtAmount'));
          $paymentData['payment_date'] = '';
          $paymentData['payment_time'] = '';
          $paymentData['payment_file'] = '';
          $paymentData['note'] = '';
        }else{
          $paymentData['payment_amount'] = str_replace(',', '', $this->input->post('txtAmount'));
          $paymentData['payment_date'] = $this->input->post('txtPaidDate');
          $paymentData['payment_time'] = '';
          $paymentData['note'] = $this->input->post('txtNote');
          $paymentData['payment_file'] = '';
          $this->load->model(array('UploadImageModel'));
          $uploadData = $this->UploadImageModel->uploadSingleImage('payment_file', 'fileUpload');
          if($uploadData['success']){
            $paymentData['payment_file'] = $uploadData['imageFile'];
          }
        }
        $paidAmount = $paymentData['payment_amount'];
        $paymentId = $this->PaymentModel->insert($paymentData);
        $countOrder = count($arrOrderId);
        $orderAmount = 0;
        for($i = 0; $i<$countOrder; $i++){
          $courseBuyer = $this->CourseBuyerModel->getData($arrOrderId[$i]);
          $courseBuyerAmount = $courseBuyer->total_amount - $courseBuyer->discount_amount;
          $dataItem = array();
          $dataItem['payment_id'] = $paymentId;
          $dataItem['course_buyer_id'] = $arrOrderId[$i];
          $dataItem['course_buyer_amount'] = $courseBuyerAmount;
          $orderAmount += $courseBuyerAmount;
          $this->PaymentItemModel->insert($dataItem);
          $this->CourseBuyerModel->payment($arrOrderId[$i]);
        }
        $vatAmount = $orderAmount * 0.07;
        if($paymentData['payment_type'] == 'creditcard'){
          $surchargeAmount = $orderAmount * $this->ConfigModel->getData('kbank_fee')/100;
          $vatAmount = ($orderAmount + $surchargeAmount) * 0.07;
          $paidAmount = $orderAmount + $surchargeAmount + $vatAmount;
          $priceData = array();
          $priceData['order_amount'] = $orderAmount;
          $priceData['surcharge_amount'] = $surchargeAmount;
          $priceData['vat_amount'] = $vatAmount;
          $priceData['payment_amount'] = $paidAmount;
          $this->PaymentModel->updatePrice($paymentId, $priceData);
          redirect('payment/post_creditcard/'.$paymentId);
        }else{
          $priceData = array();
          $priceData['order_amount'] = $orderAmount;
          $priceData['surcharge_amount'] = 0;
          $priceData['vat_amount'] = $vatAmount;
          $priceData['payment_amount'] = $paidAmount;
          $this->PaymentModel->updatePrice($paymentId, $priceData);
          $this->mail_admin_bank($paymentId);
          redirect('payment/success/');
        }
    }
    public function mail_admin_bank($paymentId){
      $this->load->model(array('ConfigModel', 'SendMailService', 'payment/PaymentModel', 'payment/PaymentItemModel', 'course/CourseBuyerModel', 'course/CourseModel'));
      $payment = $this->PaymentModel->getData($paymentId);
      $paymentItemList = $this->PaymentItemModel->getList($paymentId);
      $member = $this->MemberModel->getData($payment->member_id);
      $content = $this->load->view('payment/mail/emailadminbank', array(
        'payment' => $payment,
        'paymentItemList' => $paymentItemList,
        'member' => $member
      ), true);
      $courseNameList = '';
      foreach($paymentItemList->result() as $paymentItem){
        $courseBuyer = $this->CourseBuyerModel->getData($paymentItem->coure_buyer_id);
        $course = $this->CourseModel->getData($courseBuyer->course_id);
        $courseNameList .= ' "'.$courseBuyer->course_buyer_code.' ('.$course->course_name.')" ';
      }
      $subject = '[Submit pay-in] Paid '.$courseNameList.' online.';
      $this->SendMailService->send_mail($this->ConfigModel->getData('email_finance'), '', $subject, $content);
    }
    public function post_creditcard($paymentId){
      $this->LoginService->mustLogin();
      $this->load->model(array('ConfigModel', 'payment/KbankModel'));
      $payment = $this->PaymentModel->getData($paymentId);
      $KbankData = $this->KbankModel->getData($paymentId, $payment->payment_amount, $payment->payment_code);
      $this->load->view('payment/post_creditcard', $KbankData);
    }
    public function success(){
      $data = array();
      $content = $this->load->view('payment/success', $data, true);
      $this->MasterpageService->display($content, 'Payment', 'payment');
    }
    public function confirm($paymentId = 0){
      $this->load->model(array('ConfigModel', 'payment/KbankModel'));
      $pmgwresp2 = $this->input->post('PMGWRESP2');
      $pmgwrespData = $this->KbankModel->insertResponse($paymentId, $pmgwresp2);
      $payment = $this->PaymentModel->getData($paymentId);
      if($payment->payment_type == 'creditcard' && $payment->payment_status != 'complete'){
        if($pmgwrespData['response_code'] == '00'){
          $transamount = floatval(substr_replace($pmgwrespData['transamount'], '.', 10, 0));
          if(abs($payment->payment_amount - $transamount)<1){
            $this->PaymentModel->updateComplete($paymentId);
            $this->sendmail_creditcard_complete($paymentId);
            $this->mail_admin_creditcard($paymentId);
            echo 'success update to complete payment and update register course and register ticket status';
          }else{
            $this->PaymentModel->updateWarning($paymentId);
            echo 'paymentamount:'.$payment->payment_amount;
            echo '<br />';
            echo '$transamount:'.$transamount;
            echo '<br />';
            echo 'this payment data is not same mark as warning';
          }
        }else{
          $this->PaymentModel->updateCancel($paymentId);
          echo 'this data got error message and update to payment';
        }
      }else{
        echo 'this data has been update already';
      }
    }
    public function mail_admin_creditcard($paymentId){
      $this->load->model(array('ConfigModel', 'SendMailService', 'payment/PaymentModel', 'payment/PaymentItemModel', 'course/CourseBuyerModel', 'course/CourseModel'));
      $payment = $this->PaymentModel->getData($paymentId);
      $paymentItemList = $this->PaymentItemModel->getList($paymentId);
      $member = $this->MemberModel->getData($payment->member_id);
      $content = $this->load->view('payment/mail/emailadmincreditcard', array(
        'payment' => $payment,
        'paymentItemList' => $paymentItemList,
        'member' => $member
      ), true);
      $courseNameList = '';
      foreach($paymentItemList->result() as $paymentItem){
        $courseBuyer = $this->CourseBuyerModel->getData($paymentItem->coure_buyer_id);
        $course = $this->CourseModel->getData($courseBuyer->course_id);
        $courseNameList .= ' "'.$courseBuyer->course_buyer_code.' ('.$course->course_name.')" ';
      }
      $subject = '[Credit Card] Paid '.$courseNameList.' online';
      $this->SendMailService->send_mail($this->ConfigModel->getData('email_finance'), '', $subject, $content);
    }
    public function sendmail_creditcard_complete($paymentId){
      $this->load->model(array('SendMailService', 'EncodeService', 'course/CourseRegisterModel', 'course/CourseLocationModel'));
      $paymentItemList = $this->PaymentItemModel->getList($paymentId);
      foreach($paymentItemList->result() as $paymentItem){
        $courseBuyer = $this->CourseBuyerModel->getData($paymentItem->course_buyer_id);
        $course = $this->CourseModel->getData($courseBuyer->course_id);
        $courseLocation = $this->CourseLocationModel->getData($course->course_location_id);
        $courseRegisterList = $this->CourseRegisterModel->getList($courseBuyer->course_buyer_id);
        $mapUrl = base_url('index.php/map/index/'.$courseLocation->course_location_id);
        foreach($courseRegisterList->result() as $courseRegister){
          $hashCode = $this->EncodeService->encode_number($courseRegister->course_register_id);
          $ticketUrl = base_url('index.php/mycourse/ticket/'.$hashCode);
          $content = $this->load->view('payment/mail/paymentsuccess', array(
            'course' => $course,
            'courseRegister' => $courseRegister,
            'courseLocation' => $courseLocation,
            'courseUrl' => base_url('index.php/course/detail/'.$course->course_id),
            'ticketUrl' => $ticketUrl,
            'mapUrl' => $mapUrl
          ), true);
          $subject = '[Ticket Sent] "'.$course->course_name.'" online to '.$courseRegister->register_firstname.' '.$courseRegister->register_lastname;
          $this->SendMailService->send_mail($courseBuyer->buyer_email, '', $subject, $content);
        }
      }
    }
    public function creditcardsuccess($paymentId){
      $success = false;
      $errorMsg = '';
      if(!empty($_POST)){
        $hostResp = $this->input->post('HOSTRESP');
        $refcode = $this->input->post('REFCODE');
        $authcode = $this->input->post('AUTHCODE');
        $returninv = $this->input->post('RETURNINV');
        $uaid = $this->input->post('UAID');
        $cardnumber = $this->input->post('CARDNUMBER');
        $amount = $this->input->post('AMOUNT');
        $curIso = $this->input->post('CURISO');
        $fillspace = $this->input->post('FILLSPACE');
        if($hostResp == '00'){
          $success = true;
        }else{
          $this->lang->load('kbank', 'english');
          $errMsg = $this->lang->line('kbank_resp_'.$hostResp);
        }
      }
      $payment = $this->PaymentModel->getData($returninv);
      $data = array();
      if($success){
        $paymentItemList = $this->PaymentItemModel->getList($payment->payment_id);
        $data['paymentId'] = $payment->payment_id;
        $data['payment'] = $payment;
        $data['paymentItemList'] = $paymentItemList;
        if($paymentItemList->num_rows() == 1){
          $data['courseBuyer'] = $this->CourseBuyerModel->getData($paymentItemList->row()->course_buyer_id);
          $data['course'] = $this->CourseModel->getData($data['courseBuyer']->course_id);
        }
        $content = $this->load->view('payment/creditcardsuccess', $data, true);
      }else{
        if($payment->payment_status == 'waiting'){
          $this->PaymentModel->updateCancel($payment->payment_id);
        }
        $data['errorMsg'] = $errorMsg;
        $content = $this->load->view('payment/creditcardfail', $data, true);
      }
      $this->MasterpageService->display($content, 'Payment', 'payment');
    }
}
