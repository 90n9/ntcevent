<?php
class Map extends CI_Controller{
  function __construct(){
    parent::__construct();
    $this->load->model('course/CourseLocationModel');
  }
  public function index($courseLocationId){
    $idiom = $this->MasterpageService->getPageLang();
    $data = array(
      'lang' => $idiom,
      'courseLocation' => $this->CourseLocationModel->getData($courseLocationId)
    );
    $content = $this->load->view('map/index', $data, true);
    $this->MasterpageService->display($content, 'Location', 'map');
  }
}
