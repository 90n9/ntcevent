<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forgotpassword extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model(array('member/MemberModel', 'member/ForgotpasswordService', 'EncodeService'));
    $idiom = $this->MasterpageService->getPageLang();
  }
  public function index(){
    $email = $this->input->post('email');
    echo json_encode($this->ForgotpasswordService->forgot($email));
  }
  public function changepassword($hashCode){
    $strDecode = $this->EncodeService->decode_number($hashCode);
    if($strDecode == ''){
      $data = array();
      $content = $this->load->view('forgotpassword/errorhash', $data, true);
    }else{
      $memberId = intval($strDecode);
      $member = $this->MemberModel->getData($memberId);
      $data = array(
        'hashCode' => $hashCode,
        'member' => $member
      );
      $content = $this->load->view('forgotpassword/changepassword', $data, true);
    }
    $this->MasterpageService->display($content, 'Forgot Password', 'forgotpassword');
  }
  public function changepassword_post($hashCode){
    $strDecode = $this->EncodeService->decode_number($hashCode);
    if($strDecode == ''){
      $data = array();
      $content = $this->load->view('forgotpassword/errorhash', $data, true);
      $this->MasterpageService->display($content, 'Forgot Password', 'forgotpassword');
      exit(0);
    }
    $memberId = intval($strDecode);
    $member = $this->MemberModel->getData($memberId);
    $password = trim($this->input->post('password'));
    $confirmPassword = trim($this->input->post('confirmpassword'));
    if($password != $confirmPassword || $password == ''){
      redirect('forgotpassword/changepassword/'.$hashCode.'/error');
    }
    $this->MemberModel->changePassword($memberId, $password);
    $this->LoginService->createSession($member);
    redirect('home');
  }
}
