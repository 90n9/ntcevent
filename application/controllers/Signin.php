<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signin extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model(array('member/MemberModel'));
    $idiom = $this->MasterpageService->getPageLang();
        //$this->lang->load('signin', $idiom);
  }
  public function index(){
    $email = $this->input->post('email');
    $password = $this->input->post('password');
    echo json_encode($this->LoginService->login($email, $password));
  }
  /*
  public function index(){
    $this->session->keep_flashdata('ticketId');
    $this->session->keep_flashdata('discountCode');
    $this->session->keep_flashdata('arrQty');
    $data = array();
    $content = $this->load->view('signin/form', $data, true);
    $this->MasterpageService->display($content, 'Sign In', 'signin');
	}
  */
	public function form_post(){
    $this->session->keep_flashdata('ticketId');
    $this->session->keep_flashdata('discountCode');
    $this->session->keep_flashdata('arrQty');
    $loginName = $this->input->post('txtLoginName');
    $loginPassword = $this->input->post('txtPassword');
    $this->LoginService->login($loginName, $loginPassword);
    $before_login = $this->session->userdata('before_login');
    redirect($before_login, 'refresh');
	}
	public function logout(){
		$this->LoginService->logout();
    $before_login = $this->session->userdata('before_login');
    redirect($before_login, 'refresh');
	}
}
