<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscribe extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model(array('subscribe/SubscribeModel'));
    $idiom = $this->MasterpageService->getPageLang();
    $this->lang->load('home', $idiom);
  }
	public function index(){
		redirect('home');
	}
  public function post(){
    $email = $this->input->post('email_newsletter');
    if(!$this->SubscribeModel->isDuplicate($email)){
      $this->SubscribeModel->insert($email);
      $this->send_subscribe_email($email);
    }
    redirect('subscribe/success');
  }
  public function success(){
    $content = $this->load->view('subscribe/complete', array(), true);
    $this->MasterpageService->display($content, 'Subscribe', 'subscribe');
  }
  private function send_subscribe_email($subscribe_email){
    $this->load->model('SendMailService');
    $content = $this->load->view('subscribe/emailadmin', array(
      'subscribe_email' => $subscribe_email
    ), true);
    $subject = 'NTCEVENT - Subscribe Email';
    $this->SendMailService->send_mail($this->ConfigModel->getData('email_contact'), '', $subject, $content);
  }
}
