<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_Controller {
	public function index(){
		$this->load->model(array('course/CourseCategoryModel', 'course/PriceRangeModel', 'course/CourseModel', 'slideshow/SlideshowModel', 'course/CourseLocationModel'));
		$idiom = $this->MasterpageService->getPageLang();
		$this->lang->load('home', $idiom);
		$this->lang->load('course', $idiom);
		$data = array();
		$data['lang'] = $idiom;
		$data['mainCategoryList'] = $this->CourseCategoryModel->getList(0);
		$data['slideshowList'] = $this->SlideshowModel->getList();
		$content = $this->load->view('home/index', $data, true);
    $this->MasterpageService->addCss('assets/js/fancybox/source/jquery.fancybox.css');
    $this->MasterpageService->addJs('assets/js/fancybox/source/jquery.fancybox.js');
    $this->MasterpageService->addJs('assets/vendors/js_cookie.js');
		$this->MasterpageService->addJs('assets/js/page/home.js?v=3');

		$this->MasterpageService->display($content, 'Home', 'home');
	}
  public function popup(){
    $obj = new StdClass();
		$lang = $this->MasterpageService->getPageLang();
    $obj->uploadFile = base_url('uploads/'.$this->ConfigModel->getData(($lang == 'thai')?'popup_image_th':'popup_image_en'));
    $obj->redirectUrl = $this->ConfigModel->getData('popup_url');
    $obj->popupStatus = $this->ConfigModel->getData('popup_status');
    header('Content-type: text/javascript');
    echo json_encode($obj);
  }
  public function testmail(){
    $this->load->model('SendMailService');
    $this->SendMailService->test_mail_gun();
  }
}
