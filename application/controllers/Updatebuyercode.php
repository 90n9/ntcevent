<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UpdateBuyerCode extends CI_Controller {
	public function index(){
    $this->db->set('course_buyer_code', '');
    $this->db->set('running_number', 0);
    $this->db->update('tbl_course_buyer');
    $this->updateCode();
    redirect('updateregistercode');
	}
  private function updateCode(){
    $this->db->order_by('course_buyer_id', 'ASC');
    $query = $this->db->get('tbl_course_buyer');
  	foreach($query->result() as $courseBuyer){
      $courseCode = $this->getCourseCode($courseBuyer->course_id);
      $runningNumber = $this->getNextRunningNumber($courseBuyer->course_id);
      $courseBuyerCode = 'OD'.$courseCode.str_pad($runningNumber,3,"0",STR_PAD_LEFT);
      $this->updateToDb($courseBuyer->course_buyer_id, $courseBuyerCode, $runningNumber);
    }
  }
  private function getCourseCode($courseId){
    $retVal = '';
    $this->db->select('course_code');
    $this->db->where('course_id', $courseId);
    $query = $this->db->get('tbl_course');
  	if($query->num_rows() > 0){
      $course = $query->row();
      $retVal = substr($course->course_code, 3);
  	}
		return $retVal;
  }
  private function getNextRunningNumber($courseId){
    $this->db->select('MAX(running_number) AS max_running_number');
    $this->db->where('course_id', $courseId);
    $query = $this->db->get('tbl_course_buyer');
    if($query->num_rows() > 0){
      return $query->row()->max_running_number + 1;
    }
    return 1;
  }
  private function updateToDb($courseBuyerId, $code, $runningNumber){
    $this->db->set('course_buyer_code', $code);
    $this->db->set('running_number', $runningNumber);
    $this->db->where('course_buyer_id', $courseBuyerId);
    $this->db->update('tbl_course_buyer');
  }
}
