<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model(array('member/MemberModel', 'EncodeService'));
    $idiom = $this->MasterpageService->getPageLang();
    $this->lang->load('register', $idiom);
  }
  public function index(){
    $data = array();
    $data['firstname'] = trim($this->input->post('firstname'));
    $data['lastname'] = trim($this->input->post('lastname'));
    $data['company'] = trim($this->input->post('company'));
    $data['mobile'] = trim($this->input->post('mobile'));
    $data['email'] = trim($this->input->post('email'));
    $data['login_password'] = trim($this->input->post('password'));
    if($data['email'] == '' || $data['login_password'] == ''){
      echo json_encode(array(
        'resp_status' => false,
        'resp_msg' => $this->lang->line('register_error_form_not_complete')
      ));
      exit(0);
      return;
    }
    if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
      echo json_encode(array(
        'resp_status' => false,
        'resp_msg' => $this->lang->line('register_error_invalid_email')
      ));
      exit(0);
      return;
    }
    if($this->MemberModel->getDataByEmail($data['email'])){
      echo json_encode(array(
        'resp_status' => false,
        'resp_msg' => $this->lang->line('register_error_duplicate_email')
      ));
      exit(0);
      return;
    }
    $memberId = $this->MemberModel->insert($data);
    $this->sendVerifyEmail($memberId);
    echo json_encode(array(
      'resp_status' => true,
      'resp_msg' => 'success'
    ));
  }
  private function sendVerifyEmail($memberId){
    $member = $this->MemberModel->getData($memberId);
    $hashCode = $this->EncodeService->encode_number($memberId);
    $verifyUrl = 'register/confirm/'.$hashCode;
    $this->load->model('SendMailService');
    $content = $this->load->view('register/verifyemail', array(
      'member' => $member,
      'verifyUrl' => $verifyUrl
    ), true);
    $subject = '[Action Required] Active your account';
    $this->SendMailService->send_mail($member->email, '', $subject, $content);
  }
  public function testencode($memberId){
    $hashCode = $this->EncodeService->encode_number($memberId);
    redirect('register/confirm/'.$hashCode);
  }
  public function confirm($hashCode){
    $strDecode = $this->EncodeService->decode_number($hashCode);
    if($strDecode == ''){
      $data = array();
      $content = $this->load->view('register/errorhash', $data, true);
    }else{
      $memberId = intval($strDecode);
      $this->MemberModel->verify($memberId);
      $member = $this->MemberModel->getData($memberId);
      $this->LoginService->createSession($member);
      $data = array();
      $content = $this->load->view('register/success', $data, true);
    }
    $this->MasterpageService->display($content, 'Register', 'register');
  }
}
