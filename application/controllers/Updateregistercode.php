<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UpdateRegisterCode extends CI_Controller {
	public function index(){
    $this->db->set('course_register_code', '');
    $this->db->set('running_number', 0);
    $this->db->update('tbl_course_register');
    $this->updateCode();
    redirect('updatepaymentcode');
	}
  private function updateCode(){
    $this->db->order_by('course_register_id', 'ASC');
    $query = $this->db->get('tbl_course_register');
  	foreach($query->result() as $courseRegister){
      $courseCode = $this->getCourseCode($courseRegister->course_id);
      $runningNumber = $this->getNextRunningNumber($courseRegister->course_id);
      $courseRegisterCode = 'NTC'.$courseCode.str_pad($runningNumber,3,"0",STR_PAD_LEFT);
      $this->updateToDb($courseRegister->course_register_id, $courseRegisterCode, $runningNumber);
    }
  }
  private function getCourseCode($courseId){
    $retVal = '';
    $this->db->select('course_code');
    $this->db->where('course_id', $courseId);
    $query = $this->db->get('tbl_course');
    if($query->num_rows() > 0){
      $course = $query->row();
      $retVal = substr($course->course_code, 3);
    }
    return $retVal;
  }
  private function getNextRunningNumber($courseId){
    $this->db->select('MAX(running_number) AS max_running_number');
    $this->db->where('course_id', $courseId);
    $query = $this->db->get('tbl_course_register');
    if($query->num_rows() > 0){
      return $query->row()->max_running_number + 1;
    }
  	return 1;
  }
  private function updateToDb($courseRegisterId, $code, $runningNumber){
    $this->db->set('course_register_code', $code);
    $this->db->set('running_number', $runningNumber);
    $this->db->where('course_register_id', $courseRegisterId);
    $this->db->update('tbl_course_register');
  }
}
