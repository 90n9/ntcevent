<?php

Class Howtobuy extends CI_Controller{

  public function index(){
		$idiom = $this->MasterpageService->getPageLang();
		$this->lang->load('home', $idiom);
		$data = array();
		$data['lang'] = $idiom;
		$content = $this->load->view('howtobuy/index', $data, true);
		$this->MasterpageService->display($content, 'Howtobuy', 'howtobuy');
	}

}
