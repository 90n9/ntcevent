<?php
class Offer extends CI_Controller{
  public function index(){
    $this->load->model('offer/Offer_model');
    $content = $this->load->view('offer/list', array(
      'offer_list' => $this->Offer_model->get_list(),
      'lang' => $this->MasterpageService->getPageLang()
    ), true);
    $this->MasterpageService->display($content, 'Offer', 'offer');
  }
}
