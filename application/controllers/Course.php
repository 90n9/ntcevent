<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Course extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model(array('course/CourseCategoryModel', 'course/CourseModel', 'course/DiscountModel', 'course/PriceRangeModel', 'course/DocumentModel', 'course/TicketModel', 'course/CourseLocationModel', 'course/SpeakerModel', 'course/CourseBuyerModel', 'course/CourseRegisterModel', 'member/MemberModel', 'EncodeService'));
    $idiom = $this->MasterpageService->getPageLang();
    $this->lang->load('course', $idiom);
  }
  public function index(){
    $idiom = $this->MasterpageService->getPageLang();
    $data = array();
    $searchData = array();
    $searchData['catid'] = $this->input->get('catid');
    $searchData['type'] = $this->input->get('type');
    $searchData['price'] = $this->input->get('price');
    $searchData['page'] = (isset($_GET['page']))?$this->input->get('page'):1;
    $searchData['sort'] = $this->input->get('sort');
    $searchData['order'] = $this->input->get('order');
    $searchData['text'] = $this->input->get('text');
    $searchData['perpage'] = 15;
    $searchData['lang'] = $data['lang'] = $idiom;
    $this->load->model('course/CourseFilterService');
    $data = $this->CourseFilterService->getContent($searchData);
    $content = $this->load->view('course/list', $data, true);
    $this->MasterpageService->addJs('assets/js/page/courselist.js?v=3');
    $this->MasterpageService->display($content, 'Course', 'course');
  }
  public function detail($courseId){
    $idiom = $this->MasterpageService->getPageLang();
    $this->load->model(array('course/RelateCourseModel'));
    $this->lang->load('course', $idiom);
    $data = array();
    $data['lang'] = $idiom;
    $data['courseId'] = $courseId;
    $data['course'] = $this->CourseModel->getData($courseId);
    $data['documentList'] = $this->DocumentModel->getList($courseId);
    $data['ticketList'] = $this->TicketModel->getList($courseId);
    $content = $this->load->view('course/detail', $data, true);
    if($data['ticketList']->num_rows() == 1 && $data['ticketList']->row()->price_type == 'contact'){
      $this->MasterpageService->addJs('assets/js/page/course/contact.js');
    }else{
      $this->MasterpageService->addJs('assets/js/page/course/detail.js');
    }
    $this->MasterpageService->display($content, 'Course', 'course');
  }
  public function register($courseId){
    $data = array();
    $discount_error = '';
    if(!empty($_POST)){
      $this->session->set_flashdata('ticketId', $this->input->post('ticketId'));
      $this->session->set_flashdata('discountCode', $this->input->post('txtDiscountCode'));
      $this->session->set_flashdata('arrQty', $this->input->post('qty'));
    }else{
      $this->session->keep_flashdata('ticketId');
      $this->session->keep_flashdata('discountCode');
      $this->session->keep_flashdata('arrQty');
    }
    $this->LoginService->mustLogin();
    $arrQty = $this->session->flashdata('arrQty');
    $data['arrTicketId'] = $this->session->flashdata('ticketId');
    $data['discountCode'] = $this->session->flashdata('discountCode');
    $data['arrQty'] = $arrQty;
    $data['courseId'] = $courseId;
    $data['kbankFee'] = $this->ConfigModel->getData('kbank_fee');
    $data['course'] = $this->CourseModel->getData($courseId);
    $data['member'] = $this->MemberModel->getData($this->session->userdata('member_id'));
    $discount = $this->DiscountModel->getDataByCode($courseId, $data['discountCode']);
    $total_qty = 0;
    foreach($arrQty as $qty){
      $total_qty += $qty;
    }
    if($discount){
      date_default_timezone_set("Asia/Bangkok");
      $now = new DateTime();
      $startdate = new DateTime($discount->available_from);
      $enddate = new DateTime($discount->available_to);
      if($total_qty < $discount->min_ticket || ($discount->max_ticket >= $discount->min_ticket && $total_qty > $discount->max_ticket)){
        $discount = false;
        $discount_error = $this->lang->line('promo_error_condition_not_met');
      }elseif($now < $startdate || $now > $enddate){
        $discount = false;
        $discount_error = $this->lang->line('promo_error_expired');
      }
    }elseif(trim($data['discountCode']) != ''){
      $discount_error = $this->lang->line('promo_error_condition_not_met');
    }
    $data['discount'] = $discount;
    $data['discount_error'] = $discount_error;
    $content = $this->load->view('course/register', $data, true);
    $this->MasterpageService->addJs('assets/js/page/course/register.js?v=3');
    $this->MasterpageService->display($content, 'Course', 'course');
  }
	public function register_post($courseId){
    $this->LoginService->mustLogin();
    $buyerData = array();
    $buyerData['course_id'] = $courseId;
    $paymentChannel = $this->input->post('paymentChannel');
    $buyerData['buyer_firstname'] = $this->input->post('txtFirstname');
    $buyerData['buyer_lastname'] = $this->input->post('txtLastname');
    $buyerData['buyer_email'] = $this->input->post('txtEmail');
    $buyerData['buyer_mobile'] = $this->input->post('txtMobile');
    $buyerData['buyer_company'] = $this->input->post('txtCompany');
    $buyerData['discount_code'] = $this->input->post('txtDiscountCode');
    $buyerData['discount_id'] = 0;
    $discount = $this->DiscountModel->getDataByCode($courseId, $buyerData['discount_code']);
    if($discount){
      $buyerData['discount_id'] = $discount->discount_id;
    }
    $buyerData['is_company'] = ($this->input->post('chkCompany'))?1:0;
    $buyerData['want_payment_type'] = $this->input->post('paymentChannel');
    $buyerData['company_name'] = $this->input->post('txtVatCompany');
    $buyerData['company_address'] = $this->input->post('txtVatAddress');
    $buyerData['company_branch'] = $this->input->post('txtVatBranch');
    $buyerData['company_tax'] = $this->input->post('txtVatTaxNo');
    $arrTicketId = $this->input->post('ticketId');
    $arrRegisterFirstname = $this->input->post('txtRegisterFirstname');
    $arrRegisterLastname = $this->input->post('txtRegisterLastname');
    $arrRegisterEmail = $this->input->post('txtRegisterEmail');
    $arrRegisterMobile = $this->input->post('txtRegisterMobile');
    $arrRegisterCompany = $this->input->post('txtRegisterCompany');
    $countArr = count($arrTicketId);
    $buyerData['total_ticket'] = $countArr;
    $courseBuyerId = $this->CourseBuyerModel->insert($buyerData);
    if($buyerData['company_name'] != ''){
      $this->MemberModel->updateTaxInfo($this->session->userdata('member_id'), $buyerData);
    }
    $totalAmount = 0;
		$ticket = $this->TicketModel->getData($arrTicketId[0]);
		for($i = 0; $i<$countArr; $i++){
			$registerData = array();
			$registerData['course_buyer_id'] = $courseBuyerId;
			$registerData['course_id'] = $courseId;
			$registerData['ticket_id'] = $arrTicketId[$i];
			$registerData['price_type'] = $ticket->price_type;
			$registerData['price'] = $ticket->price;
      if($ticket->price_type == 'amount'){
        $totalAmount += $ticket->price;
      }
			$registerData['register_firstname'] = $arrRegisterFirstname[$i];
      $registerData['register_lastname'] = $arrRegisterLastname[$i];
			$registerData['register_email'] = $arrRegisterEmail[$i];
			$registerData['register_mobile'] = $arrRegisterMobile[$i];
			$registerData['register_company'] = $arrRegisterCompany[$i];
			$this->CourseRegisterModel->insert($registerData);
		}
		$courseData = array();
		$courseData['total_amount'] = $totalAmount;
    $discountAmount = 0;
    $subTotalAmount = $totalAmount;
    if($discount){
      if($discount->discount_type == 'percent'){
        $discountAmount = $totalAmount * $discount->discount_amount / 100;
      }else{
        $discountAmount = $discount->discount_amount * $countArr;
      }
      $subTotalAmount = $totalAmount - $discountAmount;
    }
    $surchargeAmount = 0;
    if($paymentChannel == 'creditcard'){
      $surchargeAmount = $subTotalAmount * $this->ConfigModel->getData('kbank_fee') / 100;
    }
    $courseData['discount_amount'] = $discountAmount;
    $courseData['surcharge_amount'] = $surchargeAmount;
		$courseData['vat_amount'] = ($subTotalAmount + $surchargeAmount) * 0.07;
    $courseData['paid_amount'] = $subTotalAmount + $surchargeAmount + $courseData['vat_amount'];
		$this->CourseBuyerModel->updateCost($courseBuyerId, $courseData);
    if($ticket->price_type == 'free'){
      $this->CourseBuyerModel->updateStatus($courseBuyerId, 'complete');
      $this->send_mail_course_free($courseId, $courseBuyerId);
    }elseif($ticket->price_type == 'amount'){
      $this->send_mail_course_bank_payment($courseId, $courseBuyerId);
    }
    $this->mail_admin_buyer($courseBuyerId);
    if($totalAmount > 0 && $paymentChannel == 'creditcard'){
      $this->load->model(array('payment/PaymentModel', 'payment/PaymentItemModel'));
      $paymentData = array();
      $paymentData['payment_type'] = 'creditcard';
      $paymentData['order_amount'] = $subTotalAmount;
      $paymentData['surcharge_amount'] = $surchargeAmount;
      $paymentData['vat_amount'] = $courseData['vat_amount'];
      $paymentData['payment_amount'] = $courseData['paid_amount'];
      $paymentData['payment_date'] = '';
      $paymentData['payment_time'] = '';
      $paymentData['payment_file'] = '';
      $paymentData['note'] = '';
      $paymentId = $this->PaymentModel->insert($paymentData);
      $dataItem = array();
      $dataItem['payment_id'] = $paymentId;
      $dataItem['course_buyer_id'] = $courseBuyerId;
      $dataItem['course_buyer_amount'] = $subTotalAmount;
      $this->PaymentItemModel->insert($dataItem);
      $this->CourseBuyerModel->payment($courseBuyerId);
      redirect('payment/post_creditcard/'.$paymentId);
    }else{
      redirect('course/register_success/'.$courseBuyerId);
    }
	}
  public function mail_admin_buyer($courseBuyerId){
    $this->load->model(array('SendMailService', 'course/CourseBuyerModel','course/CourseRegisterModel'));
    $courseBuyer = $this->CourseBuyerModel->getData($courseBuyerId);
    $course = $this->CourseModel->getData($courseBuyer->course_id);
    $courseRegisterList = $this->CourseRegisterModel->getList($courseBuyerId);
    $content = $this->load->view('course/mail/emailadminticket', array(
      'course' => $course,
      'courseBuyer' => $courseBuyer,
      'courseRegisterList' => $courseRegisterList
    ), true);
    $subject = '[Register] Request bank transfer "'. $course->course_name.'" online.';
    $this->SendMailService->send_mail($this->ConfigModel->getData('email_sales'), '', $subject, $content);
  }
  private function send_mail_course_free($courseId, $courseBuyerId){
    $course = $this->CourseModel->getData($courseId);
    $courseBuyer = $this->CourseBuyerModel->getData($courseBuyerId);
    $member = $this->MemberModel->getData($courseBuyer->member_id);
    $courseLocation = $this->CourseLocationModel->getData($course->course_location_id);
    $courseRegisterList = $this->CourseRegisterModel->getList($courseBuyerId);
    foreach($courseRegisterList->result() as $courseRegister){
      $hashCode = $this->EncodeService->encode_number($courseRegister->course_register_id);
      $ticketUrl = base_url('index.php/mycourse/ticket/'.$hashCode);
      $this->load->model('SendMailService');
      $content = $this->load->view('course/mail/coursefree', array(
        'course' => $course,
        'courseRegister' => $courseRegister,
        'courseLocation' => $courseLocation,
        'courseUrl' => base_url('index.php/course/detail/'.$courseId),
        'ticketUrl' => $ticketUrl
      ), true);
      $subject = '[Ticket Sent] "'.$course->course_name.'" online to '.$courseRegister->register_firstname.' '.$courseRegister->register_lastname;
      $this->SendMailService->send_mail($member->email, '', $subject, $content);
    }
  }
  private function send_mail_course_bank_payment($courseId, $courseBuyerId){
    $course = $this->CourseModel->getData($courseId);
    $courseBuyer = $this->CourseBuyerModel->getData($courseBuyerId);
    $member = $this->MemberModel->getData($courseBuyer->member_id);
    $this->load->model('SendMailService');
    $courseBuyerCodeHash = $this->EncodeService->encode_number($courseBuyer->course_buyer_code);
    $content = $this->load->view('course/mail/coursebankpayment', array(
      'course' => $course,
      'courseBuyer' => $courseBuyer,
      'courseBuyerCodeHash' => $courseBuyerCodeHash
    ), true);
    $subject = '"'.$course->course_name.'" Registration Confirmation';
    $this->SendMailService->send_mail($member->email, '', $subject, $content);
  }
  public function register_success($courseBuyerId){
    $this->LoginService->mustLogin();
    $idiom = $this->MasterpageService->getPageLang();
    $this->lang->load('course', 'english');
    $courseBuyer = $this->CourseBuyerModel->getData($courseBuyerId);
    $courseId = $courseBuyer->course_id;
    $data = array();
    $data['lang'] = $idiom;
    $data['courseBuyerId'] = $courseBuyerId;
    $data['courseBuyer'] = $courseBuyer;
    $data['courseId'] = $courseId;
    $data['course'] = $this->CourseModel->getData($courseId);
    $data['documentList'] = $this->DocumentModel->getList($courseId);
    $data['ticketList'] = $this->TicketModel->getList($courseId);
    $content = $this->load->view('course/success', $data, true);
    $this->MasterpageService->display($content, 'Course', 'course');
  }
  public function contact_post($courseId){
    $contactData = array();
    $contactData['course_id'] = $courseId;
    $contactData['total_ticket'] = $this->input->post('ddlTotalTicket');
    $contactData['contact_firstname'] = $this->input->post('txtFirstname');
    $contactData['contact_lastname'] = $this->input->post('txtLastname');
    $contactData['contact_email'] = $this->input->post('txtEmail');
    $contactData['contact_phone'] = $this->input->post('txtPhone');
    $contactData['contact_mobile'] = $this->input->post('txtMobile');
    $contactData['contact_company'] = $this->input->post('txtCompany');
    $contactData['contact_note'] = $this->input->post('txtNote');
    $this->load->model('course/CourseContactModel');
    $courseContactId = $this->CourseContactModel->insert($contactData);
    $this->mail_admin_contact($courseContactId);
    redirect('course/contact_complete/'.$courseId);
  }
  public function mail_admin_contact($courseContactId){
    $this->load->model(array('SendMailService', 'course/CourseContactModel'));
    $courseContact = $this->CourseContactModel->getData($courseContactId);
    $course = $this->CourseModel->getData($courseContact->course_id);
    $content = $this->load->view('course/mail/emailadminrequest', array(
      'course' => $course,
      'courseContact' => $courseContact
    ), true);
    $subject = '[Quotation Request]"'. $course->course_name.'" Quotation from online.';
    $this->SendMailService->send_mail($this->ConfigModel->getData('email_sales'), '', $subject, $content);
  }
  public function contact_complete($courseId){
    $idiom = $this->MasterpageService->getPageLang();
    $this->lang->load('course', $idiom);
    $data = array();
    $data['lang'] = $idiom;
    $data['courseId'] = $courseId;
    $data['course'] = $this->CourseModel->getData($courseId);
    $content = $this->load->view('course/contact_complete', $data, true);
    $this->MasterpageService->display($content, 'Course', 'course');
  }
}
