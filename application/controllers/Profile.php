<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model(array('member/MemberModel'));
    $idiom = $this->MasterpageService->getPageLang();
    $this->lang->load('home', $idiom);
  }
  public function index(){
    $this->LoginService->mustLogin();
    $idiom = $this->MasterpageService->getPageLang();
    $this->lang->load('profile', $idiom);
    $data = array();
    $data['member'] = $this->MemberModel->getData($this->session->userdata('member_id'));
    $content = $this->load->view('profile/form', $data, true);
    $this->MasterpageService->addJs('assets/js/page/profile/form.js?v=1');
    $this->MasterpageService->display($content, 'Profile', 'profile');
  }
  public function form_post(){
    $this->LoginService->mustLogin();
    $data = array();
    $data['firstname'] = $this->input->post('txtFirstname');
    $data['lastname'] = $this->input->post('txtLastname');
    $data['company'] = $this->input->post('txtCompany');
    $data['mobile'] = $this->input->post('txtMobile');
    $data['is_company'] = $this->input->post('rdoCompany');
    $data['company_name']  = $this->input->post('txtCompanyName');
    $data['company_address'] = $this->input->post('txtCompanyAddress');
    $data['company_branch'] = $this->input->post('txtCompanyBranch');
    $data['company_tax'] = $this->input->post('txtCompanyTax');
    $this->MemberModel->update($this->session->userdata('member_id'), $data);
    redirect('profile/success');
  }
  public function success(){
    $this->LoginService->mustLogin();
    $data = array();
    $content = $this->load->view('profile/success', $data, true);
    $this->MasterpageService->display($content, 'profile', 'profile');
  }
}
