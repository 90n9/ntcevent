<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class MyCourse extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model(array('member/MemberModel', 'course/CourseModel', 'course/TicketModel', 'course/CourseBuyerModel', 'course/CourseRegisterModel', 'course/CourseLocationModel', 'EncodeService'));
    $idiom = $this->MasterpageService->getPageLang();
    $this->lang->load('course', $idiom);
    $this->lang->load('mycourse', $idiom);
  }
	public function index($viewStatus = 'all'){
    $this->LoginService->mustLogin();
    $idiom = $this->MasterpageService->getPageLang();
    $data = array();
		$data['lang'] = $idiom;
    $data['viewStatus'] = $viewStatus;
    $data['courseBuyerList'] = $this->CourseBuyerModel->getList();
    $data['courseBuyerWaitingList'] = $this->CourseBuyerModel->getList('waiting');
    $data['courseBuyerPaymentList'] = $this->CourseBuyerModel->getList('payment');
    $data['courseBuyerCompleteList'] = $this->CourseBuyerModel->getList('complete');
    $data['courseBuyerCancelList'] = $this->CourseBuyerModel->getList('cancel');
    $content = $this->load->view('mycourse/list', $data, true);
		$this->MasterpageService->addJs('assets/js/page/mycourse/list.js');
    $this->MasterpageService->display($content, 'My Course', 'mycourse');
	}
  public function detail($courseBuyerId){
    $this->LoginService->mustLogin();
    $idiom = $this->MasterpageService->getPageLang();
    $this->lang->load('course', $idiom);
    $data = array();
    $data['lang'] = $idiom;
    $courseBuyer = $data['courseBuyer'] = $this->CourseBuyerModel->getData($courseBuyerId);
    $data['course'] = ($courseBuyer)?$this->CourseModel->getData($courseBuyer->course_id):false;
    $data['courseRegisterList'] = ($courseBuyer)?$this->CourseRegisterModel->getList($courseBuyerId):false;
    $title = $data['title'] = ($courseBuyer)?'รายละเอียดการสั่งซื้อ ':'ไม่พบการสั่งซื้อของท่าน';
    $content = $this->load->view('mycourse/detail', $data, true);
    $this->MasterpageService->display($content, $title, 'mycourse');
  }
  public function cancel($courseBuyerId){
    $this->LoginService->mustLogin();
    $this->CourseBuyerModel->cancel($courseBuyerId);
    redirect('mycourse');
  }
  public function printTickets($courseBuyerId){
    $courseRegisterList = $this->CourseRegisterModel->getList($courseBuyerId);
    if($courseRegisterList->num_rows() == 1){
      $this->printTicket($courseRegisterList->row()->course_register_id);
    }else{
      $this->printTicket($courseRegisterList->row()->course_register_id);
    }
  }
  public function printTicket($courseRegisterId){
    $courseRegister = $this->CourseRegisterModel->getData($courseRegisterId);
    $course = $this->CourseModel->getData($courseRegister->course_id);
    $dataContent = array(
      'courseRegisterId' => $courseRegisterId,
      'courseRegister' => $courseRegister,
      'course' => $course,
      'lang' => $this->MasterpageService->getPageLang()
    );
    $html = $this->load->view('mycourse/printticket', $dataContent, true);
    $stylesheet = file_get_contents(FCPATH.'assets/css/ticket.css');
    $this->load->model(array('PdfService'));
    $header = '';
    $footer = $this->load->view('mycourse/print/ticket_footer', array(), true);
    $this->PdfService->viewPdfFile($html, $stylesheet, $header, $footer, 'ticket_'.$courseRegister->course_register_code.'.pdf');
  }
  public function ticket($hashCode){
    $courseRegisterId = $this->EncodeService->decode_number($hashCode);
    $this->printTicket($courseRegisterId);
  }
}
