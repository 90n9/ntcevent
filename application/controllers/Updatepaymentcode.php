<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UpdatePaymentCode extends CI_Controller {
	public function index(){
    $this->db->set('payment_code', '');
    $this->db->set('payment_pre_code', '');
    $this->db->set('running_number', 0);
    $this->db->update('tbl_payment');
    $this->updateCode();
    echo 'complete update all code';
	}
  private function updateCode(){
    $this->db->order_by('payment_id', 'ASC');
    $query = $this->db->get('tbl_payment');
  	foreach($query->result() as $payment){
      $paymentPreCode = $this->getPreCodeFromCreatedate($payment->create_date);
      $runningNumber = $this->getNextRunningNumber($paymentPreCode);
      $paymentCode = 'INV'.$paymentPreCode.str_pad($runningNumber,3,"0",STR_PAD_LEFT);
      $this->updateToDb($payment->payment_id, $paymentCode, $paymentPreCode, $runningNumber);
    }
  }
  private function getPreCodeFromCreatedate($createDate){
  	return date('ym', strtotime($createDate));
  }
  private function getNextRunningNumber($preCode){
    $this->db->select('MAX(running_number) AS max_running_number');
    $this->db->where('payment_pre_code', $preCode);
    $query = $this->db->get('tbl_payment');
    if($query->num_rows() > 0){
    	return $query->row()->max_running_number + 1;
    }
    return 1;
  }
  private function updateToDb($paymentId, $paymentCode, $paymentPreCode, $runningNumber){
    $this->db->set('payment_code', $paymentCode);
    $this->db->set('payment_pre_code', $paymentPreCode);
    $this->db->set('running_number', $runningNumber);
    $this->db->where('payment_id', $paymentId);
    $this->db->update('tbl_payment');
  }
}
