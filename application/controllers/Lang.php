<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lang extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->session->keep_flashdata('ticketId');
    $this->session->keep_flashdata('discountCode');
    $this->session->keep_flashdata('arrQty');
    $this->session->keep_flashdata('courseBuyerList');
    $this->session->keep_flashdata('paymentType');
  }
  public function index() {
    $referred_from = $this->session->userdata('referred_from');
    redirect($referred_from, 'refresh');
  }
  public function th() {
    $this->session->set_userdata('language', 'thai');
    $referred_from = $this->session->userdata('referred_from');
    redirect($referred_from, 'refresh');
  }
  public function en() {
    $this->session->set_userdata('language', 'english');
    $referred_from = $this->session->userdata('referred_from');
    redirect($referred_from, 'refresh');
  }
}
