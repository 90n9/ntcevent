<table style="width: 100%;" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
  <tbody>
    <tr>
      <td bgcolor="#eeeeee" style="padding: 0px 20px 20px 20px;">
        <div style="padding: 10px; margin: 0px;">
          <?php $this->load->view('mailtemplate/template_header.php'); ?>
          <div align="center">
            <table style="width: 650px; font-family: Verdana; height: 250px;" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">
              <tbody>
                <tr bgcolor="#ffffff">
                  <td style="padding: 0px 20px;" bgcolor="#ffffff" height="385">
                    <?php echo $content; ?>
                  </td>
                </tr>
              </tbody>
            </table>
            <?php $this->load->view('mailtemplate/template_footer'); ?>
          </div>
        </div>
      </td>
    </tr>
  </tbody>
</table>
