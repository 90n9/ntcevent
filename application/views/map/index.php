<section id="login_bg" style="background-size:cover;padding:20px 0;">
  <div class="container">
    <div style="display:block;width:100%;max-width:600px;margin:auto;">
      <div id="login">
        <?php echo anchor('','<span class="icon-cancel" aria-hidden="true"></span>', array('class'=>'close login-close')); ?>
        <h4 class="modal-title" id="locationModalLabel<?php echo $courseLocation->course_location_id; ?>" style="color:#ff5400;"><?php echo $courseLocation->course_location_name_en; ?></h4>
        <p style="margin-bottom:0;"><?php echo $courseLocation->course_location_detail_en; ?></p>
        <div class="google-map" data-lad="<?php echo $courseLocation->lad; ?>" data-lon="<?php echo $courseLocation->lon; ?>"></div>
      </div>
    </div>
  </div>
</section>
