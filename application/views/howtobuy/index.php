<div class="fix-content-width">
	<section id="main-features" style="border-top: 1px solid #ccc;">
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1 text-center">
					<h1 style="color:#f54700;">How to Buy</h1>
				</div>
			</div><!-- End row -->
		</div><!-- End container -->
	</section><!-- End sub-header -->

  <section id="main_content">
  	<div class="container">
    	<ol class="breadcrumb">
    	  <li><?php echo anchor('home','HOME'); ?></li>
    	  <li class="active">How to Buy</li>
    	</ol>
		  <div class="row">
    		<div class="col-md-10 col-md-offset-1">

					<?php $this->load->view('howtobuy/header'); ?>

          <h4 style="color:#f54700; padding-top:10px;">วิธีการสมัครสมาชิก</h4>
					<ul style="padding-left:18px;">
	          <li>สำหรับการสมัครสมาชิกใหม่ คลิก <b style="color:#f54700;">“สมัครสมาชิก”</b> มุมขวาบนเพื่อกรอกข้อมูลส่วนตัว และสร้างรหัสผ่านเพื่อลงทะเบียน จากนั้นตรวจสอบกล่องข้อความของอีเมลที่สมัคร เพื่อยืนยันตัวตน </li>
	          <li>สำหรับผู้ที่เคยสมัครสมาชิกแล้ว คลิก <b style="color:#f54700;">“เข้าสู่ระบบ”</b> มุมขวาบน กรอกรหัสผ่านเพื่อเข้าสู่ระบบ</li>
					</ul>
          <center style="padding:10px 0 10px;">
            <?php echo '<img src="'.base_url('assets/img/howtobuy/page.png').'" alt="menu" height="650" width="650" class="img-responsive";/>'; ?>
          </center>
					<hr style="border-color: #ccc;"/>
          <h4 style="padding-top: 15px; color:#f54700;">วิธีการสั่งซื้อ</h4>
					<ol style="list-style-type=decimal;padding-left:18px;">
	          <li>คลิกคอร์สเรียนที่ต้องการ</li>
	          <li>ใส่จำนวน TICKET และ Promo code (หากมี) จากนั้นคลิกที่ปุ่ม <b style="color:#f54700;">“GET TICKET”</b> </li>

	          <center style="padding:10px 0 20px; margin-left:-18px;">
	            <?php echo '<img src="'.base_url('assets/img/howtobuy/ticket3.png').'" alt="ticket" height="300" width="630
							" class="img-responsive";/>'; ?>
	          </center>

          	<li>กรอกที่อยู่เพื่อออกใบเสร็จรับเงินและใบกำกับภาษี </li>
						<ul style="list-style-type:disc;">

          		<li style="margin-left: -23px;">หากชำระเงินในนามบุคคล กรุณาใส่ที่อยู่ตามบัตรประชาชน</li>
		          <center style="padding:5px 0 20px; margin-left:-58px;">
		            <?php echo '<img src="'.base_url('assets/img/howtobuy/individual-7.png').'" alt="individual" height="300" width="620" class="img-responsive";/>'; ?>
		          </center>

        			<li style="margin-left: -23px;">หากชำระเงินในนามบริษัท กรุณาทำเครื่องหมายที่ช่อง “ชำระเงินในนามบริษัท” (หากต้องการขอใบกำกับภาษีหัก ณ จ่าย 3% กรุณาชำระเงินจำนวนเต็มและติดต่อเจ้าหน้าที่เพื่อขอลดหย่อนภายหลัง)</li>
	          	<center style="padding:20px 0 20px; margin-left:-58px;">
	            <?php echo '<img src="'.base_url('assets/img/howtobuy/company-6.png').'" alt="company" height="300" width="620" class="img-responsive";/>'; ?>
	          	</center>
						</ul>

	          <li>ตรวจสอบหรือแก้ไขข้อมูลส่วนตัวในช่องข้อมูลผู้ซื้อตั๋ว ข้อมูลนี้จะใช้ในการส่ง TICKET หลังจากการสั่งซื้อเสร็จสิ้น </li>
	          <center style="padding:20px 0 20px; margin-left:-18px;">
	            <?php echo '<img src="'.base_url('assets/img/howtobuy/ticketname.png').'" alt="ticketname" height="300" width="610" class="img-responsive";/>'; ?>
	          </center>

						<ul style="list-style-type:disc;">
	        		<li style="margin-left: -23px;">ในกรณีผู้อบรมมิใช่คนเดียวกับผู้ซื้อตั๋ว กรุณาคลิกเครื่องหมายออกจากช่อง “ใช้ข้อมูลเดียวกับผู้ซื้อตั๋ว” และกรอกข้อมูลของผู้อบรมให้ถูกต้องเพื่อใช้ในการส่งคำยืนยันและส่งเอกสารการอบรม รวมถึงการออกใบประกาศนียบัตร</li>
		          <center style="padding:20px 0 20px; margin-left:-58px;">
		            <?php echo '<img src="'.base_url('assets/img/howtobuy/training.png').'" alt="training" height="300" width="610" class="img-responsive";/>'; ?>
		          </center>
						</ul>

          	<li>เมื่อกรอกข้อมูลครบเรียบร้อยแล้วให้เลือกช่องทางการชำระเงินและคลิกที่ปุ่ม <b style="color:#f54700;">“CONFIRM”</b></p>
		          <center style="padding:10px 0 20px; margin-left:-18px;">
		            <?php echo '<img src="'.base_url('assets/img/howtobuy/payment-1.png').'" alt="payment" height="300" width="610" class="img-responsive";/>'; ?>
		          </center>

          <li>หลังจากเสร็จสิ้นขั้นตอนแล้ว ระบบจะส่งอีเมลชี้แจงรายละเอียดของการสั่งซื้อผ่านทางอีเมลที่ผู้ซื้อกรอกไว้เบื้องต้น เมื่อขั้นตอนการชำระเงินเสร็จสมบูรณ์ ระบบจะส่ง TICKET การอบรมให้ผู้ซื้อโดยอัตโนมัติ</p>
					</ol>
          <hr style="border-color: #ccc;"/>

          <h4 style="padding-top: 15px; color:#f54700;">ตรวจสอบและแก้ไขบัญชีข้อมูลผู้ใช้</h4>
          <p>หลังการ Log in ผู้ใช้สามารถตรวจสอบ แก้ไขบัญชีข้อมูลได้ที่มุมขวาบน</p>
          <center style="padding:10px 0 20px;">
            <?php echo '<img src="'.base_url('assets/img/howtobuy/list.png').'" alt="menulist" height="300" width="610" class="img-responsive";/>'; ?>
          </center>

					<ol style="list-style-type=decimal;padding-left:18px;">
	          <li><b>“คอร์สเรียนของฉัน”</b> ผู้ใช้สามารถตรวจสอบตารางเรียน สถานะ ยอดชำระของรายวิชาที่ได้ทำการลงทะเบียน</li>

						<ul style="list-style-type:upper-alpha;">
		          <li style="margin-left: -23px;"><b style="color:#f54700;">รายการทั้งหมด</b> - รายการที่ผู้ใช้ได้ทำการลงทะเบียนทั้งหมด</li>
		          <center style="padding:20px 0 20px; margin-left:-58px;">
		            <?php echo '<img src="'.base_url('assets/img/howtobuy/viewall.png').'" alt="viewall" height="300" width="630" class="img-responsive";/>'; ?>
		          </center>
			        <li style="margin-left: -23px;"><b style="color:#f54700;">รายการรอชำระเงิน</b> – รายการลงทะเบียนที่รอการชำระเงินผ่านการโอนเงินทางธนาคาร สามารถแจ้งการชำระเงินตามขั้นตอนดังต่อไปนี้</li>

							<ol style="list-style-type:decimal">
			          <li style="margin-left: -42px;">เลือกรายการที่ต้องการแจ้งชำระเงิน</li>
			          <li style="margin-left: -42px;">กดปุ่ม “แจ้งยืนยันการโอนเงิน” กรอกวันที่โอน และแนบหลักฐานการโอนเงิน หรือ สามารถเปลี่ยนเป็นการชำระด้วยบัตรเครดิต  โดยเลือก “ชำระด้วยบัตรเครดิต”</li>
								<li style="margin-left: -42px;">กดปุ่ม “ชำระด้วยบัตรเครดิต” หากต้องการเปลี่ยนเป็นการชำระด้วยบัตรเครดิต</li>
			          <center style="padding:15px 0 20px; margin-left:-98px;">
			            <?php echo '<img src="'.base_url('assets/img/howtobuy/waiting.png').'" alt="waiting" height="300" width="630" class="img-responsive";/>'; ?>
			          </center>
							</ol>

		          <li style="margin-left: -23px;"><b style="color:#f54700;">รายการรอตรวจสอบ</b> - รายการลงทะเบียนที่ชำระเงินแล้ว รอการตรวจสอบจาก NTC admin</li>
		          <li style="margin-left: -23px;"><b style="color:#f54700;">รายการชำระเงินแล้ว</b> - รายการลงทะเบียนที่ทุกขั้นตอนเสร็จสมบูรณ์แล้ว</li>
		          <li style="margin-left: -23px;"><b style="color:#f54700;">รายการยกเลิก</b> - รายการลงทะเบียนที่ได้ทำการยกเลิกโดยผู้ใช้และ NTC</li>
						</ul>

	          <div style="margin-bottom:20px;"></div>
	          <li><b>“Wishlist”</b> ผู้ใช้สามารถเลือกวิชาเรียนก่อนโดยไม่ชำระเงินในทันที โดยการคลิกรูปหัวใจในหน้าแรกและตรวจสอบรายวิชาที่เลือกเก็บไว้หลังจากเข้าสู่บัญชีส่วนตัว</li>
	          <center style="padding:20px 0 20px; margin-left:-18px;">
	            <?php echo '<img src="'.base_url('assets/img/howtobuy/wishlist.png').'" alt="wishlist" height="300" width="620" class="img-responsive";/>'; ?>
	          </center>

	          <li><b>“ข้อมูลส่วนตัว”</b> ผู้ใช้สามารถตรวจสอบ เปลี่ยนแปลง ข้อมูลการลงทะเบียนในเว็บไซต์ ข้อมูลการชำระเงินทั้งในนามบุคคลและในนามบริษัท</li>
	          <center style="padding:20px 0 20px; margin-left:-18px;">
	            <?php echo '<img src="'.base_url('assets/img/howtobuy/myprofile1.png').'" alt="myprofile" height="300" width="620" class="img-responsive";/>'; ?>
	          </center>

	          <li><b>“เปลี่ยนรหัสผ่าน”</b> ผู้ใช้สามารถเปลี่ยนรหัสผ่านของบัญชีผู้ใช้เว็บไซต์</li>
	          <center style="padding:20px 0 20px; margin-left:-18px;">
	            <?php echo '<img src="'.base_url('assets/img/howtobuy/changepasswordcomplete2.png').'" alt="changepassword" height="300" width="620" class="img-responsive";/>'; ?>
	          </center>
					</ol>


    		</div>
      </div><!-- End row -->
  	</div><!-- End container -->
	</section><!-- End main_content-->
</div>
