<div class="fix-content-width">
	<section id="main-features" style="border-top: 1px solid #ccc;">
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1 text-center">
					<h1 style="color:#f54700;">My Profile</h1>
				</div>
			</div><!-- End row -->
		</div><!-- End container -->
	</section><!-- End sub-header -->
  <section id="main_content">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
					<h3 style="padding-bottom:20px;color:#f54700;" ><?php echo $this->lang->line('profile_my_profile'); ?></h3>
    			<?php echo form_open('profile/form_post', array('id'=>'profileform')); ?>
					<div class="box_style_2">
			      <div class="row">
			        <div class="col-md-6 col-sm-6">
			          <div class="form-group">
			            <label class="control-label"><?php echo $this->lang->line('profile_first_name'); ?></label>
			            <input type="text" class="form-control style_2" id="txtFirstname" name="txtFirstname" placeholder="ชื่อ *" required value="<?php echo $member->firstname; ?>">
			          </div>
			        </div>
			  			<div class="col-md-6 col-sm-6">
			  				<div class="form-group">
			            <label class="control-label"><?php echo $this->lang->line('profile_last_name'); ?></label>
			            <input type="text" class="form-control style_2" id="txtLastname" name="txtLastname" placeholder="นามสกุล *" required value="<?php echo $member->lastname; ?>">
			  				</div>
			  			</div>
			  		</div>
			  		<div class="row">
			  			<div class="col-md-6 col-sm-6">
			  				<div class="form-group">
			            <label class="control-label"><?php echo $this->lang->line('profile_email'); ?></label>
			            <input type="email" id="txtEmail" name="txtEmail" class="form-control style_2" placeholder="อีเมล *" readonly value="<?php echo $member->email; ?>">
			  				</div>
			  			</div>
			  			<div class="col-md-6 col-sm-6">
			  				<div class="form-group">
			            <label class="control-label"><?php echo $this->lang->line('profile_mobile'); ?></label>
			  					<input type="text" id="txtMobile" name="txtMobile" class="form-control style_2" placeholder="เบอร์โทรศัพท์มือถือ " value="<?php echo $member->mobile; ?>">
			  				</div>
			  			</div>
			  		</div>
			  		<div class="row">
			  			<div class="col-md-12">
			  				<div class="form-group">
			            <label class="control-label"><?php echo $this->lang->line('profile_company_name'); ?></label>
			  					<input type="text" id="txtCompany" name="txtCompany" class="form-control style_2" placeholder="-" value="<?php echo $member->company; ?>">
			  				</div>
			  			</div>
			  		</div>
					</div>
					<h3 style="padding-bottom:20px;color:#f54700;"><?php echo $this->lang->line('profile_payment_information'); ?></h3>
		      <div class="form-group">
		        <label class="control-label"><?php echo $this->lang->line('profile_payment_on_behalf'); ?></label>
		        <div>
		          <label>
		            <input type="radio" name="rdoCompany" value="1" <?php echo ($member->is_company == '1')?'checked="checked"':''; ?>/> <?php echo $this->lang->line('profile_payment_company'); ?>
		          </label>&nbsp;&nbsp;
		          <label>
		            <input type="radio" name="rdoCompany" value="0" <?php echo ($member->is_company == '0')?'checked="checked"':''; ?>/> <?php echo $this->lang->line('profile_payment_individual'); ?>
		          </label>
		        </div>
		      </div>

					<div style="padding-top:10px; padding-bottom:25px;">
						<label class="control-label"><?php echo $this->lang->line('profile_payment_detail'); ?></label>
					</div>
		      <div class="box_style_2">
            <div id="company-payment-form-group">
  		        <div class="form-group">
  		          <label for="txtCompanyName" class="control-label"><?php echo $this->lang->line('profile_payment_company_name'); ?></label>
  		          <input type="text" class="form-control" name="txtCompanyName" id="txtCompanyName" value="<?php echo $member->company_name; ?>" />
  		        </div>
  		        <div class="row">
  		          <div class="col-md-6">
  		            <div class="form-group">
  		              <label for="txtCompanyBranch" class="control-label"><?php echo $this->lang->line('profile_payment_branch'); ?></label>
  		              <input type="text" class="form-control" name="txtCompanyBranch" id="txtCompanyBranch" value="<?php echo $member->company_branch; ?>" />
  		            </div>
  		          </div>
  		          <div class="col-md-6">
  		            <div class="form-group">
  		              <label for="txtCompanyTax" class="control-label"><?php echo $this->lang->line('profile_payment_company_tax'); ?></label>
  		              <input type="text" class="form-control" name="txtCompanyTax" id="txtCompanyTax" value="<?php echo $member->company_tax; ?>" />
  		            </div>
  		          </div>
  		        </div>
            </div>
		        <div class="form-group">
		          <label for="txtCompanyAddress" class="control-label"
              id="labelCompanyAddress"
              data-company-label="<?php echo $this->lang->line('profile_payment_company_address'); ?>"
              data-individual-label="<?php echo $this->lang->line('profile_payment_individual_address'); ?>"
              ><?php echo $this->lang->line('profile_payment_company_address'); ?></label>
		          <textarea class="form-control" name="txtCompanyAddress" id="txtCompanyAddress"><?php echo $member->company_address; ?></textarea>
		        </div>
		      </div>
		  		<div class="row">
		  			<div class="col-md-12"></div>
		  			<div class="col-md-12" style="margin-top:20px;">
		  				<button type="submit" class="button_medium" id="submit-contact"><?php echo $this->lang->line('profile_payment_save'); ?></button>
		  			</div>
		  		</div>
		  	<?php echo form_close(); ?>
		  </div>
		</div>
  	</div>
  </section>
</div>
