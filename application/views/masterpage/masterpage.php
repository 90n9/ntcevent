<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Network Training Center Co., Ltd. - Courses, Education, Event site</title>
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, maximum-scale=1, user-scalable=no">

        <!-- Favicons-->
        <link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.ico'); ?>" type="image/x-icon"/>
        <link rel="apple-touch-icon" type="image/x-icon" href="<?php echo base_url('assets/img/apple-touch-icon-57x57-precomposed.png'); ?>">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?php echo base_url('assets/img/apple-touch-icon-72x72-precomposed.png'); ?>">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?php echo base_url('assets/img/apple-touch-icon-114x114-precomposed.png'); ?>">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?php echo base_url('assets/img/apple-touch-icon-144x144-precomposed.png'); ?>">
        <!-- REVOLUTION BANNER CSS SETTINGS -->
        <link href="<?php echo base_url('assets/rs-plugin/css/settings.css'); ?>" media="screen" rel="stylesheet">
        <link href="<?php echo base_url('assets/vendors/colorbox/colorbox.css'); ?>" rel="stylesheet">
        <!-- CSS -->
        <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/superfish.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/vendors/pnotify/pnotify.custom.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/style.css?v=7'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/color_scheme.css?v=13'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/fontello/css/fontello.css'); ?>" rel="stylesheet">

        <?php $this->MasterpageService->printCss(); ?>

        <!--[if lt IE 9]>
          <script src="http://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="http://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
      <input type="hidden" id="base_url" value="<?php echo base_url(); ?>" />
        <?php $this->load->view('masterpage/header'); ?>
        <?php echo $content; ?>
        <?php $this->load->view('masterpage/footer'); ?>
        <!-- JQUERY -->
        <script src="<?php echo base_url('assets/js/jquery-1.10.2.min.js'); ?>"></script>
        <!-- jQuery REVOLUTION Slider  -->
        <script type="text/javascript" src="<?php echo base_url('assets/rs-plugin/js/jquery.themepunch.plugins.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/rs-plugin/js/jquery.themepunch.revolution.min.js'); ?>"></script>
        <script type="text/javascript">
          var revapi;
          jQuery(document).ready(function() {
            setTimeout(function(){
              revapi = jQuery('.tp-banner').revolution(
                {
                  delay:9000,
                  startwidth:1700,
                  startheight:600,
                  hideThumbs:true,
                  navigationType:"none",
                  fullWidth:"on",
                  forceFullWidth:"on"
                });
            }, 300);
          }); //ready
        </script>

        <!-- OTHER JS -->
        <script src="<?php echo base_url('assets/js/superfish.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/retina.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.validate.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.placeholder.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/functions.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/classie.js'); ?>"></script>
        <script src="<?php echo base_url('assets/vendors/jquery.browser.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/vendors/jquery.easing.1.3.js'); ?>"></script>
        <script src="<?php echo base_url('assets/vendors/colorbox/jquery.colorbox-min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/vendors/pnotify/pnotify.custom.min.js'); ?>"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrZN_hFwEb1Cy0LemtHNf1RDTX3-epNjY"></script>
        <script src="<?php echo base_url('assets/js/page/bookmark.js'); ?>"></script>
        <?php $this->MasterpageService->printJs(); ?>
        <script src="<?php echo base_url('assets/js/main.js?v=6'); ?>"></script>
        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/56cfcff0ea1b48e91d4bce6f/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
        </script>
        <!--End of Tawk.to Script-->
        <!--Start of Google Analytic Script -->
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-86941319-2', 'auto');
          ga('send', 'pageview');
        </script>
        <!--End of Google Analytic Script -->
    </body>
</html>
