<?php echo form_open('register', array('id'=>'register-form', 'style'=>'display:none')); ?>
<div class="modal-body">
  <div class="popup_form_header"><?php echo $this->lang->line('popup_sign_up_header'); ?></div>
  <div id="div-register-msg">
    <span id="text-register-msg"></span>
  </div>
  <input id="register_email" class="form-control" type="text" placeholder="<?php echo $this->lang->line('popup_sign_up_input_email'); ?>" required />
  <input id="register_password" class="form-control" type="password" placeholder="<?php echo $this->lang->line('popup_sign_up_input_password'); ?>" required />
  <input id="register_firstname" class="form-control" type="text" placeholder="<?php echo $this->lang->line('popup_sign_up_input_firstname'); ?>" required />
  <input id="register_lastname" class="form-control" type="text" placeholder="<?php echo $this->lang->line('popup_sign_up_input_lastname'); ?>" required />
  <input id="register_mobile" class="form-control" type="text" placeholder="<?php echo $this->lang->line('popup_sign_up_input_mobile'); ?>" />
  <input id="register_company" class="form-control" type="text" placeholder="<?php echo $this->lang->line('popup_sign_up_input_company'); ?>" />
</div>
<div class="modal-footer">
  <div>
    <button type="submit" class="button_fullwidth"><?php echo $this->lang->line('popup_sign_up_button'); ?></button>
  </div>
  <div>
    <button id="register_login_btn" type="button" class="btn btn-link"><?php echo $this->lang->line('popup_link_login'); ?></button>
    <button id="register_lost_btn" type="button" class="btn btn-link"><?php echo $this->lang->line('popup_link_forgotpassword'); ?></button>
  </div>
</div>
<?php echo form_close(); ?>
<div id="register-form-complete" style="display:none;">
  <div class="modal-body">
    <div class="popup_form_header">THANK YOU!</div>
    <div style="padding:20px 0;text-align:center;">Please check your inbox to verify your email.</div>
  </div>
</div>
