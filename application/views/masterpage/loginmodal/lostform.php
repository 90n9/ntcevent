<?php echo form_open('forgotpassword', array('id'=>'lost-form', 'style'=>'display:none;')); ?>
<div class="modal-body">
  <div class="popup_form_header"><?php echo $this->lang->line('popup_forgotpassword_header'); ?></div>
  <div id="div-lost-msg">
    <span id="text-lost-msg"></span>
  </div>
  <input type="text" class="form-control" id="lost_email" placeholder="<?php echo $this->lang->line('popup_forgotpassword_input_email'); ?>" required />
</div>
<div class="modal-footer">
  <div>
    <button type="submit" class="button_fullwidth"><?php echo $this->lang->line('popup_forgotpassword_button'); ?></button>
  </div>
  <div>
    <button id="lost_login_btn" type="button" class="btn btn-link"><?php echo $this->lang->line('popup_link_login'); ?></button>
    <button id="lost_register_btn" type="button" class="btn btn-link"><?php echo $this->lang->line('popup_link_sign_up'); ?></button>
  </div>
</div>
<?php echo form_close(); ?>
<div id="lost-form-complete" style="display:none;">
  <div class="modal-body">
    <div class="popup_form_header">An email has been sent!</div>
    <div style="padding:20px 0;text-align:center;">Please check your email to reset password.</div>
  </div>
</div>
