<?php echo form_open('signin', array('id'=>'login-form')); ?>
<div class="modal-body">
  <div class="popup_form_header"><?php echo $this->lang->line('popup_login_header'); ?></div>
  <div id="div-login-msg">
    <span id="text-login-msg"></span>
  </div>
  <input id="login_email" class="form-control" type="text" placeholder="<?php echo $this->lang->line('popup_login_input_email'); ?>" required>
  <input id="login_password" class="form-control" type="password" placeholder="<?php echo $this->lang->line('popup_login_input_password'); ?>" required>
</div>
<div class="modal-footer">
  <div>
    <button type="submit" class="button_fullwidth"><?php echo $this->lang->line('popup_login_button'); ?></button>
  </div>
  <div>
    <button id="login_lost_btn" type="button" class="btn btn-link"><?php echo $this->lang->line('popup_link_forgotpassword'); ?></button>
    <button id="login_register_btn" type="button" class="btn btn-link"><?php echo $this->lang->line('popup_link_sign_up'); ?></button>
  </div>
</div>
<?php echo form_close(); ?>
