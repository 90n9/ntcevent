<header>
    <div class="container">
    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-3">
            <?php echo anchor('home','<img src="'.base_url('assets/img/logo.png').'" alt="Training Center" />', array('id'=>'logo')); ?>
        </div>
        <div class="col-md-9 col-sm-9 col-xs-9">
            <div class=" pull-right">
            <?php
            if($this->LoginService->getLoginStatus()){
                ?>
                <ul class="pull-right user_panel">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><strong><?php echo $this->session->userdata('firstname'); ?></strong> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><?php echo anchor('mycourse', $this->lang->line('user_menu_mycourse')); ?></li>
                            <li><?php echo anchor('wishlist', $this->lang->line('user_menu_wishlist')); ?></li>
                            <li><?php echo anchor('profile', $this->lang->line('user_menu_profile')); ?></li>
                            <li><?php echo anchor('changepassword', $this->lang->line('user_menu_changepassword')); ?></li>
                            <li class="divider"></li>
                            <li><?php echo anchor('signin/logout','<i class="icon-off"></i> '.$this->lang->line('user_menu_logout').'</a>'); ?></li>
                        </ul>
                    </li>
                </ul>
                <?php
            }else{
              echo '<a href="#" id="login_top" class="nav-register button_top hidden-xs" role="button" data-toggle="modal" data-target="#login-modal">'.$this->lang->line('main_menu_register').'</a>';
              echo '<a href="#" id="apply" class="nav-login button_top" role="button" data-toggle="modal" data-target="#login-modal">'.$this->lang->line('main_menu_signin').'</a>';
              //echo anchor('register',$this->lang->line('main_menu_register'), array('class'=>'button_top hidden-xs', 'id'=>'login_top'));
              //echo anchor('signin',$this->lang->line('main_menu_signin'), array('class'=>'button_top', 'id'=>'apply'));
              $this->load->view('masterpage/loginmodal');
            }
            ?>
            </div>
            <div id="top_nav">
            </div>
            <ul id="top_nav">
              <li>
                <?php
                $flag_img = 'assets/img/'.(($lang == 'thai')?'flag_en.png':'flag_th.png');
                $lang_url = 'lang/'.(($lang == 'thai')?'en':'th');
                $span_txt = ($lang == 'thai')?'EN':'TH';
                $lang_msg = img($flag_img).' <span class="hidden-xs">'.$span_txt.'</span>';
                echo anchor($lang_url, $lang_msg, array('class'=>'active'));
                ?>
              </li>
            </ul>
        </div>
    </div>
</div>
</header><!-- End header -->
<nav>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div id="mobnav-btn"></div>
        <ul class="sf-menu pull-right">
          <li><?php echo anchor('home', $this->lang->line('main_menu_home')); ?></li>
          <li class="normal_drop_down">
            <?php echo anchor('course', $this->lang->line('main_menu_course')); ?>
          <div class="mobnav-subarrow"></div>
              <ul>
                  <?php
                  $courseCategoryList = $this->CourseCategoryModel->getList(0);
                  foreach($courseCategoryList->result() as $courseCategory){
                      $courseCategoryName = ($lang == 'thai')?$courseCategory->course_category_name_th:$courseCategory->course_category_name_en;
                      echo '<li>';
                      echo anchor('course?catid='.$courseCategory->course_category_id, ($courseCategoryName!= '')?$courseCategoryName:'-');
                      echo '</li>';
                  }
                  ?>
              </ul>
          </li>
          <li><?php echo anchor('offer', $this->lang->line('main_manu_promotion')); ?></li>
          <li><?php echo anchor('howtobuy', $this->lang->line('main_manu_howtobuy')); ?></li>
          <li><?php echo anchor('about', $this->lang->line('main_menu_about')); ?></li>
          <li><?php echo anchor('contact', $this->lang->line('main_menu_contact')); ?></li>
        </ul>
      </div>
    </div><!-- End row -->
  </div><!-- End container -->
</nav>
