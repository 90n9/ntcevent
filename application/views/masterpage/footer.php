<footer id="subscribeus">
	<?php $this->load->view('subscribe/form'); ?>
	<hr>
	<div class="container" id="nav-footer">
		<div class="row text-left">
			<div class="col-md-3 col-sm-3">
				<h4>Browse</h4>
				<ul>
				<?php
					echo '<li>'.anchor('about',$this->lang->line('main_menu_about')).'</li>';
					echo '<li>'.anchor('contact',$this->lang->line('main_menu_contact')).'</li>';
					echo '<li>'.anchor('privacy',$this->lang->line('main_menu_privacy')).'</li>';
					echo '<li>'.anchor('terms',$this->lang->line('main_menu_terms')).'</li>';
					echo '<li>'.anchor('offer',$this->lang->line('main_manu_promotion')).'</li>';
				?>
				</ul>
			</div><!-- End col-md-4 -->
			<div class="col-md-3 col-sm-3">
				<h4><?php echo $this->lang->line('main_menu_course'); ?></h4>
				<ul>
                    <?php
                    $courseCategoryList = $this->CourseCategoryModel->getList(0);
                    foreach($courseCategoryList->result() as $courseCategory){
                        $courseCategoryName = ($lang == 'thai')?$courseCategory->course_category_name_th:$courseCategory->course_category_name_en;
                        echo '<li>';
                        echo anchor('course?catid='.$courseCategory->course_category_id, ($courseCategoryName!= '')?$courseCategoryName:'-');
                        echo '</li>';
                    }
                    ?>
				</ul>
			</div><!-- End col-md-4 -->
			<div class="col-md-3 col-sm-3">
				<h4>Links</h4>
				<ul>
					<li><a href="http://www.trainingcenter.co.th" target="_blank"><?php echo $this->lang->line('link_main'); ?></a></li>
					<li><a href="http://tms.trainingcenter.co.th" target="_blank"><?php echo $this->lang->line('link_tms'); ?></a></li>
				</ul>
			</div><!-- End col-md-4 -->
			<div class="col-md-3 col-sm-3">
				<ul id="follow_us">
					<li><a href="https://www.facebook.com/ntcfanclub/" target="_blank"><i class="icon-facebook"></i></a></li>
					<li><a href="https://twitter.com/follow_ntc" target="_blank"><i class=" icon-twitter"></i></a></li>
					<li><a href="https://www.youtube.com/channel/UCqrbbIyLtywjXCO7V9bF76A" target="_blank"><i class="icon-youtube"></i></a></li>
					<li><a href="https://www.linkedin.com/company/network-training-center-co--ltd-" target="_blank"><i class="icon-linkedin"></i></a></li>
					<li><a href="http://line.me/ti/p/%40NTC-LINE" target="_blank"><i class="icon-line"></i></a></li>
				</ul>
				<ul>
					<li>
						<strong class="phone">+66 (0) 2634 7993-4</strong><br>
						<?php echo $this->lang->line('working_hours'); ?>
					</li>
					<li><?php echo $this->lang->line('questions?'); ?> <a href="mailto:sales@trainingcenter.co.th">sales@trainingcenter.co.th</a></li>
				</ul>
			</div><!-- End col-md-4 -->
		</div><!-- End row -->
	</div>
	<div id="copy_right">© 2016 Network Training Center Co., Ltd.</div>
</footer>
