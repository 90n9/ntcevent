<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" align="center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span class="icon-cancel" aria-hidden="true"></span>
        </button>
      </div>
      <div id="div-forms">
        <?php
        $this->load->view('masterpage/loginmodal/loginform');
        $this->load->view('masterpage/loginmodal/lostform');
        $this->load->view('masterpage/loginmodal/registerform');
        $this->load->view('masterpage/loginmodal/loading');
        ?>
      </div>
    </div>
  </div>
</div>
