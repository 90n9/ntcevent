<div class="fix-content-width">
	<section id="main-features" style="border-top: 1px solid #ccc;">
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1 text-center">
					<h1 style="color:#f54700;">Change Password</h1>
				</div>
			</div><!-- End row -->
		</div><!-- End container -->
	</section><!-- End sub-header -->
  <section id="main_content">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
					<h3 style="padding-bottom:20px;color:#f54700;" >Change Password</h3>
    			<?php echo form_open('changepassword/form_post', array('id'=>'form-changepassword')); ?>
          <div class="row">
            <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <label class="control-label"><?php echo $this->lang->line('changepassword_password'); ?></label>
                <input type="password" class="form-control style_2" id="password" name="password" placeholder="please input at least 6 character" maxlength="40" minlength="6" required />
              </div>
            </div>
            <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <label class="control-label"><?php echo $this->lang->line('changepassword_confirm_password'); ?></label>
                <input type="password" class="form-control style_2" id="confirm_password" name="confirm_password" placeholder="confirm new password *" equalto="#password" required />
              </div>
            </div>
          </div>
		  		<div class="row">
		  			<div class="col-md-12"></div>
		  			<div class="col-md-12" style="margin-top:20px;">
		  				<button type="submit" class="button_medium" id="submit-contact"><?php echo $this->lang->line('changepassword_save'); ?></button>
		  			</div>
		  		</div>
		  	<?php echo form_close(); ?>
		  </div>
		</div>
  	</div>
  </section>
</div>
