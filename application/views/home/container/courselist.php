<section id="main_content" style="padding-bottom:0">
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-md-4 btn-course-category">
                <?php echo anchor('course?catid=1','<img src="'.base_url('assets/img/banner_course/computer_network.png').'" class="img-responsive" alt="computer network" />'); ?>
            </div>
            <div class="col-xs-6 col-md-4 btn-course-category">
                <?php echo anchor('course?catid=9','<img src="'.base_url('assets/img/banner_course/it_management.png').'" class="img-responsive" alt="computer network" />'); ?>
            </div>
            <div class="col-xs-6 col-md-4 btn-course-category">
                <?php echo anchor('course?catid=11','<img src="'.base_url('assets/img/banner_course/professional_skills.png').'" class="img-responsive" alt="computer network" />'); ?>
            </div>
            <div class="col-xs-6 col-md-4 btn-course-category">
                <?php echo anchor('course?catid=24','<img src="'.base_url('assets/img/banner_course/Programming.png').'" class="img-responsive" alt="computer network" />'); ?>
            </div>
            <div class="col-xs-6 col-md-4 btn-course-category">
                <?php echo anchor('course?catid=25','<img src="'.base_url('assets/img/banner_course/Manufacturing.png').'" class="img-responsive" alt="computer network" />'); ?>
            </div>
            <div class="col-xs-6 col-md-4 btn-course-category">
                <?php echo anchor('course?catid=4','<img src="'.base_url('assets/img/banner_course/event_activity.png').'" class="img-responsive" alt="computer network" />'); ?>
            </div>
        </div>
    </div>
</section>
