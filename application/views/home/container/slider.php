<section class="tp-banner-container">
	<div class="tp-banner" >
		<ul class="sliderwrapper">	<!-- SLIDE  -->
		<?php foreach($slideshowList->result() as $slideshow){ ?>
			<li data-transition="fade" data-slotamount="4" data-masterspeed="1000" >
				<!-- MAIN IMAGE -->
				<?php echo $this->SlideshowModel->showImage($slideshow, $lang); ?>
			</li>
		<?php } ?>
		</ul>
		<div class="tp-bannertimer"></div>
	</div>
</section><!-- End slider -->