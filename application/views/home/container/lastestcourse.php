<section id="main_content">
	<div class="container">
    	<div class="row">
            <div class="col-md-12 text-center">
                <h2>Latest Courses</h2>
            </div>
        </div><!-- End row -->
        
        <div class="row">
        <?php
        $courseList = $this->CourseModel->getLastest();
        foreach($courseList->result() as $course){
            echo '<div class="col-lg-3 col-md-6 col-sm-6">';
            $this->load->view('course/thumbcontainer',array('course'=>$course));
            echo '</div>';
        }
        ?>
        </div><!-- End row -->
        <div class="row">
        	<div class="col-md-12">
                <?php echo anchor('course?type=lastest', 'View all courses', array('class'=>'button_medium_outline pull-right')); ?>
            </div>
        </div>
    </div>   <!-- End container -->
</section><!-- End section gray -->