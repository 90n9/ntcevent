<section id="main_content_gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Upcoming Courses</h2>
            </div>
        </div><!-- End row -->
        <div class="row">
            <?php
            $courseList = $this->CourseModel->getNext();
            foreach($courseList->result() as $course){
                echo '<div class="col-lg-3 col-md-6 col-sm-6">';
                $this->load->view('course/thumbcontainer',array('course'=>$course));
                echo '</div>';
            }
            ?>
        </div><!-- End row -->
        <div class="row">
            <div class="col-md-12">
                <?php echo anchor('course?type=upcoming', 'View all courses', array('class'=>'button_medium_outline pull-right')); ?>
            </div>
        </div>
    </div>   <!-- End container -->
</section><!-- End section gray -->