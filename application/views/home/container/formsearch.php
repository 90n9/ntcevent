<section id="main-features">
  <div class="container">
    <div class="row">
      <div class=" col-md-10 col-md-offset-1 text-center">
        <h2>Browse Courses</h2>
        <?php echo form_open('course',array('method'=>'get')); ?>
        <div>
          <div class="col-sm-6 col-lg-4">
            <input type="text" name="text" class="form-control" placeholder="Search" value="<?php echo (isset($text))?$text:''; ?>" />
          </div>
          <div class="col-sm-6 col-lg-3">
            <div class="styled-select">
              <select class="form-control" name="catid" id="catid">
                <option value="0">All Course</option>
                <?php
                $courseCategoryList = $this->CourseCategoryModel->getList(0);
                foreach($courseCategoryList->result() as $courseCategory){
                  $courseCategoryName = ($lang == 'thai')?$courseCategory->course_category_name_th:$courseCategory->course_category_name_en;
                  $select = (isset($catid) && $catid == $courseCategory->course_category_id)?'selected="selected"':'';
                  echo '<option value="'.$courseCategory->course_category_id.'" '.$select.'>'.$courseCategoryName.'</option>';
                }
                ?>
              </select>
            </div>
          </div>
          <div class="col-sm-6 col-lg-3">
              <div class="styled-select">
                  <select class="form-control" name="price">
                      <option value="">All Price</option>
                      <?php
                      $select = (isset($price) && $price == 'free')?'selected="selected"':'';
                      echo '<option value="free" '.$select.'>Free</option>';
                      $select = (isset($price) && $price == 'contact')?'selected="selected"':'';
                      echo '<option value="contact" '.$select.'>Contact</option>';
                      $priceList = $this->PriceRangeModel->getList();
                      foreach($priceList->result() as $priceRange){
                          $select = (isset($price) && $price == $priceRange->price_range_id)?'selected="selected"':'';
                          echo '<option value="'.$priceRange->price_range_id.'" '.$select.'>'.$priceRange->price_from.'-'.$priceRange->price_to.'</option>';
                      }
                      ?>
                  </select>
              </div>
          </div>
          <div class="col-sm-6 col-lg-2 text-left">
              <button type="submit" class=" button_subscribe"><img src="<?php echo base_url('assets/img/search.png'); ?>" height="22"  alt="search"/></button>
          </div>
        </div>
        <?php echo form_close(); ?>
      </div>
    </div>
  </div><!-- End container-->
</section><!-- End main-features -->
