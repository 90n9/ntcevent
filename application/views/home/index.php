<style>
  body{
    display:none;
  }
</style>
<div id="container-home">
  <?php $this->load->view('home/container/slider'); ?>
  <?php $this->load->view('home/container/formsearch', array('showBar'=>true)); ?>
  <div class="fix-content-width">
    <?php $this->load->view('home/container/courselist'); ?>
    <?php $this->load->view('home/container/nextcourse'); ?>
    <?php $this->load->view('home/container/lastestcourse'); ?>
  </div>
</div>
<?php
$this->load->view('course/locationallpopup');
