<div class="fix-content-width">
	<section id="main-features" style="border-top: 1px solid #ccc;">
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1 text-center">
					<h1 style="color:#f54700;">Promotions &amp; Offers</h1>
				</div>
			</div><!-- End row -->
		</div><!-- End container -->
	</section><!-- End sub-header -->

	<section id="main_content">
  	<div class="container">
    	<ol class="breadcrumb">
    	  <li><?php echo anchor('home','HOME'); ?></li>
    	  <li class="active">Promotions &amp; Offers</li>
    	</ol>
      <?php
      $counter = 0;
      foreach($offer_list->result() as $offer){
        $offer_content = ($lang == 'th')?$offer->offer_conent_th:$offer->offer_content_en;
        echo ($counter > 0)?'<hr />':'';
        $counter++;
        echo '
        <div class="row">
          <div class="col-sm-5 col-md-4"><img src="'.base_url('uploads/'.$offer->offer_image).'" class="img-responsive" /></div>
          <div class="col-sm-7 col-md-8">'.$offer_content.'</div>
        </div>';
      }
      ?>
    </div><!-- End container -->
	</section><!-- End main_content-->
</div>
