<section id="login_bg" style="padding:40px 0;">
  <div  class="container">
    <div class="row">
      <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
        <div id="login">
          <?php echo anchor('','<span class="icon-cancel" aria-hidden="true"></span>', array('class'=>'close login-close')); ?>
          <p class="text-center">
            <img src="<?php echo base_url('assets/img/logo_darken.png'); ?>" alt="">
          </p>
          <hr />
          <div style="text-align: center;">
            <?php echo form_open('forgotpassword/changepassword_post/'.$hashCode); ?>
            <h4 style="padding-top:10px;">Reset Password</h4>
            <div id="pass-info" class="clearfix"></div>
            <div style="text-align:left;">
              <div class="form-group">
                <input type="password" class="form-control" name="password" id="password" placeholder="new password" />
              </div>
              <div class="form-group">
                <input type="password" class="form-control" name="confirmpassword" id="confirmpassword" placeholder="confirm new password" />
              </div>
            </div>
            <div style="padding-top:10px;">
              <button type="submit" class="button_outline">SUBMIT</button>
            </div>
            <?php echo form_close(); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section><!-- End register -->
