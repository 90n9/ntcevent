<div class="fix-content-width">
	<section id="main-features" style="border-top: 1px solid #ccc;">
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1 text-center">
					<h1 style="color:#f54700;">About Us</h1>
				</div>
			</div><!-- End row -->
		</div><!-- End container -->
	</section><!-- End sub-header -->

	<section id="main_content">
	<div class="container">
	<ol class="breadcrumb">
	  <li><?php echo anchor('home','HOME'); ?></li>
	  <li class="active">About</li>
	</ol>
		 <div class="row">
		<div class="col-md-7">
			<?php echo $contentText; ?>
		</div>
		<div class="col-md-4 col-md-offset-1">
	        <div class="row">
	        <div class="col-md-12 col-sm-6">
				<div class="thumbnail">
					<div class="project-item-image-container">
            <?php
            $aboutSideType = $this->ConfigModel->getData('about_side_type');
            if($aboutSideType == 'img'){
              echo '<img src="'.base_url('uploads/'.$this->ConfigModel->getData('about_side_img')).'" />';
            }
            if($aboutSideType == 'video'){
              echo '<div class="video-container"><iframe width="560" height="315" src="https://www.youtube.com/embed/Rqbuj_EQAx8" frameborder="0" allowfullscreen></iframe></div>';
            }
            ?>
					</div>
        </div><!-- End thumbnail -->
				</div><!-- End col-md-12-->
	       </div>
		</div><!-- End row -->
	</div><!-- End container -->
	</section><!-- End main_content-->
</div>
