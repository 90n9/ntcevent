<div class="fix-content-width">
	<section id="main-features" style="border-top: 1px solid #ccc;">
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1 text-center">
					<h1 style="color:#f54700;">Privacy Policy</h1>
				</div>
			</div><!-- End row -->
		</div><!-- End container -->
	</section><!-- End sub-header -->
	<section id="main_content">
		<div class="container">
			<div class="row">
        <div class="col-md-8 col-md-offset-2">
					<?php echo $contentText; ?>
				</div>
			</div><!-- End row -->
		</div><!-- End container -->
	</section><!-- End main_content-->
</div>
