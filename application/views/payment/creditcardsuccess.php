<section id="login_bg">
<div  class="container">
<div class="row">
	<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
		<div id="login">
      <?php echo anchor('','<span class="icon-cancel" aria-hidden="true"></span>', array('class'=>'close login-close')); ?>
			<p class="text-center">
				<img src="<?php echo base_url('assets/img/logo_darken.png'); ?>" alt="">
			</p>
			<hr>
			<div style="text-align: center;">
				<h2 style="font-size:20px;">Your registration is complete. Thank you for your order!</h2>
        <div id="pass-info" class="clearfix"></div>
        <?php
        if($paymentItemList->num_rows() == 1){
          ?>
        <div style="padding-bottom:10px;">ORDER ID #<b><?php echo $courseBuyer->course_buyer_code; ?></b></div>
        <?php } ?>
        <div>
          Your ticket has been sent to <b><?php echo $this->session->userdata('email'); ?></b>
        </div>
        <hr />
        <?php
        if($paymentItemList->num_rows() == 1){
          ?>
          <div>Share <b><?php echo $course->course_name; ?></b></div>
          <?php
          $encodeCourseName = urlencode($course->course_name);
          $encodeUrl = urlencode(site_url('course/detail/'.$course->course_id));
          $this->load->view('course/sociallink', array('encodeCourseName'=>$encodeCourseName, 'encodeUrl'=>$encodeUrl));
        }else{
          ?>
          <div class="text-center" style="padding-top:20px;"><?php echo anchor('mycourse','Back to My Courses', array('class'=>'button_outline')); ?></div>
          <?php
        }
        ?>
      </div>
		</div>
	</div>
</div>
</div>
</section><!-- End register -->
