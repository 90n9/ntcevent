<section id="main_content" class="fix-content-width">
	<div class="container">
        <ol class="breadcrumb">
            <?php
            echo '<li>'.anchor('','หน้าแรก').'</li>';
            echo '<li class="active">'.$title.'</li>';
            ?>
        </ol>
        <div class="row">
        	<div class="col-md-8">
        		<?php $this->load->view('payment/index/forminput'); ?>
        	</div>
					<?php
					if($paymentType == 'bank'){
						?>
        	<div class="col-md-4">
        		<?php $this->load->view('payment/index/notecomment'); ?>
        	</div>
					<?php } ?>
        </div>
    </div>
</section>
