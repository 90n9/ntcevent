<?php echo form_open_multipart('payment/form_post'); ?>
	<h2>แจ้งชำระเงิน</h2>
	<div id="orderList">
		<h3 class="chapter_course">เลือกรายการที่ต้องการชำระเงิน</h3>
		<table class="table table-bordered order_table">
			<thead>
				<tr>
					<th style="text-align:center;" width="42" height="42"></th>
					<th style="text-align:center;" width="80">รหัสสั่งซื้อ</th>
					<th style="text-align:center;">ชื่อคอร์สเรียน</th>
					<th style="text-align:center;" width="88">จำนวนที่นั่ง</th>
					<th style="text-align:center;" width="121">ราคารวม Vat 7%</th>
				</tr>
			</thead>
			<tbody>
			<?php
			foreach($courseBuyerList->result() as $courseBuyer){
				$course = $this->CourseModel->getData($courseBuyer->course_id);
				?>
				<tr>
					<td width="42" height="42" style="position:relative;"><input type="checkbox" class="chkBuyer" name="chkBuyerId[]" value="<?php echo $courseBuyer->course_buyer_id; ?>" data-totalamount="<?php echo $courseBuyer->include_vat_amount; ?>" checked="checked" /></td>
					<td>#<?php echo $courseBuyer->course_buyer_id; ?></td>
					<td><?php echo $course->course_name; ?></td>
					<td style="text-align:center;"><?php echo $courseBuyer->total_ticket; ?></td>
					<td class="text-right"><?php echo number_format($courseBuyer->include_vat_amount, 2); ?></td>
				</tr>
			<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td class="text-right" colspan="4"><b>รวม (บาท)</b></td>
					<td class="text-right"><b><span class="totalAmount">0.00</span></b></td>
				</tr>
			</tfoot>
		</table>
	</div>
	<hr />
	<div class="form-group">
		<label>ช่องทางการชำระเงิน</label>
		<div>
			<label>
				<input type="radio" name="paymentChannel" value="bank" checked="checked" />
				โอนเงินผ่านธนาคาร
			</label>
		</div>
		<div>
			<label>
				<input type="radio" name="paymentChannel" value="creditcard" />
				ชำระด้วยบัตรเครดิต (ค่าธรรมเนียม 3.5%)
			</label>
		</div>
	</div>
	<div id="bankForm" class="transferForm box_style_1" style="background-color:transparent;color:#2d4050;">
		<h3>ข้อมูลการโอนเงินผ่านธนาคาร</h3>
		<div class="row">
			<div class="col-md-4"><img src="<?php echo base_url('assets/img/kbank_logo.jpg'); ?>" class="img-responsive" alt="ธนาคารกสิกรไทย" /></div>
			<div class="col-md-8">
				<div style="padding-top:10px;"><b>ชื่อบัญชี: </b>บริษัท เน็ตเวิร์ก เทรนนิ่ง เซ็นเตอร์ จำกัด</div>
				<div><b>เลขที่บัญชี: </b>001-1-30904-6</div>
			</div>
		</div>
		<hr />
		<div class="row">
			<div class="form-group col-md-6">
				<label for="txtAmount" class="control-label">จำนวนเงินโอน</label>
				<input type="text" class="form-control" name="txtAmount" id="txtAmount" placeholder="บาท" />
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-6">
				<label for="txtPaidDate" class="control-label">วันที่โอนเงิน</label>
				<input type="text" class="form-control" name="txtPaidDate" id="txtPaidDate" placeholder="ตัวอย่าง (2016-05-12)" />
			</div>
			<div class="form-group col-md-6">
				<label for="txtPaidTime" class="control-label">เวลาโอนเงิน</label>
				<input type="text" class="form-control" name="txtPaidTime" id="txtPaidTime" placeholder="ตัวอย่าง (13:15)" />
			</div>
		</div>
		<div class="form-group">
			<label for="fileUpload" class="control-label">หลักฐานการชำระเงิน</label>
			<input type="file" class="form-control" name="fileUpload" id="fileUpload" />
		</div>
	</div>
	<div id="creditcardForm" class="transferForm box_style_1" style="display:none; background-color:transparent;color:#2d4050;">
		<h3>ข้อมูลการชำระด้วยบัตรเครดิต</h3>
		<ul>
			<li>ค่าธรรมเนียม 3.5%</li>
			<li>การชำระผ่านบัตรเครดิต คุณไม่จำเป็นต้องแจ้งชำระเงิน เนื่องจากระบบจะจัดการให้คุณทันที ที่คุณชำระเงินเสร็จสมบูรณ์</li>
		</ul>
		<div>
			<label>ค่าธรรมเนียม</label>
			<p><span class="creditcardFeeAmount">0.00</span> บาท</p>
		</div>
		<div>
			<label>รวมเงินที่ต้องชำระ</label>
			<p><span class="creditcardAmount">0.00</span> บาท</p>
		</div>
	</div>
	<div class="form-group">
		<label>ข้อความถึงเจ้าหน้าที่</label>
		<textarea class="form-control" style="height:auto;" name="txtNote" id="txtNote" rows="5"></textarea>
	</div>
	<div>
		<button type="submit" class="button_medium" id="submit-contact">ยืนยัน</button>
	</div>
<?php echo form_close(); ?>