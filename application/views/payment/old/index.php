<section id="main_content" class="fix-content-width">
	<div class="container">
        <ol class="breadcrumb">
            <?php
            echo '<li>'.anchor('','หน้าแรก').'</li>';
            echo '<li class="active">'.$title.'</li>';
            ?>
        </ol>
        <div class="row">
        	<div class="col-md-8">
        		<?php $this->load->view('payment/offline_form'); ?>
        	</div>
        	<div class="col-md-4">
        		<?php $this->load->view('payment/online_link'); ?>
        	</div>
        </div>
    </div>
</section>