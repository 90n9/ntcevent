<div>
  <p>Dear NTC Sales,</p>
  <p><b><?php echo $member->firstname.' '.$member->lastname; ?></b> has submit pay-in slip.</p>
  <hr />
  <h3>ORDER INFO</h3>
  <table border="1" width="100%" cellpadding="0" cellspacing="0">
    <thead>
      <tr>
        <th>ORDER ID</th>
        <th>COURSE NAME</th>
        <th>TOTAL</th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach($paymentItemList->result() as $paymentItem){
        $courseBuyer = $this->CourseBuyerModel->getData($paymentItem->coure_buyer_id);
        $course = $this->CourseModel->getData($courseBuyer->course_id);
      ?>
      <tr>
        <th><?php echo $courseBuyer->course_buyer_code; ?></th>
        <th><?php echo $course->course_name; ?></th>
        <td><?php echo number_format($paymentItem->course_buyer_amount,2); ?> Baht</td>
      </tr>
      <?php } ?>
      <tr>
        <th colspan="2" style="text-align:right;">SUBTOTAL</th>
        <td><?php echo number_format($payment->order_amount,2); ?> Baht</td>
      </tr>
      <tr>
        <th colspan="2" style="text-align:right;">SUBTOTAL</th>
        <td><?php echo number_format($payment->order_amount,2); ?> Baht</td>
      </tr>
      <tr>
        <th colspan="2" style="text-align:right;">VAT</th>
        <td><?php echo number_format($payment->vat_amount,2); ?> Baht</td>
      </tr>
      <tr>
        <th colspan="2" style="text-align:right;">GRAND TOTAL</th>
        <td><?php echo number_format($payment->order_amount + $payment->vat_amount,2); ?> Baht</td>
      </tr>
    </tbody>
  </table>
  <hr />
  <h3>SUBMIT PAY-IN SLIP INFO</h3>
  <p>Amount : <b><?php echo $payment->payment_amount; ?></b></p>
  <p>Value Date : <b><?php echo $payment->payment_date; ?></b></p>
  <p>Upload bank trasfer slip : <b><?php echo anchor(base_url('uploads/'.$payment->payment_file),'Open File', array('target'=>'_blank')); ?></b></p>
  <p>Leave us a message :<br /><b><?php echo $payment->note; ?></b></p>
  <hr />
  <p style="color:#999;">This email has been send automatically from ntc.trainingcenter.co.th</p>
</div>
