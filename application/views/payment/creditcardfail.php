<section id="login_bg">
<div  class="container">
<div class="row">
	<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
		<div id="login">
      <?php echo anchor('','<span class="icon-cancel" aria-hidden="true"></span>', array('class'=>'close login-close')); ?>
			<p class="text-center">
				<img src="<?php echo base_url('assets/img/logo.png'); ?>" alt="">
			</p>
			<hr>
			<div style="text-align: center;">
				<h2 style="font-size:20px; padding-bottom:10px;">Payment Fail</h2>
        <div id="pass-info" class="clearfix"></div>
        <div><?php echo $errorMsg; ?>.<br />You can register again and try to pay with bank transfer.</div>
        <div style="padding-top:20px;">
          <?php echo anchor('mycourse/', 'BACK TO MY COURSE', array('class'=>'button_outline')); ?>
        </div>
      </div>
		</div>
	</div>
</div>
</div>
</section><!-- End register -->
