<div id="orderList">
  <table class="table table-bordered order_table">
    <thead>
      <tr>
        <th style="text-align:center;" width="80"><?php echo $this->lang->line('payment_order_id'); ?></th>
        <th style="text-align:center;"><?php echo $this->lang->line('payment_course_name'); ?></th>
        <th style="text-align:center;" width="88"><?php echo $this->lang->line('payment_quantity'); ?></th>
        <th style="text-align:center;" width="121"><?php echo $this->lang->line('payment_total_baht'); ?></th>
      </tr>
    </thead>
    <tbody>
    <?php
    $totalAmount = 0;
    foreach($courseBuyerList as $courseBuyerId){
      $courseBuyer = $this->CourseBuyerModel->getData($courseBuyerId);
      $course = $this->CourseModel->getData($courseBuyer->course_id);
      $courseAmount = $courseBuyer->total_amount - $courseBuyer->discount_amount;
      $totalAmount += $courseAmount;
      ?>
      <tr>
        <td>
          <input type="hidden" name="chkBuyerId[]" value="<?php echo $courseBuyer->course_buyer_id; ?>" />
          #<?php echo $courseBuyer->course_buyer_code; ?></td>
        <td><?php echo $course->course_name; ?></td>
        <td style="text-align:center;"><?php echo $courseBuyer->total_ticket; ?></td>
        <td class="text-right"><?php echo number_format($courseAmount, 2); ?></td>
      </tr>
    <?php
    }
    $surchargeAmount = 0;
    if($paymentType != 'bank'){
      $kbankFee = $this->ConfigModel->getData('kbank_fee');
      $surchargeAmount = $totalAmount * $kbankFee / 100;
    }
    $vatAmount = ($totalAmount + $surchargeAmount) * 0.07;
    $paidAmount = $totalAmount + $surchargeAmount + $vatAmount;
    ?>
    </tbody>
    <tfoot>
      <tr <?php echo ($paymentType == 'bank' && count($courseBuyerList) <= 1)?'style="display:none;"':''; ?>>
        <td class="text-right" colspan="3"><b><?php echo $this->lang->line('payment_total_baht'); ?></b></td>
        <td class="text-right"><b><span id="totalPrice" data-totalamount="<?php echo $totalAmount; ?>" data-surchargeamount="<?php echo $surchargeAmount; ?>" data-vatamount="<?php echo $vatAmount; ?>" data-paidamount="<?php echo $paidAmount; ?>"><?php echo number_format($totalAmount, 2); ?></span></b></td>
      </tr>
      <?php
      if($paymentType == 'bank'){
        ?>
        <tr>
          <td class="text-right" colspan="3"><b><?php echo $this->lang->line('payment_vat'); ?></b></td>
          <td class="text-right"><b><span id="vatPrice"><?php echo number_format($vatAmount, 2); ?></span></b></td>
        </tr>
        <tr>
          <td class="text-right" colspan="3"><b><?php echo $this->lang->line('payment_grand_total'); ?></b></td>
          <td class="text-right"><span id="paidPrice"><b><?php echo number_format($totalAmount + $surchargeAmount + $vatAmount, 2); ?></b></span></td>
        </tr>
      <?php } ?>
    </tfoot>
  </table>
</div>
