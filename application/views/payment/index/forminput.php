<?php echo form_open_multipart('payment/form_post', array('id' => 'form-payment')); ?>
	<h2><?php echo $this->lang->line('payment_'.$paymentType.'_header'); ?></h2>
  <input type="hidden" name="paymentChannel" id="paymentChannel" value="<?php echo $paymentType; ?>" />
  <?php $this->load->view('payment/index/courselist'); ?>
	<hr />
	<div class="transferForm box_style_1" style="background-color:transparent;color:#2d4050;">
		<?php
    if($paymentType == 'bank'){
      $this->load->view('payment/index/bankform');
    }else{
      $this->load->view('payment/index/creditcardform');
    }
    ?>
	</div>
	<?php
	if($paymentType == 'bank'){
	?>
	<div class="form-group">
		<label><?php echo $this->lang->line('payment_form_note'); ?></label>
		<textarea class="form-control" style="height:auto;" name="txtNote" id="txtNote" rows="5"></textarea>
	</div>
	<?php } ?>
	<div>
		<button type="submit" class="button_medium" id="submit-contact" style="min-width:140px;"><?php echo $this->lang->line('payment_submit'); ?></button>
		<?php
			$cancelUrl = ($courseBuyerId != 0)?'mycourse/detail/'.$courseBuyerId: 'mycourse/index/waiting';
			echo anchor($cancelUrl, $this->lang->line('payment_back'), array('class'=>'button_medium_outline'));
		?>
	</div>
<?php echo form_close(); ?>
