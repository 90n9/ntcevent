<?php
$totalAmount = 0;
foreach($courseBuyerList as $courseBuyerId){
  $courseBuyer = $this->CourseBuyerModel->getData($courseBuyerId);
  $courseAmount = $courseBuyer->total_amount - $courseBuyer->discount_amount;
  $totalAmount += $courseAmount;
}
$surchargeAmount = 0;
$kbankFee = 0;
if($paymentType != 'bank'){
  $kbankFee = $this->ConfigModel->getData('kbank_fee');
  $surchargeAmount = $totalAmount * $kbankFee / 100;
}
$vatAmount = ($totalAmount + $surchargeAmount) * 0.07;
$paidAmount = $totalAmount + $surchargeAmount + $vatAmount;
?>
<h3 style="padding-bottom:10px;"><?php echo $this->lang->line('course_creditcard_header'); ?></h3>
<input type="hidden" name="txtAmount" id="txtAmount">
<div class="row">
  <div class="col-md-5"><img src="<?php echo base_url('assets/img/payment/creditcard.jpg'); ?>" class="img-responsive" alt="Credit Card" /></div>
  <div class="col-md-7" style="padding-top:10px;">
    <div class="row">
      <label class="col-xs-6" style="font-size=14px; font-weight:normal;"><?php echo $this->lang->line('payment_total'); ?></label>
      <p class="col-xs-6 text-right"><span class="subTotalAmount"><?php echo number_format($totalAmount, 2); ?></span> <?php echo $this->lang->line('baht'); ?></p>
    </div>
    <div class="row">
      <label class="col-xs-6" style="font-size:14px; font-weight:normal;"><?php echo $this->lang->line('course_creditcard_surcharges').' '.$kbankFee; ?>%</label>
      <p class="col-xs-6 text-right"><span class="creditcardFeeAmount"><?php echo number_format($surchargeAmount, 2) ?></span> <?php echo $this->lang->line('baht'); ?></p>
    </div>
    <div class="row">
      <label class="col-xs-6" style="font-weight:normal;">VAT 7%</label>
      <p class="col-xs-6 text-right"><span class="creditcardVatAmount"><?php echo number_format($vatAmount, 2); ?></span> <?php echo $this->lang->line('baht'); ?></p>
    </div>
    <div class="row" style="color:#f54700;font-size:1.1em;">
      <label class="col-xs-6"><?php echo $this->lang->line('course_grand_total'); ?></label>
      <p class="col-xs-6 text-right"><b><span class="creditcardAmount"><?php echo number_format($paidAmount, 2); ?></span> <?php echo $this->lang->line('baht'); ?></b></p>
    </div>
    <div class="text-right">
      <span><img src="<?php echo base_url('assets/img/payment/ico_visa.jpg'); ?>" alt="Visa" /></span>
      <span><img src="<?php echo base_url('assets/img/payment/ico_mastercard.jpg'); ?>" alt="Mastercard" /></span>
    </div>
  </div>
</div>
