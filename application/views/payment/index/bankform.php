<h3><?php echo $this->lang->line('payment_bank_transfer_instruction'); ?></h3>
<div class="row">
  <div class="col-md-4"><img src="<?php echo base_url('assets/img/kbank_logo.jpg'); ?>" class="img-responsive" alt="KBank" /></div>
  <div class="col-md-8" style="text-align:left;">
    <div style="padding-top:10px;"><b style="display:inline-block; width:85px;"><?php echo $this->lang->line('payment_bank_account_name'); ?> </b><?php echo $this->lang->line('payment_bank_account_name_value'); ?></div>
    <div><b style="display:inline-block; width:85px;"><?php echo $this->lang->line('payment_bank_account_number'); ?> </b>001-1-30904-6</div>
  </div>
</div>
<hr />
<div class="row">
  <div class="form-group col-md-6">
    <label for="txtAmount" class="control-label"><?php echo $this->lang->line('payment_bank_amount'); ?></label>
    <input type="text" class="required form-control" name="txtAmount" id="txtAmount" placeholder="" required />
  </div>
  <div class="form-group col-md-6">
    <label for="txtPaidDate" class="control-label"><?php echo $this->lang->line('payment_bank_value_date'); ?></label>
    <input type="text" class="required form-control" name="txtPaidDate" id="txtPaidDate" placeholder="ตัวอย่าง (12-05-2016)" required/>
  </div>
</div>
<div class="form-group">
  <label for="fileUpload" class="control-label"><?php echo $this->lang->line('payment_bank_upload'); ?></label>
  <input type="file" class="required form-control" name="fileUpload" id="fileUpload" required/>
</div>
