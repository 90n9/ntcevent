<section id="login_bg">
<div  class="container">
<div class="row">
	<div style="max-width:500px; margin:auto;">
		<div id="login">
      <?php echo anchor('','<span class="icon-cancel" aria-hidden="true"></span>', array('class'=>'close login-close')); ?>
			<p class="text-center">
				<img src="<?php echo base_url('assets/img/logo_darken.png'); ?>" alt="">
			</p>
			<hr>
			<div style="text-align: center;">
				<h4>Thank you for submitting pay-in slip.</h4>
				<div id="pass-info" class="clearfix"></div>
				<div>Your registration status will be updated after the transaction is confirmed within 3 business days.</div>
			</div>
			<div class="text-center" style="padding-top:20px;"><?php echo anchor('mycourse','Back to My Courses', array('class'=>'button_outline')); ?></div>
		</div>
	</div>
</div>
</div>
</section><!-- End register -->
