<section id="login_bg">
  <div  class="container">
    <div style="max-width: 400px;margin: auto;" >
      <div id="login">
        <?php echo anchor('','<span class="icon-cancel" aria-hidden="true"></span>', array('class'=>'close login-close')); ?>
        <p class="text-center">
          <img src="<?php echo base_url('assets/img/logo_darken.png'); ?>" alt="">
        </p>
        <hr />
        <div style="text-align: center;">
          <h2 style="padding:10px;color:#f54700">THANK YOU!</h2>
          <div>You've been added to our mailing list and will now be among the first to hear about our special offers.</div>
          <div id="pass-info" class="clearfix"></div>
          <div style="padding:10px 0;">
            <?php echo anchor('', 'BACK TO HOME', array('class'=>'button_outline')); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
