	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<h3>Subscribe to our Newsletter for latest news.</h3>
				<div id="message-newsletter"></div>
				<?php echo form_open('subscribe/post',array('name'=>'newsletter', 'id'=>'newsletter', 'class'=>'form-inline')); ?>
					<input name="email_newsletter" id="email_newsletter" type="email" value="" placeholder="Your Email" class="required form-control" required>
					<button type="submit" id="submit-newsletter" class=" button_outline"> Subscribe</button>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
