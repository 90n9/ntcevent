<section id="main_content" class="fix-content-width">
	<div class="container">
        <ol class="breadcrumb">
            <?php echo '<li>'.anchor('','หน้าแรก').'</li>'; ?>
            <li class="active">Wishlist</li>
        </ol>
    	<div class="row">
            <div class="col-md-12 text-center">
                <h2>My Wishlist</h2>
            </div>
            <?php
            foreach($wishlistList->result() as $wishlist){
                $course = $this->CourseModel->getData($wishlist->course_id);
                echo '<div class="col-lg-3 col-md-4">';
                $this->load->view('course/thumbcontainer',array('course'=>$course));
                echo '</div>';
            }
            ?>
   		</div><!-- End row -->
    </div><!-- End container -->
</section><!-- End main_content -->
<?php
$this->load->view('course/locationallpopup');
