<div>
  <p>Dear NTC Sales,</p>
  <p><b><?php echo $courseContact->contact_firstname.' '.$courseContact->contact_lastname; ?></b> has request a quotation of course "<?php echo $course->course_name; ?>" on our website.</p>
  <hr />
  <h3>REQUEST FOR QUOTATION INFO</h3>
  <p>First Name : <b><?php echo $courseContact->contact_firstname; ?></b></p>
  <p>Last Name : <b><?php echo $courseContact->contact_lastname; ?></b></p>
  <p>Email : <b><?php echo $courseContact->contact_email; ?></b></p>
  <p>Phone Number : <b><?php echo $courseContact->contact_phone; ?></b></p>
  <p>Mobile Number : <b><?php echo $courseContact->contact_mobile; ?></b></p>
  <p>Company : <b><?php echo $courseContact->contact_company; ?></b></p>
  <p>Request Ticket : <b><?php echo $courseContact->total_ticket; ?></b></p>
  <p>Message :<br /><b><?php echo $courseContact->contact_note; ?></b></p>
  <hr />
  <p style="color:#999;">This email has been send automatically from ntc.trainingcenter.co.th</p>
</div>
