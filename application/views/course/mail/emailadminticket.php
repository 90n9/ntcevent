<div>
  <p>Dear NTC Sales,</p>
  <p><b><?php echo $courseBuyer->buyer_firstname.' '.$courseBuyer->buyer_lastname; ?></b> has registered "<?php echo $course->course_name; ?>" course on our website.</p>
  <hr />
  <h3>ORDER INFO</h3>
  <p>ORDER ID : <b><?php echo $courseBuyer->course_buyer_code; ?></b></p>
  <p>REQUEST PAYMENT CHANNEL : <b><?php echo ($courseBuyer->want_payment_type == 'bank')?'Bank Transfer':'Creditcard Payment'; ?></b></p>
  <table border="1" width="100%" cellpadding="0" cellspacing="0">
    <tbody>
      <tr>
        <th style="text-align:right;">TICKETS (<?php echo $courseBuyer->total_ticket; ?>)</th>
        <td style="text-align:right;"><?php echo number_format($courseBuyer->total_amount,2); ?> Baht</td>
      </tr>
      <?php if($courseBuyer->discount_code != ''){ ?>
      <tr>
        <th style="text-align:right;">DISCOUNT (<?php echo $courseBuyer->discount_code; ?>)</th>
        <td style="text-align:right;"><?php echo number_format($courseBuyer->discount_amount,2); ?> Baht</td>
      </tr>
      <?php } ?>
      <?php if($courseBuyer->surcharge_amount != 0){ ?>
      <tr>
        <th style="text-align:right;">SURCHARGE</th>
        <td style="text-align:right;"><?php echo number_format($courseBuyer->surcharge_amount,2); ?> Baht</td>
      </tr>
      <?php } ?>
      <tr>
        <th style="text-align:right;">VAT</th>
        <td style="text-align:right;"><?php echo number_format($courseBuyer->vat_amount,2); ?> Baht</td>
      </tr>
      <tr>
        <th style="text-align:right;">GRAND TOTAL</th>
        <td style="text-align:right;"><?php echo number_format($courseBuyer->paid_amount,2); ?> Baht</td>
      </tr>
    </tbody>
  </table>
  <hr />
  <h3>TAX INFO</h3>
  <p>PAYMENT TYPE : <b><?php echo ($courseBuyer->is_company == 0)?'Individual':'Company'; ?></b></p>
  <?php if($courseBuyer->is_company == 1){ ?>
  <p>Company : <b><?php echo $courseBuyer->company_name; ?></b></p>
  <p>Branch : <b><?php echo $courseBuyer->company_branch; ?></b></p>
  <p>TAX ID : <b><?php echo $courseBuyer->company_tax; ?></b></p>
  <?php } ?>
  <p>Address : <b><?php echo $courseBuyer->company_address; ?></b></p>
  <hr />
  <h3>BUYER INFO</h3>
  <p>Name : <b><?php echo $courseBuyer->buyer_firstname.' '.$courseBuyer->buyer_lastname; ?></b></p>
  <p>Email : <b><?php echo $courseBuyer->buyer_email; ?></b></p>
  <p>Mobile : <b><?php echo $courseBuyer->buyer_mobile; ?></b></p>
  <p>Company :<br /><b><?php echo $courseBuyer->buyer_company; ?></b></p>
  <hr />
  <h3>ATTENDEE INFO</h3>
  <?php
  $ticketIndex = 0;
  foreach($courseRegisterList->result() as $courseRegister){
    $ticketIndex++;
  ?>
  <h4>Ticket <?php echo $ticketIndex; ?></h4>
  <p>REGISTER CODE: <b><?php echo $courseRegister->course_register_code; ?></b></p>
  <p>Name: <b><?php echo $courseRegister->register_firstname.' '.$courseRegister->register_lastname; ?></b></p>
  <p>Email: <b><?php echo $courseRegister->register_email; ?></b></p>
  <p>Mobile: <b><?php echo $courseRegister->register_mobile; ?></b></p>
  <p>Company: <b><?php echo $courseRegister->register_company; ?></b></p>
  <hr />
  <?php } ?>
  <p style="color:#999;">This email has been send automatically from ntc.trainingcenter.co.th</p>
</div>
