<aside class="col-lg-3 col-md-4 col-sm-4">
  <div class="box_style_1">
  	<h4>Categories</h4>
      <ul class="submenu-col">
        <?php
        $courseUrlNoCat = str_replace('&catid='.$catid,'',$courseUrl);
        $courseUrlNoCat = str_replace('type=lastest','v=1',$courseUrlNoCat);
        $courseUrlNoCat = str_replace('&sort=DESC&order=create_date','',$courseUrlNoCat);
        $parentId = 0;
        if($catid != 0){
          $courseCategory = $this->CourseCategoryModel->getData($catid);
          if($courseCategory){
            if($courseCategory->parent_id != 0){
              $parentId = $courseCategory->parent_id;
            }else{
              $parentId = $catid;
            }
          }
        }
        echo '<li style="font-size:18px;">'.anchor($courseUrlNoCat, 'All Courses', array('id'=>'active')).'</li>';
        $rootCategoryList = $this->CourseCategoryModel->getList(0);
        foreach($rootCategoryList->result() as $rootCategory){
          $strId = ($parentId == $rootCategory->course_category_id)?'active':'';
          $courseCatUrl = $courseUrlNoCat.'&catid='.$rootCategory->course_category_id;
          echo '<li>'.anchor($courseCatUrl, $this->CourseCategoryModel->getName($rootCategory->course_category_id, $lang), array('id'=>$strId)).'</li>';
          if($parentId == $rootCategory->course_category_id){
            $courseCategoryList = $this->CourseCategoryModel->getList($parentId);
            foreach($courseCategoryList->result() as $courseCategory){
              $activeClass = ($courseCategory->course_category_id == $catid)?'active':'';
              $courseCatUrl = $courseUrlNoCat.'&catid='.$courseCategory->course_category_id;
              echo '<li  style="padding-left:10px;">'.anchor($courseCatUrl, $this->CourseCategoryModel->getName($courseCategory->course_category_id, $lang), array('class'=>$activeClass)).'</li>';
            }
          }
        }
        ?>
      </ul>
  </div>
</aside>
