<div class="fix-content-width" style="background-color: #f9f9f9;">
    <section>
        <img src="<?php echo base_url('uploads/'.$course->cover_image); ?>" class="fit" />
    </section>
    <?php echo form_open('course/register_post/'.$courseId); ?>
    <section id="main_content">
        <div class="container">
            <ol class="breadcrumb">
                <li><?php echo anchor('home', 'Home'); ?></li>
                <li><?php echo anchor('course', 'Courses'); ?></li>
                <li><?php echo anchor('course/detail/'.$course->course_id, $course->course_name); ?></li>
                <li class="active">Register</li>
            </ol>
            <div class="row">
              <div class="col-md-offset-2 col-md-8">
                <h2><?php echo $course->course_name; ?></h2>
                <?php if($discount_error != ''){ ?>
                  <div id="message-contact" style="display: block;"><div class="error_message"><?php echo $discount_error; ?></div></div>
                <?php } ?>
                <hr>
                <?php
                $this->load->view('course/register/tickettable');
                ?>
              </div>
            </div><!-- End row -->
        </div><!-- End container -->
    </section>
    <section id="main_content">
      <div class="container">
        <div class="row">
          <div class="col-md-offset-2 col-md-8">
            <?php
            $this->load->view('course/register/vatform');
            ?>
          </div>
        </div><!-- End row -->
      </div>
    </section>
    <section id="main_content_grey" style="margin-top:40px;">
      <div class="container">
        <div class="row">
          <div class="col-md-offset-2 col-md-8">
            <?php
            $this->load->view('course/register/buyerform');
            ?>
          </div>
        </div><!-- End row -->
      </div>
    </section>
    <section id="main_content">
      <div class="container">
        <div class="row">
          <div class="col-md-offset-2 col-md-8">
            <?php
            $this->load->view('course/register/ticketform');
            ?>
          </div>
        </div><!-- End row -->
      </div>
    </section>
    <section id="main_content_grey" style="margin-top:40px;">
      <div class="container">
        <div class="row">
          <div class="col-md-offset-2 col-md-8">
            <?php
            $this->load->view('course/register/paymentform');
            ?>
            <button type="submit" class="button_fullwidth" style="font-size:18px; margin-left:auto; margin-right:auto; width:180px; display:block; padding:15px; margin-bottom:40px;">Confirm</button>
          </div>
        </div><!-- End row -->
      </div>
    </section>
    <?php echo form_close(); ?>
</div>
