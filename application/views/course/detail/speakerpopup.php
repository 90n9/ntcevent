<div class="modal fade" id="speakerModal" tabindex="-1" role="dialog" aria-labelledby="speakerModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div>
          <div class="pull-left">
            <img src="<?php echo base_url('uploads/'.$speaker->speaker_image); ?>" class="img-circle" style="max-width:80px;" alt="">
          </div>
          <div style="margin-left:100px;">
              <h4 class="modal-title" id="speakerModalLabel"><?php echo $speaker->speaker_name; ?></h4>
              <p style="margin:10px 0;"><?php echo $speaker->speaker_detail; ?></p>
          </div>
        </div>
        <div style="display:block;clear:both;"></div>
      </div>
    </div>
  </div>
</div>
