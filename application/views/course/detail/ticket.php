<?php echo form_open('course/register/'.$courseId, array('id'=>'form-sel-ticket')); ?>
<div class="table-responsive">
	<table class="table">
		<thead>
			<tr>
				<th><?php echo $this->lang->line('course_ticket_type'); ?></th>
				<th><?php echo $this->lang->line('course_unit_price'); ?></th>
				<th><?php echo $this->lang->line('course_quantity'); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($ticketList->result() as $ticket){ ?>
			<tr>
				<td><?php
					echo $ticket->ticket_name;
					if($ticket->is_show_description == 1){
						echo '<p>'.$ticket->ticket_description.'</p>';
					}
				?></td>
				<td><?php
				if($ticket->price_type == 'free'){
					echo 'Free';
				}elseif($ticket->price_type == 'contact'){
					echo 'Contact Us';
				}else{
					echo number_format($ticket->price);
				}
				?></td>
				<td>
					<input type="hidden" name="ticketId[]" value="<?php echo $ticket->ticket_id; ?>" />
					<select name="qty[]">
						<?php
              $max_buy = ($ticket->ticket_qty > 5)?5:$ticket->ticket_qty;
							for($i = 1; $i<= $max_buy; $i++){
								echo '<option>'.$i.'</option>';
							}
						?>
					</select>
				</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
<?php
if($this->CourseModel->hasCoupon($courseId)){
?>
<div class="row">
	<label class="col-md-2 text-right" style="padding-top:10px;">Promo code :</label>
	<div class="col-md-6">
		<input type="text" class="form-control" name="txtDiscountCode" id="txtDiscountCode" placeholder="Insert promo code" />
	</div>
</div>
<?php }else{ ?>
	<input type="hidden" name="txtDiscountCode" />
<?php } ?>
<button type="submit" class="button_fullwidth">Get Ticket</button>
<?php echo form_close(); ?>
