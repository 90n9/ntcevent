<?php
$ticket = $ticketList->row();
$member = array();
if($this->session->userdata('member_id')){
	$member = $this->MemberModel->getData($this->session->userdata('member_id'));
}
echo form_open('course/contact_post/'.$courseId, array('id'=>'form-contact'));
?><div>
	<h3 class="text-center" style="padding-bottom:25px;"><?php echo $this->lang->line('course_contact_request'); ?></h3>
	<!-- ขอใบเสนอราคา -->

  <div class="row">
    <div class="col-md-6">
      <label for="txtFirstname"><?php echo $this->lang->line('course_contact_request_firstname'); ?>*</label>
      <div class="form-group">
        <input type="text" name="txtFirstname" id="txtFirstname" class="required form-control" value="<?php echo ($member)?$member->firstname:''; ?>" required>
        <span class="input-icon"><i class="icon-user"></i></span>
      </div>
    </div>
    <div class="col-md-6">
      <label for="txtLastname"><?php echo $this->lang->line('course_contact_request_lastname'); ?>*</label>
      <div class="form-group">
        <input type="text" name="txtLastname" id="txtLastname" class="required form-control" value="<?php echo ($member)?$member->lastname:''; ?>" required>
        <span class="input-icon"><i class="icon-user"></i></span>
      </div>
    </div>
  </div>
	<label for="txtEmail">Email*</label>
	<div class="form-group">
		<input type="email" name="txtEmail" id="txtEmail" class="required form-control" value="<?php echo ($member)?$member->email:''; ?>" required>
		<span class="input-icon"><i class="icon-email"></i></span>
	</div>
  <div class="row">
    <div class="col-md-6">
    	<label for="txtPhone"><?php echo $this->lang->line('course_contact_request_phone'); ?></label>
    	<div class="form-group">
    		<input type="text" name="txtPhone" id="txtPhone" class="form-control">
    		<span class="input-icon"><i class="icon-phone"></i></span>
    	</div>
    </div>
    <div class="col-md-6">
    	<label for="txtMobile"><?php echo $this->lang->line('course_contact_request_mobile'); ?>*</label>
    	<div class="form-group">
    		<input type="text" name="txtMobile" id="txtMobile" class="required form-control" value="<?php echo ($member)?$member->mobile:''; ?>" required>
    		<span class="input-icon"><i class="icon-mobile" style="font-size:24px;"></i></span>
    	</div>
    </div>
  </div>
	<div class="row">
		<div class="col-sm-8">
			<label for="txtCompany"><?php echo $this->lang->line('course_contact_request_company'); ?>*</label>
	    <div class="form-group">
	        <input type="text" name="txtCompany" id="txtCompany" class="required form-control" value="<?php echo ($member)?$member->company:''; ?>" required>
	        <span class="input-icon"><i class="icon-building"></i></span>
	    </div>
		</div>
		<div class="col-sm-4">
			<label><?php echo $this->lang->line('course_contact_request_attendee'); ?>*</label>
			<div class="form-group">
				<input type="text" class="required form-control" name="ddlTotalTicket" id="ddlTotalTicket" value="1" min="1">
			</div>
		</div>
	</div>
	<label for="txtNote"><?php echo $this->lang->line('course_contact_request_message'); ?></label>
    <div class="form-group">
        <textarea name="txtNote" id="txtNote" class="form-control" style="height:80px;"></textarea>
    </div>
	<button type="submit" class="button_fullwidth">Send Request</button>
</div>
<?php
echo form_close();
