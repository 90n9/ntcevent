<div class="box_style_1" style="border: none;box-shadow: none;">
    <h4>Download files</h4>
    <?php foreach($documentList->result() as $document){ ?>
        <div class="media">
            <div class="media-body">
            <?php
            echo '<h5 class="media-heading"><a href="'.base_url('uploads/'.$document->document_file).'" target="_blank"><i class="icon-download"></i> '.$document->document_name.'</a></h5>'
            ?>
            </div>
        </div>
    <?php } ?>
    <br>
    <?php
    if($course->speaker_type != 'none' && $course->speaker_id > 0){
        $speaker = $this->SpeakerModel->getData($course->speaker_id);
        if($speaker){
      ?>
      <h4><?php echo ($course->speaker_type == 'speaker')?'Speaker':'Instructor'; ?></h4>
      <div class="media">
          <div class="pull-left">
              <a href="#" data-toggle="modal" data-target="#speakerModal"><img src="<?php echo base_url('uploads/'.$speaker->speaker_image); ?>" class="img-circle" style="max-width:80px;" alt=""></a>
          </div>
          <div class="media-body" style="padding-left:22px;">
              <h5 class="media-heading" style="font-size:15px;"><a href="#" data-toggle="modal" data-target="#speakerModal"><?php echo $speaker->speaker_name; ?></a></h5>
              <p><?php echo $speaker->speaker_short_detail; ?></p>
          </div>
      </div>
        <?php
        $this->load->view('course/detail/speakerpopup', array('speaker'=>$speaker));
      }
    }
    ?>
</div>

<div class="box_style_1" style="border: none;box-shadow: none;margin-bottom:0;">
  <h4>Recommended Courses</h4>
</div>
<div style="color:black;padding: 0 25px 5px 25px;">
  <style>
  .thumb-relatecourse a{
    color:#fff;
  }
  .thumb-relatecourse a:hover{
    color:#ff5400;
  }
  </style>
  <?php
  $relateCourse = $this->RelateCourseModel->getRelateList($course->course_id, $course->course_category_id);
  foreach($relateCourse['sameCatList']->result() as $course){
    $this->load->view('course/thumbcontainer2',array('course'=>$course));
  }
  if($relateCourse['sameCatList']->num_rows() < 3){
    foreach($relateCourse['otherCatList']->result() as $course){
      $this->load->view('course/thumbcontainer2',array('course'=>$course));
    }
  }
  ?>
</div>
