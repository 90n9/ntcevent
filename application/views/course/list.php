<style>
body{
  display:none;
}
</style>
<?php $this->load->view('home/container/formsearch', array('showBar'=>false)); ?>
<section id="main_content" class="fix-content-width">
  <div class="container">
    <ol class="breadcrumb">
      <?php
      echo '<li>'.anchor('', $this->lang->line('main_menu_home')).'</li>';
      echo '<li '.(($type=='' && $catid==0 )?'class="active"':'').'>'.anchor('course',$this->lang->line('main_menu_course')).'</li>';
      if($type != ''){
        echo '<li class="active">'.(($type == 'upcoming')?'UPCOMING COURSES':'LATEST COURSES').'</li>';
      }
      if($catid != 0){
        echo '<li class="active">'.$this->CourseCategoryModel->getName($catid, $lang).'</li>';
      }
      ?>
    </ol>
    <div class="row">
      <?php $this->load->view('course/list/sidesearch'); ?>
      <div class="col-lg-9 col-md-8 col-sm-8">
        <div class="row">
        <?php
          if($courseList->num_rows() == 0 && $courseCloseList->num_rows() == 0){
            echo '<div class="text-center" style="font-size:14px;color:#999;padding:20px 0;">
            <h4>NO SALES COURSE</h4>
            <p>We have no open courses for this category at this time.<br />
            Come back at check us out again. More interesting courses are coming soon!</p>
            </div>';
          }
          foreach($courseList->result() as $course){
            echo '<div class="col-lg-4 col-sm-6">';
            $this->load->view('course/thumbcontainer',array('course'=>$course));
            echo '</div>';
          }
          if($courseList->num_rows() < $perpage){
            foreach($courseCloseList->result() as $course){
              echo '<div class="col-lg-4 col-sm-6">';
              $this->load->view('course/thumbcontainer',array('course'=>$course));
              echo '</div>';
            }
          }
          ?>
        </div><!-- End row -->
      </div><!-- End col-lg-9-->
    </div><!-- End row -->
    <hr>
    <?php
    $totalPage = ceil($courseCount/$perpage);
    if($courseCount > $perpage){
        ?>
    <div class="row">
    	<div class="col-md-12 text-center">
        	<ul class="pagination">
                <?php
                if($page > 1){
                    $prevPage = $page-1;
                    echo '<li>'.anchor($courseUrl.'&page='.$prevPage,'&laquo;').'</li>';
                }
                $startListPage = $page-2;
                $startListPage = ($startListPage < 1)?1:$startListPage;
                $endListPage = $page+2;
                $endListPage = ($endListPage > $totalPage)?$totalPage:$endListPage;
                for($i = $startListPage; $i <= $endListPage; $i++){
                    echo '<li>'.anchor($courseUrl.'&page='.$i, $i).'</li>';
                }
                if($page < $totalPage){
                    $nextPage = $page+1;
                    echo '<li>'.anchor($courseUrl.'&page='.$nextPage,'&raquo;').'</li>';
                }
                ?>
            </ul>
        </div>
    </div>
    <?php } ?>
    </div><!-- End container -->
</section><!-- End main_content -->
<?php
$this->load->view('course/locationallpopup');
