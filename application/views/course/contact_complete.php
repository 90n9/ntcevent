<div class="fix-content-width">
    <section>
        <img src="<?php echo base_url('uploads/'.$course->cover_image); ?>" class="fit" />
    </section>
    <section id="main_content">
        <div class="container">
            <ol class="breadcrumb">
                <li><?php echo anchor('home', 'Home'); ?></li>
                <li><?php echo anchor('course', 'Courses'); ?></li>
                <li class="active"><?php echo $course->course_name; ?></li>
            </ol>
            <div class="text-center">
                <h2 style="font-size:26px; font-wieght:bold; color:#f54700; padding-bottom:0;" >Your request has been sent.</h2>
                <div style="font-size:24px; padding-top:10px;">Thank you for your interest.</div>
                <div style="line-height:25px;padding-bottom:40px">
                  <div style="padding-top:40px;">We will reply by email shortly.</div>
                  <div>If you have more questions, please feel free to contact us.</div>
                </div>
                <div><?php echo anchor('course?type=upcoming','EXPLORE MORE COURSES', array('class'=>'button_outline')); ?></div>
            </div>
        </div><!-- End container -->
    </section>
</div>
