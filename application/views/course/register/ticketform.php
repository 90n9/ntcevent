<?php
$totalTicket = count($arrTicketId);
$ticketCounter = 0;
for($i = 0; $i<$totalTicket; $i++){
    if($arrQty[$i] > 0){
        $ticket = $this->TicketModel->getData($arrTicketId[$i]);
        for($j = 0; $j<$arrQty[$i]; $j++){
            $ticketCounter++;
            ?>
            <div>
                <h3 class="text-center"><?php echo $this->lang->line('course_ticket_form_header'); ?> <?php echo $ticketCounter; ?></h3>
                <?php if($ticketCounter == 1){ ?>
                <div class="form-group">
                    <label><input class="chkSame" type="checkbox" name="chkSame[]" <?php echo ($ticketCounter == 1)?'checked':''; ?>/> <?php echo $this->lang->line('course_ticket_form_same_buyer'); ?></label>
                </div>
                <?php } ?>
                <div class="divFormRegister"  <?php echo ($ticketCounter == 1)?'style="display:none;"':''; ?>>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <input type="hidden" name="ticketId[]" value="<?php echo $ticket->ticket_id; ?>">
                        <input type="text" name="txtRegisterFirstname[]" class="required form-control txtRegisterFirstname" placeholder="* First Name" value="<?php echo ($ticketCounter == 1)?$member->firstname:''; ?>" required>
                        <span class="input-icon"><i class="icon-user"></i></span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <input type="text" name="txtRegisterLastname[]" class="required form-control txtRegisterLastname" placeholder="* Last Name" value="<?php echo ($ticketCounter == 1)?$member->lastname:''; ?>" required>
                        <span class="input-icon"><i class="icon-user"></i></span>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <input type="text" name="txtRegisterEmail[]" class="required form-control txtRegisterEmail" placeholder="* Email" value="<?php echo ($ticketCounter == 1)?$member->email:''; ?>" required>
                        <span class="input-icon"><i class="icon-email"></i></span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <input type="text" name="txtRegisterMobile[]" class="required form-control txtRegisterMobile" placeholder="* Mobile Phone Number" value="<?php echo ($ticketCounter == 1)?$member->mobile:''; ?>" required>
                        <span class="input-icon"><i class="icon-mobile" style="font-size:24px;"></i></span>
                    </div>
                  </div>
                </div>

                    <div class="form-group">
                        <input type="text" name="txtRegisterCompany[]" class="required form-control txtRegisterCompany" placeholder="* Company Name" value="<?php echo ($ticketCounter == 1)?$member->company:''; ?>" required>
                        <span class="input-icon"><i class="icon-building"></i></span>
                    </div>
                </div>
            </div>
            <?php
        }
    }
}
?>
