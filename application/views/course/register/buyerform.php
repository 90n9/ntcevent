<div>
	<h3 class="text-center"><?php echo $this->lang->line('course_ticket_buyer_information'); ?></h3>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
					<input type="text" name="txtFirstname" id="txtFirstname" class="required form-control buyerInp" placeholder="* First Name" value="<?php echo ($member)?$member->firstname:''; ?>" required>
				<span class="input-icon"><i class="icon-user"></i></span>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<input type="text" name="txtLastname" id="txtLastname" class="required form-control buyerInp" placeholder="* Last Name" value="<?php echo ($member)?$member->lastname:''; ?>" required>
				<span class="input-icon"><i class="icon-user"></i></span>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<input type="text" name="txtEmail" id="txtEmail" class="required form-control buyerInp" placeholder="* Email" value="<?php echo ($member)?$member->email:''; ?>" required>
				<span class="input-icon"><i class="icon-email"></i></span>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<input type="text" name="txtMobile" id="txtMobile" class="required form-control buyerInp" placeholder="* Mobile Phone Number" value="<?php echo ($member)?$member->mobile:''; ?>" required>
				<span class="input-icon"><i class="icon-mobile" style="font-size:24px;"></i></span>
			</div>
		</div>
	</div>
	<div class="form-group">
		<input type="text" name="txtCompany" id="txtCompany" class="required form-control buyerInp" placeholder="* Company Name" value="<?php echo ($member)?$member->company:''; ?>" required>
		<span class="input-icon"><i class="icon-building"></i></span>
	</div>
</div>
