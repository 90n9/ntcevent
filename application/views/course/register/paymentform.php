<div class="zero-payment">
	<h3 class="text-center"><?php echo $this->lang->line('course_payment_header'); ?></h3>
	<div class="form-group" style="font-size=15px;">
		<label><?php echo $this->lang->line('course_choose_payment'); ?></label>
		<div>
			<label style="font-weight:normal; padding-left:25px;">
				<input type="radio" name="paymentChannel" value="bank" checked="checked" />
				<?php echo $this->lang->line('course_payment_bank_transfer'); ?>
			</label>
		</div>
		<div>
			<label style="font-weight:normal; padding-left:25px;">
				<input type="radio" name="paymentChannel" value="creditcard" />
				<?php echo $this->lang->line('course_payment_creditcard'); ?>
			</label>
		</div>
	</div>
	<div id="bankForm" class="transferForm box_style_1" style="background-color:transparent;color:#2d4050;position:relative;">
		<span class="tape"></span>

		<h3><?php echo $this->lang->line('course_bank_header'); ?></h3>
		<div class="row">
			<div class="col-md-4"><img src="<?php echo base_url('assets/img/kbank_logo.jpg'); ?>" class="img-responsive" alt="KBank" /></div>
			<div class="col-md-8">
				<div style="padding-top:10px; padding-left:90px; font-size:15px;"><b style="display:inline-block; width:85px;"><?php echo $this->lang->line('course_bank_account_name'); ?> </b><?php echo $this->lang->line('course_bank_account_name_value'); ?></div>
				<div style="padding-left:90px; font-size:15px;"><b style="display:inline-block; width:85px;"><?php echo $this->lang->line('course_bank_account_number'); ?> </b>001-1-30904-6</div>
			</div>
		</div>
    <br />
	</div>
	<div id="creditcardForm" class="transferForm box_style_1" style="display:none; background-color:transparent;color:#2d4050;position:relative;">
		<span class="tape"></span>

		<h3><?php echo $this->lang->line('course_creditcard_header'); ?></h3>
		<div class="row">
			<div class="col-md-5"><img src="<?php echo base_url('assets/img/payment/creditcard.jpg'); ?>" class="img-responsive" alt="Credit Card" /></div>
			<div class="col-md-7" style="padding-top:10px;">
				<div class="row">
					<label class="col-sm-6" style="font-weight:normal;">Subtotal</label>
					<p class="col-sm-6 text-right"><span class="subTotalAmount">0.00</span> <?php echo $this->lang->line('baht'); ?></p>
				</div>
				<div class="row">
					<label class="col-sm-6" style="font-weight:normal;"><?php echo $this->lang->line('course_creditcard_surcharges'); ?> 3%</label>
					<p class="col-sm-6 text-right"><span class="creditcardFeeAmount">0.00</span> <?php echo $this->lang->line('baht'); ?></p>
				</div>
				<div class="row">
					<label class="col-sm-6" style="font-weight:normal;">VAT 7%</label>
					<p class="col-sm-6 text-right"><span class="creditcardVatAmount">0.00</span> <?php echo $this->lang->line('baht'); ?></p>
				</div>
				<div class="row" style="color:#f54700;font-size:15px;font-weight:bold;">
					<label class="col-sm-6"><?php echo $this->lang->line('course_grand_total'); ?></label>
					<p class="col-sm-6 text-right"><span class="creditcardAmount">0.00</span> <?php echo $this->lang->line('baht'); ?></p>
				</div>
				<div class="text-right">
					<span><img src="<?php echo base_url('assets/img/payment/ico_visa.jpg'); ?>" alt="Visa" /></span>
					<span><img src="<?php echo base_url('assets/img/payment/ico_mastercard.jpg'); ?>" alt="Mastercard" /></span>
				</div>
			</div>
		</div>
	</div>
</div>
