<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th><?php echo $this->lang->line('course_ticket_type'); ?></th>
                <th style="text-align:right;"><?php echo $this->lang->line('course_unit_price'); ?></th>
                <th style="text-align:center;"><?php echo $this->lang->line('course_quantity'); ?></th>
                <th style="text-align:right;"><?php echo $this->lang->line('course_total_baht'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $totalTicket = count($arrTicketId);
            $totalPrice = 0;
            for($i = 0; $i<$totalTicket; $i++){
                if($arrQty[$i] > 0){
                    $ticket = $this->TicketModel->getData($arrTicketId[$i]);
                    $subPrice = 0;
                    echo '<tr>';
                    echo '<td>'.$ticket->ticket_name.'</td>';
                    echo '<td style="text-align:right;">';
                    if($ticket->price_type == 'free'){
                        echo 'Free';
                        $subPrice = 'Free';
                    }elseif($ticket->price_type == 'contact'){
                        echo 'Contact';
                        $subPrice = 'Contact';
                    }else{
                        echo number_format($ticket->price, 2);
                        $subPrice = $ticket->price * $arrQty[$i];
                        $totalPrice += $subPrice;
                        $subPrice = number_format($subPrice, 2);
                    }
                    echo '</td>';
                    echo '<td style="text-align:center;">'.$arrQty[$i].'</td>';
                    echo '<td style="text-align:right;">'.$subPrice.'</td>';
                    echo '</tr>';
                }
            }
            ?>
        </tbody>
        <tfoot>
            <?php
              $discountPrice = 0;
              if($totalPrice > 0){
                if($discount){
                  if($discount->discount_type == 'percent'){
                    $discountPrice = ($totalPrice * $discount->discount_amount) / 100;
                  }else{
                    $discountPrice = $discount->discount_amount;
                  }
                  ?>
                  <tr style="color: red;">
                      <td style="text-align:right;" colspan="3"><?php echo $this->lang->line('course_discount').' ('. strtoupper($discountCode).')'; ?></td>
                      <td style="text-align:right;font-weight:bold;"><?php echo number_format($discountPrice, 2); ?></td>
                  </tr>
                  <?php
                }
                $subTotalPrice = $totalPrice - $discountPrice;
                $surchargeAmount = $subTotalPrice * $kbankFee / 100;
                ?>
            <tr id="trSubTotalPrice" <?php echo ($discount)?'':'style="display:none;"'; ?>>
                <td style="text-align:right;" colspan="3"><?php echo $this->lang->line('course_sub_total'); ?></td>
                <td style="text-align:right;font-weight:bold;"><span id="subTotalPrice" data-amount="<?php echo $subTotalPrice; ?>"><?php echo number_format($subTotalPrice, 2); ?></span></td>
            </tr>
            <tr id="surchargeRow" style="display:none;">
                <td style="text-align:right;" colspan="3"><?php echo $this->lang->line('course_creditcard_surcharges').' '.$kbankFee; ?>%</td>
                <td style="text-align:right;font-weight:bold;"><span id="surchargeAmount"><?php echo number_format($surchargeAmount, 2); ?></span>
                  <input type="hidden" name="kbank_fee" id="kbank_fee" value="<?php echo $kbankFee; ?>" />
                </td>
            </tr>
            <tr>
                <td style="text-align:right;" colspan="3"><?php echo $this->lang->line('course_vat'); ?></td>
                <td style="text-align:right;font-weight:bold;"><span id="vatPrice"><?php echo number_format($subTotalPrice * 0.07, 2); ?></span></td>
            </tr>
            <tr style="color:#f54700;font-size:1.2em;">
                <td style="text-align:right;font-weight:bold" colspan="3"><?php echo $this->lang->line('course_grand_total'); ?></td>
                <td style="text-align:right;font-weight:bold;"><span id="paidPrice"><?php echo number_format($subTotalPrice * 1.07, 2); ?></span></td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <td ></td>
            </tr>
            <?php }else{ ?>
            <tr>
                <td colspan="3"></td>
                <td ><span id="subTotalPrice" data-amount="<?php echo $totalPrice; ?>"></span></td>
            </tr>
            <?php } ?>
        </tfoot>
    </table>
</div>
<span id="discountPrice" data-amount="<?php echo $discountPrice; ?>"></span>
<input type="hidden" name="txtDiscountCode" id="txtDiscountCode" value="<?php echo $discountCode; ?>" />
