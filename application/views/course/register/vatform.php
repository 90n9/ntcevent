<div class="zero-payment">
  <div class="form-group">
    <label><input type="checkbox" id="chkCompany" name="chkCompany" value="1" <?php echo ($member && $member->is_company == 1)?'checked="checked"':''; ?> /> <?php echo $this->lang->line('course_billing_for_company'); ?></label>
  </div>
  <div class="box_style_1" style="background-color:transparent;color:#2d4050;">
    <span class="tape" style="
    top: 30px;
"></span>
    <h3 class="text-center" id="header-vat-form"
    data-company-html="<?php echo $this->lang->line('course_company_information'); ?>"
    data-individual-html="<?php echo $this->lang->line('course_individual_information'); ?>"
    ><?php echo $this->lang->line('course_company_information'); ?></h3>
    <p><?php echo $this->lang->line('course_company_note'); ?></p>
    <div id="companyForm">
      <div class="form-group">
      	<input type="text" name="txtVatCompany" id="txtVatCompany" class="form-control" placeholder="* Company Name" value="<?php echo ($member)?$member->company_name:''; ?>">
      	<span class="input-icon"><i class="icon-building"></i></span>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <input type="text" name="txtVatBranch" id="txtVatBranch" class="required form-control" placeholder="* Branch" value="<?php echo ($member)?$member->company_branch:''; ?>">
            <span class="input-icon"><i class="icon-building"></i></span>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <input type="text" name="txtVatTaxNo" id="txtVatTaxNo" class="required form-control" placeholder="* Tax ID" value="<?php echo ($member)?$member->company_tax:''; ?>">
            <span class="input-icon"><i class="icon-vcard"></i></span>
          </div>
        </div>
      </div>
    </div>
    <div class="form-group">
      <textarea name="txtVatAddress" id="txtVatAddress" class="required form-control" placeholder="* Billing Address" style="height:60px;" required><?php echo ($member)?$member->company_address:''; ?></textarea>
    	<span class="input-icon"><i class="icon-home"></i></span>
    </div>
  </div>
</div>
