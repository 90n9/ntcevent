<div class="fix-content-width">
    <section>
        <img src="<?php echo base_url('uploads/'.$course->cover_image); ?>" class="fit" />
    </section>
    <section id="main_content">
        <div class="container">
            <ol class="breadcrumb">
                <li><?php echo anchor('home', 'Home'); ?></li>
                <li><?php echo anchor('course', 'Courses'); ?></li>
                <li class="active"><?php echo $course->course_name; ?></li>
            </ol>
            <?php if($courseBuyer->paid_amount > 0){ ?>
            <div class="text-center" style="padding-top:30px;">
                <h2 style="font-size:26px; font-wieght:bold; color:#f54700; padding-bottom:0;" >Your Registration is complete. </h2>
                <div style="font-size:24px; padding-top:10px;">Thank you for your order!</div>
              <div style="line-height:25px;">
                <div style="padding-top:40px;">Order ID #<span style="font-weight:bold; font-size:1.2em;"><?php echo $courseBuyer->course_buyer_code; ?></span></div>
                <div>Bank account information has been sent to <b><?php echo $this->session->userdata('email'); ?></b>.</div>
                <div>Please make a payment within 7 days to complete your order.</div>
              </div>

              <div class="box_style_1" style="background-color: transparent; color: #2d4050; position: relative; max-width: 470px;
              margin: auto; margin-top: 30px; margin-bottom: 30px; padding-top: 30px; padding-bottom: 20px;">
                <span class="tape"></span>
                <div class="row">
          			  <div class="col-md-2"><img src="<?php echo base_url('assets/img/kbank_logo_small.jpg'); ?>" class="img-responsive" alt="KBank" /></div>
          			  <div class="col-md-10" style="text-align:left;">
          				  <div style="padding-top:5px; font-size:15px;"><b style="display:inline-block; width:85px;"><?php echo $this->lang->line('course_bank_account_name'); ?> </b><?php echo $this->lang->line('course_bank_account_name_value'); ?></div>
          				  <div style="font-size:15px;"><b style="display:inline-block; width:85px;"><?php echo $this->lang->line('course_bank_account_number'); ?> </b>001-1-30904-6</div>
          			  </div>
          		  </div>
              </div>

                <div>Share <b><?php echo $course->course_name; ?></b></div>
                <div>
                  <?php
                  $encodeCourseName = urlencode($course->course_name);
                  $encodeUrl = urlencode(site_url('course/detail/'.$course->course_id));
                  $this->load->view('course/sociallink', array('encodeCourseName'=>$encodeCourseName, 'encodeUrl'=>$encodeUrl));
                  ?>
                </div>
            </div>
            <div class="text-center" style="padding-top:20px;"><?php echo anchor('mycourse','Back to My Courses', array('class'=>'button_outline')); ?></div>
            <?php }else{ ?>
              <div class="text-center" style="padding-top:30px">
                  <h2 style="font-size:26px; font-wieght:bold; color:#f54700; padding-bottom:0;">Your registration is complete.</h2>
                  <div style="font-size:24px; padding-top:10px;">Thank you for your order!</div>

                  <div style="padding-top:40px;">Order ID #<span style="font-weight:bold; font-size:1.2em;"><?php echo $courseBuyer->course_buyer_code; ?></span></div>
                  <div style="padding-top:20px;">
                    <?php echo anchor('mycourse/printtickets/'.$courseBuyer->course_buyer_id, 'PRINT TICKETS', array('class'=>'button_outline', 'target'=>'_blank')); ?>
                  </div>
                  <hr />
                  <div>Event are better when your friends are there too.</div>
                  <div>Who is coming with you?</div>
                  <div>Share <?php echo $course->course_name; ?></div>
                  <div>
                    <?php
                    $encodeCourseName = urlencode($course->course_name);
                    $encodeUrl = urlencode(site_url('course/detail/'.$course->course_id));
                    $this->load->view('course/sociallink', array('encodeCourseName'=>$encodeCourseName, 'encodeUrl'=>$encodeUrl));
                    ?>
                  </div>

              </div>
            <?php } ?>
        </div><!-- End container -->
    </section>
</div>
