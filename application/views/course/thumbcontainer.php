<?php
$detailUrl = 'course/detail/'.$course->course_id;
$categoryUrl = 'course?catid='.$course->course_category_id;
$wishlistStatus = $this->WishlistModel->getStatus($course->course_id);
$displayPrice = '';
$discountPrice = '';
switch($course->course_price_type){
  case 'contact':
    $displayPrice = 'Contact us';
    break;
  case 'free':
    $discountPrice = 'Free of charge';
    break;
  case 'amount':
    if($course->course_before_discount_price > 0){
      $displayPrice = number_format($course->course_before_discount_price).' '.$this->lang->line('baht');
      $discountPrice = number_format($course->course_price).' '.$this->lang->line('baht');
    }else{
      $displayPrice = number_format($course->course_price).' '.$this->lang->line('baht');
    }
    break;
}
?>
<div class="col-item">
    <div class="photo">
        <?php echo anchor($detailUrl, '<img src="'.base_url('uploads/'.$course->thumb_image).'" alt="" />'); ?>
        <div class="cat_row">
            <?php
            echo anchor($categoryUrl, $this->CourseCategoryModel->getName($course->course_category_id, $lang));
            if($course->course_start <= date("Y-m-d")){
                echo '<span class="pull-right" style="background-color: black;">CLOSE</span>';
            }elseif($course->course_full_status == 'full'){
                echo '<span class="pull-right" style="background-color: red;">FULL</span>';
            }elseif($course->course_full_status == 'almost'){
                echo '<span class="pull-right" style="background-color: orange;">ALMOST FULL</span>';
            }
            ?>
        </div>
    </div>
    <div class="info">
        <div class="row">
            <div class="course_info col-md-12 col-sm-12">
                <h4 style="margin:0;margin-bottom:10px;"><?php echo anchor($detailUrl, ''.$course->course_name.''); ?></h4>
                <p style="margin-bottom:0"><?php echo $this->DateTimeService->displayDateTime($course->course_start, $lang); ?> (</i><?php echo $course->course_length; ?> Days</span>)</p>
                <p style="margin-bottom:0">
                  <a href="#" data-toggle="modal" data-target="#locationModal<?php echo $course->course_location_id; ?>" style="height:auto;">
                    <i class="icon-location-1"></i> <?php echo $this->CourseLocationModel->getName($course->course_location_id, $lang); ?>
                  </a>
                </p>
                <div class="rating"><span><?php echo $course->course_lang; ?></span></div>
                <div class="price pull-right">
                    <div <?php echo ($discountPrice != '')?'style="text-decoration:line-through;font-size:0.9em;"':'style="color:#2d4050"'; ?>><?php echo $displayPrice; ?></div>
                    <div style="color:red;"><?php echo $discountPrice; ?></div>
                </div>
            </div>
        </div>
        <div class="separator clearfix">
            <p class="btn-details" style="width:100%;"><?php echo anchor($detailUrl,'<i class=" icon-list"></i> Details');?></p>
            <div class="btn-bookmark <?php echo ($wishlistStatus)?'active':''; ?>" data-courseid="<?php echo $course->course_id; ?>" data-wishliststatus="<?php echo ($wishlistStatus)?1:0; ?>" data-url="<?php echo site_url('wishlist/') ?>"><i class="icon-heart"></i></div>
        </div>
    </div>
</div>
