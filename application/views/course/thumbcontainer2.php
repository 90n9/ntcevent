<?php
$detailUrl = 'course/detail/'.$course->course_id;
$categoryUrl = 'course?catid='.$course->course_category_id;
$wishlistStatus = $this->WishlistModel->getStatus($course->course_id);
$displayPrice = '';
$discountPrice = '';
switch($course->course_price_type){
  case 'contact':
    $displayPrice = 'Contact us';
    break;
  case 'free':
    $discountPrice = 'Free of charge';
    break;
  case 'amount':
    if($course->course_before_discount_price > 0){
      $displayPrice = number_format($course->course_before_discount_price).' '.$this->lang->line('baht');
      $discountPrice = number_format($course->course_price).' '.$this->lang->line('baht');
    }else{
      $displayPrice = number_format($course->course_price).' '.$this->lang->line('baht');
    }
    break;
}
?>
<div class="thumb-relatecourse" style="border:1px solid #333;margin-bottom:10px;padding-right:10px;color:#fff;">
  <div class="row">
    <div class="col-xs-4">
      <div class="photo">
        <?php echo anchor($detailUrl, '<img src="'.base_url('uploads/'.$course->thumb_image).'" style="width:100%;" alt="" />'); ?>
      </div>
    </div>
    <div class="col-xs-8">
      <div class="info" style="padding:10px 0;">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <h4 style="margin:0;margin-bottom:0;font-size:15px;overflow: hidden;height: 33px;"><?php echo anchor($detailUrl, ''.$course->course_name.''); ?></h4>
            <div class="price pull-right" style="font-weight:normal;margin-top:0;font-size:15px;margin-top:10px;">
              <span <?php echo ($discountPrice != '')?'style="color:#ddd;text-decoration:line-through;font-size:0.9em;display:none;"':'style="color:#ddd"'; ?>><?php echo $displayPrice; ?></span>
              <span style="color:red;"><?php echo $discountPrice; ?></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
