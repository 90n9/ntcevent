<div class="fix-content-width">
    <section>
        <img src="<?php echo base_url('uploads/'.$course->cover_image); ?>" class="fit" />
    </section>
    <section id="main_content" class="" style="padding:0;">
      <div class="row" style="background-color: #1c1c1c;padding:0;margin:0;">
        <div class="col-md-8" style="background-color: #ffffff;padding:40px 0;-webkit-box-shadow: -3px 0px 5px 0px rgba(0, 0, 0, 0.1), 3px 0px 5px 0px rgba(0, 0, 0, 0.1);-moz-box-shadow: -3px 0px 5px 0px rgba(0, 0, 0, 0.1), 3px 0px 5px 0px rgba(0, 0, 0, 0.1);box-shadow: 3px 0px 5px 0px rgba(0, 0, 0, 0.1), -3px 0px 5px 0px rgba(0, 0, 0, 0.1);margin:0;">
          <div style="padding:0 20px;">
            <ol class="breadcrumb">
                <li><?php echo anchor('home',$this->lang->line('main_menu_home')); ?></li>
                <li><?php echo anchor('course', $this->lang->line('main_menu_course')); ?></li>
                <li class="active"><?php echo $course->course_name; ?></li>
            </ol>
            <h2 style="padding-bottom:10px;"><?php echo $course->course_name; ?></h2>
            <p style="margin-bottom:0">
              <?php
              echo '<i class="icon-calendar"></i>';
              echo $this->DateTimeService->displayDateList($course->course_date, $lang);
              echo ' <i class="icon-clock"></i> ';
              echo $this->DateTimeService->displayTime($course->course_start_time, $lang).' - '.$this->DateTimeService->displayTime($course->course_end_time, $lang);
              ?>
              (</i><?php echo $course->course_length; ?> Days</span>)</p>
            <p style="margin-bottom:20px">
              <a href="#" data-toggle="modal" data-target="#locationModal<?php echo $course->course_location_id; ?>" style="height:auto;">
                <i class="icon-location-1"></i> <?php echo $this->CourseLocationModel->getName($course->course_location_id, $lang); ?>
              </a>
            </p>
            <div><?php echo $course->course_description; ?></div>
            <a name="buyticket"></a>
            <h4 style="padding-top:50px;font-size:14px;color:#666;">SHARE WITH FRIENDS</h4>
            <?php
            $encodeCourseName = urlencode($course->course_name);
            $encodeUrl = urlencode(site_url('course/detail/'.$course->course_id));
            $this->load->view('course/sociallink', array('encodeCourseName'=>$encodeCourseName, 'encodeUrl'=>$encodeUrl));
            ?>
            <hr>
            <?php
            if($ticketList->num_rows() == 0){
              echo '<div>'.$this->lang->line('no_ticket_found').'</div>';
            }elseif($ticketList->num_rows() == 1 && $ticketList->row()->price_type == 'contact'){
              $this->load->view('course/detail/contactprice');
            }else{
              $this->load->view('course/detail/ticket');
            }
            ?>
          </div>
        </div><!-- End col-md-8  -->
        <aside class="col-md-4" style="padding:40px 0;margin:0;"><?php $this->load->view('course/detail/sidecolumn'); ?></aside> <!-- End col-md-4 -->
      </div><!-- End row -->
    </section>
</div>
<?php
$this->load->view('course/locationallpopup');
