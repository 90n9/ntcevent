<!-- Location Modal -->
<div class="modal fade" id="locationModal<?php echo $courseLocation->course_location_id; ?>" tabindex="-1" role="dialog" aria-labelledby="locationModalLabel<?php echo $courseLocation->course_location_id; ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="locationModalLabel<?php echo $courseLocation->course_location_id; ?>" style="color:#ff5400;"><?php echo $courseLocation->course_location_name_en; ?></h4>
        <p style="margin-bottom:0;"><?php echo $courseLocation->course_location_detail_en; ?></p>
        <div class="google-map" data-lad="<?php echo $courseLocation->lad; ?>" data-lon="<?php echo $courseLocation->lon; ?>"></div>
      </div>
    </div>
  </div>
</div>
