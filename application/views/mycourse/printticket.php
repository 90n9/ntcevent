<div style="padding:0 20px;">
<?php
$this->load->view('mycourse/print/ticket_header');
$this->load->view('mycourse/print/courseinfo');
?>
</div>
<table width="100%">
  <tr>
    <td rowspan="2" width="40" style="padding-left:20px;padding-bottom:20px;">
      <img src="<?php echo base_url('assets/img/ico-cut.jpg'); ?>" width="20" />
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="seperate-table">
      PLEASE PRINT YOUR TICKET
    </td>
  </tr>
</table>
<div style="padding:0 20px;">
  <?php
  $this->load->view('mycourse/print/ticketinfo');
  ?>
  <div class="text_ps" style="padding-bottom:20px;font-style:italic;">* Please bring along your sweater or jacket, as it may be cold sometimes in training room.</div>
  <div style="padding:10px;background-color:#f5f5f5;border-radius:5px;color:#707070;">
    <h5 style="text-decoration:underline;margin:0 0 5px 0;">IMPORTANT:</h5>
    <div style="padding-left:5px;font-size:12px;">
      <div style="padding-bottom:3px;">[1] Network Training Center (NTC) reserves the right to reschedule or cancel any courses if necessary.</div>
      <div style="padding-bottom:3px;">[2] Cancellation or rescheduling must be made in writing at least 10 business days prior to the first day of the training.</div>
      <div style="padding-bottom:3px;">[3] No refund will be issued for no-shows.</div>
    </div>
  </div>
</div>
