<table class="border-table" width="100%" cellpadding="0" cellspacing="0" border="0">
  <tr>
    <td colspan="3" class="border-table-header"><?php echo $course->course_name; ?></td>
  </tr>
  <tr>
    <td colspan="2" style="padding:20px;padding-bottom:10px;position:relative;">
      <h2 style="font-weight:normal;"><?php echo $courseRegister->register_firstname.' '.$courseRegister->register_lastname; ?></h2>
    </td>
    <td rowspan="2" style="text-align:center;align:top;font-size:12px;" width="150">
      <div>
          <b>ORDER ID:</b><br /> #<?php echo $courseRegister->course_register_code; ?>
        </div>
      <img src="<?php echo base_url('qrimg.php'); ?>" class="border-1-px" style="margin:10px;" />
    </td>
  </tr>
  <tr>
    <td width="135" style="padding:10px 20px 25px 20px;">
      <?php
      if($course->thumb_image != ''){
        $imgUrl = base_url('uploads/'.$course->thumb_image);
        echo '<table style="border:1px solid #000;padding:0px;"><tr><td>';
        echo '<img src="'.$imgUrl.'" width="150" />';
        echo '</td></tr></table>';
      }
      ?>
    </td>
    <td style="padding-top:35px;vertical-align:top;">
      <div><span style="color: #f54700;">DATE :</span>
        <?php
        echo $this->DateTimeService->displayDateList($course->course_date, $lang);
        echo ' ';
        ?>
        (<?php echo $course->course_length; ?> Days</span>)
      </div>
      <div style="font-size:4px;"><br /></div>
      <div><span style="color: #f54700;">TIME :</span>
        <?php echo $this->DateTimeService->displayTime($course->course_start_time, $lang).' - '.$this->DateTimeService->displayTime($course->course_end_time, $lang); ?>
      </div>
      <div style="font-size:4px;"><br /></div>
      <div><span style="color: #f54700;">LOCATION : </span><?php echo $this->CourseLocationModel->getName($course->course_location_id, $lang); ?></div>
      <div style="font-size:6px;"><br /></div>
      <div style="font-size:14px;"><?php echo $this->CourseLocationModel->getDetail($course->course_location_id, $lang); ?></div>
    </td>
  </tr>
</table>
