<div style="color:#555555;padding:0 10px;font-size:16px;">
  <p>Dear <?php echo $courseRegister->register_firstname; ?>,</p>
  <p>Thank you for registering for the <b>"<?php echo $course->course_name; ?>"</b>. Please follow the details.</p>
</div>
<table width="100%" class="table-ticket-info" style="border-spacing:0;">
  <tr style="background-color:#fdeada;">
    <td class="content-header">COURSE:</td>
    <td><?php echo $course->course_name; ?></td>
  </tr>
  <tr>
    <td class="content-header">DATE:</td>
    <td><?php
    echo $this->DateTimeService->displayDateList($course->course_date, $lang);
    echo ' | ';
    echo $this->DateTimeService->displayTime($course->course_start_time, $lang).' - '.$this->DateTimeService->displayTime($course->course_end_time, $lang);
    ?>
    (</i><?php echo $course->course_length; ?> Days</span>)</td></tr>
  <tr style="background-color:#fdeada;">
    <td class="content-header">LOCATION:</td>
    <td><?php echo $this->CourseLocationModel->getName($course->course_location_id, $lang); ?></td>
  </tr>
  <tr>
    <td></td>
    <td><?php echo $this->CourseLocationModel->getDetail($course->course_location_id, $lang); ?></td>
  </tr>
</table>
<br />
<table>
  <tr>
    <td style="padding-right:10px;">
      <img src="<?php echo base_url('assets/img/socials/map.jpg'); ?>" width="470" />
    </td>
    <td width="270" style="border:1px dotted #555555; padding:15px 10px; border-radius:5px;font-size:14px;color:#555555;">
        If you have any questions or need more information, please feel free to contact us.
        <br />&nbsp;
        <table>
          <tr>
            <td style="font-size:14px;color:#555;">Tel:</td>
            <td style="font-size:14px;color:#555;">+66 (0) 2634 7993-4</td>
          </tr>
          <tr>
            <td style="font-size:14px;color:#555;padding-right:5px;">E-mail:</td>
            <td style="font-size:14px;color:#555;">support@trainingcenter.co.th</td>
          </tr>
        </table>
    </td>
  </tr>
</table>
