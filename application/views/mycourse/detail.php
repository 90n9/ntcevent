<section id="main_content" class="fix-content-width">
	<div class="container">
        <ol class="breadcrumb">
            <?php
            echo '<li>'.anchor('','หน้าแรก').'</li>';
            echo '<li>'.anchor('mycourse','คอร์สเรียนของฉัน').'</li>';
            echo '<li class="active">'.$title.'</li>';
            ?>
        </ol>
        <div class="row">
        	<?php if($courseBuyer){ ?>
        	<aside class="col-md-4">
	        	<div class=" box_style_1 profile" style="background:transparent;">
	                <ul>
	                    <li><?php echo $this->lang->line('mycourse_order_id'); ?> <strong class="pull-right"><?php echo $courseBuyer->course_buyer_code; ?></strong></li>
	                    <li><?php echo $this->lang->line('mycourse_status'); ?> <strong class="pull-right"><?php echo $this->lang->line('mycourse_list_status_'.$courseBuyer->course_buyer_status); ?></strong></li>
	                    <li><?php echo $this->lang->line('mycourse_quantity'); ?> <strong class="pull-right"><?php
											if($courseBuyer->course_buyer_status != 'cancel'){
												echo $courseBuyer->total_ticket;
											}else {
												echo '-';
											}
											?></strong></li>
	                    <li><?php echo $this->lang->line('mycourse_total'); ?> <strong class="pull-right"><?php
											if($courseBuyer->course_buyer_status != 'cancel'){
												echo number_format($courseBuyer->total_amount - $courseBuyer->discount_amount, 2); ?> <?php echo $this->lang->line('baht');
											}else {
												echo '-';
											}
											?></strong></li>
	                    <li><?php echo $this->lang->line('mycourse_vat'); ?> <strong class="pull-right"><?php
											if($courseBuyer->course_buyer_status != 'cancel'){
											echo number_format($courseBuyer->vat_amount, 2); ?> <?php echo $this->lang->line('baht');
											}else {
											echo '-';
											}
											?></strong></li>
											<li>
												<?php echo $this->lang->line('mycourse_grand_total'); ?> <strong class="pull-right"><?php
												if($courseBuyer->course_buyer_status != 'cancel'){
													echo number_format($courseBuyer->paid_amount, 2); ?> <?php echo $this->lang->line('baht');
													}else {
														echo '-';
													}
													?></strong></li>
	                    <li><?php echo $this->lang->line('mycourse_buyer_name'); ?> <strong class="pull-right"><?php echo $courseBuyer->buyer_firstname.' '.$courseBuyer->buyer_lastname; ?></strong></li>
	                    <li><?php echo $this->lang->line('mycourse_email'); ?> <strong class="pull-right"><?php echo $courseBuyer->buyer_email; ?></strong></li>
	                    <li><?php echo $this->lang->line('mycourse_mobile'); ?> <strong class="pull-right"><?php echo $courseBuyer->buyer_mobile; ?></strong></li>
	                    <li><?php echo $this->lang->line('mycourse_company_name'); ?>  <strong class="pull-right"><?php echo $courseBuyer->buyer_company; ?></strong></li>
	                </ul>
	            </div>
			</aside>
			<div class="col-md-8">
                <h3 style="padding-bottom:10px;"><?php echo anchor('course/detail/'.$course->course_id, $course->course_name); ?></h3>
								<p style="margin-bottom:0"><i class="icon-calendar"></i> <?php echo $this->DateTimeService->displayDateList($course->course_date, $lang); ?> (</i><?php echo $course->course_length; ?> Days</span>)</p>
								<p style="margin-bottom:0"><i class="icon-clock"></i> <?php 
								echo $this->DateTimeService->displayTime($course->course_start_time, $lang).' - '.$this->DateTimeService->displayTime($course->course_end_time, $lang); ?></p>
                <p style="margin-bottom:20px"><i class="icon-location-1"></i> <?php echo $this->CourseLocationModel->getName($course->course_location_id, $lang); ?></p>
        		<div class="table-responsive">
				    <table class="table">
				        <thead>
				            <tr>
				                <th><?php echo $this->lang->line('mycourse_ticket_type');?></th>
				                <th><?php echo $this->lang->line('mycourse_attendee_information'); ?></th>
				                <th></th>
				            </tr>
				        </thead>
				        <tbody>
				            <?php
				            $totalTicket = $courseRegisterList->num_rows();
				            $totalPrice = 0;
				            foreach($courseRegisterList->result() as $courseRegister){
			                    $ticket = $this->TicketModel->getData($courseRegister->ticket_id);
			                    $subPrice = 0;
			                    echo '<tr>';
													echo '<td>';
													if($courseBuyer->course_buyer_status != 'cancel' && $ticket){
														echo $ticket->ticket_name;
													}else {
														echo '-';
													}
													echo '</td><td>';
													if($courseBuyer->course_buyer_status != 'cancel'){
			                    	echo $courseRegister->register_firstname.' '.$courseRegister->register_lastname;
														echo '<br />';
														echo $courseRegister->register_email;
														echo '<br />';
														echo $courseRegister->register_mobile;
														echo '<br />';
														echo $courseRegister->register_company;
													}else {
														echo '-';
													}

			                    echo '</td><td>';
                          if($courseBuyer->course_buyer_status == 'complete'){
                            echo anchor('mycourse/printTicket/'.$courseRegister->course_register_id, $this->lang->line('mycourse_print') ,array('class'=>'button_medium','target'=>'_blank'));
                          }elseif($courseBuyer->course_buyer_status == 'cancel'){
                            echo '';
                          }else{
                            echo 'Processing...';
                          }
			                    echo '</td>';
			                    echo '</tr>';
				            }
				            ?>
				        </tbody>
				        <tfooter>
				        	<tr><td colspan="3"></td></tr>
				        </tfooter>
				    </table>
				</div>
					<div>
						<?php
            if($courseBuyer->course_buyer_status == 'waiting'){
              $paymentBankUrl = 'payment/index/'.$courseBuyer->course_buyer_id;
              $paymentCreditUrl = 'payment/index/'.$courseBuyer->course_buyer_id.'/credit';
              $cancelUrl = 'mycourse/cancel/'.$courseBuyer->course_buyer_id;
              ?>
              <div class="box_style_1" style="background-color: transparent; color: #2d4050; position: relative;
              margin-bottom: 30px;padding-bottom: 20px;">
                <h3 style="margin-top:0;"><?php echo $this->lang->line('course_bank_header'); ?></h3>
                <div class="row">
          			  <div class="col-md-2"><img src="<?php echo base_url('assets/img/kbank_logo_small.jpg'); ?>" class="img-responsive" alt="KBank" /></div>
          			  <div class="col-md-10" style="text-align:left;">
          				  <div style="padding-top:5px; font-size:15px;"><b style="display:inline-block; width:85px;"><?php echo $this->lang->line('course_bank_account_name'); ?> </b><?php echo $this->lang->line('course_bank_account_name_value'); ?></div>
          				  <div style="font-size:15px;"><b style="display:inline-block; width:85px;"><?php echo $this->lang->line('course_bank_account_number'); ?> </b>001-1-30904-6</div>
          			  </div>
          		  </div>
              </div>
              <?php
              echo anchor($paymentBankUrl, $this->lang->line('mycourse_submit_payin'), array('class'=>'button_medium'));
              echo ' ';
              echo anchor($paymentCreditUrl, $this->lang->line('mycourse_payment_creditcard'), array('class'=>'button_medium', 'style'=>'margin:0 5px;'));
              ?>
              <button type="button" class="button_medium_outline" data-toggle="modal" data-target=".cancel-modal"><?php echo $this->lang->line('mycourse_list_cancel_order'); ?></button>
              <div class="modal fade cancel-modal" tabindex="-1" role="dialog" aria-labelledby="cancel-modal-label">
                <div class="modal-dialog" role="document" style="margin-top:55px;">
                  <div class="modal-content" style="max-width:350px;margin-left: auto;margin-right: auto;">
                    <div class="modal-body" style="padding-top:10px;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span class="icon-cancel" aria-hidden="true"></span>
                        </button>
                        <div style="clear:both;"></div>
                      <div style="color:#f54700;font-size: 20px;text-align:center;margin-bottom:10px;"> <?php echo $this->lang->line('mycourse_detail_cancel_name'); ?></div>
                      <p style="text-align:center;color:#666;padding:10px 0;"><?php echo $this->lang->line('mycourse_detail_cancel_detail'); ?></p>
                      <div style="text-align:center;">
                          <?php
                          echo anchor($cancelUrl, $this->lang->line('mycourse_detail_cancel_yes'), array('class'=>'button_fullwidth', 'style'=>'text-transform: none;width:auto;display:inline-block;'));
                          ?>
													<button type="button" class="button_medium_outline" data-dismiss="modal" aria-label="Close" style="text-align:center;text-transform: none;">
	                          <?php echo $this->lang->line('mycourse_detail_cancel_no'); ?>
                          </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php
            }elseif($courseBuyer->course_buyer_status == 'payment'){
              echo '<div class="alert alert-warning">';
              echo '<i class="icon-warning-empty" aria-hidden"true"></i> ';
              echo $this->lang->line('mycourse_list_waiting_for_approval_detail');
              echo '</div>';
            }
						?>
					</div>
    		</div>
    		<?php } ?>
        </div>
    </div>
</section>
