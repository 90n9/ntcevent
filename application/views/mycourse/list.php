<div id="page-my-course-list">
	<section id="main_content" class="fix-content-width">
		<div class="container">
			<ol class="breadcrumb">
				<?php
				echo '<li>'.anchor('',$this->lang->line('main_menu_home')).'</li>';
				echo '<li class="active">'.$this->lang->line('user_menu_mycourse').'</li>';
				?>
			</ol>
			<div class="hidden-md hidden-lg">
				View order:
        <div class="dropdown inline">
          <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <?php
            switch($viewStatus){
              case 'all':
                echo $this->lang->line('mycourse_list_view_all_orders');
                break;
              case 'waiting':
                echo $this->lang->line('mycourse_list_waiting_for_payment');
                break;
              case 'payment':
                echo $this->lang->line('mycourse_list_waiting_for_approval');
                break;
              case 'complete':
                echo $this->lang->line('mycourse_list_completed_orders');
                break;
              case 'cancel':
                echo $this->lang->line('mycourse_list_cancelled_orders');
                break;
            }
             ?>
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li><?php echo anchor('mycourse/index/all', $this->lang->line('mycourse_list_view_all_orders')); ?></li>
            <li><?php echo anchor('mycourse/index/waiting', $this->lang->line('mycourse_list_waiting_for_payment')); ?></li>
            <li><?php echo anchor('mycourse/index/payment', $this->lang->line('mycourse_list_waiting_for_approval')); ?></li>
            <li><?php echo anchor('mycourse/index/complete', $this->lang->line('mycourse_list_completed_orders')); ?></li>
            <li><?php echo anchor('mycourse/index/cancel', $this->lang->line('mycourse_list_cancelled_orders')); ?></li>
          </ul>
        </div>
			</div>
			<ul id="mytabs" class="nav nav-pills hidden-xs hidden-sm">
        <?php
        echo '<li'.(($viewStatus == 'all')?' class="active"':'').'>'.anchor('mycourse/index/all', $this->lang->line('mycourse_list_view_all_orders')).'</li>';
        echo '<li'.(($viewStatus == 'waiting')?' class="active"':'').'>'.anchor('mycourse/index/waiting', $this->lang->line('mycourse_list_waiting_for_payment')).'</li>';
        echo '<li'.(($viewStatus == 'payment')?' class="active"':'').'>'.anchor('mycourse/index/payment', $this->lang->line('mycourse_list_waiting_for_approval')).'</li>';
        echo '<li'.(($viewStatus == 'complete')?' class="active"':'').'>'.anchor('mycourse/index/complete', $this->lang->line('mycourse_list_completed_orders')).'</li>';
        echo '<li'.(($viewStatus == 'cancel')?' class="active"':'').'>'.anchor('mycourse/index/cancel', $this->lang->line('mycourse_list_cancelled_orders')).'</li>';
        ?>
			</ul>
			<div class="tab-content">
        <?php
        switch($viewStatus){
          case 'all':
            $this->load->view('mycourse/list/courseall');
            break;
          case 'waiting':
            $this->load->view('mycourse/list/coursewaiting');
            break;
          case 'payment':
            $this->load->view('mycourse/list/coursepayment');
            break;
          case 'complete':
            $this->load->view('mycourse/list/coursecomplete');
            break;
          case 'cancel':
            $this->load->view('mycourse/list/coursecancel');
            break;
        }
        ?>
			</div>
		</div><!-- End container -->
	</section><!-- End main_content -->
</div>
