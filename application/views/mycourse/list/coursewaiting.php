<p> <?php echo $this->lang->line('mycourse_list_waiting_for_payment_detail'); ?></p>
<?php if($courseBuyerWaitingList->num_rows() > 0){ ?>
<?php echo form_open('payment'); ?>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
              <th></th>
              <th><?php echo $this->lang->line('mycourse_list_course_name') ?></th>
              <th class="text-center hidden-xs"><?php echo $this->lang->line('mycourse_list_schedule'); ?></th>
              <th class="text-center hidden-xs"><?php echo $this->lang->line('mycourse_list_status') ?></th>
              <th class="text-right hidden-xs" style="padding-right:40px;"><?php echo $this->lang->line('mycourse_list_grand_total') ?></th>
            </tr>
        </thead>
        <tbody>
        <?php
        foreach($courseBuyerWaitingList->result() as $courseBuyer){
            $course = $this->CourseModel->getData($courseBuyer->course_id);
            $ticketUrl = 'mycourse/detail/'.$courseBuyer->course_buyer_id;
            $schedule = $this->DateTimeService->displayDateList($course->course_date, $lang);
            $scheduleTime = $this->DateTimeService->displayTime($course->course_start_time, $lang).' - '.$this->DateTimeService->displayTime($course->course_end_time, $lang);
            ?>
            <tr class="clickable-row" data-href="<?php echo site_url($ticketUrl); ?>">
                <td class="unclickable"><input type="checkbox" name="chkCourseBuyerId[]" value="<?php echo $courseBuyer->course_buyer_id; ?>" /></td>
                <td>
                  <div><?php echo anchor($ticketUrl,$courseBuyer->course_buyer_code); ?></div>
                  <div style="font-size:16px; font-weight:bold;"><?php echo anchor($ticketUrl, $course->course_name); ?></div>
                  <div>Date of purchase : <?php echo $this->DateTimeService->displayOnlyDate($courseBuyer->create_date, $lang); ?></div>
                  <div class="hidden-sm hidden-md hidden-lg">
                    <div><?php echo $this->lang->line('mycourse_list_schedule'); ?> : <?php echo $schedule; ?> (<?php echo $scheduleTime; ?>)</div>
                    <div><?php echo $this->lang->line('mycourse_list_status'); ?> : <span style="font-weight:bold;color:#5bc0de;"> <?php echo $this->lang->line('mycourse_list_status_'.$courseBuyer->course_buyer_status); ?></span></div>
                    <?php
                    if($courseBuyer->course_buyer_status == 'waiting'){
                      list($createDate, $createTime) = explode(' ', $courseBuyer->create_date);
                      $expireDate = strtotime("+7 day", strtotime($createDate));
                      $startDate = strtotime($course->course_start);
                      if($expireDate > $startDate){
                        $expireDate = $startDate;
                      }
                      echo '<div>Order Expire by : '.$this->DateTimeService->displayDate(date('Y-m-d', $expireDate), $lang).'</div>';
                    }
                    ?>
                    <div><?php echo $this->lang->line('mycourse_list_grand_total'); ?> : <?php
                    if($courseBuyer->course_buyer_status != 'cancel'){
                      echo number_format($courseBuyer->paid_amount, 2);
                    } else {
                      echo '0.00';
                    }
                    ?></div>
                  </div>
                </td>
                <td class="text-center hidden-xs"><?php echo $schedule; ?><br />(<?php echo $scheduleTime; ?>)</td>
                <td class="text-center hidden-xs">
                  <span style="font-weight:bold; color:#5bc0de;"><?php echo $this->lang->line('mycourse_list_status_'.$courseBuyer->course_buyer_status); ?></span>
                  <?php
                  list($createDate, $createTime) = explode(' ', $courseBuyer->create_date);
                  $expireDate = strtotime("+7 day", strtotime($createDate));
                  $startDate = strtotime($course->course_start);
                  if($expireDate > $startDate){
                    $expireDate = $startDate;
                  }
                  echo '<div>Expired:'.$this->DateTimeService->displayDate(date('Y-m-d', $expireDate), $lang).'</div>';
                  ?>
                </td>
                <td class="text-right hidden-xs" style="padding-right:40px;"><?php echo number_format($courseBuyer->paid_amount, 2); ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<button type="submit" class="button_medium" name="payment" value="bank"> <?php echo $this->lang->line('mycourse_list_waiting_for_payment_bank'); ?></button>
<button type="submit" class="button_medium" name="payment" value="creditcard"> <?php echo $this->lang->line('mycourse_list_waiting_for_payment_creditcard') ?></button>
<?php echo form_close(); ?>
<?php }else{ ?>
    <p><?php echo $this->lang->line('mycourse_list_waiting_for_payment_noshow_detail'); ?></p>
<?php } ?>
