<?php if($courseBuyerCancelList->num_rows() > 0){ ?>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
              <th><?php echo $this->lang->line('mycourse_list_course_name') ?></th>
              <th class="text-center hidden-xs"><?php echo $this->lang->line('mycourse_list_schedule'); ?></th>
              <th class="text-center hidden-xs"><?php echo $this->lang->line('mycourse_list_status') ?></th>
              <th class="text-right hidden-xs" style="padding-right:40px;"><?php echo $this->lang->line('mycourse_list_grand_total') ?></th>
            </tr>
        </thead>
        <tbody>
        <?php
        foreach($courseBuyerCancelList->result() as $courseBuyer){
            $course = $this->CourseModel->getData($courseBuyer->course_id);
            $ticketUrl = 'mycourse/detail/'.$courseBuyer->course_buyer_id;
            $schedule = $this->DateTimeService->displayDateList($course->course_date, $lang);
            $scheduleTime = $this->DateTimeService->displayTime($course->course_start_time, $lang).' - '.$this->DateTimeService->displayTime($course->course_end_time, $lang);
            ?>
            <tr class="clickable-row" data-href="<?php echo site_url($ticketUrl); ?>">
                <td>
                  <div><?php echo anchor($ticketUrl, $courseBuyer->course_buyer_code); ?></div>
                  <div style="font-size:16px; font-weight:bold;"><?php echo anchor($ticketUrl, $course->course_name); ?></div>
                  <div>Date of purchase : <?php echo $this->DateTimeService->displayOnlyDate($courseBuyer->create_date, $lang); ?></div>
                  <div class="hidden-sm hidden-md hidden-lg">
                    <div><?php echo $this->lang->line('mycourse_list_schedule'); ?> : <?php echo $schedule; ?> (<?php echo $scheduleTime; ?>)</div>
                    <div><?php echo $this->lang->line('mycourse_list_status'); ?> : <span style="font-weight:bold;color:#d9534f;"> <?php echo $this->lang->line('mycourse_list_status_'.$courseBuyer->course_buyer_status); ?></span></div>
                  </div>
                </td>
                <td class="text-center hidden-xs"><?php echo $schedule; ?><br />(<?php echo $scheduleTime; ?>)</td>
                <td class="text-center hidden-xs"><span style="font-weight:bold; color:#d9534f;"><?php echo $this->lang->line('mycourse_list_status_'.$courseBuyer->course_buyer_status); ?></span></td>
                <td class="text-right hidden-xs" style="padding-right:40px;">-</td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<?php }else{ ?>
    <p><?php echo $this->lang->line('mycourse_list_cancelled_orders_noshow_detail'); ?></p>
<?php } ?>
