<section id="login_bg">
<div class="container">
<div class="row">
	<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
		<div id="login">
      <?php echo anchor('','<span class="icon-cancel" aria-hidden="true"></span>', array('class'=>'close login-close')); ?>
			<p class="text-center">
				<img src="<?php echo base_url('assets/img/logo_darken.png'); ?>" alt="">
			</p>
      <hr />
			<div style="text-align: center;">
				<h4>Your message has been sent.</h4>
        <p>We will reply by email as soon as possible.</p>
        <div style="padding:10px 0;">
          <?php echo anchor('', 'BACK TO HOME', array('class'=>'button_outline')); ?>
        </div>
      </div>
		</div>
	</div>
</div>
</div>
</section><!-- End register -->
