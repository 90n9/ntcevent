<div>
  <p>Dear NTC Support,</p>
  <p><b><?php echo $contact_firstname.' '.$contact_lastname; ?></b> has contact us on our website.</p>
  <hr />
  <h3>CONTACT INFO</h3>
  <p>First Name : <b><?php echo $contact_firstname; ?></b></p>
  <p>Last Name : <b><?php echo $contact_lastname; ?></b></p>
  <p>Email : <b><?php echo $contact_email; ?></b></p>
  <p>Phone Number : <b><?php echo $contact_phone; ?></b></p>
  <p>Message :<br /><b><?php echo $contact_detail; ?></b></p>
  <hr />
  <p style="color:#999;">This email has been send automatically from ntc.trainingcenter.co.th</p>
</div>
