<div class="fix-content-width" style="background-color: #f9f9f9;">
<section id="main_content">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<h3 style="font-size:1.5em;">Network Training Center Co.,Ltd.</h3>
				<ul id="contact-info">
					<li><i class="icon-home"></i> 177/1 BUI Bldg., 14th Fl., Unit 1, 3 &amp; 4<br />Surawongse Rd.
Suriyawongse, Bangrak, <br />Bangkok, THAILAND 10500</li>
					<li><i class="icon-phone"></i> +66 (0) 2634 7993-4</li>
					<li><i class="icon-print"></i> +66 (0) 2634 7995</li>
					<li><i class=" icon-email"></i> <a href="mailto:sales@trainingcenter.co.th">sales@trainingcenter.co.th</a></li>
				</ul>
				<hr>
				<h3 style="margin-bottom:20px;">Customer Service</h3>
				<div>
					<i class="icon-phone"></i> +66 (0) 2634 7993-4 #12<br />
					<i class=" icon-email"></i> <a href="mailto:support@trainingcenter.co.th">support@trainingcenter.co.th</a>
				</div>
				<hr>
				<h3 style="margin-bottom:20px;">Follow us</h3>
				<ul id="follow_us_contacts">
					<li><a href="https://www.facebook.com/ntcfanclub/" target="_blank"><i class="icon-facebook"></i></a></li>
					<li><a href="https://twitter.com/follow_ntc" target="_blank"><i class=" icon-twitter"></i></a></li>
					<li><a href="https://www.youtube.com/channel/UCqrbbIyLtywjXCO7V9bF76A" target="_blank"><i class="icon-youtube"></i></a></li>
					<li><a href="https://www.linkedin.com/company/network-training-center-co--ltd-" target="_blank"><i class="icon-linkedin"></i></a></li>
					<li><a href="http://line.me/ti/p/%40NTC-LINE" target="_blank"><i class="icon-line"></i></a></li>
				</ul>
			</div>
			<div class="col-md-8">
				<div class=" box_style_2">
					<span class="tape"></span>
					<div class="row">
						<div class="col-md-12">
							<h3><?php echo $this->lang->line('contact_form_header'); ?></h3>
						</div>
					</div>
					<div id="message-contact"></div>
					<?php echo form_open('contact/form_post', array('id'=>'contactform')); ?>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<input type="text" class="form-control style_2" id="txtFirstname" name="txtFirstname" placeholder="<?php echo $this->lang->line('contact_form_firstname'); ?> *" required>
		                            <span class="input-icon"><i class="icon-user"></i></span>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<input type="text" class="form-control style_2" id="txtLastname" name="txtLastname" placeholder="<?php echo $this->lang->line('contact_form_lastname'); ?> *" required>
		                            <span class="input-icon"><i class="icon-user"></i></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<input type="email" id="txtEmail" name="txtEmail" class="form-control style_2" placeholder="<?php echo $this->lang->line('contact_form_email'); ?> *" required>
		                            <span class="input-icon"><i class="icon-email"></i></span>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<input type="text" id="txtPhone" name="txtPhone" class="form-control style_2" placeholder="<?php echo $this->lang->line('contact_form_phone'); ?>">
		                            <span class="input-icon"><i class="icon-mobile" style="font-size:24px;"></i></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<textarea rows="5" id="txtDetail" name="txtDetail" class="form-control" placeholder="<?php echo $this->lang->line('contact_form_message'); ?> *" style="height:200px;" required></textarea>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">

							</div>
							<div class="col-md-12">
								<button type="submit" class="button_medium" id="submit-contact"><?php echo $this->lang->line('contact_form_submit'); ?></button>
							</div>
						</div>
					</form>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="main_content_grey" style="margin-top:40px;">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
        <?php
        $imgUrl = base_url('assets/img/map.jpg');
        $imgStr = '<img class="img-responsive" style="width:100%" src="'.$imgUrl.'" alt="ntc map" />';
        echo anchor($imgUrl,$imgStr, array('id'=>'img-popup'));
        ?>
				<div style="padding:20px 0;">
					<a href="http://maps.google.co.th/maps/ms?hl=th&amp;vpsrc=6&amp;ctz=-420&amp;ie=UTF8&amp;msa=0&amp;msid=213240621214832930493.0004b1c7a9f9bb107ac21&amp;ll=13.727551,100.525627&amp;spn=0,0&amp;t=m&amp;iwloc=0004b1c7ad927885da3ad&amp;output=embed" target="_blank"><img src="<?php echo base_url('assets/img/google-maps-logo.png'); ?>" alt="google map" /></a>
				</div>
			</div>
			<div class="col-md-4">
				<div style="padding:0 20px;">
					<h4>Directions: BTS</h4>
					<p><?php echo $this->lang->line('contact_bts_detail'); ?></p>
					<hr>
					<h4>Directions: MRT </h4>
					<p><?php echo $this->lang->line('contact_mrt_detail'); ?></p>
					<hr>
					<h4>Bus</h4>
					<?php echo $this->lang->line('contact_bus_detail'); ?>
				</div>
			</div>
			<div class="col-md-4">
				<div style="padding:0 20px;">
				<h4>Express Way</h4>
				<?php echo $this->lang->line('contact_express_detail'); ?>
				</div>
			</div>
		</div>
	</div>
</section>
</div>
