<section id="login_bg">
  <div  class="container">
    <div class="row">
      <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
        <div id="login">
          <?php echo anchor('','<span class="icon-cancel" aria-hidden="true"></span>', array('class'=>'close login-close')); ?>
          <p class="text-center">
            <img src="<?php echo base_url('assets/img/logo_darken.png'); ?>" alt="">
          </p>
          <hr />
          <div style="text-align: center;">
            <h4 style="background:#bb3333;color:#fff;padding:10px;"><i class="icon-warning"></i> Verify link has been broken!</h4>
            <div id="pass-info" class="clearfix"></div>
            <div style="color:#bb3333;">Please contact us to slove this problem!</div>
            <div style="padding-top:20px;">
              <?php echo anchor('contact', 'CONTACTUS', array('class'=>'button_outline')); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section><!-- End register -->
