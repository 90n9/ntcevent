<p>Dear <?php echo $member->firstname; ?>,</p>
<p>Thank you for signing up!</p>
<p>Please verify your email. Click the link below.</p>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr><td align="center" style="padding: 10px 0;">
    <table width="170" border="0" cellpadding="0" cellspacing="0">
      <tr><td align="center" valign="middle" bgcolor="#f54700" style="border-radius:5px;padding:15px 20px;"><?php echo anchor($verifyUrl, 'VERIFY EMAIL', array('target'=>'_blank', 'style'=>'font-size:18px;color:#fff;text-decoration:none;')); ?></td></tr>
    </table>
  </td></tr>
</table>
<p>Or copy this URL and paste on new browser<br />
<?php echo base_url('index.php/'.$verifyUrl); ?></p>
<p>If you have any questions please contact<br />
support@trainingcenter.co.th or 02 643 7993-4</p>
<p>Thank you and see you at site!<br />
Cheers!</p>
<p>The NTC Team</p>
