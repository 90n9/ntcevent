<section id="login_bg">
<div  class="container">
<div class="row">
	<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
		<div id="login">
			<p class="text-center">
				<img src="<?php echo base_url('assets/img/logo.png'); ?>" alt="">
			</p>
			<hr>
			<?php echo form_open('register/form_post'); ?>
				<div class="form-group">
					<input type="text" name="txtFirstname" id="txtFirstname" class="form-control required" placeholder="First Name">
					<span class="input-icon"><i class=" icon-user"></i></span>
				</div>
				<div class="form-group">
					<input type="text" name="txtLastname" id="txtLastname" class="form-control required" placeholder="Last Name">
					<span class="input-icon"><i class=" icon-user"></i></span>
				</div>
				<div class="form-group">
					<input type="text" name="txtEmail" id="txtEmail" class="form-control required" placeholder="Email">
					<span class="input-icon"><i class="icon-email"></i></span>
				</div>
				<div class="form-group">
					<input type="text" name="txtMobile" id="txtMobile" class="form-control required" placeholder="Mobile Phone Number">
					<span class="input-icon"><i class="icon-phone"></i></span>
				</div>
				<div class="form-group">
					<input type="text" name="txtCompany" id="txtCompany" class="form-control required" placeholder="Company">
					<span class="input-icon"><i class="icon-building"></i></span>
				</div>
				<div class="form-group">
					<input type="password" name="txtPassword" id="txtPassword" class="form-control required" placeholder="Password">
					<span class="input-icon"><i class=" icon-lock"></i></span>
				</div>
				<div class="form-group">
					<input type="password" name="txtConfirmPassword" id="txtConfirmPassword" class="form-control required" placeholder="Confirm password">
					<span class="input-icon"><i class=" icon-lock"></i></span>
				</div>
                <div id="pass-info" class="clearfix"></div>
				<button type="submit" class="button_fullwidth">Create an account</button>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
</div>
</section><!-- End register -->
