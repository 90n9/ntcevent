<table style="width: 650px;" border="0" cellspacing="0">
  <tbody>
    <tr bgcolor="#FFFFFF">
      <td width="650" height="74">
        <p style="text-align: justify;"><span style="font-size: 10pt; font-family: tahoma, arial, helvetica, sans-serif;"><span style="color: #333333;">Dear <?php echo $member->firstname; ?>,</span><br /> <br />
          <span style="color: #333333;">Thank you for signing up! <br />
            Please activate your account. Click the link below.</span></span></p>
      </td>
    </tr>
  </tbody>
</table>
<br />
<table style="width: 650px;" border="0" cellspacing="0">
  <tbody>
    <tr bgcolor="#FFFFFF">
      <td width="650" height="74">
        <table style="width: 650px;" border="0" cellspacing="0">
          <tbody>
            <tr>
            <td width="227" height="37"><span style="font-family: tahoma, arial, helvetica, sans-serif;">&nbsp;</span></td>
            <td style="text-align: center;" bgcolor="#f54700" width="169">
              <span style="color: #ffffff; font-size: 10pt; font-family: tahoma, arial, helvetica, sans-serif;"><strong><span class="style3">
                <?php echo anchor($verifyUrl, 'ACTIVE YOUR ACCOUNT', array('target'=>'_blank', 'style'=>'color:#fff;text-decoration:none;')); ?>
              </span></strong></span>
            </td>
            <td width="248"><span style="font-family: tahoma, arial, helvetica, sans-serif;">&nbsp;</span></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
<br />
<table style="width: 650px;" border="0" cellspacing="0">
  <tbody>
    <tr bgcolor="#FFFFFF">
      <td width="650" height="74">
        <p style="text-align: left;">
          <span style="font-size: 10pt; font-family: tahoma, arial, helvetica, sans-serif;">
            <span style="color: #333333;">Or copy this URL and paste on new browser<br /> <span style="text-decoration: underline; color: #0000ff;"><?php echo base_url('index.php/'.$verifyUrl); ?></span><br /><br />
            If you have any questions please contact<br /> <span style="text-decoration: underline;"><span class="style1" style="color: #0000ff; text-decoration: underline;">support@trainingcenter.co.th</span></span> or 02 643 7993-4<br /> <br />
            Thank you and see you at the site!<br /><br />
            Cheers!<br />
            The NTC Team </span>
          </span>
        </p>
      </td>
    </tr>
  </tbody>
</table>
